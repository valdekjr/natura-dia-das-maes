var gulp = require('gulp'),
	path = require('path'),
	concatCss = require('gulp-concat-css'),
	minifyCSS = require('gulp-minify-css'),
	appPath = path.resolve('app'),
	publicPath = path.resolve('public'),
	profilesPath = path.resolve('../resources/profiles'),
	bowerPath = path.resolve('bower_components');

var getDest = function(env, folder) {
	return path.join(profilesPath, env + '/frontend/public/' + folder);
};

var bowerize = function(item) {
	return path.join(bowerPath, item);
};

var css_plugins = [
	'slick-carousel/slick/slick.css'
].map(bowerize);

gulp.task('copy:assets_mb', function() {
	gulp.src(css_plugins)
			.pipe(concatCss('assets_mb.css'))
			.pipe(minifyCSS())
			.pipe(gulp.dest(path.join(publicPath, 'css')));
});

gulp.task('copy:salveqa', ['less:static', 'vendor', 'preprocessors'], function() {
	// css
	gulp.src(path.join(publicPath, 'css', '*.css'))
		.pipe(gulp.dest(getDest('salveqa', 'css')));

	// js vendor
	gulp.src(path.join(publicPath, 'js', 'vendor.js'))
		.pipe(gulp.dest(getDest('salveqa', 'js')));

	// js header
	gulp.src(path.join(publicPath, 'js_static', '**/*.js'))
		.pipe(gulp.dest(getDest('salveqa', 'js_static')));

	// images
	gulp.src(path.join(publicPath, 'img', '**/*.*'))
		.pipe(gulp.dest(getDest('salveqa', 'img')));

	// images header
	gulp.src(path.join(publicPath, 'img_static', '**/*.*'))
		.pipe(gulp.dest(getDest('salveqa', 'img_static')));

	// jsons
	gulp.src(path.join(publicPath, 'musics', '**/*.*'))
		.pipe(gulp.dest(getDest('salveqa', 'musics')));

	gulp.src(path.join(publicPath, 'js', 'main.js'))
		.pipe(gulp.dest(getDest('salveqa', 'js')));

	// fontes
	// @todo: tratar para receber novas fontes. Dessa forma esta 
	// comtemplando apenas as fontes padroes do cms. Caso
	// seja necessario adicionar novas fontes, e necessario
	// criar um novo padrao, separando as fontes padroes em um 
	// outro diretorio
	gulp.src(path.join(publicPath, 'fonts', '**/*.*'))
		.pipe(gulp.dest(getDest('salveqa', 'fonts')));
});

gulp.task('copy:stage', ['vendor', 'preprocessors'], function() {
	var info = require(path.join(appPath, 'configs', 'tpl-stage.js'));
	var dest = info.assetsPath + info.version + '/';

	// css
	gulp.src([
			path.join(publicPath, 'css', '*.css'),
			'!' + path.join(publicPath, 'css', 'header.css')
			//'!' + path.join(publicPath, 'css', 'font.css')
		])
		.pipe(gulp.dest(getDest('stage', dest + 'css')));

	gulp.src(path.join(publicPath, 'js', 'main.js'))
		.pipe(gulp.dest(getDest('stage', dest + 'js')));

	// jsons
	gulp.src(path.join(publicPath, 'musics', '**/*.*'))
		.pipe(gulp.dest(getDest('stage', dest + 'musics')));

	// images
	gulp.src(path.join(publicPath, 'img', '**/*.*'))
		.pipe(gulp.dest(getDest('stage', dest + 'img')));

	// js vendor
	gulp.src(path.join(publicPath, 'js', 'vendor.js'))
		.pipe(gulp.dest(getDest('stage', dest + 'js')));
});

gulp.task('copy:prod', ['vendor', 'preprocessors'], function() {
	var info = require(path.join(appPath, 'configs', 'tpl-prod.js'));
	var dest = info.assetsPath + info.version + '/';

	// css
	gulp.src([
			path.join(publicPath, 'css', '*.css'),
			'!' + path.join(publicPath, 'css', 'header.css')
			//'!' + path.join(publicPath, 'css', 'font.css')
		])
		.pipe(gulp.dest(getDest('prod', dest + 'css')));

	gulp.src(path.join(publicPath, 'js', 'main.js'))
		.pipe(gulp.dest(getDest('prod', dest + 'js')));

	// jsons
	gulp.src(path.join(publicPath, 'musics', '**/*.*'))
		.pipe(gulp.dest(getDest('prod', dest + 'musics')));

	// images
	gulp.src(path.join(publicPath, 'img', '**/*.*'))
		.pipe(gulp.dest(getDest('prod', dest + 'img')));

	// js vendor
	gulp.src(path.join(publicPath, 'js', 'vendor.js'))
		.pipe(gulp.dest(getDest('prod', dest + 'js')));
});
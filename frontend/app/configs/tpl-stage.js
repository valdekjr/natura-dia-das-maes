var data = require('../data-content/posts');

/**
 Trocar a string <nome-do-projeto>
 pelo nome do diretorio do projeto na ferramenta. Ex.:
 kaiak-expedicao
 @name Config
*/
module.exports = {

  /**
   Titulo da pagina
   @property {string} title Titulo da pagina
  */
  title: "Natura Toolkit Dia das Mães",

  /**
   Descricao da pagina
   @property {string} description Descricao da pagina
  */
  description: "",

  /**
   Url base da pagina. Ela varia no ambiente de producao,
   podendo ser relativa
   @property {string} baseUrl Base de url do projeto
  */
  baseUrl: 'http://www.ferramentacn.com.br/temp/natura-toolkit_natal2015/static/compartilhenatal/assets/v1-8',

  /**
   Tambem e base URL do projeto, mas sempre deve ser usada
   com o endereco absoluto. Muito util para usar nos projetos
   com integracao de APIs como Facebook (para post de arquivos)
   @property {string} absUrl Base de url do projeto com endereco absoluto
  */
  absUrl: 'http://www.ferramentacn.com.br/temp/natura-toolkit_natal2015/static/compartilhenatal/assets/v1-8',

  /**
   Url de servico backend. Deve ser utilizada em
   requisições ajax na aplicacao
   @property {string} serviceUrl Url do servico
  */
  serviceUrl: "http://natal2015-859774376.us-east-1.elb.amazonaws.com/image.php",
  serviceUrlSaveCn: "http://natal2015-859774376.us-east-1.elb.amazonaws.com/cndb/data.php",
  // serviceUrl: "http://salveqa.com.br/natura-toolkit-natal-be/image.php",

  serviceUrlWhats: "https://329lxrolmg.execute-api.us-east-1.amazonaws.com/prod/whats",

  /**
   @todo: aplicar o caminho relativo em producao
          ou o caminho de wcs staging absoluto
          para funcionar corretamente no ambiente
          de stagging
  */
  siteCFUrl: "http://wcsdcstaging.natura.net/www/presentes/natal-natura/",

  /**
   Numero da versao do pacote. Essa propriedade e usada
   nos pacotes de stage e producao. Seu valor sera concatenado
   com a propriedade assetsPath para gerar o caminho
   do pacote.
   @property {string} version Versao do pacote
  */
  version: 'v1-8',

  /**
   Caminho dos assets do projeto. Essa propriedade e usada
   para gerar o caminho do pacote para os ambientes
   stage e salveqa. Os arquivos de assets serao copiados
   dentro do caminho decladado nessa propriedade concatenado
   ao valor declarado na propriedade de versao. Ex.:
   version: "v1",
   assetsPath: "static/teste/assets",

   Os diretorios dos assets devem ficar assim:
   static/teste/assets/v1/img
   static/teste/assets/v1/js
   static/teste/assets/v1/css

   @property {string} assetsPath Caminho do pacote
  */
  assetsPath: 'static/compartilhenatal/assets/',

  /**
   Extencao da pagina deve ser usada quando o projeto
   possuir mais de uma pagina. Isso facilita
   a aplicacao de links, pois o desenvolvedor podera
   trabalhar simulando o ambiente de producao ou stage.
   Esse valor deve ser vazio nos nas configuracoes
   de stage e producao. A aplicacao esta no exemplo:
   <a href="{{baseUrl}}/outrapagina{{extPage}}">

   Para o ambiente local, o html ficaria assim:
   <a href="http://localhost:3000/outrapagina.html">

   Assim a navegacao se torna possivel, pois nos ambientes
   de prod e salveqa, nao temos essa extencao. Entao nos
   htmls compilados do seus ambientes esse mesmo link
   fica assim:
   <a href="/static/nome-do-projeto/assets/v1/outrapagina">

   @property {string} extPage Extencao que sera renderizada no html
  */
  extPage: "",

  /**
   Controla a exibicao do header e footer que simula o
   ambiente de producao. Essa propriedade deve ser
   falsa nos ambientes de stage e producao
   @property {boolean} hasHeader
  */
  hasHeader: false,

  /**
   Facebook ID para usar nas aplicacoes com integracao
   @propriety {string} Numero do ID do Facebook
  */
  fb_id: "337335196462604",

  /**
   Nome do album que serao armazenados os posts realizados pela campanha
   @propriety {string} Numero do ID do Facebook
   */
  fbAlbumName: "Dia das Mães Natura",

  /**
   Numero da chave para usar a API do
   google encrtador de URL
   @param googleApiKey
   */
  googleApiKey: "AIzaSyAkl-UsosFU4XF-jR2KARVai4KaFztYK8c",

  /**
   Url de redirecionamento da versao mobile.
   Usado em Natura para versao temporaria enquanto
   a versao desktop nao esta completamente responsiva
   @property {string} Url da versao mobie
   */
  mbUrl: "http://wcsdcstaging.natura.net/www/compartilhenatal-mobile",

  /**
   Dados para serem aplicados no html
   @param googleApiKey
   */

  gaDefaultCategory: 'natal-toolkit',

  content: data
}

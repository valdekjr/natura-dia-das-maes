/**
 Pagina de exemplo de modulo javascript
 @class Exemplo
*/
var Passo2 = {

  /**
   Metodo que inicia o modulo
   @method init
  */
  init: function() {

    // executa o metodo
    this.setElements();
    this.bind();
  },

  /**
   Adiciona os objetos do DOM as propriedades
   ao modulo
   @method setElements
  */
  setElements: function() {
  },

  /**
   Metodo atribui os eventos aos objetos
   @method bind
  */
  bind: function() {
    var _that = this;

  }
};

// exemplo de exportacao do modulo
export default Passo2;
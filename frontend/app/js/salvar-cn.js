var SalvarCn = {
    saveData: saveData
};

function saveData(data){

    return $.ajax({
        url: PersistData.serviceUrlSaveCn,
        type:'POST',
        data:data,
        xhrFields: {
            withCredentials: true
        }
    });
}

export default SalvarCn;
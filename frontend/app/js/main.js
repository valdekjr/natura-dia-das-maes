/**
 Arquivo principal da aplicacao, normalmente quando
 o projeto possui apenas uma sessão tudo é importado
 e chamado nesse arquivo. Caso o projeto creça, é
 importante criar outro arquivo nesse modelo e adicionar
 na tarefa de scripts (a partir da linha 22 do arquivo
 browserifyBundler.js).
 Vamos supor que a outra sessão utilize componentes
 diferentes da home. Entao é necessário adicionar
 o arquivo após a linha 22. Ex.:

 path.join(appPath, 'js', 'main.js'),
 path.join(appPath, 'js', 'outroarquivo.js')

 o projeto irá compilar um novo arquivo chamado
 outroarquivo.js na apsta public.
 */

// exemplo de como um modulo e importado para utilizacao
import ScrollAnchor from './utils/scroll-anchor';
import MaterialDivulgacao from './material-divulgacao';
// import DivulgueWhatsApp from './divulgue-whatsapp';
import DivulgueFacebook from './divulgue-facebook';
import Blacklist from './blacklist.js';
// import VideoDivulgue from './videodivulgue';

var Main = {
    init: function () {
        ScrollAnchor.init();
        MaterialDivulgacao.init();
        // DivulgueWhatsApp.init();
        DivulgueFacebook.init();
        Blacklist.init();
        // VideoDivulgue.init();
        this.trackPageView();
        this.modalTermos();
    },
    trackPageView: function() {
        var _that = this;
        if (typeof ga === "undefined") {
            setTimeout(function() {
                _that.trackPageView();
            }, 500);
            return false;
        }
        ga('send', 'pageview', {
            'page': "/www/ferramenta-de-vendas/dia-das-maes",
            'title': ""
        });
    },
    modalTermos: function(){
        $(".footer-toolkit  a").on("click",function(e){
            e.preventDefault();
            switch ($(this).attr('href')) {
                case '#politica' :
                    $('.content-termos, .content-video').hide();
                    $('.content-politica').show();

                    $('.lightbox').fadeIn(400, function () {
                        $('.content-politica').jScrollPane({showArrows: true, autoReinitialise: true});
                    } );
                    break;
                case '#termos' :
                    $('.content-politica, .content-video').hide();
                    $('.content-termos').show();

                    $('.lightbox').fadeIn(400, function () {
                        $('.content-termos').jScrollPane({showArrows: true, autoReinitialise: true});
                    } );
                    break;
            }
        });
        $(".lightbox .close, .lightbox .bg").on("click",function(e){
            e.stopPropagation();
            $('.lightbox').fadeOut();
        });
    }
};

$(document).ready(function() {
   var target = parent;

    Main.init();
   //  $("#divFaceApp").hide();
   //  $("#p2Facebook").hide();
   //
    if (document.location.href.indexOf('debug') > 0) {
      $("#divFaceAppShare").show();
   }

    parent.postMessage({
      action: 'height',
      height: $('body').height()
    }, "*");

    $("#p2Facebook").on("click", function(){
      target.postMessage({
        action: 'scroll',
        posY: $("#divFaceApp").position().top
      }, "*");
   });

   $("#p2Email").on("click", function(){
     target.postMessage({
      action: 'scroll',
      posY: $("#divEmail").position().top
     }, "*");
  });

});

import Cookie from './utils/cookie';
import Facebook from './utils/facebook';
import Utils from './utils/utils';
import Modal from './modal.js';

/**
 Classe que controla a sessao divulgue
 no facebook. Faz o controle de exibicao
 com o dropbox, paginacao e compartilha
 na timeline do usuario
 @class DivulgueFacebook
*/
var DivulgueFacebook = {
   filterSelector: '#fbSelCat',
   postsSelector: '#fbCustomPosts',
   showMoreSelector: '#showMoreFace',
   itemsPerPage: 3,
   currentFilter: 'all',
   init: function () {
      Facebook.init();
      this.dropdown();
      this.filter();
      this.showMore();
      this.share();
   },

   /**
   Controla o dropdown de filtros
   @method dropdown
   */
   dropdown: function() {
      var _that = this;
      $(document).on('click', this.filterSelector + ' .sc-dd-current', function() {
         var $father = $(this).parent();
         if ($father.hasClass('actived')) {
            $father.removeClass('actived');
         } else {
            $father.addClass('actived');
         }
      });

      $(document).on('mouseleave', this.filterSelector, function() {
         _that.closeFilter();
      });
   },

   /**
   Filtra as sugestes de posts disponiveis
   @method filter
   */
   filter: function() {
      var _that = this;
      var itemSelector = this.filterSelector + ' .sc-dd-item';

      $(document).on('click', itemSelector, function() {
         var $el = $(this);
         var target = $el.attr('data-item');

         if (typeof target !== typeof undefined && target !== false) {

            var originalTarget = target;
            _that.currentFilter = target;
            target = (target == 'all') ? $.trim($el.html()) : target;
            _that.closeFilter();
            $(itemSelector).removeClass('actived');
            $(this).addClass('actived');
            $(_that.filterSelector + ' .sc-dd-cur-lbl').html($el.text());
            _that.clearAllFilterPosts();
            _that.setFilterItems(originalTarget);
         }
      });
   },

   /**
   Fecha o dropdown dos filtros
   @method closeFilter
   */
   closeFilter: function() {
      $(this.filterSelector).removeClass('actived');
   },

   /**
   Limpa todos os filtros nos posts para compartilhamento
   @method clearAllFilterPosts
   */
   clearAllFilterPosts: function() {
      $(this.postsSelector + ' .sc-item').removeClass('sc-none');
   },

   /**
   Limpa todos os filtros nos posts para compartilhamento
   @method hideAllFilterPosts
   */
   hideAllFilterPosts: function() {
      $(this.postsSelector + ' .sc-item').addClass('sc-none');
   },

   hasTarget: function (itemClauster, target) {
      var _return = false;

      if (itemClauster.indexOf(' ') === -1) {
         if (itemClauster === target)
            return true;
         else
            return false;
      }

      var arr = itemClauster.split(' ');
      $.each(arr, function() {
         if (this === target) {
            _return = true;
            return false;
         }
      });
      return _return;
   },

   /**
   Aplica o filtro selecionado nos posts
   customizados
   @method setFilterItems
   @param {string} target Filtro de exibicao nos elementos com o atributo data-item
   */
   setFilterItems: function(target) {
      trackAnalytics(false, 'facebook', 'filtro-'+target);

      var _that = this;
      var $items = $(this.postsSelector + ' .sc-item[data-item]');
      var count = 0;

      if (typeof $items !== typeof undefined && $items !== false) {
         this.hideAllFilterPosts();
         this.showShowMore();

         $items.each(function(i) {
            if (_that.hasTarget($(this).data('item'), target) || target === 'all') {
               $(this).removeClass('sc-none');
               count++;

               if (count >= (_that.itemsPerPage * 2)) {
                  count = 0;
                  return false;
               }
            }
         });

         if (!this.getTotalItemsLeft())
            this.hideShowMore();
      }
   },

   /**
   Exibe mais posts que estao disponiveis na pagina
   @method showMore
   */
   showMore: function() {
      var _that = this;
      var $items = $(this.postsSelector + ' .sc-item[data-item]');

      if (!_that.getTotalItemsLeft())
         _that.hideShowMore();

      $(document).on('click', this.showMoreSelector, function() {
         var count = 0;

         if ($items) {
            $items.each(function() {
               if ($(this).hasClass('sc-none') && (_that.hasTarget($(this).data('item'), _that.currentFilter) || _that.currentFilter == 'all')) {
                  $(this).removeClass('sc-none');
                  count++;

                  if (count >= _that.itemsPerPage) {
                     count = 0;
                     return false;
                  }
               }
            });

            if (!_that.getTotalItemsLeft())
               _that.hideShowMore();
         }

         // parent.postMessage($('body').height(), "*");
         parent.postMessage({
            action: 'height',
            height: $('body').height()
         }, "*");

      });
   },

   /**
   Retorna o total de items que precisam ser carregados
   @method getTotalItemsLeft
   */
   getTotalItemsLeft: function() {
      var attr = (this.currentFilter === 'all') ? '' : '[data-item~="' + this.currentFilter + '"]';
      var $itemsLeft = $(this.postsSelector + ' .sc-none.sc-item' + attr);
      return $itemsLeft.length;
   },

   /**
   Esconde botao de exibir mais posts
   @method hideShowMore
   */
   hideShowMore: function() {
      $(this.showMoreSelector).addClass('sc-none');
   },

   /**
   Exibe botao de exibir mais posts
   @method showShowMore
   */
   showShowMore: function() {
      $(this.showMoreSelector).removeClass('sc-none');
   },

   /**
   Compartilha post customizado no facebook
   @method share
   */
   share: function() {
      var _that = this;

      this.selectShares = [
         this.postsSelector + ' .sc-item .sc-btn',
         '[data-simple-share]  .sc-btn'
      ].join(',');

      $(document).on('click', this.selectShares, function() {

         if (!$(this).hasClass('loading')) {
            _that.clearLoadingShare();
            $(this).addClass('loading');

            // Create Base64 Object
            var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9+/=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/rn/g,"n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}

            var data = (localStorage.getItem('natura_dia_das_maes')) ? localStorage.getItem('natura_dia_das_maes').trim() : 'nome=&telefone=&rede=';
            var $father = $(this).parents('.sc-item');
            var postId = $father.data('post');
            var url = Utils.getImagePath(postId, '') + '&' + data;

            if ($father.data('simple-share'))
               url = _that.getImagePathSimple($father);

            FB.getLoginStatus(function(response) {
               if (response.status === 'connected') {
                  Facebook.share(url, postId, _that, $father);
               } else {
                  FB.login(function(response) {
                     if (response && !response.error) {
                        if (response.authResponse) {
                           Facebook.share(url, postId, _that, $father);
                        } else {
                           fail();
                        }
                     } else {
                        fail(response);
                        alert ('Você precisa aceitar o aplicativo para compartilhar');
                     }
                  }, {scope:'publish_actions'});
               }
            });

            // FB.ui({
            //    method: 'share',
            //    app_id: PersistData.fb_id,
            //    href: url.toString().replace('image', 'www/share'),
            //    picture: url,
            //    success: function (response) {
            //       Modal.open({
            //          el: '#formShareSuccess',
            //          autoClose: 2500,
            //          callback: function () {
            //          }
            //       });
            //
            //       trackAnalytics(false, 'facebook', 'compartilhe_sucesso-' + postId);
            //       _that.clearLoadingShare();
            //
            //       if ($father.data('cover')) {
            //          if (typeof response.id !== "undefined" && typeof response.fb_url !== "undefined") {
            //             window.open(response.fb_url + "?preview_cover=" + response.id);
            //          }
            //       }
            //    },
            //    fail: function () {
            //       trackAnalytics(false, 'facebook', 'compartilhe_erro-' + postId);
            //       _that.clearLoadingShare();
            //    }
            // });

            // Facebook.me({
            //    picture: url,
            //    redir: $father.data('cover') ? true : false,
            //
            //    callback: function(response) {
            //       Modal.open({
            //          el: '#formShareSuccess',
            //          autoClose: 2500,
            //          callback: function() {}
            //       });
            //
            //       trackAnalytics(false, 'facebook', 'compartilhe_sucesso-'+postId);
            //
            //       _that.clearLoadingShare();
            //       if ($father.data('cover')) {
            //          if (typeof response.id !== "undefined" && typeof response.fb_url !== "undefined") {
            //             window.open(response.fb_url + "?preview_cover=" + response.id);
            //          }
            //       }
            //    },
            //    fail:function(){
            //       trackAnalytics(false, 'facebook', 'compartilhe_erro-'+postId);
            //    }
            // });
         }
      });
   },

   /**
   Remove classe loading dos botoes de compartilhamento
   @method clearAllFilterPosts
   */
   clearLoadingShare: function() {
      $(this.selectShares).removeClass('loading');
   },

   getImagePathSimple: function($post) {
      var rel_path = $post.find('.sc-img').attr('src');

      if (rel_path.indexOf('http') !== -1)
         return rel_path;

      return PersistData.absUrl + rel_path;
   }
};

export default DivulgueFacebook;

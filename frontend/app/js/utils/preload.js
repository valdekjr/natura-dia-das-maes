var Preload = function(images, callback) {
  this.callback = callback || function() {};
  this.map = [];
  this.path = PersistData.absUrl + "/img";
  this.setImages(images);
  this.reload();
  this.loadAll();
};

Preload.prototype.setImages = function(images) {
  var _that = this;
  // $('[data-scpreload]').each(function() {
  $.each(images, function() {
    if (!_that.imgs)
      _that.imgs = [];

    _that.imgs.push(this);
  });
};

Preload.prototype.reload = function() {
  var _that = this;
  if (this.preload) {
    this.preload.close();
  }

  this.manifest = this.imgs;
  this.preload = new createjs.LoadQueue(true);

  var handleFileLoad = function (event) {
    // console.log('handleFileLoad');
    var img = event.result;
    _that.map[_that.map.length] = {
      src: String(String(event.src).toLowerCase()).replace(this.path,'').replace('.jpg','').replace('.png',''),
      data:img
    };

    if (_that.map.length < _that.imgs.length) {

    } else {
      // if (_that.preload.progress == 1) {
      //  _that.stop();
      //  _that.addToDom();
      // }
    }
    // console.log(_that.map);
  };

  function handleFileProgress(event) {
    // console.log('handleFileProgress');
  }

  function handleOveralProgress(event) {
    // console.log('handleOveralProgress');
    // console.log('total :: ' + _that.preload.progress);
    var perc = _that.preload.progress * 100;
    // console.log('perc :: ' + perc + '%');
  }

  function handleFileError(event) {
    // console.log('error');
    // console.log(event);
  }

  function handleFilesComplete() {
    // console.log("handleFilesComplete");
      _that.stop();
      _that.callback();
  }

  this.preload.on("fileload", handleFileLoad, this);
  this.preload.on("progress", handleOveralProgress, this);
  this.preload.on("fileprogress", handleFileProgress, this);
  this.preload.on("complete", handleFilesComplete, this);
};

Preload.prototype.loadAll = function() {
  var _that = this;
  while (this.manifest.length > 0) {
    loadAnother();
  }

  function loadAnother() {
    var item = _that.manifest.shift();
    _that.preload.loadFile(item);
  };
};

Preload.prototype.stop = function() {
  if (this.preload) {
    this.preload.close();
  }
};

Preload.prototype.setImageTag = function(el, filename, path) {
  path = path || this.path;
  el.attr('src', path + filename);
};

export default Preload;

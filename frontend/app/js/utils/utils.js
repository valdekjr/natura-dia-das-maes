import Base64 from './base64';
import Cookie from './cookie';

var Utils = {

  /**
   Retorna uma hash com base64 de um objeto
   @method getHash
   @param {object} data
  */
  getHash: function(data) {
    var get_params = $.param(data),
        base64_params = Base64.encode(get_params);
    return base64_params;
  },

  /**
   Recupera dados enviados pelo usuario
   que estao gravados no cookie
   @method getCookieData
  */
  getCookieData: function() {
    var cookie = Cookie.get('natura_dia_das_maes');
    if (!cookie)
      return {};

    if (typeof cookie.rede !== "undefined")
      cookie.rede =  decodeURIComponent(cookie.rede);
    if (typeof cookie.telefone !== "undefined")
      cookie.telefone = cookie.telefone.replace("+", " ");
    return cookie;
  },

  getURLData: function() {
    var tmp = 'nome='+localStorage.getItem('nome');
    tmp += '&telefone='+localStorage.getItem('telefone');
    tmp += '&rede='+decodeURIComponent(localStorage.getItem('rede'));
    tmp += '&goo='+localStorage.getItem('goo');

    return Utils.getHash(tmp);
  },

  /**
   Devolve o caminho da imagem com os dados
   preenchidos e convertidos para base64
   @method getImagePath
   @param {string} post Nome da imagem no servico
   @param {object} data Objeto com as informacoes
  */
  getImagePath: function(post, data) {
    var data = data || null,
        params = {};

    params["post"] = post;

    if (data) {
      params["data"] = Utils.getHash(data);
      params["goo"] = data.goo;
    }

    // return 'http://dpz.natura.monstroestudio.com.br/www/slim/public/'+params['post']+'/'+params['data']+'/'+params['goo'];
    return PersistData.serviceUrl + '?' + $.param(params);
  },

  /**
   Retorna o template com a faixa
  */
  getTemplateTrack: function(data) {
     var rede = (data.rede != "") ? PersistData.siteCFUrl + data.rede : null;
     var nome = (typeof data.nome !== "undefined") ? decodeURIComponent(data.nome.replace(/\+/g, ' ')) : null;
     var telefone = (typeof data.telefone !== "undefined")  ? data.telefone.replace(/\+/g, ' ') : null;
     var hideClass = (rede && rede.length) ? '' : 'sc-none';
     var firstName=null;

     if (nome){
        firstName = nome.split(' ')[0];
     }

     var _html = '<div class="sc-item-lbl">';
     _html    += ' <p class="sc-item-lbl-title">';
     _html    += (nome) ? '    Compre com <span class="sc-item-person">' + firstName + '</span>' : '';
     _html    += ' </p>';


     if (telefone && telefone != '') {
        telefone = this.clearPhoneNumber(telefone);
        telefone = "55" + telefone;

        _html    += ' <p class="sc-item-lbl-contact">';
        _html    += '   Ligue para ';
        _html    += '   <span class="sc-item-phone">';
        _html    +=      this.formatPhoneNumber(telefone);
        _html    += '   </span>';
        _html    += ' </p>';
     }

     if (rede) {
        _html    += ' <p class="sc-item-lbl-contact">';
        _html    += '   <span class="sc-item-url-wrap ' + hideClass + '">';
        _html    += (telefone) ? 'Ou acesse ' : 'Acesse ';
        _html    += '     <span class="sc-item-url">';
        _html    +=       decodeURIComponent(rede);
        _html    += '     </span>';
        _html    += '   </span>';
        _html    += ' </p>';
     }

     _html    += '</div>';

     if (nome) {
        return _html;
     } else {
        return null;
     }
  },
  getParamsObj: function(serializedString) {
    var str = decodeURI(serializedString);
    var pairs = str.split('&');
    var obj = {}, p, idx, val;
    for (var i=0, n=pairs.length; i < n; i++) {
      p = pairs[i].split('=');
      idx = p[0];

      if (idx.indexOf("[]") == (idx.length - 2)) {
        // Eh um vetor
        var ind = idx.substring(0, idx.length-2)
        if (obj[ind] === undefined) {
          obj[ind] = [];
        }
        obj[ind].push(p[1]);
      }
      else {
        obj[idx] = p[1];
      }
    }
    return obj;
  },

  /**
   Deixa padrao de numero de telefone apenas com numeros
   @method clearPhoneNumber
   @param {string} number Numero conforme exeplo (11) 23456-7890
  */
  clearPhoneNumber: function(number) {
    return number.replace(/\s/g, '')
                 .replace(/\+/g, '')
                 .replace('-', '')
                 .replace('(', '')
                 .replace(')', '');
  },

  /**
   Formata numero de telefone para o padrao (xx) XXXXX-XXXX
   @method formatPhoneNumber
   @param {string} number Numero conforme exeplo 5511234567890
  */
  formatPhoneNumber: function(number) {
    if (number.length > 12)
      return $.trim(number.replace(/(\d{2})(\d{2})(\d{5})(\d{4})/, "($2) $3-$4"));
    else
      return $.trim(number.replace(/(\d{2})(\d{2})(\d{4})(\d{4})/, "($2) $3-$4"));
  }
};
export default Utils;

import Cookie from './utils/cookie';
import Utils from './utils/utils';

var Modal = {

   open: function(opts) {
      var _that = this;
      var callback = opts.callback || function() {};
      var init = opts.init || function() {};

      this.btnClose();
      this.autoClose = opts.autoClose || null;

      this.$el = $(opts.el);
      this.$modal = $('#scModal');
      this.$body = $('body');
      this.$wrap = $('.sc-md-wrapper');

      if (this.$modal.hasClass('sc-none')) {
         this.$modal.removeClass('sc-none').addClass('actived');
         this.$body.addClass('sc-ovf-hidden');
      }

      this.clearItems(!this.$modal.hasClass('sc-none'));

      setTimeout(function() {
         _that.$el.addClass('actived');
         _that.$wrap.css("margin-top", '1250px');

         init(_that.$el);

         setTimeout(function() {
            _that.$wrap.addClass('actived');

            if (_that.autoClose) {
               _that.timeAutoClose = setTimeout(function() {
                  _that.close(function() {
                     callback();
                  });
               }, _that.autoClose);
            } else {
               callback(false);
            }
         }, 300);
      }, 100);
   },

   close: function(callback) {
      var _that = this;
      var callback = callback || function() {};

      this.$modal.removeClass('actived');
      this.$body.removeClass('sc-ovf-hidden');

      setTimeout(function() {
         _that.$modal.addClass('sc-none')
         _that.clearItems();

         // @todo: remover esse tratamento
         $('.sc-list .sc-item .sc-btn').removeClass('loading');

         var cookie = Cookie.get('natura_natal');
         if (!!cookie) {
            if (typeof cookie.whats_number !== "undefined") {
               $('#formNumWp').val(Utils.formatPhoneNumber(cookie.whats_number));
            }
         }

         _that.$modal.removeClass('loading');
         callback();
      }, 300);
   },
   clearItems: function(isOpen) {
      $('.sc-md-container').removeClass('actived');
      if (!isOpen) {
         this.$wrap.removeClass('actived');
      }
   },
   btnClose: function() {
      var _that = this;
      $(document).on('click', '.sc-md-close', function() {
         if (_that.timeAutoClose)
            clearTimeout(_that.timeAutoClose);
         _that.close();
      });
   }
};

export default Modal;

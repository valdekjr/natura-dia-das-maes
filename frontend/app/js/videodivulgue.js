/**
 * Created by Valdek on 23/11/2015.
 */

import YoutubeVideo from './utils/youtube';

var VideoDivulgue = {
    openClass: 'assistindo',
    videoElId: 'vh-player',
    videoSel: '#vh-player',
    init: function(){
        this.setEl();
        this.setVars();
        this.clear();
        this.bind();
        this.openVideo2();
    },
    setEl: function(){
        this.$player = $('#vh-player');
        this.$playerContainer = $('#vh-player-container');
        this.$txtBoxVideo1 = $('#video-01-text');
        this.$txtBoxVideo2 = $('#video-02-text');
        this.$btnOpenVideo1 = $('.btn-open-video1');
        this.$btnOpenVideo2 = $('.btn-open-video2');
    },
    setVars: function(){
        this.tagVideo = this.$player.attr('data-tagvideo');
    },
    clear: function(){
        this.$txtBoxVideo1.hide();
        this.$txtBoxVideo2.hide();
        this.$btnOpenVideo1.removeClass(this.openClass);
        this.$btnOpenVideo2.removeClass(this.openClass);
    },
    bind: function(){
        var _self = this;
        this.$btnOpenVideo1.click(function(){ _self.openVideo1(); }); // Open Video 1
        this.$btnOpenVideo2.click(function(){ _self.openVideo2(); }); // Open Video 2
    },
    changeVideo: function($btn, $box){
        // Interface
        var _this = this;
        var _var = 'videoid';
        this.clear();
        $btn.addClass(this.openClass);
        $box.show();

        // Vars
        var videoId = $btn.data(_var);
        this.$playerContainer.empty();
        var _$player = $("<div />");
        _$player.attr("id", this.videoElId);
        _$player.attr("class", this.videoElId);
        _$player.attr('data-tagvideo', this.tagVideo);
        _$player.attr('data-videoid', videoId);
        this.$playerContainer.append(_$player);

        // Video
        this.video = new YoutubeVideo({
            el: _this.videoSel,
            playerVars: {
                autoplay: 0,
                showinfo: 0,
                rel: 0
            }
        });
    },
    openVideo1: function(){
        this.changeVideo(this.$btnOpenVideo1, this.$txtBoxVideo1);
    },
    openVideo2: function(){
        this.changeVideo(this.$btnOpenVideo2, this.$txtBoxVideo2);
    }
};

window.VideoDivulgue = VideoDivulgue;

export default VideoDivulgue;
(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Blacklist = {
  blocked: ["acquaflora", "adcos", "adidas", "agua de cheiro", "amend", "ana hickmann", "antonio banderas", "anus", "avon", "avora", "babaca", "babaovo", "baba-ovo", "bacura", "bagos", "baitola", "bare minerals", "beauty box", "bebum", "bel col", "belcorp", "benefit", "benetton", "besta", "betty boop", "bicha", "bio art", "bioderm", "bionatus", "bisca", "bixa", "boazuda", "boceta", "boco", "boco", "boiola", "bolagato", "bolcat", "boquete", "bosseta", "bosta", "bostana", "botanic", "boticario", "boticario", "brecha", "brexa", "brioco", "bronha", "buca", "buceta", "bunda", "bunduda", "burberry", "burra", "burro", "busseta", "cachorra", "cachorro", "cadela", "cadiveu", "caga", "cagado", "cagao", "cagona", "calvin klein", "canalha", "caralho", "carolina herrera", "casseta", "cassete", "cetaphil", "checheca", "chereca", "chibumba", "chibumbo", "chifruda", "chifrudo", "chochota", "chota", "chupada", "chupado", "clean & clear", "clinique", "clitoris", "clitoris", "cocaina", "cocaina", "coco", "coco", "colorama", "contem 1g", "corna", "corno", "cornuda", "cornudo", "corrupta", "corrupto", "cretina", "cretino", "cruz-credo", "cu", "cu ", "culhao", "culhao", "culhoes", "curalho", "curaprox", "cuzao", "cuzao", "cuzuda", "cuzudo", "davene", "debil", "debiloide", "defunto", "demonio", "demonio", "diesel", "difunto", "dior", "doida", "doido", "dolve&gabbana", "dove", "ecologie", "egua", "egua", "embelezze", "escrota", "escroto", "esporrada", "esporrado", "esporro", "esporro", "estupida", "estupida", "estupidez", "estupido", "estupido", "eudora", "evian", "fator 5", "fdp", "fedida", "fedido", "fedor", "fedorenta", "feia", "feio", "feiosa", "feioso", "feioza", "feiozo", "felacao", "felacao", "fenda", "ferrari", "fiorentino", "foda", "fodao", "fodao", "fode", "fodida", "fodido", "fornica", "fudecao", "fudecao", "fudendo", "fudida", "fudido", "furada", "furado", "furao", "furao", "furnica", "furnicar", "furo", "furona", "gabriela sabatini", "gaiata", "gaiato", "gay", "gilette", "giorgio armani", "givenchy", "gonorrea", "gonorreia", "gosma", "gosmenta", "gosmento", "granado", "grelinho", "grelo", "gucci", "guess", "homosexual", "homo-sexual", "homossexual", "hugo boss", "idiota", "idiotice", "imbecil", "impala", "iscrota", "iscroto", "jaguar", "japa", "jequiti", "johnson & johnson", "joop", "juliana paes", "katy perry", "kenzo", "keune", "kolene", "koloss", "l’aqua di fiori", "l’occitane", "la roche-posay", "lacoste", "ladra", "ladrao", "ladrao", "ladroeira", "ladrona", "lady gaga", "lalau", "lalique", "lancome", "lanza", "leprosa", "leproso", "lesbica", "lesbica", "l'occitane au bresil", "loreal paris", "lush", "mac", "macaca", "macaco", "machona", "machorra", "mahogany", "mairibel", "manguaca", "manguaca", "mary kay", "masturba", "max love", "maybeline", "maybelline", "meleca", "merda", "mija", "mijada", "mijado", "mijo", "mocrea", "mocrea", "mocreia", "mocreia", "moleca", "moleque", "mondronga", "mondrongo", "montblanc", "naba", "nadega", "nazca", "neutrogena", "niasi", "nike", "nivea", "nojeira", "nojenta", "nojento", "nojo", "novex", "nus", "o boticario", "odorata", "olhota", "otaria", "otaria", "otario", "otario", "ox", "paca", "pantene", "paspalha", "paspalhao", "paspalho", "pau ", "payot", "peia", "peido", "pemba", "penis", "penis", "pentelha", "pentelho", "perereca", "peru", "peru", "petralha", "phytoervas", "pica", "picao", "picao", "pilantra", "piranha", "piroca", "piroco", "piru", "playboy", "porra", "prada", "prega", "prostibulo", "prostibulo", "prostituta", "prostituto", "puma", "punheta", "punhetao", "punhetao", "pus", "pustula", "pustula", "puta", "puto", "puxasaco", "puxa-saco", "quem disse berenice", "rabao", "rabao", "rabo", "rabuda", "rabudao", "rabudao", "rabudo", "rabudona", "racco", "racha", "rachada", "rachadao", "rachadao", "rachadinha", "rachadinho", "rachado", "ralph lauren", "ramela", "remela", "retardada", "retardado", "revlon", "ridicula", "ridicula", "risque", "rola", "rolinha", "rosca", "sacana", "safada", "safado", "sapatao", "sapatao", "sephora", "shakira", "shiseido", "sifilis", "sifilis", "siririca", "sundown", "tarada", "tarado", "tesao", "tesao", "testuda", "tezuda", "tezudo", "the beauty box", "the body shop", "the body store", "trocha", "trolha", "troucha", "trouxa", "troxa", "vaca", "vadia", "vagabunda", "vagabundo", "vagina", "valmari", "veada", "veadao", "veadao", "veado", "viada", "viadao", "viadao", "viado", "vichy", "vult", "xana", "xaninha", "xavasca", "xerereca", "xexeca", "xibiu", "xibumba", "xochota", "xota", "xoxota", "yama", "yes cosmetics", "yves saint laurent"],
  init: function() {
    jQuery.validator.addMethod("blacklist", function(value, element) {
      return Blacklist.validate(value);
    }, "Nome inválido");
  },
  validate: function(fullname) {
    try {
      fullname = fullname.trim();
      var invalidFullname = Blacklist.inBlacklist(fullname);
      if (true === invalidFullname) {
        return false;
      }
      fullname = fullname.trim();
      var names = fullname.split(' ');
      if (names && names.length > 0) {
        var firstname = names[0];
        return false === Blacklist.inBlacklist(firstname);
      }
    } catch (ex) {
      console.log('Blacklist', 'validate', 'fullname', fullname, ex);
    }
    return false;
  },
  inBlacklist: function(rawword) {
    var word_to_check = Blacklist.normaliseChars(rawword).toLowerCase().trim();
    if (word_to_check == '') {
      return false;
    }
    var blocked_word;
    var word_size;
    var CHECK_EQUALS_LEN = 3;
    var in_black_list;
    for (var i = 0; i < Blacklist.blocked.length; ++i) {
      blocked_word = Blacklist.normaliseChars(Blacklist.blocked[i]);
      word_size = blocked_word.length;
      if (0 <= word_size && word_size <= CHECK_EQUALS_LEN) {
        in_black_list = (word_to_check == blocked_word);
      } else {
        in_black_list = (word_to_check.indexOf(blocked_word) >= 0);
      }
      if (true === in_black_list) {
        return true;
      }
    }
    return false;
  },
  normaliseChars: function(text) {
    var invalid = 'áàãâäéèêëíìîïóòõôöúùûüçÁÀÃÂÄÉÈÊËÍÌÎÏÓÒÕÖÔÚÙÛÜÇ';
    var valid = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
    var nova = '';
    for (var i = 0; i < text.length; i++) {
      if (invalid.search(text.substr(i, 1)) >= 0) {
        nova += valid.substr(invalid.search(text.substr(i, 1)), 1);
      } else {
        nova += text.substr(i, 1);
      }
    }
    return nova;
  }
};
var $__default = Blacklist;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/blacklist.js
},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__utils_47_cookie__,
    $__utils_47_facebook__,
    $__utils_47_utils__,
    $__modal_46_js__;
var Cookie = ($__utils_47_cookie__ = require("./utils/cookie"), $__utils_47_cookie__ && $__utils_47_cookie__.__esModule && $__utils_47_cookie__ || {default: $__utils_47_cookie__}).default;
var Facebook = ($__utils_47_facebook__ = require("./utils/facebook"), $__utils_47_facebook__ && $__utils_47_facebook__.__esModule && $__utils_47_facebook__ || {default: $__utils_47_facebook__}).default;
var Utils = ($__utils_47_utils__ = require("./utils/utils"), $__utils_47_utils__ && $__utils_47_utils__.__esModule && $__utils_47_utils__ || {default: $__utils_47_utils__}).default;
var Modal = ($__modal_46_js__ = require("./modal.js"), $__modal_46_js__ && $__modal_46_js__.__esModule && $__modal_46_js__ || {default: $__modal_46_js__}).default;
var DivulgueFacebook = {
  filterSelector: '#fbSelCat',
  postsSelector: '#fbCustomPosts',
  showMoreSelector: '#showMoreFace',
  itemsPerPage: 3,
  currentFilter: 'all',
  init: function() {
    Facebook.init();
    this.dropdown();
    this.filter();
    this.showMore();
    this.share();
  },
  dropdown: function() {
    var _that = this;
    $(document).on('click', this.filterSelector + ' .sc-dd-current', function() {
      var $father = $(this).parent();
      if ($father.hasClass('actived')) {
        $father.removeClass('actived');
      } else {
        $father.addClass('actived');
      }
    });
    $(document).on('mouseleave', this.filterSelector, function() {
      _that.closeFilter();
    });
  },
  filter: function() {
    var _that = this;
    var itemSelector = this.filterSelector + ' .sc-dd-item';
    $(document).on('click', itemSelector, function() {
      var $el = $(this);
      var target = $el.attr('data-item');
      if (typeof target !== typeof undefined && target !== false) {
        var originalTarget = target;
        _that.currentFilter = target;
        target = (target == 'all') ? $.trim($el.html()) : target;
        _that.closeFilter();
        $(itemSelector).removeClass('actived');
        $(this).addClass('actived');
        $(_that.filterSelector + ' .sc-dd-cur-lbl').html($el.text());
        _that.clearAllFilterPosts();
        _that.setFilterItems(originalTarget);
      }
    });
  },
  closeFilter: function() {
    $(this.filterSelector).removeClass('actived');
  },
  clearAllFilterPosts: function() {
    $(this.postsSelector + ' .sc-item').removeClass('sc-none');
  },
  hideAllFilterPosts: function() {
    $(this.postsSelector + ' .sc-item').addClass('sc-none');
  },
  hasTarget: function(itemClauster, target) {
    var _return = false;
    if (itemClauster.indexOf(' ') === -1) {
      if (itemClauster === target)
        return true;
      else
        return false;
    }
    var arr = itemClauster.split(' ');
    $.each(arr, function() {
      if (this === target) {
        _return = true;
        return false;
      }
    });
    return _return;
  },
  setFilterItems: function(target) {
    trackAnalytics(false, 'facebook', 'filtro-' + target);
    var _that = this;
    var $items = $(this.postsSelector + ' .sc-item[data-item]');
    var count = 0;
    if (typeof $items !== typeof undefined && $items !== false) {
      this.hideAllFilterPosts();
      this.showShowMore();
      $items.each(function(i) {
        if (_that.hasTarget($(this).data('item'), target) || target === 'all') {
          $(this).removeClass('sc-none');
          count++;
          if (count >= (_that.itemsPerPage * 2)) {
            count = 0;
            return false;
          }
        }
      });
      if (!this.getTotalItemsLeft())
        this.hideShowMore();
    }
  },
  showMore: function() {
    var _that = this;
    var $items = $(this.postsSelector + ' .sc-item[data-item]');
    if (!_that.getTotalItemsLeft())
      _that.hideShowMore();
    $(document).on('click', this.showMoreSelector, function() {
      var count = 0;
      if ($items) {
        $items.each(function() {
          if ($(this).hasClass('sc-none') && (_that.hasTarget($(this).data('item'), _that.currentFilter) || _that.currentFilter == 'all')) {
            $(this).removeClass('sc-none');
            count++;
            if (count >= _that.itemsPerPage) {
              count = 0;
              return false;
            }
          }
        });
        if (!_that.getTotalItemsLeft())
          _that.hideShowMore();
      }
      parent.postMessage({
        action: 'height',
        height: $('body').height()
      }, "*");
    });
  },
  getTotalItemsLeft: function() {
    var attr = (this.currentFilter === 'all') ? '' : '[data-item~="' + this.currentFilter + '"]';
    var $itemsLeft = $(this.postsSelector + ' .sc-none.sc-item' + attr);
    return $itemsLeft.length;
  },
  hideShowMore: function() {
    $(this.showMoreSelector).addClass('sc-none');
  },
  showShowMore: function() {
    $(this.showMoreSelector).removeClass('sc-none');
  },
  share: function() {
    var _that = this;
    this.selectShares = [this.postsSelector + ' .sc-item .sc-btn', '[data-simple-share]  .sc-btn'].join(',');
    $(document).on('click', this.selectShares, function() {
      if (!$(this).hasClass('loading')) {
        _that.clearLoadingShare();
        $(this).addClass('loading');
        var Base64 = {
          _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
          encode: function(e) {
            var t = "";
            var n,
                r,
                i,
                s,
                o,
                u,
                a;
            var f = 0;
            e = Base64._utf8_encode(e);
            while (f < e.length) {
              n = e.charCodeAt(f++);
              r = e.charCodeAt(f++);
              i = e.charCodeAt(f++);
              s = n >> 2;
              o = (n & 3) << 4 | r >> 4;
              u = (r & 15) << 2 | i >> 6;
              a = i & 63;
              if (isNaN(r)) {
                u = a = 64;
              } else if (isNaN(i)) {
                a = 64;
              }
              t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a);
            }
            return t;
          },
          decode: function(e) {
            var t = "";
            var n,
                r,
                i;
            var s,
                o,
                u,
                a;
            var f = 0;
            e = e.replace(/[^A-Za-z0-9+/=]/g, "");
            while (f < e.length) {
              s = this._keyStr.indexOf(e.charAt(f++));
              o = this._keyStr.indexOf(e.charAt(f++));
              u = this._keyStr.indexOf(e.charAt(f++));
              a = this._keyStr.indexOf(e.charAt(f++));
              n = s << 2 | o >> 4;
              r = (o & 15) << 4 | u >> 2;
              i = (u & 3) << 6 | a;
              t = t + String.fromCharCode(n);
              if (u != 64) {
                t = t + String.fromCharCode(r);
              }
              if (a != 64) {
                t = t + String.fromCharCode(i);
              }
            }
            t = Base64._utf8_decode(t);
            return t;
          },
          _utf8_encode: function(e) {
            e = e.replace(/rn/g, "n");
            var t = "";
            for (var n = 0; n < e.length; n++) {
              var r = e.charCodeAt(n);
              if (r < 128) {
                t += String.fromCharCode(r);
              } else if (r > 127 && r < 2048) {
                t += String.fromCharCode(r >> 6 | 192);
                t += String.fromCharCode(r & 63 | 128);
              } else {
                t += String.fromCharCode(r >> 12 | 224);
                t += String.fromCharCode(r >> 6 & 63 | 128);
                t += String.fromCharCode(r & 63 | 128);
              }
            }
            return t;
          },
          _utf8_decode: function(e) {
            var t = "";
            var n = 0;
            var r = c1 = c2 = 0;
            while (n < e.length) {
              r = e.charCodeAt(n);
              if (r < 128) {
                t += String.fromCharCode(r);
                n++;
              } else if (r > 191 && r < 224) {
                c2 = e.charCodeAt(n + 1);
                t += String.fromCharCode((r & 31) << 6 | c2 & 63);
                n += 2;
              } else {
                c2 = e.charCodeAt(n + 1);
                c3 = e.charCodeAt(n + 2);
                t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
                n += 3;
              }
            }
            return t;
          }
        };
        var data = (localStorage.getItem('natura_dia_das_maes')) ? localStorage.getItem('natura_dia_das_maes').trim() : 'nome=&telefone=&rede=';
        var $father = $(this).parents('.sc-item');
        var postId = $father.data('post');
        var url = Utils.getImagePath(postId, '') + '&' + data;
        if ($father.data('simple-share'))
          url = _that.getImagePathSimple($father);
        FB.getLoginStatus(function(response) {
          if (response.status === 'connected') {
            Facebook.share(url, postId, _that, $father);
          } else {
            FB.login(function(response) {
              if (response && !response.error) {
                if (response.authResponse) {
                  Facebook.share(url, postId, _that, $father);
                } else {
                  fail();
                }
              } else {
                fail(response);
                alert('Você precisa aceitar o aplicativo para compartilhar');
              }
            }, {scope: 'publish_actions'});
          }
        });
      }
    });
  },
  clearLoadingShare: function() {
    $(this.selectShares).removeClass('loading');
  },
  getImagePathSimple: function($post) {
    var rel_path = $post.find('.sc-img').attr('src');
    if (rel_path.indexOf('http') !== -1)
      return rel_path;
    return PersistData.absUrl + rel_path;
  }
};
var $__default = DivulgueFacebook;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/divulgue-facebook.js
},{"./modal.js":5,"./utils/cookie":8,"./utils/facebook":9,"./utils/utils":13}],3:[function(require,module,exports){
"use strict";
var $__utils_47_scroll_45_anchor__,
    $__material_45_divulgacao__,
    $__divulgue_45_facebook__,
    $__blacklist_46_js__;
var ScrollAnchor = ($__utils_47_scroll_45_anchor__ = require("./utils/scroll-anchor"), $__utils_47_scroll_45_anchor__ && $__utils_47_scroll_45_anchor__.__esModule && $__utils_47_scroll_45_anchor__ || {default: $__utils_47_scroll_45_anchor__}).default;
var MaterialDivulgacao = ($__material_45_divulgacao__ = require("./material-divulgacao"), $__material_45_divulgacao__ && $__material_45_divulgacao__.__esModule && $__material_45_divulgacao__ || {default: $__material_45_divulgacao__}).default;
var DivulgueFacebook = ($__divulgue_45_facebook__ = require("./divulgue-facebook"), $__divulgue_45_facebook__ && $__divulgue_45_facebook__.__esModule && $__divulgue_45_facebook__ || {default: $__divulgue_45_facebook__}).default;
var Blacklist = ($__blacklist_46_js__ = require("./blacklist.js"), $__blacklist_46_js__ && $__blacklist_46_js__.__esModule && $__blacklist_46_js__ || {default: $__blacklist_46_js__}).default;
var Main = {
  init: function() {
    ScrollAnchor.init();
    MaterialDivulgacao.init();
    DivulgueFacebook.init();
    Blacklist.init();
    this.trackPageView();
    this.modalTermos();
  },
  trackPageView: function() {
    var _that = this;
    if (typeof ga === "undefined") {
      setTimeout(function() {
        _that.trackPageView();
      }, 500);
      return false;
    }
    ga('send', 'pageview', {
      'page': "/www/ferramenta-de-vendas/dia-das-maes",
      'title': ""
    });
  },
  modalTermos: function() {
    $(".footer-toolkit  a").on("click", function(e) {
      e.preventDefault();
      switch ($(this).attr('href')) {
        case '#politica':
          $('.content-termos, .content-video').hide();
          $('.content-politica').show();
          $('.lightbox').fadeIn(400, function() {
            $('.content-politica').jScrollPane({
              showArrows: true,
              autoReinitialise: true
            });
          });
          break;
        case '#termos':
          $('.content-politica, .content-video').hide();
          $('.content-termos').show();
          $('.lightbox').fadeIn(400, function() {
            $('.content-termos').jScrollPane({
              showArrows: true,
              autoReinitialise: true
            });
          });
          break;
      }
    });
    $(".lightbox .close, .lightbox .bg").on("click", function(e) {
      e.stopPropagation();
      $('.lightbox').fadeOut();
    });
  }
};
$(document).ready(function() {
  var target = parent;
  Main.init();
  if (document.location.href.indexOf('debug') > 0) {
    $("#divFaceAppShare").show();
  }
  parent.postMessage({
    action: 'height',
    height: $('body').height()
  }, "*");
  $("#p2Facebook").on("click", function() {
    target.postMessage({
      action: 'scroll',
      posY: $("#divFaceApp").position().top
    }, "*");
  });
  $("#p2Email").on("click", function() {
    target.postMessage({
      action: 'scroll',
      posY: $("#divEmail").position().top
    }, "*");
  });
});


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/main.js
},{"./blacklist.js":1,"./divulgue-facebook":2,"./material-divulgacao":4,"./utils/scroll-anchor":12}],4:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__utils_47_cookie__,
    $__utils_47_scroll_45_anchor__,
    $__utils_47_utils__,
    $__utils_47_google_45_api_45_url__,
    $__utils_47_preload__,
    $__modal__,
    $__salvar_45_cn_46_js__;
var Cookie = ($__utils_47_cookie__ = require("./utils/cookie"), $__utils_47_cookie__ && $__utils_47_cookie__.__esModule && $__utils_47_cookie__ || {default: $__utils_47_cookie__}).default;
var ScrollAnchor = ($__utils_47_scroll_45_anchor__ = require("./utils/scroll-anchor"), $__utils_47_scroll_45_anchor__ && $__utils_47_scroll_45_anchor__.__esModule && $__utils_47_scroll_45_anchor__ || {default: $__utils_47_scroll_45_anchor__}).default;
var Utils = ($__utils_47_utils__ = require("./utils/utils"), $__utils_47_utils__ && $__utils_47_utils__.__esModule && $__utils_47_utils__ || {default: $__utils_47_utils__}).default;
var GoogleApiUrl = ($__utils_47_google_45_api_45_url__ = require("./utils/google-api-url"), $__utils_47_google_45_api_45_url__ && $__utils_47_google_45_api_45_url__.__esModule && $__utils_47_google_45_api_45_url__ || {default: $__utils_47_google_45_api_45_url__}).default;
var Preload = ($__utils_47_preload__ = require("./utils/preload"), $__utils_47_preload__ && $__utils_47_preload__.__esModule && $__utils_47_preload__ || {default: $__utils_47_preload__}).default;
var Modal = ($__modal__ = require("./modal"), $__modal__ && $__modal__.__esModule && $__modal__ || {default: $__modal__}).default;
var SalvarCn = ($__salvar_45_cn_46_js__ = require("./salvar-cn.js"), $__salvar_45_cn_46_js__ && $__salvar_45_cn_46_js__.__esModule && $__salvar_45_cn_46_js__ || {default: $__salvar_45_cn_46_js__}).default;
var MaterialDivugacao = {
  lastError: false,
  lastErrorEl: false,
  redeexp: /^((http:\/\/){0,1}rede\.{0,1}natura\.net\/espaco\/){0,1}([a-z0-9]{1,30}){1}\/{0,1}$/i,
  init: function() {
    this.setEls();
    this.validaName();
    this.validaRede();
    this.applyMaskForm();
    this.send();
    this.fillDataIfExist();
    this.bindControls();
  },
  matchRede: function() {
    var m = this.redeexp.exec(this.$rede.val());
    if (m == null) {
      return null;
    }
    return m[m.length - 1];
  },
  bindControls: function() {
    var _that = this;
    this.$btnSend.click(function() {
      var _valid = _that.$form.valid();
      if (_valid) {
        _that.$form.submit();
      } else {
        trackAnalytics(false, 'pagina-personalizada', 'erro');
      }
    });
  },
  setEls: function() {
    this.$form = $('#formCustomPost');
    this.$btnSend = $('#formCustomPostSend');
    this.$loadFormSend = $('#loadFormSend');
    this.$customLink = $('#customLink');
    this.$rede = $('#txtrede');
    this.$redeout = $('#lblredeout');
  },
  send: function() {
    var _that = this;
    this.$form.validate({
      onkeyup: false,
      rules: {
        nome: {
          blacklist: true,
          validaName: true,
          required: true,
          minlength: 3,
          maxlength: 50
        },
        telefone: {minlength: 14},
        rede: {validaRede: true}
      },
      errorPlacement: function(error, element) {
        if (MaterialDivugacao.lastError != error.html()) {
          MaterialDivugacao.lastError = error.html();
        }
      },
      submitHandler: function(form) {
        _that.$form.addClass('sending');
        var success = function(anchor) {
          Modal.open({
            el: '#formSuccess',
            autoClose: 2500,
            callback: function(result) {
              ScrollAnchor.go(anchor);
              _that.$form.removeClass('sending');
              _that.getEmailCustom();
              _that.getCoversCustom();
            }
          });
        };
        var dataForm = Utils.getParamsObj(_that.$form.serialize());
        var _rede = _that.matchRede();
        if (_rede != null) {
          dataForm.rede = _rede;
        }
        localStorage.setItem("natura_dia_das_maes", _that.$form.serialize());
        var cookie = Cookie.get('natura_dia_das_maes');
        if (cookie)
          Cookie.addItem('natura_dia_das_maes', dataForm);
        else
          Cookie.set('natura_dia_das_maes', dataForm);
        var data = Utils.getCookieData();
        var foneJustNumbers = Utils.clearPhoneNumber(dataForm.telefone);
        var utmtag = '&utm_source=paginapersonalizada&utm_medium=' + foneJustNumbers + '&utm_content=ativacao-toolkit&utm_campaign=natal-toolkit';
        GoogleApiUrl.getShordUrl({
          url: PersistData.siteCFUrl + data.rede + "?data=" + Utils.getHash(data) + utmtag,
          success: function(url) {
            trackAnalytics(false, 'pagina-personalizada', 'sucesso');
            Cookie.addItem('natura_dia_das_maes', {goo: url.replace('http://goo.gl/', '')});
            _that.applyTrackData(dataForm);
            _that.$loadFormSend.addClass('sc-none');
            success('#whatsapp-personal-anchor');
          }
        });
      }
    });
  },
  getEmailCustom: function() {
    var emailPostId = 'E01';
    var $emailCustom = $('.box-email-custom');
    var Base64 = {
      _keyStr: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",
      encode: function(e) {
        var t = "";
        var n,
            r,
            i,
            s,
            o,
            u,
            a;
        var f = 0;
        e = Base64._utf8_encode(e);
        while (f < e.length) {
          n = e.charCodeAt(f++);
          r = e.charCodeAt(f++);
          i = e.charCodeAt(f++);
          s = n >> 2;
          o = (n & 3) << 4 | r >> 4;
          u = (r & 15) << 2 | i >> 6;
          a = i & 63;
          if (isNaN(r)) {
            u = a = 64;
          } else if (isNaN(i)) {
            a = 64;
          }
          t = t + this._keyStr.charAt(s) + this._keyStr.charAt(o) + this._keyStr.charAt(u) + this._keyStr.charAt(a);
        }
        return t;
      },
      decode: function(e) {
        var t = "";
        var n,
            r,
            i;
        var s,
            o,
            u,
            a;
        var f = 0;
        e = e.replace(/[^A-Za-z0-9+/=]/g, "");
        while (f < e.length) {
          s = this._keyStr.indexOf(e.charAt(f++));
          o = this._keyStr.indexOf(e.charAt(f++));
          u = this._keyStr.indexOf(e.charAt(f++));
          a = this._keyStr.indexOf(e.charAt(f++));
          n = s << 2 | o >> 4;
          r = (o & 15) << 4 | u >> 2;
          i = (u & 3) << 6 | a;
          t = t + String.fromCharCode(n);
          if (u != 64) {
            t = t + String.fromCharCode(r);
          }
          if (a != 64) {
            t = t + String.fromCharCode(i);
          }
        }
        t = Base64._utf8_decode(t);
        return t;
      },
      _utf8_encode: function(e) {
        e = e.replace(/rn/g, "n");
        var t = "";
        for (var n = 0; n < e.length; n++) {
          var r = e.charCodeAt(n);
          if (r < 128) {
            t += String.fromCharCode(r);
          } else if (r > 127 && r < 2048) {
            t += String.fromCharCode(r >> 6 | 192);
            t += String.fromCharCode(r & 63 | 128);
          } else {
            t += String.fromCharCode(r >> 12 | 224);
            t += String.fromCharCode(r >> 6 & 63 | 128);
            t += String.fromCharCode(r & 63 | 128);
          }
        }
        return t;
      },
      _utf8_decode: function(e) {
        var t = "";
        var n = 0;
        var r = c1 = c2 = 0;
        while (n < e.length) {
          r = e.charCodeAt(n);
          if (r < 128) {
            t += String.fromCharCode(r);
            n++;
          } else if (r > 191 && r < 224) {
            c2 = e.charCodeAt(n + 1);
            t += String.fromCharCode((r & 31) << 6 | c2 & 63);
            n += 2;
          } else {
            c2 = e.charCodeAt(n + 1);
            c3 = e.charCodeAt(n + 2);
            t += String.fromCharCode((r & 15) << 12 | (c2 & 63) << 6 | c3 & 63);
            n += 3;
          }
        }
        return t;
      }
    };
    $('.box-email').addClass('sc-none');
    $emailCustom.addClass('loading');
    var url = Utils.getImagePath(emailPostId, '') + '&' + localStorage.getItem('natura_dia_das_maes');
    var urlDownload = url + '&download=true';
    $emailCustom.removeClass('loading');
    var $img = $emailCustom.find('img'),
        $a = $emailCustom.find('a');
    $img.attr('src', url + '&random=' + Math.random());
    $a.attr('href', urlDownload);
    setTimeout(function() {
      $emailCustom.addClass('actived');
    }, 500);
  },
  getCoversCustom: function() {
    var $item = $('.box-cover-item'),
        urls = [Utils.getImagePath('F01', Utils.getCookieData()), Utils.getImagePath('F02', Utils.getCookieData()), Utils.getImagePath('F03', Utils.getCookieData())];
    $item.addClass('loading');
    var teste = new Preload(urls, function() {
      $.each(urls, function(i) {
        var urlImg = urls[i];
        var urlDownload = urlImg + '&download=true';
        $item.eq(i).find('.sc-img-wrap .sc-img').attr('src', urlImg);
        $item.eq(i).find('.sc-img-wrap a').attr('href', urlDownload);
        setTimeout(function() {
          $item.eq(i).removeClass('loading');
        }, 1000);
      });
    });
  },
  applyMaskForm: function() {
    var $formNumCont = $('#formNumCont');
    $formNumCont.mask('(00) 00000-0000').blur(function() {
      var target,
          phone,
          element;
      target = (event.currentTarget) ? event.currentTarget : event.srcElement;
      phone = target.value.replace(/\D/g, '');
      element = $(target);
      console.log('blur', phone.length);
      $formNumCont.unmask();
      if (phone.length === 10) {
        $formNumCont.mask('(00) 0000-0000');
      } else {
        $formNumCont.mask('(00) 00000-0000');
      }
    });
  },
  applyTrackData: function(data) {
    console.log('applyTrackData');
    var _that = this;
    $('#fbCustomPosts .sc-item, #wpCustomPosts .sc-item').each(function() {
      var $target = $(this).find('.sc-item-lbl');
      if ($target.length)
        $target.remove();
      $(this).prepend(Utils.getTemplateTrack(data));
    });
  },
  fillDataIfExist: function() {
    var cookie = Cookie.get('natura_natal');
    if (cookie) {
      if (!!cookie.nome && !!cookie.telefone) {
        var telefone = cookie.telefone,
            nome = cookie.nome.replace(/\+/g, ' '),
            goo = (typeof cookie.goo !== "undefined") ? cookie.goo : null;
        this.applyTrackData(cookie);
        this.getEmailCustom();
        this.getCoversCustom();
        $('#formCustomPost .sc-form-ipt[name="nome"]').val(decodeURIComponent(nome));
        $('#formCustomPost .sc-form-ipt[name="telefone"]').unmask();
        $('#formCustomPost .sc-form-ipt[name="telefone"]').val(Utils.formatPhoneNumber("55" + Utils.clearPhoneNumber(telefone)));
      }
    }
  },
  validaName: function() {
    $.validator.addMethod("validaName", function(value, element) {
      var reg = /^([a-zA-Z ']*)$/;
      var value = $.trim(value);
      if (value.indexOf(" ") !== -1) {
        var arr = value.split(' ');
        if (arr[0].length > 15)
          return false;
      } else if (value.length > 15) {
        return false;
      } else if (!reg.test(value)) {
        return false;
      }
      return true;
    }, "Nome inválido");
  },
  validaRede: function() {
    var _that = this;
    $.validator.addMethod("validaRede", function(value, element) {
      _that.$redeout.html("");
      var str = $.trim(value);
      var empty = str.length < 1;
      if (empty) {
        return true;
      }
      var m = _that.matchRede();
      var valid = (m != null);
      if (valid) {
        var out = "http://rede.natura.net/espaco/" + m + "/";
        _that.$redeout.html(out);
      }
      return valid;
    }, "Rede inválida");
  }
};
window.MaterialDivugacao = MaterialDivugacao;
var $__default = MaterialDivugacao;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/material-divulgacao.js
},{"./modal":5,"./salvar-cn.js":6,"./utils/cookie":8,"./utils/google-api-url":10,"./utils/preload":11,"./utils/scroll-anchor":12,"./utils/utils":13}],5:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__utils_47_cookie__,
    $__utils_47_utils__;
var Cookie = ($__utils_47_cookie__ = require("./utils/cookie"), $__utils_47_cookie__ && $__utils_47_cookie__.__esModule && $__utils_47_cookie__ || {default: $__utils_47_cookie__}).default;
var Utils = ($__utils_47_utils__ = require("./utils/utils"), $__utils_47_utils__ && $__utils_47_utils__.__esModule && $__utils_47_utils__ || {default: $__utils_47_utils__}).default;
var Modal = {
  open: function(opts) {
    var _that = this;
    var callback = opts.callback || function() {};
    var init = opts.init || function() {};
    this.btnClose();
    this.autoClose = opts.autoClose || null;
    this.$el = $(opts.el);
    this.$modal = $('#scModal');
    this.$body = $('body');
    this.$wrap = $('.sc-md-wrapper');
    if (this.$modal.hasClass('sc-none')) {
      this.$modal.removeClass('sc-none').addClass('actived');
      this.$body.addClass('sc-ovf-hidden');
    }
    this.clearItems(!this.$modal.hasClass('sc-none'));
    setTimeout(function() {
      _that.$el.addClass('actived');
      _that.$wrap.css("margin-top", '1250px');
      init(_that.$el);
      setTimeout(function() {
        _that.$wrap.addClass('actived');
        if (_that.autoClose) {
          _that.timeAutoClose = setTimeout(function() {
            _that.close(function() {
              callback();
            });
          }, _that.autoClose);
        } else {
          callback(false);
        }
      }, 300);
    }, 100);
  },
  close: function(callback) {
    var _that = this;
    var callback = callback || function() {};
    this.$modal.removeClass('actived');
    this.$body.removeClass('sc-ovf-hidden');
    setTimeout(function() {
      _that.$modal.addClass('sc-none');
      _that.clearItems();
      $('.sc-list .sc-item .sc-btn').removeClass('loading');
      var cookie = Cookie.get('natura_natal');
      if (!!cookie) {
        if (typeof cookie.whats_number !== "undefined") {
          $('#formNumWp').val(Utils.formatPhoneNumber(cookie.whats_number));
        }
      }
      _that.$modal.removeClass('loading');
      callback();
    }, 300);
  },
  clearItems: function(isOpen) {
    $('.sc-md-container').removeClass('actived');
    if (!isOpen) {
      this.$wrap.removeClass('actived');
    }
  },
  btnClose: function() {
    var _that = this;
    $(document).on('click', '.sc-md-close', function() {
      if (_that.timeAutoClose)
        clearTimeout(_that.timeAutoClose);
      _that.close();
    });
  }
};
var $__default = Modal;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/modal.js
},{"./utils/cookie":8,"./utils/utils":13}],6:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var SalvarCn = {saveData: saveData};
function saveData(data) {
  return $.ajax({
    url: PersistData.serviceUrlSaveCn,
    type: 'POST',
    data: data,
    xhrFields: {withCredentials: true}
  });
}
var $__default = SalvarCn;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/salvar-cn.js
},{}],7:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var base64 = {};
base64.PADCHAR = '=';
base64.ALPHA = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
base64.makeDOMException = function() {
  var e,
      tmp;
  try {
    return new DOMException(DOMException.INVALID_CHARACTER_ERR);
  } catch (tmp) {
    var ex = new Error("DOM Exception 5");
    ex.code = ex.number = 5;
    ex.name = ex.description = "INVALID_CHARACTER_ERR";
    ex.toString = function() {
      return 'Error: ' + ex.name + ': ' + ex.message;
    };
    return ex;
  }
};
base64.getbyte64 = function(s, i) {
  var idx = base64.ALPHA.indexOf(s.charAt(i));
  if (idx === -1) {
    throw base64.makeDOMException();
  }
  return idx;
};
base64.decode = function(s) {
  s = '' + s;
  var getbyte64 = base64.getbyte64;
  var pads,
      i,
      b10;
  var imax = s.length;
  if (imax === 0) {
    return s;
  }
  if (imax % 4 !== 0) {
    throw base64.makeDOMException();
  }
  pads = 0;
  if (s.charAt(imax - 1) === base64.PADCHAR) {
    pads = 1;
    if (s.charAt(imax - 2) === base64.PADCHAR) {
      pads = 2;
    }
    imax -= 4;
  }
  var x = [];
  for (i = 0; i < imax; i += 4) {
    b10 = (getbyte64(s, i) << 18) | (getbyte64(s, i + 1) << 12) | (getbyte64(s, i + 2) << 6) | getbyte64(s, i + 3);
    x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 0xff, b10 & 0xff));
  }
  switch (pads) {
    case 1:
      b10 = (getbyte64(s, i) << 18) | (getbyte64(s, i + 1) << 12) | (getbyte64(s, i + 2) << 6);
      x.push(String.fromCharCode(b10 >> 16, (b10 >> 8) & 0xff));
      break;
    case 2:
      b10 = (getbyte64(s, i) << 18) | (getbyte64(s, i + 1) << 12);
      x.push(String.fromCharCode(b10 >> 16));
      break;
  }
  return x.join('');
};
base64.getbyte = function(s, i) {
  var x = s.charCodeAt(i);
  if (x > 255) {
    throw base64.makeDOMException();
  }
  return x;
};
base64.encode = function(s) {
  if (arguments.length !== 1) {
    throw new SyntaxError("Not enough arguments");
  }
  var padchar = base64.PADCHAR;
  var alpha = base64.ALPHA;
  var getbyte = base64.getbyte;
  var i,
      b10;
  var x = [];
  s = '' + s;
  var imax = s.length - s.length % 3;
  if (s.length === 0) {
    return s;
  }
  for (i = 0; i < imax; i += 3) {
    b10 = (getbyte(s, i) << 16) | (getbyte(s, i + 1) << 8) | getbyte(s, i + 2);
    x.push(alpha.charAt(b10 >> 18));
    x.push(alpha.charAt((b10 >> 12) & 0x3F));
    x.push(alpha.charAt((b10 >> 6) & 0x3f));
    x.push(alpha.charAt(b10 & 0x3f));
  }
  switch (s.length - imax) {
    case 1:
      b10 = getbyte(s, i) << 16;
      x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) + padchar + padchar);
      break;
    case 2:
      b10 = (getbyte(s, i) << 16) | (getbyte(s, i + 1) << 8);
      x.push(alpha.charAt(b10 >> 18) + alpha.charAt((b10 >> 12) & 0x3F) + alpha.charAt((b10 >> 6) & 0x3f) + padchar);
      break;
  }
  return x.join('');
};
var $__default = base64;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/utils/base64.js
},{}],8:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Cookie = {
  set: function(name, data) {
    var _cookieValue = name + "=" + JSON.stringify(data) + "; path=/";
    document.cookie = _cookieValue;
  },
  get: function(name) {
    var cookie = document.cookie;
    if (!cookie.length)
      return false;
    var cookies = cookie.split('; '),
        _return = null;
    $.each(cookies, function() {
      var data = this.split('=');
      if (name == data[0]) {
        _return = data[1];
        return null;
      }
    });
    return JSON.parse(_return);
  },
  addItem: function(name, items) {
    if (this.get(name)) {
      var cookie = this.get(name);
      $.each(items, function(k, v) {
        cookie[k] = v;
      });
      this.set(name, cookie);
    } else {
      this.set(name, items);
    }
  },
  clear: function(name) {
    document.cookie = name + '=; Path=/;';
  }
};
var $__default = Cookie;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/utils/cookie.js
},{}],9:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Facebook = {
  albumId: null,
  init: function() {
    var _that = this;
    this.loadtag();
    window.fbAsyncInit = function() {
      _that.fbAsyncInit();
    };
  },
  loadtag: function() {
    if (typeof FB === "undefined") {
      (function(d, s, id) {
        var js,
            fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) {
          return;
        }
        js = d.createElement(s);
        js.id = id;
        js.src = "//connect.facebook.net/pt_BR/sdk.js";
        fjs.parentNode.insertBefore(js, fjs);
      }(document, 'script', 'facebook-jssdk'));
    }
  },
  fbAsyncInit: function() {
    FB.init({
      appId: PersistData.fb_id,
      status: true,
      xfbml: true,
      version: 'v2.4'
    });
  },
  me: function(data) {
    var _that = this;
    var callback = data.callback || function() {};
    var fail = data.fail || function() {};
    this.redir = data.redir;
    FB.getLoginStatus(function(response) {
      if (response.status === 'connected') {
        _that.verificarPermissao(function() {
          _that.postImage(data, _that.token, callback);
        });
      } else {
        FB.login(function(response) {
          if (response.authResponse) {
            _that.token = response.authResponse.accessToken;
            FB.api("/me", function(response) {
              if (response && !response.error) {
                if (typeof response.link !== "undefined")
                  _that.faceProfileUrl = response.link;
                _that.verificarPermissao(function() {
                  _that.postImage(data, _that.token, callback);
                }, fail);
              } else {
                fail(response);
                alert('Você precisa aceitar o aplicativo para compartilhar');
              }
            });
          } else {
            fail();
          }
        }, {scope: 'user_photos, publish_actions'});
      }
    });
  },
  verificarPermissao: function(callback, fail) {
    FB.api('/me/permissions', function(response) {
      if (!response || response.error) {
        fail(response);
        alert('Houve um erro ao compartilhar, tente novamente.');
      } else {
        if (response.data.length) {
          var hasDeclined = false;
          $.each(response.data, function(i) {
            if (this.status == 'declined') {
              FB.login(function(response) {
                callback();
              }, {
                scope: this.permission,
                auth_type: 'rerequest'
              });
              hasDeclined = true;
            }
          });
          if (!hasDeclined) {
            callback();
          } else {
            fail();
          }
        }
      }
    });
  },
  postImage: function(data, token, callback, fail) {
    var _that = this;
    var callback = callback || function() {};
    fail = fail || function() {};
    this.getAlbumId(data, success);
    function success(albumId) {
      FB.api('/' + _that.albumId + '/photos', 'post', {
        access_token: token,
        url: data.picture
      }, faceSuccess);
      function faceSuccess(response) {
        var action = 'post';
        if (!response || response.error) {
          fail(response);
          alert('Houve um erro ao compartilhar, tente novamente.');
        } else {
          if (_that.redir) {
            _that.getFaceProfileUrl(function(action) {
              if (action) {
                response.fb_url = _that.faceProfileUrl;
                callback(response);
              } else {
                callback(response);
              }
            });
            callback(response);
          } else {
            callback(response);
          }
        }
      }
    }
  },
  getFaceProfileUrl: function(callback) {
    var _that = this;
    var callback = callback || function() {};
    FB.api("/me?fields=link", function(response) {
      if (response && !response.error) {
        if (typeof response.link !== "undefined")
          _that.faceProfileUrl = response.link;
        callback(true);
      } else {
        callback(false);
      }
    });
  },
  getAlbumId: function(data, callback) {
    var _that = this;
    var callback = callback || function() {};
    FB.api('/me/albums', function(response) {
      if (!response || response.error) {
        alert('Houve um erro ao compartilhar, tente novamente.');
      } else {
        for (var i = 0; i < response.data.length; i++) {
          if (response.data[i].name == PersistData.fbAlbumName) {
            _that.albumId = response.data[i].id;
          }
        }
        if (!_that.albumId) {
          return _that.criarAlbum(data, callback);
        } else {
          return callback(_that.albumId);
        }
      }
    });
  },
  criarAlbum: function(data, callback) {
    FB.api('/me/albums', 'post', {name: PersistData.fbAlbumName}, function(response) {
      if (!response || response.error) {
        if (response.error.code == 10) {
          alert('Voce deve aceitar a permissão de fotos para conseguir compartilhar!');
        } else {
          alert('Houve um erro ao compartilhar, tente novamente.');
        }
      } else {
        return callback(response.id);
      }
    });
  },
  share: function(url, postId, that, father) {
    console.warn('Facebook: share');
    console.log(url);
    FB.ui({
      method: 'share',
      app_id: PersistData.fb_id,
      href: url.toString().replace('image', 'www/share'),
      picture: url,
      success: function(response) {
        Modal.open({
          el: '#formShareSuccess',
          autoClose: 2500,
          callback: function() {}
        });
        trackAnalytics(false, 'facebook', 'compartilhe_sucesso-' + postId);
        that.clearLoadingShare();
        if (father.data('cover')) {
          if (typeof response.id !== "undefined" && typeof response.fb_url !== "undefined") {
            window.open(response.fb_url + "?preview_cover=" + response.id);
          }
        }
      },
      fail: function() {
        trackAnalytics(false, 'facebook', 'compartilhe_erro-' + postId);
        that.clearLoadingShare();
      }
    });
  }
};
var $__default = Facebook;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/utils/facebook.js
},{}],10:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var GoogleApiUrl = {
  getShordUrl: function(params) {
    var success = params.success || function() {},
        error = params.error || function() {},
        _data = JSON.stringify({"longUrl": params.url});
    $.ajax({
      url: this.getUrlService(),
      method: 'POST',
      contentType: 'application/json',
      processData: false,
      data: _data,
      success: function(data, textStatus, jqXHR) {
        if (!!data) {
          success(data.id);
        }
      },
      error: function(jqXHR, textStatus, errorThrown) {
        if (window.console)
          console.log('error', errorThrown, jqXHR);
        error(jqXHR.responseText);
      }
    });
  },
  getUrlService: function(url) {
    return "https://www.googleapis.com/urlshortener/v1/url?key=" + PersistData.googleApiKey;
  }
};
var $__default = GoogleApiUrl;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/utils/google-api-url.js
},{}],11:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Preload = function(images, callback) {
  this.callback = callback || function() {};
  this.map = [];
  this.path = PersistData.absUrl + "/img";
  this.setImages(images);
  this.reload();
  this.loadAll();
};
Preload.prototype.setImages = function(images) {
  var _that = this;
  $.each(images, function() {
    if (!_that.imgs)
      _that.imgs = [];
    _that.imgs.push(this);
  });
};
Preload.prototype.reload = function() {
  var _that = this;
  if (this.preload) {
    this.preload.close();
  }
  this.manifest = this.imgs;
  this.preload = new createjs.LoadQueue(true);
  var handleFileLoad = function(event) {
    var img = event.result;
    _that.map[_that.map.length] = {
      src: String(String(event.src).toLowerCase()).replace(this.path, '').replace('.jpg', '').replace('.png', ''),
      data: img
    };
    if (_that.map.length < _that.imgs.length) {} else {}
  };
  function handleFileProgress(event) {}
  function handleOveralProgress(event) {
    var perc = _that.preload.progress * 100;
  }
  function handleFileError(event) {}
  function handleFilesComplete() {
    _that.stop();
    _that.callback();
  }
  this.preload.on("fileload", handleFileLoad, this);
  this.preload.on("progress", handleOveralProgress, this);
  this.preload.on("fileprogress", handleFileProgress, this);
  this.preload.on("complete", handleFilesComplete, this);
};
Preload.prototype.loadAll = function() {
  var _that = this;
  while (this.manifest.length > 0) {
    loadAnother();
  }
  function loadAnother() {
    var item = _that.manifest.shift();
    _that.preload.loadFile(item);
  }
  ;
};
Preload.prototype.stop = function() {
  if (this.preload) {
    this.preload.close();
  }
};
Preload.prototype.setImageTag = function(el, filename, path) {
  path = path || this.path;
  el.attr('src', path + filename);
};
var $__default = Preload;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/utils/preload.js
},{}],12:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var ScrollAnchor = {
  speed: 500,
  init: function() {
    this.bind();
  },
  bind: function() {
    var _that = this;
    $(document).on('click', '[data-anchor-go]', function() {
      _that.go($(this).data('anchor-go'));
    });
  },
  go: function(target, callback) {
    var $el = $(target),
        callback = callback || function() {};
    if (!$el.length)
      return false;
    var y = $el.offset().top;
    $('html, body').animate({scrollTop: y}, this.speed, function() {
      callback();
    });
  }
};
var $__default = ScrollAnchor;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/utils/scroll-anchor.js
},{}],13:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__base64__,
    $__cookie__;
var Base64 = ($__base64__ = require("./base64"), $__base64__ && $__base64__.__esModule && $__base64__ || {default: $__base64__}).default;
var Cookie = ($__cookie__ = require("./cookie"), $__cookie__ && $__cookie__.__esModule && $__cookie__ || {default: $__cookie__}).default;
var Utils = {
  getHash: function(data) {
    var get_params = $.param(data),
        base64_params = Base64.encode(get_params);
    return base64_params;
  },
  getCookieData: function() {
    var cookie = Cookie.get('natura_dia_das_maes');
    if (!cookie)
      return {};
    if (typeof cookie.rede !== "undefined")
      cookie.rede = decodeURIComponent(cookie.rede);
    if (typeof cookie.telefone !== "undefined")
      cookie.telefone = cookie.telefone.replace("+", " ");
    return cookie;
  },
  getURLData: function() {
    var tmp = 'nome=' + localStorage.getItem('nome');
    tmp += '&telefone=' + localStorage.getItem('telefone');
    tmp += '&rede=' + decodeURIComponent(localStorage.getItem('rede'));
    tmp += '&goo=' + localStorage.getItem('goo');
    return Utils.getHash(tmp);
  },
  getImagePath: function(post, data) {
    var data = data || null,
        params = {};
    params["post"] = post;
    if (data) {
      params["data"] = Utils.getHash(data);
      params["goo"] = data.goo;
    }
    return PersistData.serviceUrl + '?' + $.param(params);
  },
  getTemplateTrack: function(data) {
    var rede = (data.rede != "") ? PersistData.siteCFUrl + data.rede : null;
    var nome = (typeof data.nome !== "undefined") ? decodeURIComponent(data.nome.replace(/\+/g, ' ')) : null;
    var telefone = (typeof data.telefone !== "undefined") ? data.telefone.replace(/\+/g, ' ') : null;
    var hideClass = (rede && rede.length) ? '' : 'sc-none';
    var firstName = null;
    if (nome) {
      firstName = nome.split(' ')[0];
    }
    var _html = '<div class="sc-item-lbl">';
    _html += ' <p class="sc-item-lbl-title">';
    _html += (nome) ? '    Compre com <span class="sc-item-person">' + firstName + '</span>' : '';
    _html += ' </p>';
    if (telefone && telefone != '') {
      telefone = this.clearPhoneNumber(telefone);
      telefone = "55" + telefone;
      _html += ' <p class="sc-item-lbl-contact">';
      _html += '   Ligue para ';
      _html += '   <span class="sc-item-phone">';
      _html += this.formatPhoneNumber(telefone);
      _html += '   </span>';
      _html += ' </p>';
    }
    if (rede) {
      _html += ' <p class="sc-item-lbl-contact">';
      _html += '   <span class="sc-item-url-wrap ' + hideClass + '">';
      _html += (telefone) ? 'Ou acesse ' : 'Acesse ';
      _html += '     <span class="sc-item-url">';
      _html += decodeURIComponent(rede);
      _html += '     </span>';
      _html += '   </span>';
      _html += ' </p>';
    }
    _html += '</div>';
    if (nome) {
      return _html;
    } else {
      return null;
    }
  },
  getParamsObj: function(serializedString) {
    var str = decodeURI(serializedString);
    var pairs = str.split('&');
    var obj = {},
        p,
        idx,
        val;
    for (var i = 0,
        n = pairs.length; i < n; i++) {
      p = pairs[i].split('=');
      idx = p[0];
      if (idx.indexOf("[]") == (idx.length - 2)) {
        var ind = idx.substring(0, idx.length - 2);
        if (obj[ind] === undefined) {
          obj[ind] = [];
        }
        obj[ind].push(p[1]);
      } else {
        obj[idx] = p[1];
      }
    }
    return obj;
  },
  clearPhoneNumber: function(number) {
    return number.replace(/\s/g, '').replace(/\+/g, '').replace('-', '').replace('(', '').replace(')', '');
  },
  formatPhoneNumber: function(number) {
    if (number.length > 12)
      return $.trim(number.replace(/(\d{2})(\d{2})(\d{5})(\d{4})/, "($2) $3-$4"));
    else
      return $.trim(number.replace(/(\d{2})(\d{2})(\d{4})(\d{4})/, "($2) $3-$4"));
  }
};
var $__default = Utils;


//# sourceURL=D:/git/natura-dia-das-maes/frontend/app/js/utils/utils.js
},{"./base64":7,"./cookie":8}],14:[function(require,module,exports){
(function (process){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length - 1; i >= 0; i--) {
    var last = parts[i];
    if (last === '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Split a filename into [root, dir, basename, ext], unix version
// 'root' is just a slash, or nothing.
var splitPathRe =
    /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
var splitPath = function(filename) {
  return splitPathRe.exec(filename).slice(1);
};

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
  var resolvedPath = '',
      resolvedAbsolute = false;

  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
    var path = (i >= 0) ? arguments[i] : process.cwd();

    // Skip empty and invalid entries
    if (typeof path !== 'string') {
      throw new TypeError('Arguments to path.resolve must be strings');
    } else if (!path) {
      continue;
    }

    resolvedPath = path + '/' + resolvedPath;
    resolvedAbsolute = path.charAt(0) === '/';
  }

  // At this point the path should be resolved to a full absolute path, but
  // handle relative paths to be safe (might happen when process.cwd() fails)

  // Normalize the path
  resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
  var isAbsolute = exports.isAbsolute(path),
      trailingSlash = substr(path, -1) === '/';

  // Normalize the path
  path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }

  return (isAbsolute ? '/' : '') + path;
};

// posix version
exports.isAbsolute = function(path) {
  return path.charAt(0) === '/';
};

// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    if (typeof p !== 'string') {
      throw new TypeError('Arguments to path.join must be strings');
    }
    return p;
  }).join('/'));
};


// path.relative(from, to)
// posix version
exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

exports.sep = '/';
exports.delimiter = ':';

exports.dirname = function(path) {
  var result = splitPath(path),
      root = result[0],
      dir = result[1];

  if (!root && !dir) {
    // No dirname whatsoever
    return '.';
  }

  if (dir) {
    // It has a dirname, strip trailing slash
    dir = dir.substr(0, dir.length - 1);
  }

  return root + dir;
};


exports.basename = function(path, ext) {
  var f = splitPath(path)[2];
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPath(path)[3];
};

function filter (xs, f) {
    if (xs.filter) return xs.filter(f);
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// String.prototype.substr - negative index don't work in IE8
var substr = 'ab'.substr(-1) === 'b'
    ? function (str, start, len) { return str.substr(start, len) }
    : function (str, start, len) {
        if (start < 0) start = str.length + start;
        return str.substr(start, len);
    }
;

}).call(this,require('_process'))
},{"_process":15}],15:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;

function drainQueue() {
    if (draining) {
        return;
    }
    draining = true;
    var currentQueue;
    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        var i = -1;
        while (++i < len) {
            currentQueue[i]();
        }
        len = queue.length;
    }
    draining = false;
}
process.nextTick = function (fun) {
    queue.push(fun);
    if (!draining) {
        setTimeout(drainQueue, 0);
    }
};

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],16:[function(require,module,exports){
(function (process,global){
(function(global) {
  'use strict';
  if (global.$traceurRuntime) {
    return;
  }
  var $Object = Object;
  var $TypeError = TypeError;
  var $create = $Object.create;
  var $defineProperties = $Object.defineProperties;
  var $defineProperty = $Object.defineProperty;
  var $freeze = $Object.freeze;
  var $getOwnPropertyDescriptor = $Object.getOwnPropertyDescriptor;
  var $getOwnPropertyNames = $Object.getOwnPropertyNames;
  var $keys = $Object.keys;
  var $hasOwnProperty = $Object.prototype.hasOwnProperty;
  var $toString = $Object.prototype.toString;
  var $preventExtensions = Object.preventExtensions;
  var $seal = Object.seal;
  var $isExtensible = Object.isExtensible;
  function nonEnum(value) {
    return {
      configurable: true,
      enumerable: false,
      value: value,
      writable: true
    };
  }
  var method = nonEnum;
  var counter = 0;
  function newUniqueString() {
    return '__$' + Math.floor(Math.random() * 1e9) + '$' + ++counter + '$__';
  }
  var symbolInternalProperty = newUniqueString();
  var symbolDescriptionProperty = newUniqueString();
  var symbolDataProperty = newUniqueString();
  var symbolValues = $create(null);
  var privateNames = $create(null);
  function isPrivateName(s) {
    return privateNames[s];
  }
  function createPrivateName() {
    var s = newUniqueString();
    privateNames[s] = true;
    return s;
  }
  function isShimSymbol(symbol) {
    return typeof symbol === 'object' && symbol instanceof SymbolValue;
  }
  function typeOf(v) {
    if (isShimSymbol(v))
      return 'symbol';
    return typeof v;
  }
  function Symbol(description) {
    var value = new SymbolValue(description);
    if (!(this instanceof Symbol))
      return value;
    throw new TypeError('Symbol cannot be new\'ed');
  }
  $defineProperty(Symbol.prototype, 'constructor', nonEnum(Symbol));
  $defineProperty(Symbol.prototype, 'toString', method(function() {
    var symbolValue = this[symbolDataProperty];
    if (!getOption('symbols'))
      return symbolValue[symbolInternalProperty];
    if (!symbolValue)
      throw TypeError('Conversion from symbol to string');
    var desc = symbolValue[symbolDescriptionProperty];
    if (desc === undefined)
      desc = '';
    return 'Symbol(' + desc + ')';
  }));
  $defineProperty(Symbol.prototype, 'valueOf', method(function() {
    var symbolValue = this[symbolDataProperty];
    if (!symbolValue)
      throw TypeError('Conversion from symbol to string');
    if (!getOption('symbols'))
      return symbolValue[symbolInternalProperty];
    return symbolValue;
  }));
  function SymbolValue(description) {
    var key = newUniqueString();
    $defineProperty(this, symbolDataProperty, {value: this});
    $defineProperty(this, symbolInternalProperty, {value: key});
    $defineProperty(this, symbolDescriptionProperty, {value: description});
    freeze(this);
    symbolValues[key] = this;
  }
  $defineProperty(SymbolValue.prototype, 'constructor', nonEnum(Symbol));
  $defineProperty(SymbolValue.prototype, 'toString', {
    value: Symbol.prototype.toString,
    enumerable: false
  });
  $defineProperty(SymbolValue.prototype, 'valueOf', {
    value: Symbol.prototype.valueOf,
    enumerable: false
  });
  var hashProperty = createPrivateName();
  var hashPropertyDescriptor = {value: undefined};
  var hashObjectProperties = {
    hash: {value: undefined},
    self: {value: undefined}
  };
  var hashCounter = 0;
  function getOwnHashObject(object) {
    var hashObject = object[hashProperty];
    if (hashObject && hashObject.self === object)
      return hashObject;
    if ($isExtensible(object)) {
      hashObjectProperties.hash.value = hashCounter++;
      hashObjectProperties.self.value = object;
      hashPropertyDescriptor.value = $create(null, hashObjectProperties);
      $defineProperty(object, hashProperty, hashPropertyDescriptor);
      return hashPropertyDescriptor.value;
    }
    return undefined;
  }
  function freeze(object) {
    getOwnHashObject(object);
    return $freeze.apply(this, arguments);
  }
  function preventExtensions(object) {
    getOwnHashObject(object);
    return $preventExtensions.apply(this, arguments);
  }
  function seal(object) {
    getOwnHashObject(object);
    return $seal.apply(this, arguments);
  }
  freeze(SymbolValue.prototype);
  function isSymbolString(s) {
    return symbolValues[s] || privateNames[s];
  }
  function toProperty(name) {
    if (isShimSymbol(name))
      return name[symbolInternalProperty];
    return name;
  }
  function removeSymbolKeys(array) {
    var rv = [];
    for (var i = 0; i < array.length; i++) {
      if (!isSymbolString(array[i])) {
        rv.push(array[i]);
      }
    }
    return rv;
  }
  function getOwnPropertyNames(object) {
    return removeSymbolKeys($getOwnPropertyNames(object));
  }
  function keys(object) {
    return removeSymbolKeys($keys(object));
  }
  function getOwnPropertySymbols(object) {
    var rv = [];
    var names = $getOwnPropertyNames(object);
    for (var i = 0; i < names.length; i++) {
      var symbol = symbolValues[names[i]];
      if (symbol) {
        rv.push(symbol);
      }
    }
    return rv;
  }
  function getOwnPropertyDescriptor(object, name) {
    return $getOwnPropertyDescriptor(object, toProperty(name));
  }
  function hasOwnProperty(name) {
    return $hasOwnProperty.call(this, toProperty(name));
  }
  function getOption(name) {
    return global.traceur && global.traceur.options[name];
  }
  function defineProperty(object, name, descriptor) {
    if (isShimSymbol(name)) {
      name = name[symbolInternalProperty];
    }
    $defineProperty(object, name, descriptor);
    return object;
  }
  function polyfillObject(Object) {
    $defineProperty(Object, 'defineProperty', {value: defineProperty});
    $defineProperty(Object, 'getOwnPropertyNames', {value: getOwnPropertyNames});
    $defineProperty(Object, 'getOwnPropertyDescriptor', {value: getOwnPropertyDescriptor});
    $defineProperty(Object.prototype, 'hasOwnProperty', {value: hasOwnProperty});
    $defineProperty(Object, 'freeze', {value: freeze});
    $defineProperty(Object, 'preventExtensions', {value: preventExtensions});
    $defineProperty(Object, 'seal', {value: seal});
    $defineProperty(Object, 'keys', {value: keys});
  }
  function exportStar(object) {
    for (var i = 1; i < arguments.length; i++) {
      var names = $getOwnPropertyNames(arguments[i]);
      for (var j = 0; j < names.length; j++) {
        var name = names[j];
        if (isSymbolString(name))
          continue;
        (function(mod, name) {
          $defineProperty(object, name, {
            get: function() {
              return mod[name];
            },
            enumerable: true
          });
        })(arguments[i], names[j]);
      }
    }
    return object;
  }
  function isObject(x) {
    return x != null && (typeof x === 'object' || typeof x === 'function');
  }
  function toObject(x) {
    if (x == null)
      throw $TypeError();
    return $Object(x);
  }
  function checkObjectCoercible(argument) {
    if (argument == null) {
      throw new TypeError('Value cannot be converted to an Object');
    }
    return argument;
  }
  function polyfillSymbol(global, Symbol) {
    if (!global.Symbol) {
      global.Symbol = Symbol;
      Object.getOwnPropertySymbols = getOwnPropertySymbols;
    }
    if (!global.Symbol.iterator) {
      global.Symbol.iterator = Symbol('Symbol.iterator');
    }
  }
  function setupGlobals(global) {
    polyfillSymbol(global, Symbol);
    global.Reflect = global.Reflect || {};
    global.Reflect.global = global.Reflect.global || global;
    polyfillObject(global.Object);
  }
  setupGlobals(global);
  global.$traceurRuntime = {
    checkObjectCoercible: checkObjectCoercible,
    createPrivateName: createPrivateName,
    defineProperties: $defineProperties,
    defineProperty: $defineProperty,
    exportStar: exportStar,
    getOwnHashObject: getOwnHashObject,
    getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
    getOwnPropertyNames: $getOwnPropertyNames,
    isObject: isObject,
    isPrivateName: isPrivateName,
    isSymbolString: isSymbolString,
    keys: $keys,
    setupGlobals: setupGlobals,
    toObject: toObject,
    toProperty: toProperty,
    typeof: typeOf
  };
})(typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : this);
(function() {
  'use strict';
  var path;
  function relativeRequire(callerPath, requiredPath) {
    path = path || typeof require !== 'undefined' && require('path');
    function isDirectory(path) {
      return path.slice(-1) === '/';
    }
    function isAbsolute(path) {
      return path[0] === '/';
    }
    function isRelative(path) {
      return path[0] === '.';
    }
    if (isDirectory(requiredPath) || isAbsolute(requiredPath))
      return;
    return isRelative(requiredPath) ? require(path.resolve(path.dirname(callerPath), requiredPath)) : require(requiredPath);
  }
  $traceurRuntime.require = relativeRequire;
})();
(function() {
  'use strict';
  function spread() {
    var rv = [],
        j = 0,
        iterResult;
    for (var i = 0; i < arguments.length; i++) {
      var valueToSpread = $traceurRuntime.checkObjectCoercible(arguments[i]);
      if (typeof valueToSpread[$traceurRuntime.toProperty(Symbol.iterator)] !== 'function') {
        throw new TypeError('Cannot spread non-iterable object.');
      }
      var iter = valueToSpread[$traceurRuntime.toProperty(Symbol.iterator)]();
      while (!(iterResult = iter.next()).done) {
        rv[j++] = iterResult.value;
      }
    }
    return rv;
  }
  $traceurRuntime.spread = spread;
})();
(function() {
  'use strict';
  var $Object = Object;
  var $TypeError = TypeError;
  var $create = $Object.create;
  var $defineProperties = $traceurRuntime.defineProperties;
  var $defineProperty = $traceurRuntime.defineProperty;
  var $getOwnPropertyDescriptor = $traceurRuntime.getOwnPropertyDescriptor;
  var $getOwnPropertyNames = $traceurRuntime.getOwnPropertyNames;
  var $getPrototypeOf = Object.getPrototypeOf;
  var $__0 = Object,
      getOwnPropertyNames = $__0.getOwnPropertyNames,
      getOwnPropertySymbols = $__0.getOwnPropertySymbols;
  function superDescriptor(homeObject, name) {
    var proto = $getPrototypeOf(homeObject);
    do {
      var result = $getOwnPropertyDescriptor(proto, name);
      if (result)
        return result;
      proto = $getPrototypeOf(proto);
    } while (proto);
    return undefined;
  }
  function superConstructor(ctor) {
    return ctor.__proto__;
  }
  function superCall(self, homeObject, name, args) {
    return superGet(self, homeObject, name).apply(self, args);
  }
  function superGet(self, homeObject, name) {
    var descriptor = superDescriptor(homeObject, name);
    if (descriptor) {
      if (!descriptor.get)
        return descriptor.value;
      return descriptor.get.call(self);
    }
    return undefined;
  }
  function superSet(self, homeObject, name, value) {
    var descriptor = superDescriptor(homeObject, name);
    if (descriptor && descriptor.set) {
      descriptor.set.call(self, value);
      return value;
    }
    throw $TypeError(("super has no setter '" + name + "'."));
  }
  function getDescriptors(object) {
    var descriptors = {};
    var names = getOwnPropertyNames(object);
    for (var i = 0; i < names.length; i++) {
      var name = names[i];
      descriptors[name] = $getOwnPropertyDescriptor(object, name);
    }
    var symbols = getOwnPropertySymbols(object);
    for (var i = 0; i < symbols.length; i++) {
      var symbol = symbols[i];
      descriptors[$traceurRuntime.toProperty(symbol)] = $getOwnPropertyDescriptor(object, $traceurRuntime.toProperty(symbol));
    }
    return descriptors;
  }
  function createClass(ctor, object, staticObject, superClass) {
    $defineProperty(object, 'constructor', {
      value: ctor,
      configurable: true,
      enumerable: false,
      writable: true
    });
    if (arguments.length > 3) {
      if (typeof superClass === 'function')
        ctor.__proto__ = superClass;
      ctor.prototype = $create(getProtoParent(superClass), getDescriptors(object));
    } else {
      ctor.prototype = object;
    }
    $defineProperty(ctor, 'prototype', {
      configurable: false,
      writable: false
    });
    return $defineProperties(ctor, getDescriptors(staticObject));
  }
  function getProtoParent(superClass) {
    if (typeof superClass === 'function') {
      var prototype = superClass.prototype;
      if ($Object(prototype) === prototype || prototype === null)
        return superClass.prototype;
      throw new $TypeError('super prototype must be an Object or null');
    }
    if (superClass === null)
      return null;
    throw new $TypeError(("Super expression must either be null or a function, not " + typeof superClass + "."));
  }
  function defaultSuperCall(self, homeObject, args) {
    if ($getPrototypeOf(homeObject) !== null)
      superCall(self, homeObject, 'constructor', args);
  }
  $traceurRuntime.createClass = createClass;
  $traceurRuntime.defaultSuperCall = defaultSuperCall;
  $traceurRuntime.superCall = superCall;
  $traceurRuntime.superConstructor = superConstructor;
  $traceurRuntime.superGet = superGet;
  $traceurRuntime.superSet = superSet;
})();
(function() {
  'use strict';
  if (typeof $traceurRuntime !== 'object') {
    throw new Error('traceur runtime not found.');
  }
  var createPrivateName = $traceurRuntime.createPrivateName;
  var $defineProperties = $traceurRuntime.defineProperties;
  var $defineProperty = $traceurRuntime.defineProperty;
  var $create = Object.create;
  var $TypeError = TypeError;
  function nonEnum(value) {
    return {
      configurable: true,
      enumerable: false,
      value: value,
      writable: true
    };
  }
  var ST_NEWBORN = 0;
  var ST_EXECUTING = 1;
  var ST_SUSPENDED = 2;
  var ST_CLOSED = 3;
  var END_STATE = -2;
  var RETHROW_STATE = -3;
  function getInternalError(state) {
    return new Error('Traceur compiler bug: invalid state in state machine: ' + state);
  }
  function GeneratorContext() {
    this.state = 0;
    this.GState = ST_NEWBORN;
    this.storedException = undefined;
    this.finallyFallThrough = undefined;
    this.sent_ = undefined;
    this.returnValue = undefined;
    this.tryStack_ = [];
  }
  GeneratorContext.prototype = {
    pushTry: function(catchState, finallyState) {
      if (finallyState !== null) {
        var finallyFallThrough = null;
        for (var i = this.tryStack_.length - 1; i >= 0; i--) {
          if (this.tryStack_[i].catch !== undefined) {
            finallyFallThrough = this.tryStack_[i].catch;
            break;
          }
        }
        if (finallyFallThrough === null)
          finallyFallThrough = RETHROW_STATE;
        this.tryStack_.push({
          finally: finallyState,
          finallyFallThrough: finallyFallThrough
        });
      }
      if (catchState !== null) {
        this.tryStack_.push({catch: catchState});
      }
    },
    popTry: function() {
      this.tryStack_.pop();
    },
    get sent() {
      this.maybeThrow();
      return this.sent_;
    },
    set sent(v) {
      this.sent_ = v;
    },
    get sentIgnoreThrow() {
      return this.sent_;
    },
    maybeThrow: function() {
      if (this.action === 'throw') {
        this.action = 'next';
        throw this.sent_;
      }
    },
    end: function() {
      switch (this.state) {
        case END_STATE:
          return this;
        case RETHROW_STATE:
          throw this.storedException;
        default:
          throw getInternalError(this.state);
      }
    },
    handleException: function(ex) {
      this.GState = ST_CLOSED;
      this.state = END_STATE;
      throw ex;
    }
  };
  function nextOrThrow(ctx, moveNext, action, x) {
    switch (ctx.GState) {
      case ST_EXECUTING:
        throw new Error(("\"" + action + "\" on executing generator"));
      case ST_CLOSED:
        if (action == 'next') {
          return {
            value: undefined,
            done: true
          };
        }
        throw x;
      case ST_NEWBORN:
        if (action === 'throw') {
          ctx.GState = ST_CLOSED;
          throw x;
        }
        if (x !== undefined)
          throw $TypeError('Sent value to newborn generator');
      case ST_SUSPENDED:
        ctx.GState = ST_EXECUTING;
        ctx.action = action;
        ctx.sent = x;
        var value = moveNext(ctx);
        var done = value === ctx;
        if (done)
          value = ctx.returnValue;
        ctx.GState = done ? ST_CLOSED : ST_SUSPENDED;
        return {
          value: value,
          done: done
        };
    }
  }
  var ctxName = createPrivateName();
  var moveNextName = createPrivateName();
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  $defineProperty(GeneratorFunctionPrototype, 'constructor', nonEnum(GeneratorFunction));
  GeneratorFunctionPrototype.prototype = {
    constructor: GeneratorFunctionPrototype,
    next: function(v) {
      return nextOrThrow(this[ctxName], this[moveNextName], 'next', v);
    },
    throw: function(v) {
      return nextOrThrow(this[ctxName], this[moveNextName], 'throw', v);
    }
  };
  $defineProperties(GeneratorFunctionPrototype.prototype, {
    constructor: {enumerable: false},
    next: {enumerable: false},
    throw: {enumerable: false}
  });
  Object.defineProperty(GeneratorFunctionPrototype.prototype, Symbol.iterator, nonEnum(function() {
    return this;
  }));
  function createGeneratorInstance(innerFunction, functionObject, self) {
    var moveNext = getMoveNext(innerFunction, self);
    var ctx = new GeneratorContext();
    var object = $create(functionObject.prototype);
    object[ctxName] = ctx;
    object[moveNextName] = moveNext;
    return object;
  }
  function initGeneratorFunction(functionObject) {
    functionObject.prototype = $create(GeneratorFunctionPrototype.prototype);
    functionObject.__proto__ = GeneratorFunctionPrototype;
    return functionObject;
  }
  function AsyncFunctionContext() {
    GeneratorContext.call(this);
    this.err = undefined;
    var ctx = this;
    ctx.result = new Promise(function(resolve, reject) {
      ctx.resolve = resolve;
      ctx.reject = reject;
    });
  }
  AsyncFunctionContext.prototype = $create(GeneratorContext.prototype);
  AsyncFunctionContext.prototype.end = function() {
    switch (this.state) {
      case END_STATE:
        this.resolve(this.returnValue);
        break;
      case RETHROW_STATE:
        this.reject(this.storedException);
        break;
      default:
        this.reject(getInternalError(this.state));
    }
  };
  AsyncFunctionContext.prototype.handleException = function() {
    this.state = RETHROW_STATE;
  };
  function asyncWrap(innerFunction, self) {
    var moveNext = getMoveNext(innerFunction, self);
    var ctx = new AsyncFunctionContext();
    ctx.createCallback = function(newState) {
      return function(value) {
        ctx.state = newState;
        ctx.value = value;
        moveNext(ctx);
      };
    };
    ctx.errback = function(err) {
      handleCatch(ctx, err);
      moveNext(ctx);
    };
    moveNext(ctx);
    return ctx.result;
  }
  function getMoveNext(innerFunction, self) {
    return function(ctx) {
      while (true) {
        try {
          return innerFunction.call(self, ctx);
        } catch (ex) {
          handleCatch(ctx, ex);
        }
      }
    };
  }
  function handleCatch(ctx, ex) {
    ctx.storedException = ex;
    var last = ctx.tryStack_[ctx.tryStack_.length - 1];
    if (!last) {
      ctx.handleException(ex);
      return;
    }
    ctx.state = last.catch !== undefined ? last.catch : last.finally;
    if (last.finallyFallThrough !== undefined)
      ctx.finallyFallThrough = last.finallyFallThrough;
  }
  $traceurRuntime.asyncWrap = asyncWrap;
  $traceurRuntime.initGeneratorFunction = initGeneratorFunction;
  $traceurRuntime.createGeneratorInstance = createGeneratorInstance;
})();
(function() {
  function buildFromEncodedParts(opt_scheme, opt_userInfo, opt_domain, opt_port, opt_path, opt_queryData, opt_fragment) {
    var out = [];
    if (opt_scheme) {
      out.push(opt_scheme, ':');
    }
    if (opt_domain) {
      out.push('//');
      if (opt_userInfo) {
        out.push(opt_userInfo, '@');
      }
      out.push(opt_domain);
      if (opt_port) {
        out.push(':', opt_port);
      }
    }
    if (opt_path) {
      out.push(opt_path);
    }
    if (opt_queryData) {
      out.push('?', opt_queryData);
    }
    if (opt_fragment) {
      out.push('#', opt_fragment);
    }
    return out.join('');
  }
  ;
  var splitRe = new RegExp('^' + '(?:' + '([^:/?#.]+)' + ':)?' + '(?://' + '(?:([^/?#]*)@)?' + '([\\w\\d\\-\\u0100-\\uffff.%]*)' + '(?::([0-9]+))?' + ')?' + '([^?#]+)?' + '(?:\\?([^#]*))?' + '(?:#(.*))?' + '$');
  var ComponentIndex = {
    SCHEME: 1,
    USER_INFO: 2,
    DOMAIN: 3,
    PORT: 4,
    PATH: 5,
    QUERY_DATA: 6,
    FRAGMENT: 7
  };
  function split(uri) {
    return (uri.match(splitRe));
  }
  function removeDotSegments(path) {
    if (path === '/')
      return '/';
    var leadingSlash = path[0] === '/' ? '/' : '';
    var trailingSlash = path.slice(-1) === '/' ? '/' : '';
    var segments = path.split('/');
    var out = [];
    var up = 0;
    for (var pos = 0; pos < segments.length; pos++) {
      var segment = segments[pos];
      switch (segment) {
        case '':
        case '.':
          break;
        case '..':
          if (out.length)
            out.pop();
          else
            up++;
          break;
        default:
          out.push(segment);
      }
    }
    if (!leadingSlash) {
      while (up-- > 0) {
        out.unshift('..');
      }
      if (out.length === 0)
        out.push('.');
    }
    return leadingSlash + out.join('/') + trailingSlash;
  }
  function joinAndCanonicalizePath(parts) {
    var path = parts[ComponentIndex.PATH] || '';
    path = removeDotSegments(path);
    parts[ComponentIndex.PATH] = path;
    return buildFromEncodedParts(parts[ComponentIndex.SCHEME], parts[ComponentIndex.USER_INFO], parts[ComponentIndex.DOMAIN], parts[ComponentIndex.PORT], parts[ComponentIndex.PATH], parts[ComponentIndex.QUERY_DATA], parts[ComponentIndex.FRAGMENT]);
  }
  function canonicalizeUrl(url) {
    var parts = split(url);
    return joinAndCanonicalizePath(parts);
  }
  function resolveUrl(base, url) {
    var parts = split(url);
    var baseParts = split(base);
    if (parts[ComponentIndex.SCHEME]) {
      return joinAndCanonicalizePath(parts);
    } else {
      parts[ComponentIndex.SCHEME] = baseParts[ComponentIndex.SCHEME];
    }
    for (var i = ComponentIndex.SCHEME; i <= ComponentIndex.PORT; i++) {
      if (!parts[i]) {
        parts[i] = baseParts[i];
      }
    }
    if (parts[ComponentIndex.PATH][0] == '/') {
      return joinAndCanonicalizePath(parts);
    }
    var path = baseParts[ComponentIndex.PATH];
    var index = path.lastIndexOf('/');
    path = path.slice(0, index + 1) + parts[ComponentIndex.PATH];
    parts[ComponentIndex.PATH] = path;
    return joinAndCanonicalizePath(parts);
  }
  function isAbsolute(name) {
    if (!name)
      return false;
    if (name[0] === '/')
      return true;
    var parts = split(name);
    if (parts[ComponentIndex.SCHEME])
      return true;
    return false;
  }
  $traceurRuntime.canonicalizeUrl = canonicalizeUrl;
  $traceurRuntime.isAbsolute = isAbsolute;
  $traceurRuntime.removeDotSegments = removeDotSegments;
  $traceurRuntime.resolveUrl = resolveUrl;
})();
(function() {
  'use strict';
  var types = {
    any: {name: 'any'},
    boolean: {name: 'boolean'},
    number: {name: 'number'},
    string: {name: 'string'},
    symbol: {name: 'symbol'},
    void: {name: 'void'}
  };
  var GenericType = function GenericType(type, argumentTypes) {
    this.type = type;
    this.argumentTypes = argumentTypes;
  };
  ($traceurRuntime.createClass)(GenericType, {}, {});
  var typeRegister = Object.create(null);
  function genericType(type) {
    for (var argumentTypes = [],
        $__1 = 1; $__1 < arguments.length; $__1++)
      argumentTypes[$__1 - 1] = arguments[$__1];
    var typeMap = typeRegister;
    var key = $traceurRuntime.getOwnHashObject(type).hash;
    if (!typeMap[key]) {
      typeMap[key] = Object.create(null);
    }
    typeMap = typeMap[key];
    for (var i = 0; i < argumentTypes.length - 1; i++) {
      key = $traceurRuntime.getOwnHashObject(argumentTypes[i]).hash;
      if (!typeMap[key]) {
        typeMap[key] = Object.create(null);
      }
      typeMap = typeMap[key];
    }
    var tail = argumentTypes[argumentTypes.length - 1];
    key = $traceurRuntime.getOwnHashObject(tail).hash;
    if (!typeMap[key]) {
      typeMap[key] = new GenericType(type, argumentTypes);
    }
    return typeMap[key];
  }
  $traceurRuntime.GenericType = GenericType;
  $traceurRuntime.genericType = genericType;
  $traceurRuntime.type = types;
})();
(function(global) {
  'use strict';
  var $__2 = $traceurRuntime,
      canonicalizeUrl = $__2.canonicalizeUrl,
      resolveUrl = $__2.resolveUrl,
      isAbsolute = $__2.isAbsolute;
  var moduleInstantiators = Object.create(null);
  var baseURL;
  if (global.location && global.location.href)
    baseURL = resolveUrl(global.location.href, './');
  else
    baseURL = '';
  var UncoatedModuleEntry = function UncoatedModuleEntry(url, uncoatedModule) {
    this.url = url;
    this.value_ = uncoatedModule;
  };
  ($traceurRuntime.createClass)(UncoatedModuleEntry, {}, {});
  var ModuleEvaluationError = function ModuleEvaluationError(erroneousModuleName, cause) {
    this.message = this.constructor.name + ': ' + this.stripCause(cause) + ' in ' + erroneousModuleName;
    if (!(cause instanceof $ModuleEvaluationError) && cause.stack)
      this.stack = this.stripStack(cause.stack);
    else
      this.stack = '';
  };
  var $ModuleEvaluationError = ModuleEvaluationError;
  ($traceurRuntime.createClass)(ModuleEvaluationError, {
    stripError: function(message) {
      return message.replace(/.*Error:/, this.constructor.name + ':');
    },
    stripCause: function(cause) {
      if (!cause)
        return '';
      if (!cause.message)
        return cause + '';
      return this.stripError(cause.message);
    },
    loadedBy: function(moduleName) {
      this.stack += '\n loaded by ' + moduleName;
    },
    stripStack: function(causeStack) {
      var stack = [];
      causeStack.split('\n').some((function(frame) {
        if (/UncoatedModuleInstantiator/.test(frame))
          return true;
        stack.push(frame);
      }));
      stack[0] = this.stripError(stack[0]);
      return stack.join('\n');
    }
  }, {}, Error);
  function beforeLines(lines, number) {
    var result = [];
    var first = number - 3;
    if (first < 0)
      first = 0;
    for (var i = first; i < number; i++) {
      result.push(lines[i]);
    }
    return result;
  }
  function afterLines(lines, number) {
    var last = number + 1;
    if (last > lines.length - 1)
      last = lines.length - 1;
    var result = [];
    for (var i = number; i <= last; i++) {
      result.push(lines[i]);
    }
    return result;
  }
  function columnSpacing(columns) {
    var result = '';
    for (var i = 0; i < columns - 1; i++) {
      result += '-';
    }
    return result;
  }
  var UncoatedModuleInstantiator = function UncoatedModuleInstantiator(url, func) {
    $traceurRuntime.superConstructor($UncoatedModuleInstantiator).call(this, url, null);
    this.func = func;
  };
  var $UncoatedModuleInstantiator = UncoatedModuleInstantiator;
  ($traceurRuntime.createClass)(UncoatedModuleInstantiator, {getUncoatedModule: function() {
      if (this.value_)
        return this.value_;
      try {
        var relativeRequire;
        if (typeof $traceurRuntime !== undefined) {
          relativeRequire = $traceurRuntime.require.bind(null, this.url);
        }
        return this.value_ = this.func.call(global, relativeRequire);
      } catch (ex) {
        if (ex instanceof ModuleEvaluationError) {
          ex.loadedBy(this.url);
          throw ex;
        }
        if (ex.stack) {
          var lines = this.func.toString().split('\n');
          var evaled = [];
          ex.stack.split('\n').some(function(frame) {
            if (frame.indexOf('UncoatedModuleInstantiator.getUncoatedModule') > 0)
              return true;
            var m = /(at\s[^\s]*\s).*>:(\d*):(\d*)\)/.exec(frame);
            if (m) {
              var line = parseInt(m[2], 10);
              evaled = evaled.concat(beforeLines(lines, line));
              evaled.push(columnSpacing(m[3]) + '^');
              evaled = evaled.concat(afterLines(lines, line));
              evaled.push('= = = = = = = = =');
            } else {
              evaled.push(frame);
            }
          });
          ex.stack = evaled.join('\n');
        }
        throw new ModuleEvaluationError(this.url, ex);
      }
    }}, {}, UncoatedModuleEntry);
  function getUncoatedModuleInstantiator(name) {
    if (!name)
      return;
    var url = ModuleStore.normalize(name);
    return moduleInstantiators[url];
  }
  ;
  var moduleInstances = Object.create(null);
  var liveModuleSentinel = {};
  function Module(uncoatedModule) {
    var isLive = arguments[1];
    var coatedModule = Object.create(null);
    Object.getOwnPropertyNames(uncoatedModule).forEach((function(name) {
      var getter,
          value;
      if (isLive === liveModuleSentinel) {
        var descr = Object.getOwnPropertyDescriptor(uncoatedModule, name);
        if (descr.get)
          getter = descr.get;
      }
      if (!getter) {
        value = uncoatedModule[name];
        getter = function() {
          return value;
        };
      }
      Object.defineProperty(coatedModule, name, {
        get: getter,
        enumerable: true
      });
    }));
    Object.preventExtensions(coatedModule);
    return coatedModule;
  }
  var ModuleStore = {
    normalize: function(name, refererName, refererAddress) {
      if (typeof name !== 'string')
        throw new TypeError('module name must be a string, not ' + typeof name);
      if (isAbsolute(name))
        return canonicalizeUrl(name);
      if (/[^\.]\/\.\.\//.test(name)) {
        throw new Error('module name embeds /../: ' + name);
      }
      if (name[0] === '.' && refererName)
        return resolveUrl(refererName, name);
      return canonicalizeUrl(name);
    },
    get: function(normalizedName) {
      var m = getUncoatedModuleInstantiator(normalizedName);
      if (!m)
        return undefined;
      var moduleInstance = moduleInstances[m.url];
      if (moduleInstance)
        return moduleInstance;
      moduleInstance = Module(m.getUncoatedModule(), liveModuleSentinel);
      return moduleInstances[m.url] = moduleInstance;
    },
    set: function(normalizedName, module) {
      normalizedName = String(normalizedName);
      moduleInstantiators[normalizedName] = new UncoatedModuleInstantiator(normalizedName, (function() {
        return module;
      }));
      moduleInstances[normalizedName] = module;
    },
    get baseURL() {
      return baseURL;
    },
    set baseURL(v) {
      baseURL = String(v);
    },
    registerModule: function(name, deps, func) {
      var normalizedName = ModuleStore.normalize(name);
      if (moduleInstantiators[normalizedName])
        throw new Error('duplicate module named ' + normalizedName);
      moduleInstantiators[normalizedName] = new UncoatedModuleInstantiator(normalizedName, func);
    },
    bundleStore: Object.create(null),
    register: function(name, deps, func) {
      if (!deps || !deps.length && !func.length) {
        this.registerModule(name, deps, func);
      } else {
        this.bundleStore[name] = {
          deps: deps,
          execute: function() {
            var $__0 = arguments;
            var depMap = {};
            deps.forEach((function(dep, index) {
              return depMap[dep] = $__0[index];
            }));
            var registryEntry = func.call(this, depMap);
            registryEntry.execute.call(this);
            return registryEntry.exports;
          }
        };
      }
    },
    getAnonymousModule: function(func) {
      return new Module(func.call(global), liveModuleSentinel);
    },
    getForTesting: function(name) {
      var $__0 = this;
      if (!this.testingPrefix_) {
        Object.keys(moduleInstances).some((function(key) {
          var m = /(traceur@[^\/]*\/)/.exec(key);
          if (m) {
            $__0.testingPrefix_ = m[1];
            return true;
          }
        }));
      }
      return this.get(this.testingPrefix_ + name);
    }
  };
  var moduleStoreModule = new Module({ModuleStore: ModuleStore});
  ModuleStore.set('@traceur/src/runtime/ModuleStore', moduleStoreModule);
  ModuleStore.set('@traceur/src/runtime/ModuleStore.js', moduleStoreModule);
  var setupGlobals = $traceurRuntime.setupGlobals;
  $traceurRuntime.setupGlobals = function(global) {
    setupGlobals(global);
  };
  $traceurRuntime.ModuleStore = ModuleStore;
  global.System = {
    register: ModuleStore.register.bind(ModuleStore),
    registerModule: ModuleStore.registerModule.bind(ModuleStore),
    get: ModuleStore.get,
    set: ModuleStore.set,
    normalize: ModuleStore.normalize
  };
  $traceurRuntime.getModuleImpl = function(name) {
    var instantiator = getUncoatedModuleInstantiator(name);
    return instantiator && instantiator.getUncoatedModule();
  };
})(typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : this);
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/utils.js";
  var $ceil = Math.ceil;
  var $floor = Math.floor;
  var $isFinite = isFinite;
  var $isNaN = isNaN;
  var $pow = Math.pow;
  var $min = Math.min;
  var toObject = $traceurRuntime.toObject;
  function toUint32(x) {
    return x >>> 0;
  }
  function isObject(x) {
    return x && (typeof x === 'object' || typeof x === 'function');
  }
  function isCallable(x) {
    return typeof x === 'function';
  }
  function isNumber(x) {
    return typeof x === 'number';
  }
  function toInteger(x) {
    x = +x;
    if ($isNaN(x))
      return 0;
    if (x === 0 || !$isFinite(x))
      return x;
    return x > 0 ? $floor(x) : $ceil(x);
  }
  var MAX_SAFE_LENGTH = $pow(2, 53) - 1;
  function toLength(x) {
    var len = toInteger(x);
    return len < 0 ? 0 : $min(len, MAX_SAFE_LENGTH);
  }
  function checkIterable(x) {
    return !isObject(x) ? undefined : x[Symbol.iterator];
  }
  function isConstructor(x) {
    return isCallable(x);
  }
  function createIteratorResultObject(value, done) {
    return {
      value: value,
      done: done
    };
  }
  function maybeDefine(object, name, descr) {
    if (!(name in object)) {
      Object.defineProperty(object, name, descr);
    }
  }
  function maybeDefineMethod(object, name, value) {
    maybeDefine(object, name, {
      value: value,
      configurable: true,
      enumerable: false,
      writable: true
    });
  }
  function maybeDefineConst(object, name, value) {
    maybeDefine(object, name, {
      value: value,
      configurable: false,
      enumerable: false,
      writable: false
    });
  }
  function maybeAddFunctions(object, functions) {
    for (var i = 0; i < functions.length; i += 2) {
      var name = functions[i];
      var value = functions[i + 1];
      maybeDefineMethod(object, name, value);
    }
  }
  function maybeAddConsts(object, consts) {
    for (var i = 0; i < consts.length; i += 2) {
      var name = consts[i];
      var value = consts[i + 1];
      maybeDefineConst(object, name, value);
    }
  }
  function maybeAddIterator(object, func, Symbol) {
    if (!Symbol || !Symbol.iterator || object[Symbol.iterator])
      return;
    if (object['@@iterator'])
      func = object['@@iterator'];
    Object.defineProperty(object, Symbol.iterator, {
      value: func,
      configurable: true,
      enumerable: false,
      writable: true
    });
  }
  var polyfills = [];
  function registerPolyfill(func) {
    polyfills.push(func);
  }
  function polyfillAll(global) {
    polyfills.forEach((function(f) {
      return f(global);
    }));
  }
  return {
    get toObject() {
      return toObject;
    },
    get toUint32() {
      return toUint32;
    },
    get isObject() {
      return isObject;
    },
    get isCallable() {
      return isCallable;
    },
    get isNumber() {
      return isNumber;
    },
    get toInteger() {
      return toInteger;
    },
    get toLength() {
      return toLength;
    },
    get checkIterable() {
      return checkIterable;
    },
    get isConstructor() {
      return isConstructor;
    },
    get createIteratorResultObject() {
      return createIteratorResultObject;
    },
    get maybeDefine() {
      return maybeDefine;
    },
    get maybeDefineMethod() {
      return maybeDefineMethod;
    },
    get maybeDefineConst() {
      return maybeDefineConst;
    },
    get maybeAddFunctions() {
      return maybeAddFunctions;
    },
    get maybeAddConsts() {
      return maybeAddConsts;
    },
    get maybeAddIterator() {
      return maybeAddIterator;
    },
    get registerPolyfill() {
      return registerPolyfill;
    },
    get polyfillAll() {
      return polyfillAll;
    }
  };
});
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Map.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Map.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      isObject = $__0.isObject,
      maybeAddIterator = $__0.maybeAddIterator,
      registerPolyfill = $__0.registerPolyfill;
  var getOwnHashObject = $traceurRuntime.getOwnHashObject;
  var $hasOwnProperty = Object.prototype.hasOwnProperty;
  var deletedSentinel = {};
  function lookupIndex(map, key) {
    if (isObject(key)) {
      var hashObject = getOwnHashObject(key);
      return hashObject && map.objectIndex_[hashObject.hash];
    }
    if (typeof key === 'string')
      return map.stringIndex_[key];
    return map.primitiveIndex_[key];
  }
  function initMap(map) {
    map.entries_ = [];
    map.objectIndex_ = Object.create(null);
    map.stringIndex_ = Object.create(null);
    map.primitiveIndex_ = Object.create(null);
    map.deletedCount_ = 0;
  }
  var Map = function Map() {
    var iterable = arguments[0];
    if (!isObject(this))
      throw new TypeError('Map called on incompatible type');
    if ($hasOwnProperty.call(this, 'entries_')) {
      throw new TypeError('Map can not be reentrantly initialised');
    }
    initMap(this);
    if (iterable !== null && iterable !== undefined) {
      for (var $__2 = iterable[$traceurRuntime.toProperty(Symbol.iterator)](),
          $__3; !($__3 = $__2.next()).done; ) {
        var $__4 = $__3.value,
            key = $__4[0],
            value = $__4[1];
        {
          this.set(key, value);
        }
      }
    }
  };
  ($traceurRuntime.createClass)(Map, {
    get size() {
      return this.entries_.length / 2 - this.deletedCount_;
    },
    get: function(key) {
      var index = lookupIndex(this, key);
      if (index !== undefined)
        return this.entries_[index + 1];
    },
    set: function(key, value) {
      var objectMode = isObject(key);
      var stringMode = typeof key === 'string';
      var index = lookupIndex(this, key);
      if (index !== undefined) {
        this.entries_[index + 1] = value;
      } else {
        index = this.entries_.length;
        this.entries_[index] = key;
        this.entries_[index + 1] = value;
        if (objectMode) {
          var hashObject = getOwnHashObject(key);
          var hash = hashObject.hash;
          this.objectIndex_[hash] = index;
        } else if (stringMode) {
          this.stringIndex_[key] = index;
        } else {
          this.primitiveIndex_[key] = index;
        }
      }
      return this;
    },
    has: function(key) {
      return lookupIndex(this, key) !== undefined;
    },
    delete: function(key) {
      var objectMode = isObject(key);
      var stringMode = typeof key === 'string';
      var index;
      var hash;
      if (objectMode) {
        var hashObject = getOwnHashObject(key);
        if (hashObject) {
          index = this.objectIndex_[hash = hashObject.hash];
          delete this.objectIndex_[hash];
        }
      } else if (stringMode) {
        index = this.stringIndex_[key];
        delete this.stringIndex_[key];
      } else {
        index = this.primitiveIndex_[key];
        delete this.primitiveIndex_[key];
      }
      if (index !== undefined) {
        this.entries_[index] = deletedSentinel;
        this.entries_[index + 1] = undefined;
        this.deletedCount_++;
        return true;
      }
      return false;
    },
    clear: function() {
      initMap(this);
    },
    forEach: function(callbackFn) {
      var thisArg = arguments[1];
      for (var i = 0; i < this.entries_.length; i += 2) {
        var key = this.entries_[i];
        var value = this.entries_[i + 1];
        if (key === deletedSentinel)
          continue;
        callbackFn.call(thisArg, value, key, this);
      }
    },
    entries: $traceurRuntime.initGeneratorFunction(function $__5() {
      var i,
          key,
          value;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              i = 0;
              $ctx.state = 12;
              break;
            case 12:
              $ctx.state = (i < this.entries_.length) ? 8 : -2;
              break;
            case 4:
              i += 2;
              $ctx.state = 12;
              break;
            case 8:
              key = this.entries_[i];
              value = this.entries_[i + 1];
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = (key === deletedSentinel) ? 4 : 6;
              break;
            case 6:
              $ctx.state = 2;
              return [key, value];
            case 2:
              $ctx.maybeThrow();
              $ctx.state = 4;
              break;
            default:
              return $ctx.end();
          }
      }, $__5, this);
    }),
    keys: $traceurRuntime.initGeneratorFunction(function $__6() {
      var i,
          key,
          value;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              i = 0;
              $ctx.state = 12;
              break;
            case 12:
              $ctx.state = (i < this.entries_.length) ? 8 : -2;
              break;
            case 4:
              i += 2;
              $ctx.state = 12;
              break;
            case 8:
              key = this.entries_[i];
              value = this.entries_[i + 1];
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = (key === deletedSentinel) ? 4 : 6;
              break;
            case 6:
              $ctx.state = 2;
              return key;
            case 2:
              $ctx.maybeThrow();
              $ctx.state = 4;
              break;
            default:
              return $ctx.end();
          }
      }, $__6, this);
    }),
    values: $traceurRuntime.initGeneratorFunction(function $__7() {
      var i,
          key,
          value;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              i = 0;
              $ctx.state = 12;
              break;
            case 12:
              $ctx.state = (i < this.entries_.length) ? 8 : -2;
              break;
            case 4:
              i += 2;
              $ctx.state = 12;
              break;
            case 8:
              key = this.entries_[i];
              value = this.entries_[i + 1];
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = (key === deletedSentinel) ? 4 : 6;
              break;
            case 6:
              $ctx.state = 2;
              return value;
            case 2:
              $ctx.maybeThrow();
              $ctx.state = 4;
              break;
            default:
              return $ctx.end();
          }
      }, $__7, this);
    })
  }, {});
  Object.defineProperty(Map.prototype, Symbol.iterator, {
    configurable: true,
    writable: true,
    value: Map.prototype.entries
  });
  function polyfillMap(global) {
    var $__4 = global,
        Object = $__4.Object,
        Symbol = $__4.Symbol;
    if (!global.Map)
      global.Map = Map;
    var mapPrototype = global.Map.prototype;
    if (mapPrototype.entries === undefined)
      global.Map = Map;
    if (mapPrototype.entries) {
      maybeAddIterator(mapPrototype, mapPrototype.entries, Symbol);
      maybeAddIterator(Object.getPrototypeOf(new global.Map().entries()), function() {
        return this;
      }, Symbol);
    }
  }
  registerPolyfill(polyfillMap);
  return {
    get Map() {
      return Map;
    },
    get polyfillMap() {
      return polyfillMap;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Map.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Set.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Set.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      isObject = $__0.isObject,
      maybeAddIterator = $__0.maybeAddIterator,
      registerPolyfill = $__0.registerPolyfill;
  var Map = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Map.js").Map;
  var getOwnHashObject = $traceurRuntime.getOwnHashObject;
  var $hasOwnProperty = Object.prototype.hasOwnProperty;
  function initSet(set) {
    set.map_ = new Map();
  }
  var Set = function Set() {
    var iterable = arguments[0];
    if (!isObject(this))
      throw new TypeError('Set called on incompatible type');
    if ($hasOwnProperty.call(this, 'map_')) {
      throw new TypeError('Set can not be reentrantly initialised');
    }
    initSet(this);
    if (iterable !== null && iterable !== undefined) {
      for (var $__4 = iterable[$traceurRuntime.toProperty(Symbol.iterator)](),
          $__5; !($__5 = $__4.next()).done; ) {
        var item = $__5.value;
        {
          this.add(item);
        }
      }
    }
  };
  ($traceurRuntime.createClass)(Set, {
    get size() {
      return this.map_.size;
    },
    has: function(key) {
      return this.map_.has(key);
    },
    add: function(key) {
      this.map_.set(key, key);
      return this;
    },
    delete: function(key) {
      return this.map_.delete(key);
    },
    clear: function() {
      return this.map_.clear();
    },
    forEach: function(callbackFn) {
      var thisArg = arguments[1];
      var $__2 = this;
      return this.map_.forEach((function(value, key) {
        callbackFn.call(thisArg, key, key, $__2);
      }));
    },
    values: $traceurRuntime.initGeneratorFunction(function $__7() {
      var $__8,
          $__9;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              $__8 = this.map_.keys()[Symbol.iterator]();
              $ctx.sent = void 0;
              $ctx.action = 'next';
              $ctx.state = 12;
              break;
            case 12:
              $__9 = $__8[$ctx.action]($ctx.sentIgnoreThrow);
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = ($__9.done) ? 3 : 2;
              break;
            case 3:
              $ctx.sent = $__9.value;
              $ctx.state = -2;
              break;
            case 2:
              $ctx.state = 12;
              return $__9.value;
            default:
              return $ctx.end();
          }
      }, $__7, this);
    }),
    entries: $traceurRuntime.initGeneratorFunction(function $__10() {
      var $__11,
          $__12;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              $__11 = this.map_.entries()[Symbol.iterator]();
              $ctx.sent = void 0;
              $ctx.action = 'next';
              $ctx.state = 12;
              break;
            case 12:
              $__12 = $__11[$ctx.action]($ctx.sentIgnoreThrow);
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = ($__12.done) ? 3 : 2;
              break;
            case 3:
              $ctx.sent = $__12.value;
              $ctx.state = -2;
              break;
            case 2:
              $ctx.state = 12;
              return $__12.value;
            default:
              return $ctx.end();
          }
      }, $__10, this);
    })
  }, {});
  Object.defineProperty(Set.prototype, Symbol.iterator, {
    configurable: true,
    writable: true,
    value: Set.prototype.values
  });
  Object.defineProperty(Set.prototype, 'keys', {
    configurable: true,
    writable: true,
    value: Set.prototype.values
  });
  function polyfillSet(global) {
    var $__6 = global,
        Object = $__6.Object,
        Symbol = $__6.Symbol;
    if (!global.Set)
      global.Set = Set;
    var setPrototype = global.Set.prototype;
    if (setPrototype.values) {
      maybeAddIterator(setPrototype, setPrototype.values, Symbol);
      maybeAddIterator(Object.getPrototypeOf(new global.Set().values()), function() {
        return this;
      }, Symbol);
    }
  }
  registerPolyfill(polyfillSet);
  return {
    get Set() {
      return Set;
    },
    get polyfillSet() {
      return polyfillSet;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Set.js" + '');
System.registerModule("traceur-runtime@0.0.79/node_modules/rsvp/lib/rsvp/asap.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/node_modules/rsvp/lib/rsvp/asap.js";
  var len = 0;
  function asap(callback, arg) {
    queue[len] = callback;
    queue[len + 1] = arg;
    len += 2;
    if (len === 2) {
      scheduleFlush();
    }
  }
  var $__default = asap;
  var browserGlobal = (typeof window !== 'undefined') ? window : {};
  var BrowserMutationObserver = browserGlobal.MutationObserver || browserGlobal.WebKitMutationObserver;
  var isWorker = typeof Uint8ClampedArray !== 'undefined' && typeof importScripts !== 'undefined' && typeof MessageChannel !== 'undefined';
  function useNextTick() {
    return function() {
      process.nextTick(flush);
    };
  }
  function useMutationObserver() {
    var iterations = 0;
    var observer = new BrowserMutationObserver(flush);
    var node = document.createTextNode('');
    observer.observe(node, {characterData: true});
    return function() {
      node.data = (iterations = ++iterations % 2);
    };
  }
  function useMessageChannel() {
    var channel = new MessageChannel();
    channel.port1.onmessage = flush;
    return function() {
      channel.port2.postMessage(0);
    };
  }
  function useSetTimeout() {
    return function() {
      setTimeout(flush, 1);
    };
  }
  var queue = new Array(1000);
  function flush() {
    for (var i = 0; i < len; i += 2) {
      var callback = queue[i];
      var arg = queue[i + 1];
      callback(arg);
      queue[i] = undefined;
      queue[i + 1] = undefined;
    }
    len = 0;
  }
  var scheduleFlush;
  if (typeof process !== 'undefined' && {}.toString.call(process) === '[object process]') {
    scheduleFlush = useNextTick();
  } else if (BrowserMutationObserver) {
    scheduleFlush = useMutationObserver();
  } else if (isWorker) {
    scheduleFlush = useMessageChannel();
  } else {
    scheduleFlush = useSetTimeout();
  }
  return {get default() {
      return $__default;
    }};
});
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Promise.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Promise.js";
  var async = System.get("traceur-runtime@0.0.79/node_modules/rsvp/lib/rsvp/asap.js").default;
  var registerPolyfill = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js").registerPolyfill;
  var promiseRaw = {};
  function isPromise(x) {
    return x && typeof x === 'object' && x.status_ !== undefined;
  }
  function idResolveHandler(x) {
    return x;
  }
  function idRejectHandler(x) {
    throw x;
  }
  function chain(promise) {
    var onResolve = arguments[1] !== (void 0) ? arguments[1] : idResolveHandler;
    var onReject = arguments[2] !== (void 0) ? arguments[2] : idRejectHandler;
    var deferred = getDeferred(promise.constructor);
    switch (promise.status_) {
      case undefined:
        throw TypeError;
      case 0:
        promise.onResolve_.push(onResolve, deferred);
        promise.onReject_.push(onReject, deferred);
        break;
      case +1:
        promiseEnqueue(promise.value_, [onResolve, deferred]);
        break;
      case -1:
        promiseEnqueue(promise.value_, [onReject, deferred]);
        break;
    }
    return deferred.promise;
  }
  function getDeferred(C) {
    if (this === $Promise) {
      var promise = promiseInit(new $Promise(promiseRaw));
      return {
        promise: promise,
        resolve: (function(x) {
          promiseResolve(promise, x);
        }),
        reject: (function(r) {
          promiseReject(promise, r);
        })
      };
    } else {
      var result = {};
      result.promise = new C((function(resolve, reject) {
        result.resolve = resolve;
        result.reject = reject;
      }));
      return result;
    }
  }
  function promiseSet(promise, status, value, onResolve, onReject) {
    promise.status_ = status;
    promise.value_ = value;
    promise.onResolve_ = onResolve;
    promise.onReject_ = onReject;
    return promise;
  }
  function promiseInit(promise) {
    return promiseSet(promise, 0, undefined, [], []);
  }
  var Promise = function Promise(resolver) {
    if (resolver === promiseRaw)
      return;
    if (typeof resolver !== 'function')
      throw new TypeError;
    var promise = promiseInit(this);
    try {
      resolver((function(x) {
        promiseResolve(promise, x);
      }), (function(r) {
        promiseReject(promise, r);
      }));
    } catch (e) {
      promiseReject(promise, e);
    }
  };
  ($traceurRuntime.createClass)(Promise, {
    catch: function(onReject) {
      return this.then(undefined, onReject);
    },
    then: function(onResolve, onReject) {
      if (typeof onResolve !== 'function')
        onResolve = idResolveHandler;
      if (typeof onReject !== 'function')
        onReject = idRejectHandler;
      var that = this;
      var constructor = this.constructor;
      return chain(this, function(x) {
        x = promiseCoerce(constructor, x);
        return x === that ? onReject(new TypeError) : isPromise(x) ? x.then(onResolve, onReject) : onResolve(x);
      }, onReject);
    }
  }, {
    resolve: function(x) {
      if (this === $Promise) {
        if (isPromise(x)) {
          return x;
        }
        return promiseSet(new $Promise(promiseRaw), +1, x);
      } else {
        return new this(function(resolve, reject) {
          resolve(x);
        });
      }
    },
    reject: function(r) {
      if (this === $Promise) {
        return promiseSet(new $Promise(promiseRaw), -1, r);
      } else {
        return new this((function(resolve, reject) {
          reject(r);
        }));
      }
    },
    all: function(values) {
      var deferred = getDeferred(this);
      var resolutions = [];
      try {
        var count = values.length;
        if (count === 0) {
          deferred.resolve(resolutions);
        } else {
          for (var i = 0; i < values.length; i++) {
            this.resolve(values[i]).then(function(i, x) {
              resolutions[i] = x;
              if (--count === 0)
                deferred.resolve(resolutions);
            }.bind(undefined, i), (function(r) {
              deferred.reject(r);
            }));
          }
        }
      } catch (e) {
        deferred.reject(e);
      }
      return deferred.promise;
    },
    race: function(values) {
      var deferred = getDeferred(this);
      try {
        for (var i = 0; i < values.length; i++) {
          this.resolve(values[i]).then((function(x) {
            deferred.resolve(x);
          }), (function(r) {
            deferred.reject(r);
          }));
        }
      } catch (e) {
        deferred.reject(e);
      }
      return deferred.promise;
    }
  });
  var $Promise = Promise;
  var $PromiseReject = $Promise.reject;
  function promiseResolve(promise, x) {
    promiseDone(promise, +1, x, promise.onResolve_);
  }
  function promiseReject(promise, r) {
    promiseDone(promise, -1, r, promise.onReject_);
  }
  function promiseDone(promise, status, value, reactions) {
    if (promise.status_ !== 0)
      return;
    promiseEnqueue(value, reactions);
    promiseSet(promise, status, value);
  }
  function promiseEnqueue(value, tasks) {
    async((function() {
      for (var i = 0; i < tasks.length; i += 2) {
        promiseHandle(value, tasks[i], tasks[i + 1]);
      }
    }));
  }
  function promiseHandle(value, handler, deferred) {
    try {
      var result = handler(value);
      if (result === deferred.promise)
        throw new TypeError;
      else if (isPromise(result))
        chain(result, deferred.resolve, deferred.reject);
      else
        deferred.resolve(result);
    } catch (e) {
      try {
        deferred.reject(e);
      } catch (e) {}
    }
  }
  var thenableSymbol = '@@thenable';
  function isObject(x) {
    return x && (typeof x === 'object' || typeof x === 'function');
  }
  function promiseCoerce(constructor, x) {
    if (!isPromise(x) && isObject(x)) {
      var then;
      try {
        then = x.then;
      } catch (r) {
        var promise = $PromiseReject.call(constructor, r);
        x[thenableSymbol] = promise;
        return promise;
      }
      if (typeof then === 'function') {
        var p = x[thenableSymbol];
        if (p) {
          return p;
        } else {
          var deferred = getDeferred(constructor);
          x[thenableSymbol] = deferred.promise;
          try {
            then.call(x, deferred.resolve, deferred.reject);
          } catch (r) {
            deferred.reject(r);
          }
          return deferred.promise;
        }
      }
    }
    return x;
  }
  function polyfillPromise(global) {
    if (!global.Promise)
      global.Promise = Promise;
  }
  registerPolyfill(polyfillPromise);
  return {
    get Promise() {
      return Promise;
    },
    get polyfillPromise() {
      return polyfillPromise;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Promise.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/StringIterator.js", [], function() {
  "use strict";
  var $__2;
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/StringIterator.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      createIteratorResultObject = $__0.createIteratorResultObject,
      isObject = $__0.isObject;
  var toProperty = $traceurRuntime.toProperty;
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  var iteratedString = Symbol('iteratedString');
  var stringIteratorNextIndex = Symbol('stringIteratorNextIndex');
  var StringIterator = function StringIterator() {};
  ($traceurRuntime.createClass)(StringIterator, ($__2 = {}, Object.defineProperty($__2, "next", {
    value: function() {
      var o = this;
      if (!isObject(o) || !hasOwnProperty.call(o, iteratedString)) {
        throw new TypeError('this must be a StringIterator object');
      }
      var s = o[toProperty(iteratedString)];
      if (s === undefined) {
        return createIteratorResultObject(undefined, true);
      }
      var position = o[toProperty(stringIteratorNextIndex)];
      var len = s.length;
      if (position >= len) {
        o[toProperty(iteratedString)] = undefined;
        return createIteratorResultObject(undefined, true);
      }
      var first = s.charCodeAt(position);
      var resultString;
      if (first < 0xD800 || first > 0xDBFF || position + 1 === len) {
        resultString = String.fromCharCode(first);
      } else {
        var second = s.charCodeAt(position + 1);
        if (second < 0xDC00 || second > 0xDFFF) {
          resultString = String.fromCharCode(first);
        } else {
          resultString = String.fromCharCode(first) + String.fromCharCode(second);
        }
      }
      o[toProperty(stringIteratorNextIndex)] = position + resultString.length;
      return createIteratorResultObject(resultString, false);
    },
    configurable: true,
    enumerable: true,
    writable: true
  }), Object.defineProperty($__2, Symbol.iterator, {
    value: function() {
      return this;
    },
    configurable: true,
    enumerable: true,
    writable: true
  }), $__2), {});
  function createStringIterator(string) {
    var s = String(string);
    var iterator = Object.create(StringIterator.prototype);
    iterator[toProperty(iteratedString)] = s;
    iterator[toProperty(stringIteratorNextIndex)] = 0;
    return iterator;
  }
  return {get createStringIterator() {
      return createStringIterator;
    }};
});
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/String.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/String.js";
  var createStringIterator = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/StringIterator.js").createStringIterator;
  var $__1 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      maybeAddFunctions = $__1.maybeAddFunctions,
      maybeAddIterator = $__1.maybeAddIterator,
      registerPolyfill = $__1.registerPolyfill;
  var $toString = Object.prototype.toString;
  var $indexOf = String.prototype.indexOf;
  var $lastIndexOf = String.prototype.lastIndexOf;
  function startsWith(search) {
    var string = String(this);
    if (this == null || $toString.call(search) == '[object RegExp]') {
      throw TypeError();
    }
    var stringLength = string.length;
    var searchString = String(search);
    var searchLength = searchString.length;
    var position = arguments.length > 1 ? arguments[1] : undefined;
    var pos = position ? Number(position) : 0;
    if (isNaN(pos)) {
      pos = 0;
    }
    var start = Math.min(Math.max(pos, 0), stringLength);
    return $indexOf.call(string, searchString, pos) == start;
  }
  function endsWith(search) {
    var string = String(this);
    if (this == null || $toString.call(search) == '[object RegExp]') {
      throw TypeError();
    }
    var stringLength = string.length;
    var searchString = String(search);
    var searchLength = searchString.length;
    var pos = stringLength;
    if (arguments.length > 1) {
      var position = arguments[1];
      if (position !== undefined) {
        pos = position ? Number(position) : 0;
        if (isNaN(pos)) {
          pos = 0;
        }
      }
    }
    var end = Math.min(Math.max(pos, 0), stringLength);
    var start = end - searchLength;
    if (start < 0) {
      return false;
    }
    return $lastIndexOf.call(string, searchString, start) == start;
  }
  function includes(search) {
    if (this == null) {
      throw TypeError();
    }
    var string = String(this);
    if (search && $toString.call(search) == '[object RegExp]') {
      throw TypeError();
    }
    var stringLength = string.length;
    var searchString = String(search);
    var searchLength = searchString.length;
    var position = arguments.length > 1 ? arguments[1] : undefined;
    var pos = position ? Number(position) : 0;
    if (pos != pos) {
      pos = 0;
    }
    var start = Math.min(Math.max(pos, 0), stringLength);
    if (searchLength + start > stringLength) {
      return false;
    }
    return $indexOf.call(string, searchString, pos) != -1;
  }
  function repeat(count) {
    if (this == null) {
      throw TypeError();
    }
    var string = String(this);
    var n = count ? Number(count) : 0;
    if (isNaN(n)) {
      n = 0;
    }
    if (n < 0 || n == Infinity) {
      throw RangeError();
    }
    if (n == 0) {
      return '';
    }
    var result = '';
    while (n--) {
      result += string;
    }
    return result;
  }
  function codePointAt(position) {
    if (this == null) {
      throw TypeError();
    }
    var string = String(this);
    var size = string.length;
    var index = position ? Number(position) : 0;
    if (isNaN(index)) {
      index = 0;
    }
    if (index < 0 || index >= size) {
      return undefined;
    }
    var first = string.charCodeAt(index);
    var second;
    if (first >= 0xD800 && first <= 0xDBFF && size > index + 1) {
      second = string.charCodeAt(index + 1);
      if (second >= 0xDC00 && second <= 0xDFFF) {
        return (first - 0xD800) * 0x400 + second - 0xDC00 + 0x10000;
      }
    }
    return first;
  }
  function raw(callsite) {
    var raw = callsite.raw;
    var len = raw.length >>> 0;
    if (len === 0)
      return '';
    var s = '';
    var i = 0;
    while (true) {
      s += raw[i];
      if (i + 1 === len)
        return s;
      s += arguments[++i];
    }
  }
  function fromCodePoint() {
    var codeUnits = [];
    var floor = Math.floor;
    var highSurrogate;
    var lowSurrogate;
    var index = -1;
    var length = arguments.length;
    if (!length) {
      return '';
    }
    while (++index < length) {
      var codePoint = Number(arguments[index]);
      if (!isFinite(codePoint) || codePoint < 0 || codePoint > 0x10FFFF || floor(codePoint) != codePoint) {
        throw RangeError('Invalid code point: ' + codePoint);
      }
      if (codePoint <= 0xFFFF) {
        codeUnits.push(codePoint);
      } else {
        codePoint -= 0x10000;
        highSurrogate = (codePoint >> 10) + 0xD800;
        lowSurrogate = (codePoint % 0x400) + 0xDC00;
        codeUnits.push(highSurrogate, lowSurrogate);
      }
    }
    return String.fromCharCode.apply(null, codeUnits);
  }
  function stringPrototypeIterator() {
    var o = $traceurRuntime.checkObjectCoercible(this);
    var s = String(o);
    return createStringIterator(s);
  }
  function polyfillString(global) {
    var String = global.String;
    maybeAddFunctions(String.prototype, ['codePointAt', codePointAt, 'endsWith', endsWith, 'includes', includes, 'repeat', repeat, 'startsWith', startsWith]);
    maybeAddFunctions(String, ['fromCodePoint', fromCodePoint, 'raw', raw]);
    maybeAddIterator(String.prototype, stringPrototypeIterator, Symbol);
  }
  registerPolyfill(polyfillString);
  return {
    get startsWith() {
      return startsWith;
    },
    get endsWith() {
      return endsWith;
    },
    get includes() {
      return includes;
    },
    get repeat() {
      return repeat;
    },
    get codePointAt() {
      return codePointAt;
    },
    get raw() {
      return raw;
    },
    get fromCodePoint() {
      return fromCodePoint;
    },
    get stringPrototypeIterator() {
      return stringPrototypeIterator;
    },
    get polyfillString() {
      return polyfillString;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/String.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/ArrayIterator.js", [], function() {
  "use strict";
  var $__2;
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/ArrayIterator.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      toObject = $__0.toObject,
      toUint32 = $__0.toUint32,
      createIteratorResultObject = $__0.createIteratorResultObject;
  var ARRAY_ITERATOR_KIND_KEYS = 1;
  var ARRAY_ITERATOR_KIND_VALUES = 2;
  var ARRAY_ITERATOR_KIND_ENTRIES = 3;
  var ArrayIterator = function ArrayIterator() {};
  ($traceurRuntime.createClass)(ArrayIterator, ($__2 = {}, Object.defineProperty($__2, "next", {
    value: function() {
      var iterator = toObject(this);
      var array = iterator.iteratorObject_;
      if (!array) {
        throw new TypeError('Object is not an ArrayIterator');
      }
      var index = iterator.arrayIteratorNextIndex_;
      var itemKind = iterator.arrayIterationKind_;
      var length = toUint32(array.length);
      if (index >= length) {
        iterator.arrayIteratorNextIndex_ = Infinity;
        return createIteratorResultObject(undefined, true);
      }
      iterator.arrayIteratorNextIndex_ = index + 1;
      if (itemKind == ARRAY_ITERATOR_KIND_VALUES)
        return createIteratorResultObject(array[index], false);
      if (itemKind == ARRAY_ITERATOR_KIND_ENTRIES)
        return createIteratorResultObject([index, array[index]], false);
      return createIteratorResultObject(index, false);
    },
    configurable: true,
    enumerable: true,
    writable: true
  }), Object.defineProperty($__2, Symbol.iterator, {
    value: function() {
      return this;
    },
    configurable: true,
    enumerable: true,
    writable: true
  }), $__2), {});
  function createArrayIterator(array, kind) {
    var object = toObject(array);
    var iterator = new ArrayIterator;
    iterator.iteratorObject_ = object;
    iterator.arrayIteratorNextIndex_ = 0;
    iterator.arrayIterationKind_ = kind;
    return iterator;
  }
  function entries() {
    return createArrayIterator(this, ARRAY_ITERATOR_KIND_ENTRIES);
  }
  function keys() {
    return createArrayIterator(this, ARRAY_ITERATOR_KIND_KEYS);
  }
  function values() {
    return createArrayIterator(this, ARRAY_ITERATOR_KIND_VALUES);
  }
  return {
    get entries() {
      return entries;
    },
    get keys() {
      return keys;
    },
    get values() {
      return values;
    }
  };
});
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Array.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Array.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/ArrayIterator.js"),
      entries = $__0.entries,
      keys = $__0.keys,
      values = $__0.values;
  var $__1 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      checkIterable = $__1.checkIterable,
      isCallable = $__1.isCallable,
      isConstructor = $__1.isConstructor,
      maybeAddFunctions = $__1.maybeAddFunctions,
      maybeAddIterator = $__1.maybeAddIterator,
      registerPolyfill = $__1.registerPolyfill,
      toInteger = $__1.toInteger,
      toLength = $__1.toLength,
      toObject = $__1.toObject;
  function from(arrLike) {
    var mapFn = arguments[1];
    var thisArg = arguments[2];
    var C = this;
    var items = toObject(arrLike);
    var mapping = mapFn !== undefined;
    var k = 0;
    var arr,
        len;
    if (mapping && !isCallable(mapFn)) {
      throw TypeError();
    }
    if (checkIterable(items)) {
      arr = isConstructor(C) ? new C() : [];
      for (var $__2 = items[$traceurRuntime.toProperty(Symbol.iterator)](),
          $__3; !($__3 = $__2.next()).done; ) {
        var item = $__3.value;
        {
          if (mapping) {
            arr[k] = mapFn.call(thisArg, item, k);
          } else {
            arr[k] = item;
          }
          k++;
        }
      }
      arr.length = k;
      return arr;
    }
    len = toLength(items.length);
    arr = isConstructor(C) ? new C(len) : new Array(len);
    for (; k < len; k++) {
      if (mapping) {
        arr[k] = typeof thisArg === 'undefined' ? mapFn(items[k], k) : mapFn.call(thisArg, items[k], k);
      } else {
        arr[k] = items[k];
      }
    }
    arr.length = len;
    return arr;
  }
  function of() {
    for (var items = [],
        $__4 = 0; $__4 < arguments.length; $__4++)
      items[$__4] = arguments[$__4];
    var C = this;
    var len = items.length;
    var arr = isConstructor(C) ? new C(len) : new Array(len);
    for (var k = 0; k < len; k++) {
      arr[k] = items[k];
    }
    arr.length = len;
    return arr;
  }
  function fill(value) {
    var start = arguments[1] !== (void 0) ? arguments[1] : 0;
    var end = arguments[2];
    var object = toObject(this);
    var len = toLength(object.length);
    var fillStart = toInteger(start);
    var fillEnd = end !== undefined ? toInteger(end) : len;
    fillStart = fillStart < 0 ? Math.max(len + fillStart, 0) : Math.min(fillStart, len);
    fillEnd = fillEnd < 0 ? Math.max(len + fillEnd, 0) : Math.min(fillEnd, len);
    while (fillStart < fillEnd) {
      object[fillStart] = value;
      fillStart++;
    }
    return object;
  }
  function find(predicate) {
    var thisArg = arguments[1];
    return findHelper(this, predicate, thisArg);
  }
  function findIndex(predicate) {
    var thisArg = arguments[1];
    return findHelper(this, predicate, thisArg, true);
  }
  function findHelper(self, predicate) {
    var thisArg = arguments[2];
    var returnIndex = arguments[3] !== (void 0) ? arguments[3] : false;
    var object = toObject(self);
    var len = toLength(object.length);
    if (!isCallable(predicate)) {
      throw TypeError();
    }
    for (var i = 0; i < len; i++) {
      var value = object[i];
      if (predicate.call(thisArg, value, i, object)) {
        return returnIndex ? i : value;
      }
    }
    return returnIndex ? -1 : undefined;
  }
  function polyfillArray(global) {
    var $__5 = global,
        Array = $__5.Array,
        Object = $__5.Object,
        Symbol = $__5.Symbol;
    maybeAddFunctions(Array.prototype, ['entries', entries, 'keys', keys, 'values', values, 'fill', fill, 'find', find, 'findIndex', findIndex]);
    maybeAddFunctions(Array, ['from', from, 'of', of]);
    maybeAddIterator(Array.prototype, values, Symbol);
    maybeAddIterator(Object.getPrototypeOf([].values()), function() {
      return this;
    }, Symbol);
  }
  registerPolyfill(polyfillArray);
  return {
    get from() {
      return from;
    },
    get of() {
      return of;
    },
    get fill() {
      return fill;
    },
    get find() {
      return find;
    },
    get findIndex() {
      return findIndex;
    },
    get polyfillArray() {
      return polyfillArray;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Array.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Object.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Object.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      maybeAddFunctions = $__0.maybeAddFunctions,
      registerPolyfill = $__0.registerPolyfill;
  var $__1 = $traceurRuntime,
      defineProperty = $__1.defineProperty,
      getOwnPropertyDescriptor = $__1.getOwnPropertyDescriptor,
      getOwnPropertyNames = $__1.getOwnPropertyNames,
      isPrivateName = $__1.isPrivateName,
      keys = $__1.keys;
  function is(left, right) {
    if (left === right)
      return left !== 0 || 1 / left === 1 / right;
    return left !== left && right !== right;
  }
  function assign(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      var props = source == null ? [] : keys(source);
      var p,
          length = props.length;
      for (p = 0; p < length; p++) {
        var name = props[p];
        if (isPrivateName(name))
          continue;
        target[name] = source[name];
      }
    }
    return target;
  }
  function mixin(target, source) {
    var props = getOwnPropertyNames(source);
    var p,
        descriptor,
        length = props.length;
    for (p = 0; p < length; p++) {
      var name = props[p];
      if (isPrivateName(name))
        continue;
      descriptor = getOwnPropertyDescriptor(source, props[p]);
      defineProperty(target, props[p], descriptor);
    }
    return target;
  }
  function polyfillObject(global) {
    var Object = global.Object;
    maybeAddFunctions(Object, ['assign', assign, 'is', is, 'mixin', mixin]);
  }
  registerPolyfill(polyfillObject);
  return {
    get is() {
      return is;
    },
    get assign() {
      return assign;
    },
    get mixin() {
      return mixin;
    },
    get polyfillObject() {
      return polyfillObject;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Object.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Number.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Number.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      isNumber = $__0.isNumber,
      maybeAddConsts = $__0.maybeAddConsts,
      maybeAddFunctions = $__0.maybeAddFunctions,
      registerPolyfill = $__0.registerPolyfill,
      toInteger = $__0.toInteger;
  var $abs = Math.abs;
  var $isFinite = isFinite;
  var $isNaN = isNaN;
  var MAX_SAFE_INTEGER = Math.pow(2, 53) - 1;
  var MIN_SAFE_INTEGER = -Math.pow(2, 53) + 1;
  var EPSILON = Math.pow(2, -52);
  function NumberIsFinite(number) {
    return isNumber(number) && $isFinite(number);
  }
  ;
  function isInteger(number) {
    return NumberIsFinite(number) && toInteger(number) === number;
  }
  function NumberIsNaN(number) {
    return isNumber(number) && $isNaN(number);
  }
  ;
  function isSafeInteger(number) {
    if (NumberIsFinite(number)) {
      var integral = toInteger(number);
      if (integral === number)
        return $abs(integral) <= MAX_SAFE_INTEGER;
    }
    return false;
  }
  function polyfillNumber(global) {
    var Number = global.Number;
    maybeAddConsts(Number, ['MAX_SAFE_INTEGER', MAX_SAFE_INTEGER, 'MIN_SAFE_INTEGER', MIN_SAFE_INTEGER, 'EPSILON', EPSILON]);
    maybeAddFunctions(Number, ['isFinite', NumberIsFinite, 'isInteger', isInteger, 'isNaN', NumberIsNaN, 'isSafeInteger', isSafeInteger]);
  }
  registerPolyfill(polyfillNumber);
  return {
    get MAX_SAFE_INTEGER() {
      return MAX_SAFE_INTEGER;
    },
    get MIN_SAFE_INTEGER() {
      return MIN_SAFE_INTEGER;
    },
    get EPSILON() {
      return EPSILON;
    },
    get isFinite() {
      return NumberIsFinite;
    },
    get isInteger() {
      return isInteger;
    },
    get isNaN() {
      return NumberIsNaN;
    },
    get isSafeInteger() {
      return isSafeInteger;
    },
    get polyfillNumber() {
      return polyfillNumber;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Number.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/polyfills.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/polyfills.js";
  var polyfillAll = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js").polyfillAll;
  polyfillAll(Reflect.global);
  var setupGlobals = $traceurRuntime.setupGlobals;
  $traceurRuntime.setupGlobals = function(global) {
    setupGlobals(global);
    polyfillAll(global);
  };
  return {};
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/polyfills.js" + '');

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"_process":15,"path":14}]},{},[3,16]);

<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 03/10/2015
 * Time: 13:05
 */
$baseurl = $address = "http://" . $_SERVER["SERVER_NAME"] . dirname($_SERVER['SCRIPT_NAME']) . "/image.php";
require_once(__DIR__ . "/core/util/const_helper.php");
?>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Mock Image</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
        <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        <script src="js/base64.js"></script>
        <style>
            .img-center {
                display: table;
                margin: 0 auto;
                background-repeat: no-repeat;
            }
            #imgresultado {

            }
            input {
                font-family: Courier New;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="jumbotron">
                <h1>Gerar Imagens</h1>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Parâmetros</h4>
                </div>
                <div class="panel-body">
                    <div class="col-xs-12">
                        <div class="col-xs-12 col-md-6">
                            <label for="txtnome">Nome</label>
                            <input type="text" id="txtnome" class="form-control" />
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <label for="txttel">Telefone</label>
                            <input type="tel" id="txttel" class="form-control" />
                        </div>
                        <div class="col-xs-12">
                            <label for="txturl">Rede</label>
                            <input type="url" id="txturl" class="form-control" />
                        </div>
                        <div class="col-xs-12" style="margin-top:15px;">
                            <button type="button" id="btnautofill" class="btn btn-warning">Auto Fill</button>
                            <button type="button" id="btngerar" class="btn btn-primary">Gerar</button>
                        </div>
                        <div class="col-xs-12" style="margin-top:15px;">
                            <label for="txtoriurl">URL da Imagem</label>
                            <input type="url" id="txtoriurl" class="form-control" />
                        </div>
                        <div class="col-xs-12">
                            <label for="txtnaturalurl">URL da Landing Page da Natura</label>
                            <input type="url" id="txtnaturalurl" class="form-control" />
                        </div>
                        <div class="col-xs-12">
                            <label for="txtgenurl">URL da Landing Page da Natura Encurtada</label>
                            <input type="url" id="txtgenurl" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Resultado</h4>
                </div>
                <div class="panel-body" style="display: table; margin: 0 auto">
                    <img id="imgresultado" src="./image.php" />
                </div>
            </div>

            <div class="panel panel-danger">
                <div class="panel-heading">
                    <h4>GD Constants</h4>
                </div>
                <div class="panel-body">
                    <?= _php_imagetype_img_gd_print_constants(); ?>
                </div>
            </div>

        </div>
    </body>

    <script>
        $(function(){

            /**
             * Functions and Constants
             */

            baseurl = "<?= $baseurl ?>?";

            function setbackground(imgurl) {
                nocacheurl = imgurl + '&nocache=' + new Date().getTime();
                $('#imgresultado').attr("src", nocacheurl);
                console.log(nocacheurl);
            }

            function printurl(original, newurl) {
                $('#txtoriurl').val(original);
                $('#txtgenurl').val(newurl);
            }

            function echo () {
                if (window.console) window.console.log(arguments);
            }

            /**
             * Handlers
             */

            function shorturl(url, cb) {
                var apikey = "AIzaSyAkl-UsosFU4XF-jR2KARVai4KaFztYK8c";
                var _data = JSON.stringify( { "longUrl": url } );
                var _googleurl = "https://www.googleapis.com/urlshortener/v1/url?key=" + apikey;

                // Call google API
                $.ajax({
                    url: _googleurl,
                    method: 'POST',
                    contentType: 'application/json',
                    processData: false,
                    data: _data,
                    success: function(data, textStatus, jqXHR){
                        var newurl = data.id;
                        cb(url, newurl);
                    },
                    error: function(jqXHR, textStatus, errorThrown){
                        cb(url, false);
                    }
                });
            }

            $('#btngerar').click(function(){
                var params = {
                    nome     : $('#txtnome').val(),
                    telefone : $('#txttel').val(),
                    rede     : $('#txturl').val(),
                    nocache : new Date().getTime()
                };
                // Read params and transform to base64
                var get_params = "nome=" + params.nome + "&telefone=" + params.telefone + "&rede=" + params.rede;
                var base64_params = base64.encode( encodeURIComponent(get_params) );
                // Natura URL
                var naturaurl = "http://www.natura.com.br/natal/?data=" + base64_params;
                shorturl(naturaurl, function(url, newurl){
                    console.log('X', url, newurl);
                    // Read short url and decouple
                    var parturl;
                    try {
                        parturl = newurl.substring(newurl.lastIndexOf('/')+1);
                    } catch (ex) {
                        parturl = '';
                    }
                    // Set background com os dados pessoais e a url da natura GOO (goo)
                    var imageurl = baseurl + 'goo=' + parturl + '&data=' + base64_params;
                    setbackground(imageurl);
                    // Fill text box
                    $('#txtnaturalurl').val(url);
                    $('#txtgenurl').val(newurl);
                    $('#txtoriurl').val(imageurl);
                });
            });

            $('#btnautofill').click(function(){
                $('#txtnome').val('João Simão Tomé');
                $('#txttel').val('(11) 98888-7777');
                $('#txturl').val('http://rede.natura.net/espaco/JoaoAtumGomes');
            });

            /**
             * Load calls
             */
            setbackground(baseurl);

        });
    </script>
</html>
<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 11/10/2015
 * Time: 15:39
 */

namespace Core;

abstract class AbstractBlacklistHelper {

    const CHECK_ITEM_EQUALS = 3;

    protected $blacklist;

    function __construct() {
        $this->defineBlacklist();
    }

    protected abstract function defineBlacklist();

    protected function normaliseWord($string){
        $s = preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
        $s = mb_convert_case($s, MB_CASE_LOWER);
        return $s;
    }

    protected function inBlacklist($itemToCheck) {
        $word_to_check = $this->normaliseWord($itemToCheck);
        foreach($this->blacklist as $blocked_word) {
            $word_size    = strlen($blocked_word);
            // Verificacao muda de acordo com o o tamanho
            // da palavra
            if (0 <= $word_size && $word_size <= AbstractBlacklistHelper::CHECK_ITEM_EQUALS) {
                $in_black_list = $word_to_check == $blocked_word;
            }  else {
                $in_black_list = FALSE !== ( stripos($word_to_check, $blocked_word) );
            }
            if (TRUE === $in_black_list) {
                return TRUE;
            }
        }
        return FALSE;
    }

    public function validate($itemToCheck) {
        if ( FALSE === is_string($itemToCheck) ) {
            return FALSE;
        }
        return FALSE === $this->inBlacklist($itemToCheck);
    }

    public function notInBlacklist($itemToCheck) {
        return $this->validate($itemToCheck);
    }
}

class NomesBlacklistHelper extends AbstractBlacklistHelper {

    protected function defineBlacklist() {
        $this->blacklist = [	"acquaflora",	"adcos",	"adidas",	"agua de cheiro",	"amend",	"ana hickmann",	"antonio banderas",	"anus",	"avon",	"avora",	"babaca",	"babaovo",	"baba-ovo",	"bacura",	"bagos",	"baitola",	"bare minerals",	"beauty box",	"bebum",	"bel col",	"belcorp",	"benefit",	"benetton",	"besta",	"betty boop",	"bicha",	"bio art",	"bioderm",	"bionatus",	"bisca",	"bixa",	"boazuda",	"boceta",	"boco",	"boco",	"boiola",	"bolagato",	"bolcat",	"boquete",	"bosseta",	"bosta",	"bostana",	"botanic",	"boticario",	"boticario",	"brecha",	"brexa",	"brioco",	"bronha",	"buca",	"buceta",	"bunda",	"bunduda",	"burberry",	"burra",	"burro",	"busseta",	"cachorra",	"cachorro",	"cadela",	"cadiveu",	"caga",	"cagado",	"cagao",	"cagona",	"calvin klein",	"canalha",	"caralho",	"carolina herrera",	"casseta",	"cassete",	"cetaphil",	"checheca",	"chereca",	"chibumba",	"chibumbo",	"chifruda",	"chifrudo",	"chochota",	"chota",	"chupada",	"chupado",	"clean & clear",	"clinique",	"clitoris",	"clitoris",	"cocaina",	"cocaina",	"coco",	"coco",	"colorama",	"contem 1g",	"corna",	"corno",	"cornuda",	"cornudo",	"corrupta",	"corrupto",	"cretina",	"cretino",	"cruz-credo",	"cu",	"cu ",	"culhao",	"culhao",	"culhoes",	"curalho",	"curaprox",	"cuzao",	"cuzao",	"cuzuda",	"cuzudo",	"davene",	"debil",	"debiloide",	"defunto",	"demonio",	"demonio",	"diesel",	"difunto",	"dior",	"doida",	"doido",	"dolve&gabbana",	"dove",	"ecologie",	"egua",	"egua",	"embelezze",	"escrota",	"escroto",	"esporrada",	"esporrado",	"esporro",	"esporro",	"estupida",	"estupida",	"estupidez",	"estupido",	"estupido",	"eudora",	"evian",	"fator 5",	"fdp",	"fedida",	"fedido",	"fedor",	"fedorenta",	"feia",	"feio",	"feiosa",	"feioso",	"feioza",	"feiozo",	"felacao",	"felacao",	"fenda",	"ferrari",	"fiorentino",	"foda",	"fodao",	"fodao",	"fode",	"fodida",	"fodido",	"fornica",	"fudecao",	"fudecao",	"fudendo",	"fudida",	"fudido",	"furada",	"furado",	"furao",	"furao",	"furnica",	"furnicar",	"furo",	"furona",	"gabriela sabatini",	"gaiata",	"gaiato",	"gay",	"gilette",	"giorgio armani",	"givenchy",	"gonorrea",	"gonorreia",	"gosma",	"gosmenta",	"gosmento",	"granado",	"grelinho",	"grelo",	"gucci",	"guess",	"homosexual",	"homo-sexual",	"homossexual",	"hugo boss",	"idiota",	"idiotice",	"imbecil",	"impala",	"iscrota",	"iscroto",	"jaguar",	"japa",	"jequiti",	"johnson & johnson",	"joop",	"juliana paes",	"katy perry",	"kenzo",	"keune",	"kolene",	"koloss",	"l’aqua di fiori",	"l’occitane",	"la roche-posay",	"lacoste",	"ladra",	"ladrao",	"ladrao",	"ladroeira",	"ladrona",	"lady gaga",	"lalau",	"lalique",	"lancome",	"lanza",	"leprosa",	"leproso",	"lesbica",	"lesbica",	"l'occitane au bresil",	"loreal paris",	"lush",	"mac",	"macaca",	"macaco",	"machona",	"machorra",	"mahogany",	"mairibel",	"manguaca",	"manguaca",	"mary kay",	"masturba",	"max love",	"maybeline",	"maybelline",	"meleca",	"merda",	"mija",	"mijada",	"mijado",	"mijo",	"mocrea",	"mocrea",	"mocreia",	"mocreia",	"moleca",	"moleque",	"mondronga",	"mondrongo",	"montblanc",	"naba",	"nadega",	"nazca",	"neutrogena",	"niasi",	"nike",	"nivea",	"nojeira",	"nojenta",	"nojento",	"nojo",	"novex",	"nus",	"o boticario",	"odorata",	"olhota",	"otaria",	"otaria",	"otario",	"otario",	"ox",	"paca",	"pantene",	"paspalha",	"paspalhao",	"paspalho",	"pau ",	"payot",	"peia",	"peido",	"pemba",	"penis",	"penis",	"pentelha",	"pentelho",	"perereca",	"peru",	"peru",	"petralha",	"phytoervas",	"pica",	"picao",	"picao",	"pilantra",	"piranha",	"piroca",	"piroco",	"piru",	"playboy",	"porra",	"prada",	"prega",	"prostibulo",	"prostibulo",	"prostituta",	"prostituto",	"puma",	"punheta",	"punhetao",	"punhetao",	"pus",	"pustula",	"pustula",	"puta",	"puto",	"puxasaco",	"puxa-saco",	"quem disse berenice",	"rabao",	"rabao",	"rabo",	"rabuda",	"rabudao",	"rabudao",	"rabudo",	"rabudona",	"racco",	"racha",	"rachada",	"rachadao",	"rachadao",	"rachadinha",	"rachadinho",	"rachado",	"ralph lauren",	"ramela",	"remela",	"retardada",	"retardado",	"revlon",	"ridicula",	"ridicula",	"risque",	"rola",	"rolinha",	"rosca",	"sacana",	"safada",	"safado",	"sapatao",	"sapatao",	"sephora",	"shakira",	"shiseido",	"sifilis",	"sifilis",	"siririca",	"sundown",	"tarada",	"tarado",	"tesao",	"tesao",	"testuda",	"tezuda",	"tezudo",	"the beauty box",	"the body shop",	"the body store",	"trocha",	"trolha",	"troucha",	"trouxa",	"troxa",	"vaca",	"vadia",	"vagabunda",	"vagabundo",	"vagina",	"valmari",	"veada",	"veadao",	"veadao",	"veado",	"viada",	"viadao",	"viadao",	"viado",	"vichy",	"vult",	"xana",	"xaninha",	"xavasca",	"xerereca",	"xexeca",	"xibiu",	"xibumba",	"xochota",	"xota",	"xoxota",	"yama",	"yes cosmetics",	"yves saint laurent"];
    }

}
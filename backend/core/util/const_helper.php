<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 03/10/2015
 * Time: 13:35
 */

define('IMAGE_WIDTH', 100);
define('IMAGE_HEIGHT', 100);

/**
 * Le todas as contantes definidas no PHP e entao retorna como
 * chave / valor. Quando houver um filtro, ira aplicar o filtro
 * desejado.
 * @param $filters array de strings
 * @param $inverseFilter quando TRUE, serve para negar o Filtro
 * @return array
 */
function _read_constants($filters, $inverseFilter = FALSE) {
    // Le todas as constantes
    $arr = get_defined_constants();
    // Prepara o array de resultado
    $arr_result = array();

    // Navega em todas as constantes definidas no PHP
    foreach($arr as $k => $v) {

        // Check
        $match = FALSE;

        // Navega nos filtros selecionados
        foreach($filters as $filter) {

            // Valida pelo tamanho do filtro
            $filter_size = strlen($filter);
            $key_size = strlen($k);
            $valid_key =
                $filters == NULL ||
                count($filters) <= 0 ||
                $filter_size < $key_size;

            // Se o tamanho nao for valido, vai para o proximo filtro
            if (FALSE === $valid_key) {
                continue;
            }

            // Verifica se a chave esta de acordo com o filtro
            $key_to_check = strtolower($k);
            $filter_to_check = strtolower($filter);
            $notFound = FALSE === stripos($key_to_check, $filter_to_check);

            // Match
            // Nao encontrado = BOM quando inverse
            // Nao encontrado = RUIM quando NAO inverse
            $match =
                (FALSE === $inverseFilter && FALSE === $notFound) ||
                (TRUE === $inverseFilter && TRUE === $notFound);

            if (TRUE === $match) {
                break;
            }
        }

        // Adiciona ao resultado
        if (TRUE === $match) {
            $arr_result[$k] = $v;
        }

    }
    return $arr_result;
}

function _key_value_as_table($arr_result) {
    $fixed_style = "style='font-family: Courier New'";
    $o  = "";
    $o .= "<table class='table table-striped'>";
    $o .= "<tr $fixed_style><th>LINE#</th><th>KEY</th><th>VALUE</th></tr>";
    $i = 0;

    foreach ($arr_result as $k => $v) {
        $i++;
        $o .= "<tr $fixed_style><td>$i</td><td>$k</td><td>$v</td></tr>";
    }
    $o .= "</table>";
    return  $o;
}

function _php_imagetype_img_gd_print_constants() {
    echo _key_value_as_table(_read_constants(array("PHP_VERSION", "PHP_RELEASE", "R_VERSION", "GD_", "MYSQL")));
}

function _gd_print_constants() {
    echo _key_value_as_table(_read_constants(array("GD_")));
}

function dbg($o, $isJson = FALSE) {
    if (TRUE === $isJson) {
        header('Content-Type: application/json');
        $o = json_encode($o);
    }
    print $o;
    exit();
}

function _toupper($data) {
    return mb_convert_case($data, MB_CASE_UPPER, 'UTF-8');
}

function dbgt($o) { dbg($o, FALSE); }
function dbgj($o) { dbg($o, TRUE);  }
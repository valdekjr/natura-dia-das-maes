<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 03/10/2015
 * Time: 17:01
 */

function _req_dic_val($array, $field, $default_value = "") {
    try {
        $value = isset( $array[$field] ) ? $array[$field] : $default_value;
        return $value;
    } catch (Exception $e) {
        return $default_value;
    }
}

function _req_get($field, $default_value = "") { return _req_dic_val($_GET, $field, $default_value); }
function _req_post($field, $default_value = "") { return _req_dic_val($_POST, $field, $default_value); }
<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 11/10/2015
 * Time: 15:32
 */

namespace Core;

require_once(__DIR__ . "/util/const_helper.php");

class LabelDataReader {

    const NOME_PADRAO = "NATURA";

    // Dados de usuario
    public $nome; // nome inteiro
    public $primeiroNome; // primeiro nome
    public $telefone; // telefone do usuario
    public $rede; // rede natura do usuario
    public $valido; // variavel que diz se os dados estao validos
    public $idImagem; // id da imagem / post que o usuario solicitou
    public $goo; // url gerada pelo usuario
    public $type; // tipo da imagem, w=whatsapp, usado para aplicar scale na imagem
    public $data; // dados encoded base 64

    // Dados de imagem
    public $imagens; // lista com todas as imagens
    public $imagem; // dados da imagem
    public $arquivo; // nome do arquivo
    public $altura; // altura do arquivo
    public $x; // x do label da imagem
    public $y; // y do label da imagem
    public $point; // point (x,y) da etiqueta imagem

    // Construtor
    function __construct() {
        $this->valido = FALSE;
        $this->defineData();
        $this->readData();
    }

    // Define o array de imagens
    protected function defineData() {
        $this->imagens = array (
            '01' => array( 'filename' => 'post_01.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '02' => array( 'filename' => 'post_02.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '03' => array( 'filename' => 'post_03.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '04' => array( 'filename' => 'post_04.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '05' => array( 'filename' => 'post_05.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '06' => array( 'filename' => 'post_06.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '07' => array( 'filename' => 'post_07.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '08' => array( 'filename' => 'post_08.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '09' => array( 'filename' => 'post_09.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '10' => array( 'filename' => 'post_10.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '11' => array( 'filename' => 'post_11.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '12' => array( 'filename' => 'post_12.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '13' => array( 'filename' => 'post_13.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '14' => array( 'filename' => 'post_14.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '15' => array( 'filename' => 'post_15.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '16' => array( 'filename' => 'post_16.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '17' => array( 'filename' => 'post_17.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '18' => array( 'filename' => 'post_18.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            '19' => array( 'filename' => 'post_19.jpg', 'x' => '0', 'y' => '500', 'height' => '90' ) ,
            'F01' => array( 'filename' => 'face_01.jpg', 'x' => '0', 'y' => '17', 'height' => '90' ) ,
            'E01' => array( 'filename' => 'email_01.jpg', 'x' => '300', 'y' => '1590', 'height' => '90' ));
    }

    // Faz a leitura e o tratamento dos dados
    protected function readData() {
        $this->idImagem = _req_get('post', '01');
        $this->type = _req_get('type');
        $this->goo = _req_get('goo');
        $this->data = _req_get('data');
        $this->nome = _req_get('nome');
        $this->telefone = _req_get('telefone');
        $this->rede = _req_get('rede');

        // Validar id da imagem
        $idValido = $this->validarIdImagem();
        if (FALSE === $idValido) {
            $this->idImagem = '01';
        }
        // Dados da Imagem
        $this->imagem   = $this->imagens[$this->idImagem];
        $this->arquivo  = "images/" . $this->imagem['filename'];
        $this->x        = intval($this->imagem['x']);
        $this->y        = intval($this->imagem['y']);
        $this->altura   = intval($this->imagem['height']);
        $this->point    = new \Core\Image\Point($this->x, $this->y);

        // Dados do usuario
        $this->goo = $this->validarGoo($this->goo);

        // Nome, telefone, rede
        if (!empty($this->data)) {
           $decode64 = base64_decode($this->data);
           if (FALSE === $decode64) {
               return;
           }
           $decodeUrl = urldecode($decode64);

           // Cria os parametros para parse da string
           $nome = '';
           $telefone = '';
           $rede = '';
           parse_str($decodeUrl);

           // Le os parametros
           $this->nome = $nome;
           $this->telefone = $telefone;
           $this->rede = $rede;
        }

        // Validar nome
        $this->nome = $this->validarNome();
        if (FALSE !== $this->nome) {
            $this->valido = TRUE;
        }
    }

    // Valida o id da imagem
    protected function validarIdImagem() {
        $id = (string)$this->idImagem;
        foreach($this->imagens as $k => $v) {
            $key = (string)$k;
            $_same = $key === $id;
            if ($_same) {
                return TRUE;
            }
        }
        return FALSE;
    }

    // Valida a url goo
    protected function validarGoo($goo) {
        // TODO: mudar URL padrao com chave da Salve
        // Url padrao gerada de natura - http://goo.gl/myzd5l
        // Quando nao houver, vai para URL padrao
        if ( '' === $goo ) {
            $goo = "myzd5l";
        }
        return $goo;
    }

    // Extrai o primeiro nome
    protected function primeiroNome($nome) {
        if ( is_string($nome) ) {
            $nome = trim($nome);
            $nomes = explode(" ", $nome);
            if (count($nomes) > 0) {
                return $nomes[0];
            }
        }
        return $nome;
    }

    // Valida as letras
    protected function validarLetras($nome) {
        $rex = '/^([A-Z,a-z, ,.,ã,Ã,á,Á,à,À,â,Ã,é,É,è,È,ê,Ê,í,Í,ì,Ì,î,Î,ó,Ó,ò,Ò,ô,Ô,õ,Õ,ú,Ú,ù,Ù,û,Û,ü,Ü,-])*$/i';
        $r   = preg_match($rex, $nome);
        return 1 == $r;
    }

    // Valida o nome
    protected function validarNome() {
        $nome = $this->nome;
        if ( is_string($nome) === FALSE ) {
            return FALSE;
        }
        $helper = new NomesBlacklistHelper();
        $nome         = trim($nome);
        $primeiroNome = _toupper($this->primeiroNome($nome));
        $sz           = strlen($nome) > 0;
        $vl           = $this->validarLetras($primeiroNome);
        $wl           = $helper->notInBlacklist($primeiroNome);
        // dbgj(array($nome, $primeiroNome, $sz, $vl, $wl));
        return
            FALSE === $sz || FALSE === $vl || FALSE === $wl
                ? FALSE
                : mb_strtoupper($primeiroNome);
    }

}

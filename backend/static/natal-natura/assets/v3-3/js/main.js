(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Banner = {
  init: function() {
    console.log('Banner: Mode=ON');
    this.setVars();
    this.setEl();
    this.playVideo();
    this.closeVideo();
    this._log('YoutubeVideo', new YoutubeVideo({}));
  },
  setVars: function() {
    this.videoElId = 'vh-player';
    this.videoSel = '#vh-player';
  },
  setEl: function() {
    this.$playButtons = $('.btn-natura-play-video');
    this.$bannerParts = $('.banner-modal-part');
    this.$btnCloseVideo = $('.btn-close-video');
    this.$playerContainer = $('#vh-player-container');
  },
  createPlayer: function(tagVideo, videoId) {
    var _$player = $("<div />");
    _$player.attr("id", this.videoElId);
    _$player.attr("class", this.videoElId);
    _$player.attr('data-tagvideo', tagVideo);
    _$player.attr('data-videoid', videoId);
    return _$player;
  },
  playVideo: function() {
    var _that = this;
    this.$playButtons.click(function() {
      var _el = this;
      var _this = $(this);
      var _videoId = _this.attr('data-videoid');
      _that.$playerContainer.empty();
      var _$player = _that.createPlayer('natura-natal', _videoId);
      _that.$playerContainer.append(_$player);
      this.video = new YoutubeVideo({
        el: _that.videoSel,
        playerVars: {
          autoplay: 0,
          showinfo: 0,
          rel: 0
        }
      });
      _that.$bannerParts.show();
      _that._log('BTN-PLAY-CLICK', _videoId, _el);
    });
  },
  closeVideo: function() {
    var _that = this;
    this.$btnCloseVideo.click(function() {
      _that.$playerContainer.empty();
      _that.$bannerParts.hide();
      _that._log('Close button clicked');
    });
  },
  _log: function() {
    try {
      console.log('Banner.Logger', arguments);
    } catch (ex) {}
  }
};
var $__default = Banner;
window.Banner = Banner;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/banner.js
},{}],2:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Blacklist = {
  blocked: ["acquaflora", "adcos", "adidas", "agua de cheiro", "amend", "ana hickmann", "antonio banderas", "anus", "avon", "avora", "babaca", "babaovo", "baba-ovo", "bacura", "bagos", "baitola", "bare minerals", "beauty box", "bebum", "bel col", "belcorp", "benefit", "benetton", "besta", "betty boop", "bicha", "bio art", "bioderm", "bionatus", "bisca", "bixa", "boazuda", "boceta", "boco", "boco", "boiola", "bolagato", "bolcat", "boquete", "bosseta", "bosta", "bostana", "botanic", "boticario", "boticario", "brecha", "brexa", "brioco", "bronha", "buca", "buceta", "bunda", "bunduda", "burberry", "burra", "burro", "busseta", "cachorra", "cachorro", "cadela", "cadiveu", "caga", "cagado", "cagao", "cagona", "calvin klein", "canalha", "caralho", "carolina herrera", "casseta", "cassete", "cetaphil", "checheca", "chereca", "chibumba", "chibumbo", "chifruda", "chifrudo", "chochota", "chota", "chupada", "chupado", "clean & clear", "clinique", "clitoris", "clitoris", "cocaina", "cocaina", "coco", "coco", "colorama", "contem 1g", "corna", "corno", "cornuda", "cornudo", "corrupta", "corrupto", "cretina", "cretino", "cruz-credo", "cu", "cu ", "culhao", "culhao", "culhoes", "curalho", "curaprox", "cuzao", "cuzao", "cuzuda", "cuzudo", "davene", "debil", "debiloide", "defunto", "demonio", "demonio", "diesel", "difunto", "dior", "doida", "doido", "dolve&gabbana", "dove", "ecologie", "egua", "egua", "embelezze", "escrota", "escroto", "esporrada", "esporrado", "esporro", "esporro", "estupida", "estupida", "estupidez", "estupido", "estupido", "eudora", "evian", "fator 5", "fdp", "fedida", "fedido", "fedor", "fedorenta", "feia", "feio", "feiosa", "feioso", "feioza", "feiozo", "felacao", "felacao", "fenda", "ferrari", "fiorentino", "foda", "fodao", "fodao", "fode", "fodida", "fodido", "fornica", "fudecao", "fudecao", "fudendo", "fudida", "fudido", "furada", "furado", "furao", "furao", "furnica", "furnicar", "furo", "furona", "gabriela sabatini", "gaiata", "gaiato", "gay", "gilette", "giorgio armani", "givenchy", "gonorrea", "gonorreia", "gosma", "gosmenta", "gosmento", "granado", "grelinho", "grelo", "gucci", "guess", "homosexual", "homo-sexual", "homossexual", "hugo boss", "idiota", "idiotice", "imbecil", "impala", "iscrota", "iscroto", "jaguar", "japa", "jequiti", "johnson & johnson", "joop", "juliana paes", "katy perry", "kenzo", "keune", "kolene", "koloss", "l�aqua di fiori", "l�occitane", "la roche-posay", "lacoste", "ladra", "ladrao", "ladrao", "ladroeira", "ladrona", "lady gaga", "lalau", "lalique", "lancome", "lanza", "leprosa", "leproso", "lesbica", "lesbica", "l'occitane au bresil", "loreal paris", "lush", "mac", "macaca", "macaco", "machona", "machorra", "mahogany", "mairibel", "manguaca", "manguaca", "mary kay", "masturba", "max love", "maybeline", "maybelline", "meleca", "merda", "mija", "mijada", "mijado", "mijo", "mocrea", "mocrea", "mocreia", "mocreia", "moleca", "moleque", "mondronga", "mondrongo", "montblanc", "naba", "nadega", "nazca", "neutrogena", "niasi", "nike", "nivea", "nojeira", "nojenta", "nojento", "nojo", "novex", "nus", "o boticario", "odorata", "olhota", "otaria", "otaria", "otario", "otario", "ox", "paca", "pantene", "paspalha", "paspalhao", "paspalho", "pau ", "payot", "peia", "peido", "pemba", "penis", "penis", "pentelha", "pentelho", "perereca", "peru", "peru", "petralha", "phytoervas", "pica", "picao", "picao", "pilantra", "piranha", "piroca", "piroco", "piru", "playboy", "porra", "prada", "prega", "prostibulo", "prostibulo", "prostituta", "prostituto", "puma", "punheta", "punhetao", "punhetao", "pus", "pustula", "pustula", "puta", "puto", "puxasaco", "puxa-saco", "quem disse berenice", "rabao", "rabao", "rabo", "rabuda", "rabudao", "rabudao", "rabudo", "rabudona", "racco", "racha", "rachada", "rachadao", "rachadao", "rachadinha", "rachadinho", "rachado", "ralph lauren", "ramela", "remela", "retardada", "retardado", "revlon", "ridicula", "ridicula", "risque", "rola", "rolinha", "rosca", "sacana", "safada", "safado", "sapatao", "sapatao", "sephora", "shakira", "shiseido", "sifilis", "sifilis", "siririca", "sundown", "tarada", "tarado", "tesao", "tesao", "testuda", "tezuda", "tezudo", "the beauty box", "the body shop", "the body store", "trocha", "trolha", "troucha", "trouxa", "troxa", "vaca", "vadia", "vagabunda", "vagabundo", "vagina", "valmari", "veada", "veadao", "veadao", "veado", "viada", "viadao", "viadao", "viado", "vichy", "vult", "xana", "xaninha", "xavasca", "xerereca", "xexeca", "xibiu", "xibumba", "xochota", "xota", "xoxota", "yama", "yes cosmetics", "yves saint laurent"],
  init: function() {
    console.log('Banner: Mode=ON');
  },
  validate: function(fullname) {
    try {
      fullname = fullname.trim();
      var invalidFullname = Blacklist.inBlacklist(fullname);
      if (true === invalidFullname) {
        return false;
      }
      fullname = fullname.trim();
      var names = fullname.split(' ');
      if (names && names.length > 0) {
        var firstname = names[0];
        return false === Blacklist.inBlacklist(firstname);
      }
    } catch (ex) {
      console.log('Blacklist', 'validate', 'fullname', fullname, ex);
    }
    return false;
  },
  inBlacklist: function(rawword) {
    var word_to_check = Blacklist.normaliseChars(rawword).toLowerCase().trim();
    if (word_to_check == '') {
      return false;
    }
    var blocked_word;
    var word_size;
    var CHECK_EQUALS_LEN = 3;
    var in_black_list;
    for (var i = 0; i < Blacklist.blocked.length; ++i) {
      blocked_word = Blacklist.normaliseChars(Blacklist.blocked[i]);
      word_size = blocked_word.length;
      if (0 <= word_size && word_size <= CHECK_EQUALS_LEN) {
        in_black_list = (word_to_check == blocked_word);
      } else {
        in_black_list = (word_to_check.indexOf(blocked_word) >= 0);
      }
      if (true === in_black_list) {
        return true;
      }
    }
    return false;
  },
  normaliseChars: function(text) {
    var invalid = '����������������������������������������������';
    var valid = 'aaaaaeeeeiiiiooooouuuucAAAAAEEEEIIIIOOOOOUUUUC';
    var nova = '';
    for (var i = 0; i < text.length; i++) {
      if (invalid.search(text.substr(i, 1)) >= 0) {
        nova += valid.substr(invalid.search(text.substr(i, 1)), 1);
      } else {
        nova += text.substr(i, 1);
      }
    }
    return nova;
  }
};
window.Blacklist = Blacklist;
var $__default = Blacklist;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/blacklist.js
},{}],3:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__blacklist_46_js__,
    $__produtos_46_js__,
    $__filtro_46_js__;
var Blacklist = ($__blacklist_46_js__ = require("./blacklist.js"), $__blacklist_46_js__ && $__blacklist_46_js__.__esModule && $__blacklist_46_js__ || {default: $__blacklist_46_js__}).default;
var Produtos = ($__produtos_46_js__ = require("./produtos.js"), $__produtos_46_js__ && $__produtos_46_js__.__esModule && $__produtos_46_js__ || {default: $__produtos_46_js__}).default;
var Filtro = ($__filtro_46_js__ = require("./filtro.js"), $__filtro_46_js__ && $__filtro_46_js__.__esModule && $__filtro_46_js__ || {default: $__filtro_46_js__}).default;
var Cndata = {
  DEFAULT_NAME: 'NATURA',
  data: ko.observable(''),
  nome: ko.observable(''),
  telefone: ko.observable(''),
  rede: ko.observable(''),
  exibirTelefone: ko.observable(false),
  init: function() {
    console.log('Cndata: Mode=ON');
    var data = Cndata.querystring("data");
    var decoded64 = base64.decode(data);
    var rawdata = '?' + decodeURIComponent(decoded64);
    var nome = Cndata.extractParam('nome', rawdata);
    var telefone = Cndata.extractParam('telefone', rawdata);
    var rede = Cndata.extractParam('rede', rawdata);
    this.bind();
    var _nome = nome;
    var blacklistValid = Blacklist.validate(nome);
    if (false === blacklistValid) {
      nome = Cndata.DEFAULT_NAME;
    } else {
      var nomes = nome.split(' ');
      if (nomes && nomes.length && nomes.length > 0) {
        nome = nomes[0];
      }
    }
    var nomeFormatado = (nome != '' ? (nome) : Cndata.DEFAULT_NAME).toUpperCase();
    Cndata.nome(nomeFormatado);
    Cndata.telefone(telefone);
    Cndata.rede(rede);
    Cndata.data(data);
  },
  ctaExibirTelefone: function() {
    if (this.isCommon()) {
      return false;
    }
    return !this.ctaExibirRede();
  },
  ctaExibirRede: function() {
    if (this.isCommon()) {
      return false;
    }
    var empty = this.rede().trim() == '';
    return !empty;
  },
  sendEventCompreAgora: function(label) {
    var evt = 'compre-agora';
    if (this.ctaExibirRede()) {
      evt += '-cn';
    }
    window.trackAnalytics('dia-das-maes-pagina', evt, label);
  },
  abrirEscolhido1: function(idx) {
    this.sendEventCompreAgora('produto-1');
    var p = window.Filtro.produto1()[idx];
    window.Produtos.abrirProduto(p);
  },
  abrirEscolhido2: function(idx) {
    this.sendEventCompreAgora('produto-2');
    var p = window.Filtro.produto2()[idx];
    window.Produtos.abrirProduto(p);
  },
  abrirFiltro: function(idx) {
    console.log('Cndata', 'abrirFiltro', idx);
    this.sendEventCompreAgora('filtro');
    var p = window.Filtro.produto0()[idx];
    window.Produtos.abrirProduto(p);
  },
  abrirCarrosel: function() {
    this.sendEventCompreAgora('carrossel-natura');
    var p = window.Produtos.currentCarrosel();
    p.utm_medium = 'Botao_Compre_Agora_Carrosel';
    window.Produtos.abrirProduto(p);
  },
  bind: function() {},
  hasCn: function() {
    return !Cndata.isCommon();
  },
  redirectIfHasNoCn: function() {},
  telefoneOrigem: function(origem) {
    if (origem === 'presentes') {
      return window.Filtro.produto0().nome;
    } else if (origem === 'carrosel') {
      var p = window.Produtos.currentCarrosel();
      if (false === p) {
        return 'geral';
      } else {
        return p.nome;
      }
    } else if (origem === 'escolha') {
      return 'lista-escolha-mais-presentes';
    } else {
      return 'geral';
    }
  },
  habilitarTelefone: function(origem) {
    var gaOrigem = Cndata.telefoneOrigem(origem);
    Cndata.exibirTelefone(true);
    trackAnalytics('natal-pagina', gastring(gaOrigem), 'ver-telefone');
  },
  isCommon: function() {
    return Cndata.data() == '';
  },
  extractParam: function(name, rawdata) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(rawdata);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
  },
  querystring: function(name) {
    return Cndata.extractParam(name, location.search);
  }
};
window.Cndata = Cndata;
var $__default = Cndata;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/cndata.js
},{"./blacklist.js":2,"./filtro.js":6,"./produtos.js":12}],4:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Datacache = {
  produtosArray: ko.observableArray([]),
  init: function() {
    console.log('Datacache: Mode=ON');
    this.createProdutos();
  },
  createProdutos: function() {
    this.produtosArray.removeAll();
    var items = ["53", "58", "59", "60", "61", "62", "63", "40", "42", "43", "44", "45", "46", "47", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "41", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22"];
    for (var i = 0; i < items.length; ++i) {
      var key = items[i];
      var p = this.produtos[key];
      if (p == null) {
        console.log('Datacache', 'createProdutos', 'key_not_found', key);
        continue;
      }
      if (p.hidevitrine) {
        continue;
      }
      p.vitrineBaseUrl = window.PersistData.baseUrl + p.vitrine;
      this.produtosArray.push(p);
    }
    return this.produtosArray;
  },
  "produtos": {
    "01": {
      "id": "01",
      "postid": "post01",
      "nome": "Mamãe e Bebê",
      "escolhidas": "/img/escolhidas/01.png",
      "lightbox": "/img/lightbox/01.jpg",
      "presentes": "/img/presentes/01.png",
      "vitrine": "/img/vitrine/01.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 130,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-mamae-e-bebe-agua-de-colonia-sabonete-liquido-condicionador-trocador-embalagem-56793",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "02": {
      "id": "02",
      "postid": "post02",
      "nome": "Essencial Estilo Masculino",
      "escolhidas": "/img/escolhidas/02.png",
      "lightbox": "/img/lightbox/02.jpg",
      "presentes": "/img/presentes/02.png",
      "vitrine": "/img/vitrine/02.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 179,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-essencial-estilo-masculino-deo-parfum-gel-pos-barba-sabonete-em-barra-embalagem-56566",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "03": {
      "id": "03",
      "postid": "post03",
      "nome": "Kaiak Expedição",
      "escolhidas": "/img/escolhidas/03.png",
      "lightbox": "/img/lightbox/03.jpg",
      "presentes": "/img/presentes/03.png",
      "vitrine": "/img/vitrine/03.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 136.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-kaiak-expedicao-desodorante-colonia-shampoo-gel-para-barbear-gel-apos-barba-gel-fixador-embalagem-56985",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "04": {
      "id": "04",
      "postid": "post04",
      "nome": "Humor a Rigor",
      "escolhidas": "/img/escolhidas/04.png",
      "lightbox": "/img/lightbox/04.jpg",
      "presentes": "/img/presentes/04.png",
      "vitrine": "/img/vitrine/04.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 94.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-humor-a-rigor-desodorante-colonia-shampoo-sabonete-liquido-embalagem-56792",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "05": {
      "id": "05",
      "postid": "post05",
      "nome": "#Urbano ",
      "escolhidas": "/img/escolhidas/05.png",
      "lightbox": "/img/lightbox/05.jpg",
      "presentes": "/img/presentes/05.png",
      "vitrine": "/img/vitrine/05.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 101.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-urbano-desodorante-colonia-shampoo-desodorante-spray-embalagem-56571",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "06": {
      "id": "06",
      "postid": "post06",
      "nome": "#Urbano Barba",
      "escolhidas": "/img/escolhidas/06.png",
      "lightbox": "/img/lightbox/06.jpg",
      "presentes": "/img/presentes/06.png",
      "vitrine": "/img/vitrine/06.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 47.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-urbano-barba-gel-para-barbear-gel-pos-barba-sabonete-em-barra-embalagem-57304",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "07": {
      "id": "07",
      "postid": "post07",
      "nome": "Naturé Mocinhas",
      "escolhidas": "/img/escolhidas/07.png",
      "lightbox": "/img/lightbox/07.jpg",
      "presentes": "/img/presentes/07.png",
      "vitrine": "/img/vitrine/07.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 59.2,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-nature-mocinhas-colonia-mocinhas-sabonete-em-barra-mochila-embalagem-56570",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "08": {
      "id": "08",
      "postid": "post08",
      "nome": "Naturé Mocinhos",
      "escolhidas": "/img/escolhidas/08.png",
      "lightbox": "/img/lightbox/08.jpg",
      "presentes": "/img/presentes/08.png",
      "vitrine": "/img/vitrine/08.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 59.2,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-nature-mocinhos-colonia-mocinhos-sabonete-em-barra-mochila-embalagem-56581",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "09": {
      "id": "09",
      "postid": "post09",
      "nome": "Luna",
      "escolhidas": "/img/escolhidas/09.png",
      "lightbox": "/img/lightbox/09.jpg",
      "presentes": "/img/presentes/09.png",
      "vitrine": "/img/vitrine/09.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 124,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-luna-rose-desodorante-colonia-oleo-corporal-embalagem-56567",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "10": {
      "id": "10",
      "postid": "post10",
      "nome": "Essencial Feminino",
      "escolhidas": "/img/escolhidas/10.png",
      "lightbox": "/img/lightbox/10.jpg",
      "presentes": "/img/presentes/10.png",
      "vitrine": "/img/vitrine/10.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 172,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-essencial-feminino-deo-parfum-desodorante-hidratante-sabonete-liquido-embalagem-56569",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "11": {
      "id": "11",
      "postid": "post11",
      "nome": "Tododia Macadâmia",
      "escolhidas": "/img/escolhidas/11.png",
      "lightbox": "/img/lightbox/11.jpg",
      "presentes": "/img/presentes/11.png",
      "vitrine": "/img/vitrine/11.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 79.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-tododia-macadamia-sabonete-creme-de-banho-hidratante-creme-para-as-maos-creme-para-os-pes-embalagem-56574",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "12": {
      "id": "12",
      "postid": "post12",
      "nome": "Aquarela",
      "escolhidas": "/img/escolhidas/12.png",
      "lightbox": "/img/lightbox/12.jpg",
      "presentes": "/img/presentes/12.png",
      "vitrine": "/img/vitrine/12.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 64.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-aquarela-estojo-de-sombras-embalagem-56582",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "13": {
      "id": "13",
      "postid": "post13",
      "nome": "Águas Banho de Energia",
      "escolhidas": "/img/escolhidas/13.png",
      "lightbox": "/img/lightbox/13.jpg",
      "presentes": "/img/presentes/13.png",
      "vitrine": "/img/vitrine/13.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 46.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-aguas-banho-de-energia-desodorante-colonia-sabonete-em-barra-embalagem-56563",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "14": {
      "id": "14",
      "postid": "post14",
      "nome": "Biografia Desperte Feminino",
      "escolhidas": "/img/escolhidas/14.png",
      "lightbox": "/img/lightbox/14.jpg",
      "presentes": "/img/presentes/14.png",
      "vitrine": "/img/vitrine/14.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 89.8,
      "linkrede": "http://rede.natura.net/espaco/presente-biografia-desperte-feminino-desodorante-colonia-embalagem-56579",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "15": {
      "id": "15",
      "postid": "post15",
      "nome": "Ekos Polpas dos Biomas Amazônicos",
      "escolhidas": "/img/escolhidas/15.png",
      "lightbox": "/img/lightbox/15.jpg",
      "presentes": "/img/presentes/15.png",
      "vitrine": "/img/vitrine/15.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 44.5,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-ekos-polpas-dos-biomas-amazonicos-3und-de-40g-embalagem-56578",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "16": {
      "id": "16",
      "postid": "post16",
      "nome": "Ekos Sabonetes dos Biomas Amazônicos",
      "escolhidas": "/img/escolhidas/16.png",
      "lightbox": "/img/lightbox/16.jpg",
      "presentes": "/img/presentes/16.png",
      "vitrine": "/img/vitrine/16.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 30.5,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-ekos-sabonetes-dos-biomas-amazonicos-6und-de-50g-56333",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "17": {
      "id": "17",
      "postid": "post17",
      "nome": "Essencial Feminino - Coleção Especial",
      "escolhidas": "/img/escolhidas/17.png",
      "lightbox": "/img/lightbox/17.jpg",
      "presentes": "/img/presentes/17.png",
      "vitrine": "/img/vitrine/17.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 65,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-essencial-feminino-colecao-especial-deo-parfum-embalagem-56166",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "18": {
      "id": "18",
      "postid": "post18",
      "nome": "Ekos Pitanga Preta",
      "escolhidas": "/img/escolhidas/18.png",
      "lightbox": "/img/lightbox/18.jpg",
      "presentes": "/img/presentes/18.png",
      "vitrine": "/img/vitrine/18.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 86.3,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-ekos-pitanga-preta-desodorante-colonia-polpa-para-as-maos-sabonete-liquido-embalagem-56538",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "19": {
      "id": "19",
      "postid": "post19",
      "nome": "Sève",
      "escolhidas": "/img/escolhidas/19.png",
      "lightbox": "/img/lightbox/19.jpg",
      "presentes": "/img/presentes/19.png",
      "vitrine": "/img/vitrine/19.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 74.5,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-seve-oleo-desodorante-corporal-embalagem-56564",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "20": {
      "id": "20",
      "postid": "post20",
      "nome": "Humor Perfeito",
      "escolhidas": "/img/escolhidas/20.png",
      "lightbox": "/img/lightbox/20.jpg",
      "presentes": "/img/presentes/20.png",
      "vitrine": "/img/vitrine/20.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 94.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-humor-perfeito-desodorante-colonia-desodorante-hidratante-embalagem-56575",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "21": {
      "id": "21",
      "postid": "post21",
      "nome": "Tododia Amora e Amêndoas",
      "escolhidas": "/img/escolhidas/21.png",
      "lightbox": "/img/lightbox/21.jpg",
      "presentes": "/img/presentes/21.png",
      "vitrine": "/img/vitrine/21.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 23.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-tododia-amora-e-amendoas-sabonete-em-barra-desodorante-hidratante-creme-para-as-maos-embalagem-56572",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "22": {
      "id": "22",
      "postid": "post22",
      "nome": "Crer pra ver",
      "escolhidas": "/img/escolhidas/22.png",
      "lightbox": "/img/lightbox/22.jpg",
      "presentes": "/img/presentes/22.png",
      "vitrine": "/img/vitrine/22.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 29.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-crer-para-ver-livro-lapis-de-colorir-embalagem-59065",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "23": {
      "id": "23",
      "postid": "post23",
      "nome": "Look Festa",
      "escolhidas": "/img/escolhidas/23.png",
      "lightbox": "/img/lightbox/23.jpg",
      "presentes": "/img/presentes/23.png",
      "vitrine": "/img/vitrine/23.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 129,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/una-a2",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "24": {
      "id": "24",
      "postid": "post24",
      "nome": "Look Clássico",
      "escolhidas": "/img/escolhidas/24.png",
      "lightbox": "/img/lightbox/24.jpg",
      "presentes": "/img/presentes/24.png",
      "vitrine": "/img/vitrine/24.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 58.9,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/una-a2",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "25": {
      "id": "25",
      "postid": "post25",
      "nome": "Look Express",
      "escolhidas": "/img/escolhidas/25.png",
      "lightbox": "/img/lightbox/25.jpg",
      "presentes": "/img/presentes/25.png",
      "vitrine": "/img/vitrine/25.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 44.5,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/faces-56b",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "26": {
      "id": "26",
      "postid": "post26",
      "nome": "Amó Feminino",
      "escolhidas": "/img/escolhidas/26.png",
      "lightbox": "/img/lightbox/26.jpg",
      "presentes": "/img/presentes/26.png",
      "vitrine": "/img/vitrine/26.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 108,
      "linkrede": "http://rede.natura.net/espaco/amo-desodorante-colonia-feminino-75ml-52811",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "27": {
      "id": "27",
      "postid": "post27",
      "nome": "Amó Masculino",
      "escolhidas": "/img/escolhidas/27.png",
      "lightbox": "/img/lightbox/27.jpg",
      "presentes": "/img/presentes/27.png",
      "vitrine": "/img/vitrine/27.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 108,
      "linkrede": "http://rede.natura.net/espaco/amo-desodorante-colonia-masculino-75ml-52809",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "28": {
      "id": "28",
      "postid": "post28",
      "nome": "Una versão exclusiva",
      "escolhidas": "/img/escolhidas/28.png",
      "lightbox": "/img/lightbox/28.jpg",
      "presentes": "/img/presentes/28.png",
      "vitrine": "/img/vitrine/28.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 179,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-una-vaporizador-refis-deo-parfum-estojo-54228",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "29": {
      "id": "29",
      "postid": "post29",
      "nome": "Amis",
      "escolhidas": "/img/escolhidas/29.png",
      "lightbox": "/img/lightbox/29.jpg",
      "presentes": "/img/presentes/29.png",
      "vitrine": "/img/vitrine/29.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 65,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-amis-desodorante-colonia-balm-labial-glitter-iluminador-desodorante-hidratante-embalagem-56535",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "30": {
      "id": "30",
      "postid": "post30",
      "nome": "Kriska Descoberta",
      "escolhidas": "/img/escolhidas/30.png",
      "lightbox": "/img/lightbox/30.jpg",
      "presentes": "/img/presentes/30.png",
      "vitrine": "/img/vitrine/30.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 79.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-kriska-descoberta-desodorante-colonia-desodorante-hidratante-sabonete-em-barra-embalagem-56988",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "31": {
      "id": "31",
      "postid": "post31",
      "nome": "Tododia Sabonetes Sortidos",
      "escolhidas": "/img/escolhidas/31.png",
      "lightbox": "/img/lightbox/31.jpg",
      "presentes": "/img/presentes/31.png",
      "vitrine": "/img/vitrine/31.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 21.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-tododia-sabonetes-sortidos-6und-de-90g-55235",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "32": {
      "id": "32",
      "postid": "post32",
      "nome": "Tododia Pera e Lichia",
      "escolhidas": "/img/escolhidas/32.png",
      "lightbox": "/img/lightbox/32.jpg",
      "presentes": "/img/presentes/32.png",
      "vitrine": "/img/vitrine/32.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 42.5,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-tododia-pera-e-lichia-desodorante-hidratante-sabonete-liquido-embalagem-56794",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "33": {
      "id": "33",
      "postid": "post33",
      "nome": "Ekos Óleos Biomas da Amazônia",
      "escolhidas": "/img/escolhidas/33.png",
      "lightbox": "/img/lightbox/33.jpg",
      "presentes": "/img/presentes/33.png",
      "vitrine": "/img/vitrine/33.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 75.6,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-ekos-oleos-biomas-da-amazonia-3-oleos-embalagem-56568",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "34": {
      "id": "34",
      "postid": "post34",
      "nome": "Sr. N",
      "escolhidas": "/img/escolhidas/34.png",
      "lightbox": "/img/lightbox/34.jpg",
      "presentes": "/img/presentes/34.png",
      "vitrine": "/img/vitrine/34.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 57.7,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-srn-balm-apos-barba-creme-de-barbear-shampoo-sabonete-em-barra-embalagem-56986",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "35": {
      "id": "35",
      "postid": "post35",
      "nome": "Kaiak",
      "escolhidas": "/img/escolhidas/35.png",
      "lightbox": "/img/lightbox/35.jpg",
      "presentes": "/img/presentes/35.png",
      "vitrine": "/img/vitrine/35.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 124.6,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-Kaiak-3-desodorante-colonia-embalagem-57369",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "36": {
      "id": "36",
      "postid": "post36",
      "nome": "Crer Para Ver Feminino",
      "escolhidas": "/img/escolhidas/36.png",
      "lightbox": "/img/lightbox/36.jpg",
      "presentes": "/img/presentes/36.png",
      "vitrine": "/img/vitrine/36.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 40.3,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-crer-para-ver-vestido-infantil-embalagem-60027",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "37": {
      "id": "37",
      "postid": "post37",
      "nome": "Crer Para Ver Masculino",
      "escolhidas": "/img/escolhidas/37.png",
      "lightbox": "/img/lightbox/37.jpg",
      "presentes": "/img/presentes/37.png",
      "vitrine": "/img/vitrine/37.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 34.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-crer-para-ver-camiseta-infantil-masculina-embalagem-60024",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "38": {
      "id": "38",
      "postid": "post38",
      "nome": "Una Look Natal Sofisticado",
      "escolhidas": "/img/escolhidas/38.png",
      "lightbox": "/img/lightbox/38.jpg",
      "presentes": "/img/presentes/38.png",
      "vitrine": "/img/vitrine/38.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 89.8,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/una-a2",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "39": {
      "id": "39",
      "postid": "post39",
      "nome": "Aquarela Look Olhar Poderoso",
      "escolhidas": "/img/escolhidas/39.png",
      "lightbox": "/img/lightbox/39.jpg",
      "presentes": "/img/presentes/39.png",
      "vitrine": "/img/vitrine/39.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 57.6,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/aquarela-49b",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "40": {
      "id": "40",
      "postid": "post40",
      "nome": "Biografia Feminino",
      "escolhidas": "/img/escolhidas/40.png",
      "lightbox": "/img/lightbox/40.jpg",
      "presentes": "/img/presentes/40.png",
      "vitrine": "/img/vitrine/40.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 39.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-biografia-feminino-sabonete-em-barra-desodorante-hidratante-embalagem-60583",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "41": {
      "id": "41",
      "postid": "post41",
      "nome": "Biografia Desperte Masculino",
      "escolhidas": "/img/escolhidas/41.png",
      "lightbox": "/img/lightbox/41.jpg",
      "presentes": "/img/presentes/41.png",
      "vitrine": "/img/vitrine/41.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 99.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-biografia-desperte-masculino-desodorante-colonia-shampoo-gel-fixador-embalagem-56580",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "42": {
      "id": "42",
      "postid": "post42",
      "nome": "Ekos Açaí",
      "escolhidas": "/img/escolhidas/42.png",
      "lightbox": "/img/lightbox/42.jpg",
      "presentes": "/img/presentes/42.png",
      "vitrine": "/img/vitrine/42.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 55.8,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-ekos-acai-sabonete-liquido-polpa-hidratante-embalagem-56791",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "43": {
      "id": "43",
      "postid": "post43",
      "nome": "Ekos Castanha",
      "escolhidas": "/img/escolhidas/43.png",
      "lightbox": "/img/lightbox/43.jpg",
      "presentes": "/img/presentes/43.png",
      "vitrine": "/img/vitrine/43.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 72.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-ekos-castanha-polpa-hidratante-oleo-desodorante-hidratante-sabonetes-em-barra-embalagem-60458",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "44": {
      "id": "44",
      "postid": "post44",
      "nome": "Ekos Priprioca",
      "escolhidas": "/img/escolhidas/44.png",
      "lightbox": "/img/lightbox/44.jpg",
      "presentes": "/img/presentes/44.png",
      "vitrine": "/img/vitrine/44.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 114.6,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-ekos-priprioca-essencia-desodorante-hidratante-polpa-hidratante-embalagem-56536",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "45": {
      "id": "45",
      "postid": "post45",
      "nome": "Faces Look Prático",
      "escolhidas": "/img/escolhidas/45.png",
      "lightbox": "/img/lightbox/45.jpg",
      "presentes": "/img/presentes/45.png",
      "vitrine": "/img/vitrine/45.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 39.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-faces-look-pratico-mascara-fantastica-batom-necessaire-embalagem-58215",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "46": {
      "id": "46",
      "postid": "post46",
      "nome": "Aquarela Look Combinações Ousadas",
      "escolhidas": "/img/escolhidas/46.png",
      "lightbox": "/img/lightbox/46.jpg",
      "presentes": "/img/presentes/46.png",
      "vitrine": "/img/vitrine/46.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 57.6,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/aquarela-49b",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "47": {
      "id": "47",
      "postid": "post47",
      "nome": "Crer Para Ver",
      "escolhidas": "/img/escolhidas/47.png",
      "lightbox": "/img/lightbox/47.jpg",
      "presentes": "/img/presentes/47.png",
      "vitrine": "/img/vitrine/47.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 29,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-crer-para-ver-lapis-minicadernos-embalagem-59610",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "48": {
      "id": "48",
      "postid": "post48",
      "nome": "Esta Flor",
      "escolhidas": "/img/escolhidas/48.png",
      "lightbox": "/img/lightbox/48.jpg",
      "presentes": "/img/presentes/48.png",
      "vitrine": "/img/vitrine/48.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 158,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/esta-flor-cat1960007",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "49": {
      "id": "49",
      "postid": "post49",
      "nome": "Una Look Natal Prático e Marcante",
      "escolhidas": "/img/escolhidas/49.png",
      "lightbox": "/img/lightbox/49.jpg",
      "presentes": "/img/presentes/49.png",
      "vitrine": "/img/vitrine/49.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 64,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/una-a2",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "50": {
      "id": "50",
      "postid": "post50",
      "nome": "Ekos Coleção Óleos concentrados",
      "escolhidas": "/img/escolhidas/50.png",
      "lightbox": "/img/lightbox/50.jpg",
      "presentes": "/img/presentes/50.png",
      "vitrine": "/img/vitrine/50.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 129,
      "linkrede": "http://rede.natura.net/espaco/colecao-de-oleos-concentrados-ekos-4-frascos-de-30ml-52067",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "51": {
      "id": "51",
      "postid": "post51",
      "nome": "Naturé Kit Diversão e Proteção Submarino Meninas",
      "escolhidas": "/img/escolhidas/51.png",
      "lightbox": "/img/lightbox/51.jpg",
      "presentes": "/img/presentes/51.png",
      "vitrine": "/img/vitrine/51.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 59.9,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/nature-67b",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "52": {
      "id": "52",
      "postid": "post52",
      "nome": "Naturé Kit Diversão e Proteção Submarino Meninos",
      "escolhidas": "/img/escolhidas/52.png",
      "lightbox": "/img/lightbox/52.jpg",
      "presentes": "/img/presentes/52.png",
      "vitrine": "/img/vitrine/52.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 59.9,
      "linkrede": "http://rede.natura.net/espaco/nossos-produtos/nature-67b",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "53": {
      "id": "53",
      "postid": "post53",
      "nome": "Homem",
      "escolhidas": "/img/escolhidas/53.png",
      "lightbox": "/img/lightbox/53.jpg",
      "presentes": "/img/presentes/53.png",
      "vitrine": "/img/vitrine/53.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 117,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-homem-desodorante-spray-desodorante-colonia-60455",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "54": {
      "id": "54",
      "postid": "post54",
      "nome": "Homem Madeiras",
      "escolhidas": "/img/escolhidas/54.png",
      "lightbox": "/img/lightbox/54.jpg",
      "presentes": "/img/presentes/54.png",
      "vitrine": "/img/vitrine/54.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 117,
      "linkrede": "http://rede.natura.net/espaco/desodorante-colonia-natura-homem-madeiras-100ml-53254",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "55": {
      "id": "55",
      "postid": "post55",
      "nome": "#Urbano Recria",
      "escolhidas": "/img/escolhidas/55.png",
      "lightbox": "/img/lightbox/55.jpg",
      "presentes": "/img/presentes/55.png",
      "vitrine": "/img/vitrine/55.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 79.8,
      "linkrede": "http://rede.natura.net/espaco/desodorante-colonia-urbano-recria-masculino-100ml-55111",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "56": {
      "id": "56",
      "postid": "post56",
      "nome": "#Urbano Recria + Mochila Branco e Preto",
      "escolhidas": "/img/escolhidas/56.png",
      "lightbox": "/img/lightbox/56.jpg",
      "presentes": "/img/presentes/56.png",
      "vitrine": "/img/vitrine/56.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 154.8,
      "linkrede": "http://rede.natura.net/espaco/conjunto-exclusivo-desodorante-colonia-mochila-56412",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "57": {
      "id": "57",
      "postid": "post57",
      "nome": "#Urbano Recria + Mochila Colorida",
      "escolhidas": "/img/escolhidas/57.png",
      "lightbox": "/img/lightbox/57.jpg",
      "presentes": "/img/presentes/57.png",
      "vitrine": "/img/vitrine/57.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 154.8,
      "linkrede": "http://rede.natura.net/espaco/conjunto-exclusivo-desodorante-colonia-mochila-56412",
      "extra1": "",
      "extra2": "",
      "hidevitrine": true
    },
    "58": {
      "id": "58",
      "postid": "post58",
      "nome": "Tododia Frutas Vermelhas",
      "escolhidas": "/img/escolhidas/58.png",
      "lightbox": "/img/lightbox/58.jpg",
      "presentes": "/img/presentes/58.png",
      "vitrine": "/img/vitrine/58.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 36.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-tododia-frutas-vermelhas-desodorante-hidratante-corporal-sabonetes-em-barra-60459",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "59": {
      "id": "59",
      "postid": "post59",
      "nome": "Mamãe e Bebê",
      "escolhidas": "/img/escolhidas/59.png",
      "lightbox": "/img/lightbox/59.jpg",
      "presentes": "/img/presentes/59.png",
      "vitrine": "/img/vitrine/59.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 56.5,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-mamae-e-bebe-agua-de-colonia-sabonete-em-barra-bolsa-60463",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "60": {
      "id": "60",
      "postid": "post60",
      "nome": "Una",
      "escolhidas": "/img/escolhidas/60.png",
      "lightbox": "/img/lightbox/60.jpg",
      "presentes": "/img/presentes/60.png",
      "vitrine": "/img/vitrine/60.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 84.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-aquarela-estojo-de-sombras-batom-balm-rosa-61090",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "61": {
      "id": "61",
      "postid": "post61",
      "nome": "Tododia Framboesa e Pimenta Rosa",
      "escolhidas": "/img/escolhidas/61.png",
      "lightbox": "/img/lightbox/61.jpg",
      "presentes": "/img/presentes/61.png",
      "vitrine": "/img/vitrine/61.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 49.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-tododia-framboesa-e-pimenta-rosa-creme-hidratante-para-maos-creme-desodorante-para-os-pes-desodorante-hidratante-corporal-60454",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "62": {
      "id": "62",
      "postid": "post62",
      "nome": "Una",
      "escolhidas": "/img/escolhidas/62.png",
      "lightbox": "/img/lightbox/62.jpg",
      "presentes": "/img/presentes/62.png",
      "vitrine": "/img/vitrine/62.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 210,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-una-desodorante-hidratante-corporal-deo-parfum-necessaire-60456",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    },
    "63": {
      "id": "63",
      "postid": "post63",
      "nome": "Una",
      "escolhidas": "/img/escolhidas/63.png",
      "lightbox": "/img/lightbox/63.jpg",
      "presentes": "/img/presentes/63.png",
      "vitrine": "/img/vitrine/63.png",
      "filtroselprenome": "PRESENTE NATURA",
      "preco": 95.9,
      "linkrede": "http://rede.natura.net/espaco/presente-natura-una-oleo-desodorante-corporal-batom-intenso-necessaire-60466",
      "extra1": "",
      "extra2": "",
      "hidevitrine": false
    }
  },
  "filtros": {
    "ocasiao": [{
      "id": "A",
      "ocasiao": "Amigo Secreto"
    }, {
      "id": "B",
      "ocasiao": "Ano Novo"
    }, {
      "id": "C",
      "ocasiao": "Natal"
    }],
    "quem": [{
      "id": "A",
      "quem": "Para ela",
      "genero": "F"
    }, {
      "id": "B",
      "quem": "Para ele",
      "genero": "M"
    }, {
      "id": "C",
      "quem": "Para menina",
      "genero": "F"
    }, {
      "id": "D",
      "quem": "Para menino",
      "genero": "M"
    }, {
      "id": "E",
      "quem": "Para bebê",
      "genero": "M"
    }],
    "apessoae": [{
      "id": "A",
      "F": "Autêntica",
      "M": "Autêntico"
    }, {
      "id": "B",
      "F": "Conectada com a natureza",
      "M": "Conectado com a natureza"
    }, {
      "id": "C",
      "F": "Criativa",
      "M": "Criativo"
    }, {
      "id": "D",
      "F": "Divertida",
      "M": "Divertido"
    }, {
      "id": "E",
      "F": "Mãezona",
      "M": "Paizão"
    }, {
      "id": "F",
      "F": "Moderninha",
      "M": "Moderninho"
    }, {
      "id": "G",
      "F": "Romântica",
      "M": "Romântico"
    }, {
      "id": "H",
      "F": "Vaidosa",
      "M": "Vaidoso"
    }],
    "relacao": [{
      "id": "A",
      "F": "Amor",
      "M": "Amor"
    }, {
      "id": "B",
      "F": "Avó",
      "M": "Avô"
    }, {
      "id": "C",
      "F": "Chefe",
      "M": "Chefe"
    }, {
      "id": "D",
      "F": "Colega de trabalho",
      "M": "Colega de trabalho"
    }, {
      "id": "E",
      "F": "Filha",
      "M": "Filho"
    }, {
      "id": "F",
      "F": "Mãe",
      "M": "Pai"
    }, {
      "id": "G",
      "F": "Melhor amiga",
      "M": "Melhor amigo"
    }, {
      "id": "H",
      "F": "Prima",
      "M": "Primo"
    }, {
      "id": "I",
      "F": "Professora",
      "M": "Professor"
    }, {
      "id": "J",
      "F": "Sobrinha",
      "M": "Sobrinho"
    }, {
      "id": "K",
      "F": "Sogra",
      "M": "Sogro"
    }, {
      "id": "L",
      "F": "Tia",
      "M": "Tio"
    }, {
      "id": "M",
      "F": "Vizinha",
      "M": "Vizinho"
    }],
    "valor": [{
      "id": "A",
      "label": "De R$ 20,00 até R$ 50,00",
      "min": 20,
      "max": 50
    }, {
      "id": "B",
      "label": "Entre R$ 50,00 até R$ 80,00",
      "min": 50,
      "max": 80
    }, {
      "id": "C",
      "label": "De R$ 80,00 até R$ 110,00",
      "min": 80,
      "max": 110
    }, {
      "id": "D",
      "label": "Acima de R$ 110,00",
      "min": 110,
      "max": 999999
    }]
  },
  "frases": {
    "E": {
      "M": ["Seu filho vai curtir tanto este PRESENTÃO que vai até compartilhar nas redes sociais.", "Seu filho vai dizer que você acertou em cheio com esta RECORDAÇÃO INESQUECÍVEL.", "Você vai fazer a alegria do filho com uma LEMBRANÇA dessas. "],
      "F": ["Sua filha vai curtir tanto este PRESENTÃO que vai até compartilhar nas redes sociais.", "Sua filha vai dizer que você acertou em cheio com esta RECORDAÇÃO INESQUECÍVEL.", "Você vai fazer a alegria da filha com uma LEMBRANÇA dessas."]
    },
    "A": {
      "M": ["Todo amor merece uma RECORDAÇÃO INESQUECÍVEL para deixar o Natal ainda mais apaixonante.", "Seu amor + um PRESENTÃO = a fórmula perfeita para ganhar um monte de beijos.", "Seu amor vai fazer aquele olhar apaixonado enquanto abre esta LEMBRANÇA."],
      "F": ["Todo amor merece uma RECORDAÇÃO INESQUECÍVEL para deixar o Natal ainda mais apaixonante.", "Seu amor + um PRESENTÃO = a fórmula perfeita para ganhar um monte de beijos.", "Seu amor vai fazer aquele olhar apaixonado enquanto abre esta LEMBRANÇA."]
    },
    "D": {
      "M": ["Para aquele colega de trabalho que almoça junto, um PRESENTE especial.", "O colega de trabalho que está sempre por perto para ajudar também merece uma LEMBRANÇA.", "O colega de trabalho vai votar em você para funcionário do mês depois deste PRESENTÃO. "],
      "F": ["Para aquele colega de trabalho que almoça junto, um PRESENTE especial.", "A colega de trabalho que está sempre por perto para ajudar também merece uma LEMBRANÇA.", "A colega de trabalho vai votar em você para funcionário do mês depois deste PRESENTÃO."]
    },
    "L": {
      "M": ["Tio que é tio abre a LEMBRANÇA e já aperta suas bochechas para agradecer.", "Aquele tio vai até esquecer a piada do pavê depois deste PRESENTÃO.", "Tem sempre aquele tio que derrama lágrimas de emoção quando ganha uma RECORDAÇÃO INESQUECÍVEL."],
      "F": ["Tia que é tia abre a LEMBRANÇA e já aperta suas bochechas para agradecer.", "Aquela tia vai até esquecer a piada do pavê depois deste PRESENTÃO.", "Tem sempre aquela tia que derrama lágrimas de emoção quando ganha uma RECORDAÇÃO INESQUECÍVEL."]
    },
    "B": {
      "M": ["Avô sempre acha uma fofura receber uma LEMBRANÇA do neto querido.", "O avô vai criar um clima de final de novela para abrir a RECORDAÇÃO INESQUECÍVEL.", "Seu avô vai se emocionar com o tanto de carinho que vem junto deste PRESENTÃO."],
      "F": ["Avó sempre acha uma fofura receber uma LEMBRANÇA do neto querido.", "A avó vai criar um clima de final de novela para abrir a RECORDAÇÃO INESQUECÍVEL.", "Sua avó vai se emocionar com o tanto de carinho que vem junto deste PRESENTÃO."]
    },
    "K": {
      "M": ["Seu sogro estava só esperando o PRESENTÃO para agradecer por você ter entrado pra família.", "Nada como agradar o sogro com uma RECORDAÇÃO INESQUECÍVEL para ele saber o quanto é especial.", "Abrace seu sogro quando entregar este PRESENTE. Essa cena vai virar foto de porta-retrato."],
      "F": ["Sua sogra estava só esperando o PRESENTÃO para agradecer por você ter entrado pra família.", "Nada como agradar a sogra com uma RECORDAÇÃO INESQUECÍVEL para ela saber o quanto é especial.", "Abrace sua sogra quando entregar este PRESENTE. Essa cena vai virar foto de porta-retrato."]
    },
    "M": {
      "M": ["Aquele vizinho merece um PRESENTE por sempre regar suas plantas. ", "Vizinho que compartilha a senha do wi-fi também merece uma LEMBRANÇA.", "Sabe o vizinho que sempre lembra de você na hora da carona? Dê para ele uma RECORDAÇÃO ESPECIAL. "],
      "F": ["Aquela vizinha merece um PRESENTE por sempre regar suas plantas. ", "Vizinha que compartilha a senha do wi-fi também merece uma LEMBRANÇA.", "Sabe a vizinha que sempre lembra de você na hora da carona? Dê para ela uma RECORDAÇÃO ESPECIAL. "]
    },
    "C": {
      "M": ["O chefe vai dar os parabéns por escolher uma LEMBRANÇA que combina perfeitamente com ele.", "Seu chefe vai comemorar dizendo que este PRESENTE veio no melhor prazo.", "Surpresa especial para o chefe que está sempre presente: um PRESENTÃO de Natal. "],
      "F": ["A chefe vai dar os parabéns por escolher uma LEMBRANÇA que combina perfeitamente com ela. ", "Sua chefe vai comemorar dizendo que este PRESENTE veio no melhor prazo.", "Surpresa especial para o chefe que está sempre presente: um PRESENTÃO de Natal. "]
    },
    "I": {
      "M": ["Seu professor vai achar esta LEMBRANÇA nota 10.", "Aquele professor vai lembrar deste PRESENTE na hora de corrigir a prova.", "O professor que deixa a matéria na lousa por mais tempo merece um PRESENTÃO."],
      "F": ["Sua professora vai achar esta LEMBRANÇA nota 10.", "Aquela professora vai lembrar deste PRESENTE na hora de corrigir a prova.", "A professora que deixa a matéria na lousa por mais tempo merece um PRESENTÃO."]
    },
    "G": {
      "M": ["Aquele melhor amigo que puxa você para a selfie ao abrir o PRESENTÃO.", "O melhor amigo merece uma RECORDAÇÃO INESQUECÍVEL neste fim de ano.", "Seu melhor amigo vai te envolver em um abraço apertado depois de ganhar esta LEMBRANÇA."],
      "F": ["Aquela melhor amiga que puxa você para a selfie ao abrir o PRESENTÃO.", "A melhor amiga merece uma RECORDAÇÃO INESQUECÍVEL neste fim de ano.", "Sua melhor amiga vai te envolver em um abraço apertado depois de ganhar esta LEMBRANÇA."]
    },
    "F": {
      "M": ["Seu pai vai se emocionar ganhando um PRESENTÃO de quem o conhece perfeitamente. ", "Seu pai vai abrir este PRESENTE e dizer que não tem ninguém no mundo como você. ", "Seu pai vai dizer que esta RECORDAÇÃO ESPECIAL fez lembrar de muitos momentos de vocês. "],
      "F": ["Sua mãe vai se emocionar ganhando um PRESENTÃO de quem a conhece perfeitamente. ", "Sua mãe vai abrir este PRESENTE e dizer que não tem ninguém no mundo como você. ", "Sua mãe vai dizer que esta RECORDAÇÃO ESPECIAL fez lembrar de muitos momentos de vocês. "]
    },
    "H": {
      "M": ["Aquele primo vai gostar tanto deste PRESENTE que vai mostrar para a família toda. ", "O primo vai adorar a LEMBRANÇA que combina com o seu jeito de ser. ", "Seu primo vai querer ser o primeiro a ganhar este PRESENTÃO de Natal. "],
      "F": ["Aquela prima vai gostar tanto deste PRESENTE que vai mostrar para a família toda. ", "A prima vai adorar a LEMBRANÇA que combina com o seu jeito de ser. ", "Sua prima vai querer ser o primeiro a ganhar este PRESENTÃO de Natal. "]
    },
    "J": {
      "M": ["Para aquele sobrinho que vai usar esta LEMBRANÇA quando vocês forem passear juntos. ", "Especialmente para o sobrinho que adora ganhar um PRESENTÃO de Natal.  ", "Tem o sobrinho que ganha o PRESENTE e agradece com um abraço forte. "],
      "F": ["Para aquela sobrinha que vai usar esta LEMBRANÇA quando vocês forem passear juntos. ", "Especialmente para a sobrinha que adora ganhar um PRESENTÃO de Natal.  ", "Tem a sobrinha que ganha o PRESENTE e agradece com um abraço forte. "]
    }
  },
  "cache1": {
    "givenOcasiao": {
      "A": ["A", "B", "C", "D"],
      "B": ["A", "B", "C", "D"],
      "C": ["A", "B", "C", "D"]
    },
    "givenQuem": {
      "A": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
      "B": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
      "C": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
      "D": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"]
    },
    "givenAPessoaE": {
      "A": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "B": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "C": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "D": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "E": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "F": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "G": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "H": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "I": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "J": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "K": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "L": ["A", "B", "C", "D", "E", "F", "G", "H"],
      "M": ["A", "B", "C", "D", "E", "F", "G", "H"]
    }
  },
  "cache2": {
    "AAAA": ["09", "10", "11", "12", "14", "17", "20", "21", "23", "24", "25", "26", "28", "30", "33", "38", "39", "46", "48", "49", "58", "60", "61", "62", "63"],
    "AAAB": ["09", "10", "11", "13", "14", "15", "16", "17", "18", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AAAC": ["11", "12", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AAAD": ["11", "12", "13", "15", "16", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "33", "38", "39", "40", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "AAAE": ["09", "10", "11", "12", "14", "17", "20", "21", "22", "23", "24", "25", "28", "30", "33", "38", "39", "45", "46", "48", "49", "58", "60", "61", "62", "63"],
    "AAAF": ["09", "10", "11", "12", "13", "14", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "58", "60", "61", "62", "63"],
    "AAAG": ["09", "10", "11", "12", "13", "14", "17", "20", "21", "23", "24", "25", "28", "30", "38", "39", "40", "46", "48", "49", "60", "62", "63"],
    "AAAH": ["09", "10", "11", "12", "14", "15", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "40", "42", "43", "44", "45", "46", "49", "60", "63"],
    "AAAI": ["09", "11", "12", "13", "15", "16", "18", "20", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "AAAJ": ["09", "11", "12", "13", "14", "15", "18", "20", "21", "22", "23", "24", "25", "28", "30", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60", "63"],
    "AAAK": ["10", "11", "13", "14", "15", "16", "17", "18", "20", "21", "23", "24", "25", "28", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AAAL": ["09", "10", "11", "12", "13", "14", "15", "18", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AAAM": ["11", "12", "13", "15", "16", "18", "21", "22", "23", "24", "25", "30", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AABA": ["11", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61"],
    "AABB": ["11", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AABC": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AABD": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AABE": ["11", "21", "22", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61"],
    "AABF": ["11", "13", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61"],
    "AABG": ["11", "13", "21", "23", "24", "25", "38", "39", "46", "48", "49", "50", "60"],
    "AABH": ["11", "15", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "AABI": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AABJ": ["11", "13", "15", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "50", "60"],
    "AABK": ["11", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AABL": ["11", "13", "15", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AABM": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AACA": ["09", "10", "11", "12", "14", "20", "21", "23", "24", "25", "26", "29", "30", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "AACB": ["09", "10", "11", "13", "14", "15", "16", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AACC": ["11", "12", "13", "15", "16", "21", "22", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AACD": ["11", "12", "13", "15", "16", "20", "21", "22", "23", "24", "25", "30", "33", "38", "39", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "AACE": ["09", "10", "11", "12", "14", "20", "21", "22", "23", "24", "25", "29", "30", "33", "38", "39", "45", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "AACF": ["09", "10", "11", "12", "13", "14", "20", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "AACG": ["09", "10", "11", "12", "13", "14", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "AACH": ["09", "10", "11", "12", "14", "15", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "42", "43", "44", "45", "46", "49", "60", "63"],
    "AACI": ["09", "11", "12", "13", "15", "16", "20", "21", "22", "23", "24", "25", "33", "38", "39", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "AACJ": ["09", "11", "12", "13", "14", "15", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "42", "43", "44", "45", "46", "49", "50", "60", "63"],
    "AACK": ["10", "11", "13", "14", "15", "16", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AACL": ["09", "10", "11", "12", "13", "14", "15", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AACM": ["11", "12", "13", "15", "16", "21", "22", "23", "24", "25", "30", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "AADA": ["09", "11", "12", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "49", "60"],
    "AADB": ["09", "11", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "AADC": ["11", "12", "21", "22", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "AADD": ["11", "12", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "AADE": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "45", "46", "49", "60"],
    "AADF": ["09", "11", "12", "20", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "AADG": ["09", "11", "12", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "49", "60"],
    "AADH": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "AADI": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "31", "32", "38", "39", "45", "46", "49", "60"],
    "AADJ": ["11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "AADK": ["11", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "AADL": ["09", "11", "12", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "AADM": ["11", "12", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "46", "49", "60"],
    "AAEA": ["09", "10", "11", "12", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "AAEB": ["09", "10", "11", "13", "16", "17", "19", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "AAEC": ["11", "12", "13", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "AAED": ["11", "12", "13", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "AAEE": ["09", "11", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "AAEF": ["09", "10", "11", "12", "13", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "AAEG": ["09", "10", "11", "12", "13", "17", "20", "21", "23", "24", "25", "28", "38", "39", "40", "46", "48", "49", "50", "60", "62", "63"],
    "AAEH": ["09", "10", "11", "12", "20", "21", "23", "24", "25", "31", "32", "38", "39", "40", "46", "49", "60", "63"],
    "AAEI": ["09", "11", "12", "13", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "AAEJ": ["09", "11", "12", "13", "20", "21", "23", "24", "25", "28", "31", "32", "38", "39", "46", "49", "50", "60", "63"],
    "AAEK": ["10", "11", "13", "16", "17", "19", "20", "21", "23", "24", "25", "28", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "AAEL": ["09", "10", "11", "12", "13", "19", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "AAEM": ["11", "12", "13", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "AAFA": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "26", "28", "29", "30", "38", "39", "46", "48", "49", "60", "62", "63"],
    "AAFB": ["09", "11", "14", "17", "18", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "AAFC": ["11", "12", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "AAFD": ["11", "12", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "AAFE": ["09", "11", "12", "14", "17", "20", "21", "22", "23", "24", "25", "28", "29", "30", "38", "39", "45", "46", "48", "49", "60", "62", "63"],
    "AAFF": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "AAFG": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "28", "29", "30", "38", "39", "46", "48", "49", "60", "62", "63"],
    "AAFH": ["09", "11", "12", "14", "18", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60", "63"],
    "AAFI": ["09", "11", "12", "18", "20", "21", "22", "23", "24", "25", "31", "32", "38", "39", "45", "46", "49", "60"],
    "AAFJ": ["09", "11", "12", "14", "18", "20", "21", "22", "23", "24", "25", "28", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60", "63"],
    "AAFK": ["11", "14", "17", "18", "20", "21", "23", "24", "25", "28", "31", "32", "38", "39", "46", "49", "60"],
    "AAFL": ["09", "11", "12", "14", "18", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "AAFM": ["11", "12", "18", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "46", "49", "60"],
    "AAGA": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "26", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "AAGB": ["09", "10", "11", "17", "19", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "AAGC": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "AAGD": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "AAGE": ["09", "10", "11", "12", "17", "21", "22", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "AAGF": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "AAGG": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "AAGH": ["09", "10", "11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60", "63"],
    "AAGI": ["09", "11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "AAGJ": ["09", "11", "12", "21", "22", "23", "24", "25", "28", "38", "39", "46", "49", "60", "63"],
    "AAGK": ["10", "11", "17", "19", "21", "23", "24", "25", "28", "38", "39", "46", "49", "60"],
    "AAGL": ["09", "10", "11", "12", "19", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "AAGM": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "AAHA": ["09", "10", "11", "12", "14", "17", "21", "23", "24", "25", "26", "28", "29", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "AAHB": ["09", "10", "11", "13", "14", "15", "16", "17", "18", "19", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "AAHC": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "AAHD": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60"],
    "AAHE": ["09", "10", "11", "12", "14", "17", "21", "23", "24", "25", "28", "29", "38", "39", "45", "46", "48", "49", "50", "60", "62", "63"],
    "AAHF": ["09", "10", "11", "12", "13", "14", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "AAHG": ["09", "10", "11", "12", "13", "14", "17", "21", "23", "24", "25", "28", "29", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "AAHH": ["09", "10", "11", "12", "14", "15", "18", "21", "23", "24", "25", "29", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60", "63"],
    "AAHI": ["09", "11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60"],
    "AAHJ": ["09", "11", "12", "13", "14", "15", "18", "21", "23", "24", "25", "28", "29", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "50", "60", "63"],
    "AAHK": ["10", "11", "13", "14", "15", "16", "17", "18", "19", "21", "23", "24", "25", "28", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "AAHL": ["09", "10", "11", "12", "13", "14", "15", "18", "19", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "AAHM": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "ABAA": ["02", "03", "04", "05", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "ABAB": ["02", "03", "04", "34"],
    "ABAC": ["03", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABAD": ["03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABAE": ["02", "03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABAF": ["02", "03", "04", "05", "06", "34", "41", "53", "54", "55", "56", "57"],
    "ABAG": ["02", "03", "04", "05", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABAH": ["02", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABAI": ["03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABAJ": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABAK": ["02", "03", "04", "34", "53", "54", "55", "56", "57"],
    "ABAL": ["02", "03", "04", "34", "41", "53", "54", "55", "56", "57"],
    "ABAM": ["03", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABBA": ["03", "35"],
    "ABBB": ["03"],
    "ABBC": ["03", "22", "35"],
    "ABBD": ["03", "22", "35"],
    "ABBE": ["03", "22", "35"],
    "ABBF": ["03"],
    "ABBG": ["03", "35"],
    "ABBH": ["22", "35"],
    "ABBI": ["03", "22", "35"],
    "ABBJ": ["22", "35"],
    "ABBK": ["03"],
    "ABBL": ["03"],
    "ABBM": ["03", "22", "35"],
    "ABCA": ["03", "04", "05", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "ABCB": ["03", "04"],
    "ABCC": ["03", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABCD": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABCE": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABCF": ["03", "04", "05", "06", "41", "53", "54", "55", "56", "57"],
    "ABCG": ["03", "04", "05", "06", "35", "41", "53", "54", "55", "56", "57"],
    "ABCH": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABCI": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABCJ": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABCK": ["03", "04", "53", "54", "55", "56", "57"],
    "ABCL": ["03", "04", "41", "53", "54", "55", "56", "57"],
    "ABCM": ["03", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "ABDA": ["03", "04", "05", "06", "35", "41", "55", "56", "57"],
    "ABDB": ["03", "04", "34"],
    "ABDC": ["03", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "ABDD": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "ABDE": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "ABDF": ["03", "04", "05", "06", "34", "41", "55", "56", "57"],
    "ABDG": ["03", "04", "05", "06", "34", "35", "41", "55", "56", "57"],
    "ABDH": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "ABDI": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "ABDJ": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "ABDK": ["03", "04", "34", "55", "56", "57"],
    "ABDL": ["03", "04", "34", "41", "55", "56", "57"],
    "ABDM": ["03", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "ABEA": ["02", "03", "04", "05", "06", "41", "53", "54"],
    "ABEB": ["02", "03", "04", "34"],
    "ABEC": ["03", "05", "06", "34", "41", "53", "54"],
    "ABED": ["03", "04", "05", "06", "34", "41", "53", "54"],
    "ABEE": ["34", "41", "53", "54"],
    "ABEF": ["02", "03", "04", "05", "06", "34", "41", "53", "54"],
    "ABEG": ["02", "03", "04", "05", "06", "34", "41", "53", "54"],
    "ABEH": ["02", "04", "05", "06", "41", "53", "54"],
    "ABEI": ["03", "04", "05", "06", "34", "41", "53", "54"],
    "ABEJ": ["04", "05", "06", "41", "53", "54"],
    "ABEK": ["02", "03", "04", "34", "53", "54"],
    "ABEL": ["02", "03", "04", "34", "41", "53", "54"],
    "ABEM": ["03", "05", "06", "34", "41", "53", "54"],
    "ABFA": ["02", "04", "05", "06", "27", "35", "41", "55", "56", "57"],
    "ABFB": ["02", "04"],
    "ABFC": ["05", "06", "22", "35", "41", "55", "56", "57"],
    "ABFD": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "ABFE": ["02", "04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "ABFF": ["02", "04", "05", "06", "41", "55", "56", "57"],
    "ABFG": ["02", "04", "05", "06", "35", "41", "55", "56", "57"],
    "ABFH": ["02", "04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "ABFI": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "ABFJ": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "ABFK": ["02", "04", "55", "56", "57"],
    "ABFL": ["02", "04", "41", "55", "56", "57"],
    "ABFM": ["05", "06", "22", "35", "41", "55", "56", "57"],
    "ABGA": ["02", "27", "41"],
    "ABGB": ["02"],
    "ABGC": ["22", "41"],
    "ABGD": ["22", "41"],
    "ABGE": ["02", "22", "41"],
    "ABGF": ["02", "41"],
    "ABGG": ["02", "41"],
    "ABGH": ["02", "22", "41"],
    "ABGI": ["22", "41"],
    "ABGJ": ["22", "41"],
    "ABGK": ["02"],
    "ABGL": ["02", "41"],
    "ABGM": ["22", "41"],
    "ABHA": ["02", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "ABHB": ["02", "34"],
    "ABHC": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABHD": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABHE": ["02", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABHF": ["02", "06", "34", "41", "53", "54", "55", "56", "57"],
    "ABHG": ["02", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABHH": ["02", "06", "35", "41", "53", "54", "55", "56", "57"],
    "ABHI": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ABHJ": ["06", "35", "41", "53", "54", "55", "56", "57"],
    "ABHK": ["02", "34", "53", "54", "55", "56", "57"],
    "ABHL": ["02", "34", "41", "53", "54", "55", "56", "57"],
    "ABHM": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "ACAE": ["07", "36", "47", "51"],
    "ACAG": ["47"],
    "ACAH": ["07", "36", "47", "51"],
    "ACAJ": ["07", "36", "47", "51"],
    "ACBE": ["47", "52"],
    "ACBG": ["47"],
    "ACBH": ["47"],
    "ACBJ": ["47"],
    "ACCE": ["07", "22", "47", "51"],
    "ACCG": ["47"],
    "ACCH": ["07", "22", "47", "51"],
    "ACCJ": ["07", "22", "47", "51"],
    "ACDE": ["07", "22", "47", "51"],
    "ACDG": ["47"],
    "ACDH": ["07", "22", "47", "51"],
    "ACDJ": ["07", "22", "47", "51"],
    "ACFE": ["07", "22", "47", "51"],
    "ACFG": ["47"],
    "ACFH": ["07", "22", "47", "51"],
    "ACFJ": ["07", "22", "47", "51"],
    "ACGE": ["36", "51"],
    "ACGH": ["36", "51"],
    "ACGJ": ["36", "51"],
    "ACHE": ["36", "51"],
    "ACHH": ["36", "51"],
    "ACHJ": ["36", "51"],
    "ADAE": ["08", "37", "52"],
    "ADAH": ["08", "37", "52"],
    "ADAJ": ["08", "37", "52"],
    "ADCE": ["08", "22", "52"],
    "ADCH": ["08", "22", "52"],
    "ADCJ": ["08", "22", "52"],
    "ADDE": ["08", "22", "52"],
    "ADDH": ["08", "22", "52"],
    "ADDJ": ["08", "22", "52"],
    "ADFE": ["08", "22", "52"],
    "ADFH": ["08", "22", "52"],
    "ADFJ": ["08", "22", "52"],
    "ADGE": ["37", "52"],
    "ADGH": ["37", "52"],
    "ADGJ": ["37", "52"],
    "ADHE": ["37", "52"],
    "ADHH": ["37", "52"],
    "ADHJ": ["37", "52"],
    "AEDE": ["01", "59"],
    "AEDH": ["01", "59"],
    "AEDJ": ["01", "59"],
    "BAAA": ["09", "10", "11", "12", "14", "17", "20", "21", "23", "24", "25", "26", "28", "30", "33", "38", "39", "46", "48", "49", "58", "60", "61", "62", "63"],
    "BAAB": ["09", "10", "11", "13", "14", "15", "16", "17", "18", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BAAC": ["11", "12", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BAAD": ["11", "12", "13", "15", "16", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "33", "38", "39", "40", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "BAAE": ["09", "10", "11", "12", "14", "17", "20", "21", "22", "23", "24", "25", "28", "30", "33", "38", "39", "45", "46", "48", "49", "58", "60", "61", "62", "63"],
    "BAAF": ["09", "10", "11", "12", "13", "14", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "58", "60", "61", "62", "63"],
    "BAAG": ["09", "10", "11", "12", "13", "14", "17", "20", "21", "23", "24", "25", "28", "30", "38", "39", "40", "46", "48", "49", "60", "62", "63"],
    "BAAH": ["09", "10", "11", "12", "14", "15", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "40", "42", "43", "44", "45", "46", "49", "60", "63"],
    "BAAI": ["09", "11", "12", "13", "15", "16", "18", "20", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "BAAJ": ["09", "11", "12", "13", "14", "15", "18", "20", "21", "22", "23", "24", "25", "28", "30", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60", "63"],
    "BAAK": ["10", "11", "13", "14", "15", "16", "17", "18", "20", "21", "23", "24", "25", "28", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BAAL": ["09", "10", "11", "12", "13", "14", "15", "18", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BAAM": ["11", "12", "13", "15", "16", "18", "21", "22", "23", "24", "25", "30", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BABA": ["11", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61"],
    "BABB": ["11", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BABC": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BABD": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BABE": ["11", "21", "22", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "51", "58", "60", "61"],
    "BABF": ["11", "13", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61"],
    "BABG": ["11", "13", "21", "23", "24", "25", "38", "39", "46", "48", "49", "50", "60"],
    "BABH": ["11", "15", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "BABI": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BABJ": ["11", "13", "15", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "50", "60"],
    "BABK": ["11", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BABL": ["11", "13", "15", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BABM": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BACA": ["09", "10", "11", "12", "14", "20", "21", "23", "24", "25", "26", "29", "30", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "BACB": ["09", "10", "11", "13", "14", "15", "16", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BACC": ["11", "12", "13", "15", "16", "21", "22", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BACD": ["11", "12", "13", "15", "16", "20", "21", "22", "23", "24", "25", "30", "33", "38", "39", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "BACE": ["09", "10", "11", "12", "14", "20", "21", "22", "23", "24", "25", "29", "30", "33", "38", "39", "45", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "BACF": ["09", "10", "11", "12", "13", "14", "20", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "BACG": ["09", "10", "11", "12", "13", "14", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "BACH": ["09", "10", "11", "12", "14", "15", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "42", "43", "44", "45", "46", "49", "60", "63"],
    "BACI": ["09", "11", "12", "13", "15", "16", "20", "21", "22", "23", "24", "25", "33", "38", "39", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "BACJ": ["09", "11", "12", "13", "14", "15", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "42", "43", "44", "45", "46", "49", "50", "60", "63"],
    "BACK": ["10", "11", "13", "14", "15", "16", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BACL": ["09", "10", "11", "12", "13", "14", "15", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BACM": ["11", "12", "13", "15", "16", "21", "22", "23", "24", "25", "30", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "BADA": ["09", "11", "12", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "49", "60"],
    "BADB": ["09", "11", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "BADC": ["11", "12", "21", "22", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "BADD": ["11", "12", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "BADE": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "45", "46", "49", "60"],
    "BADF": ["09", "11", "12", "20", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "BADG": ["09", "11", "12", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "49", "60"],
    "BADH": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "BADI": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "31", "32", "38", "39", "45", "46", "49", "60"],
    "BADJ": ["11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "BADK": ["11", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "BADL": ["09", "11", "12", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "BADM": ["11", "12", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "46", "49", "60"],
    "BAEA": ["09", "10", "11", "12", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "BAEB": ["09", "10", "11", "13", "16", "17", "19", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "BAEC": ["11", "12", "13", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "BAED": ["11", "12", "13", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "BAEE": ["09", "11", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "BAEF": ["09", "10", "11", "12", "13", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "BAEG": ["09", "10", "11", "12", "13", "17", "20", "21", "23", "24", "25", "28", "38", "39", "40", "46", "48", "49", "50", "60", "62", "63"],
    "BAEH": ["09", "10", "11", "12", "20", "21", "23", "24", "25", "31", "32", "38", "39", "40", "46", "49", "60", "63"],
    "BAEI": ["09", "11", "12", "13", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "BAEJ": ["09", "11", "12", "13", "20", "21", "23", "24", "25", "28", "31", "32", "38", "39", "46", "49", "50", "60", "63"],
    "BAEK": ["10", "11", "13", "16", "17", "19", "20", "21", "23", "24", "25", "28", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "BAEL": ["09", "10", "11", "12", "13", "19", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "BAEM": ["11", "12", "13", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "BAFA": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "26", "28", "29", "30", "38", "39", "46", "48", "49", "60", "62", "63"],
    "BAFB": ["09", "11", "14", "17", "18", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "BAFC": ["11", "12", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "BAFD": ["11", "12", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "BAFE": ["09", "11", "12", "14", "17", "20", "21", "22", "23", "24", "25", "28", "29", "30", "38", "39", "45", "46", "48", "49", "60", "62", "63"],
    "BAFF": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "BAFG": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "28", "29", "30", "38", "39", "46", "48", "49", "60", "62", "63"],
    "BAFH": ["09", "11", "12", "14", "18", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60", "63"],
    "BAFI": ["09", "11", "12", "18", "20", "21", "22", "23", "24", "25", "31", "32", "38", "39", "45", "46", "49", "60"],
    "BAFJ": ["09", "11", "12", "14", "18", "20", "21", "22", "23", "24", "25", "28", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60", "63"],
    "BAFK": ["11", "14", "17", "18", "20", "21", "23", "24", "25", "28", "31", "32", "38", "39", "46", "49", "60"],
    "BAFL": ["09", "11", "12", "14", "18", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "BAFM": ["11", "12", "18", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "46", "49", "60"],
    "BAGA": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "26", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "BAGB": ["09", "10", "11", "17", "19", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "BAGC": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "BAGD": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "BAGE": ["09", "10", "11", "12", "17", "21", "22", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "BAGF": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "BAGG": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "BAGH": ["09", "10", "11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60", "63"],
    "BAGI": ["09", "11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "BAGJ": ["09", "11", "12", "21", "22", "23", "24", "25", "28", "38", "39", "46", "49", "60", "63"],
    "BAGK": ["10", "11", "17", "19", "21", "23", "24", "25", "28", "38", "39", "46", "49", "60"],
    "BAGL": ["09", "10", "11", "12", "19", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "BAGM": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "BAHA": ["09", "10", "11", "12", "14", "17", "21", "23", "24", "25", "26", "28", "29", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "BAHB": ["09", "10", "11", "13", "14", "15", "16", "17", "18", "19", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "BAHC": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "BAHD": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60"],
    "BAHE": ["09", "10", "11", "12", "14", "17", "21", "23", "24", "25", "28", "29", "38", "39", "45", "46", "48", "49", "50", "60", "62", "63"],
    "BAHF": ["09", "10", "11", "12", "13", "14", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "BAHG": ["09", "10", "11", "12", "13", "14", "17", "21", "23", "24", "25", "28", "29", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "BAHH": ["09", "10", "11", "12", "14", "15", "18", "21", "23", "24", "25", "29", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60", "63"],
    "BAHI": ["09", "11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60"],
    "BAHJ": ["09", "11", "12", "13", "14", "15", "18", "21", "23", "24", "25", "28", "29", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "50", "60", "63"],
    "BAHK": ["10", "11", "13", "14", "15", "16", "17", "18", "19", "21", "23", "24", "25", "28", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "BAHL": ["09", "10", "11", "12", "13", "14", "15", "18", "19", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "BAHM": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "BBAA": ["02", "03", "04", "05", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "BBAB": ["02", "03", "04", "34"],
    "BBAC": ["03", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBAD": ["03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBAE": ["02", "03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBAF": ["02", "03", "04", "05", "06", "34", "41", "53", "54", "55", "56", "57"],
    "BBAG": ["02", "03", "04", "05", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBAH": ["02", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBAI": ["03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBAJ": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBAK": ["02", "03", "04", "34", "53", "54", "55", "56", "57"],
    "BBAL": ["02", "03", "04", "34", "41", "53", "54", "55", "56", "57"],
    "BBAM": ["03", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBBA": ["03", "35"],
    "BBBB": ["03"],
    "BBBC": ["03", "22", "35"],
    "BBBD": ["03", "22", "35"],
    "BBBE": ["03", "22", "35"],
    "BBBF": ["03"],
    "BBBG": ["03", "35"],
    "BBBH": ["22", "35"],
    "BBBI": ["03", "22", "35"],
    "BBBJ": ["22", "35"],
    "BBBK": ["03"],
    "BBBL": ["03"],
    "BBBM": ["03", "22", "35"],
    "BBCA": ["03", "04", "05", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "BBCB": ["03", "04"],
    "BBCC": ["03", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBCD": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBCE": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBCF": ["03", "04", "05", "06", "41", "53", "54", "55", "56", "57"],
    "BBCG": ["03", "04", "05", "06", "35", "41", "53", "54", "55", "56", "57"],
    "BBCH": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBCI": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBCJ": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBCK": ["03", "04", "53", "54", "55", "56", "57"],
    "BBCL": ["03", "04", "41", "53", "54", "55", "56", "57"],
    "BBCM": ["03", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "BBDA": ["03", "04", "05", "06", "35", "41", "55", "56", "57"],
    "BBDB": ["03", "04", "34"],
    "BBDC": ["03", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "BBDD": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "BBDE": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "BBDF": ["03", "04", "05", "06", "34", "41", "55", "56", "57"],
    "BBDG": ["03", "04", "05", "06", "34", "35", "41", "55", "56", "57"],
    "BBDH": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "BBDI": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "BBDJ": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "BBDK": ["03", "04", "34", "55", "56", "57"],
    "BBDL": ["03", "04", "34", "41", "55", "56", "57"],
    "BBDM": ["03", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "BBEA": ["02", "03", "04", "05", "06", "41", "53", "54"],
    "BBEB": ["02", "03", "04", "34"],
    "BBEC": ["03", "05", "06", "34", "41", "53", "54"],
    "BBED": ["03", "04", "05", "06", "34", "41", "53", "54"],
    "BBEE": ["34", "41", "53", "54"],
    "BBEF": ["02", "03", "04", "05", "06", "34", "41", "53", "54"],
    "BBEG": ["02", "03", "04", "05", "06", "34", "41", "53", "54"],
    "BBEH": ["02", "04", "05", "06", "41", "53", "54"],
    "BBEI": ["03", "04", "05", "06", "34", "41", "53", "54"],
    "BBEJ": ["04", "05", "06", "41", "53", "54"],
    "BBEK": ["02", "03", "04", "34", "53", "54"],
    "BBEL": ["02", "03", "04", "34", "41", "53", "54"],
    "BBEM": ["03", "05", "06", "34", "41", "53", "54"],
    "BBFA": ["02", "04", "05", "06", "27", "35", "41", "55", "56", "57"],
    "BBFB": ["02", "04"],
    "BBFC": ["05", "06", "22", "35", "41", "55", "56", "57"],
    "BBFD": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "BBFE": ["02", "04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "BBFF": ["02", "04", "05", "06", "41", "55", "56", "57"],
    "BBFG": ["02", "04", "05", "06", "35", "41", "55", "56", "57"],
    "BBFH": ["02", "04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "BBFI": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "BBFJ": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "BBFK": ["02", "04", "55", "56", "57"],
    "BBFL": ["02", "04", "41", "55", "56", "57"],
    "BBFM": ["05", "06", "22", "35", "41", "55", "56", "57"],
    "BBGA": ["02", "27", "41"],
    "BBGB": ["02"],
    "BBGC": ["22", "41"],
    "BBGD": ["22", "41"],
    "BBGE": ["02", "22", "41"],
    "BBGF": ["02", "41"],
    "BBGG": ["02", "41"],
    "BBGH": ["02", "22", "41"],
    "BBGI": ["22", "41"],
    "BBGJ": ["22", "41"],
    "BBGK": ["02"],
    "BBGL": ["02", "41"],
    "BBGM": ["22", "41"],
    "BBHA": ["02", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "BBHB": ["02", "34"],
    "BBHC": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBHD": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBHE": ["02", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBHF": ["02", "06", "34", "41", "53", "54", "55", "56", "57"],
    "BBHG": ["02", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBHH": ["02", "06", "35", "41", "53", "54", "55", "56", "57"],
    "BBHI": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BBHJ": ["06", "35", "41", "53", "54", "55", "56", "57"],
    "BBHK": ["02", "34", "53", "54", "55", "56", "57"],
    "BBHL": ["02", "34", "41", "53", "54", "55", "56", "57"],
    "BBHM": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "BCAE": ["07", "36", "47", "51"],
    "BCAG": ["47"],
    "BCAH": ["07", "36", "47", "51"],
    "BCAJ": ["07", "36", "47", "51"],
    "BCBE": ["47"],
    "BCBG": ["47"],
    "BCBH": ["47"],
    "BCBJ": ["47"],
    "BCCE": ["07", "22", "47", "51"],
    "BCCG": ["47"],
    "BCCH": ["07", "22", "47", "51"],
    "BCCJ": ["07", "22", "47", "51"],
    "BCDE": ["07", "22", "47", "51"],
    "BCDG": ["47"],
    "BCDH": ["07", "22", "47", "51"],
    "BCDJ": ["07", "22", "47", "51"],
    "BCFE": ["07", "22", "47", "51"],
    "BCFG": ["47"],
    "BCFH": ["07", "22", "47", "51"],
    "BCFJ": ["07", "22", "47", "51"],
    "BCGE": ["36", "51"],
    "BCGH": ["36", "51"],
    "BCGJ": ["36", "51"],
    "BCHE": ["36", "51"],
    "BCHH": ["36", "51"],
    "BCHJ": ["36", "51"],
    "BDAE": ["08", "37", "52"],
    "BDAH": ["08", "37", "52"],
    "BDAJ": ["08", "37", "52"],
    "BDCE": ["08", "22", "52"],
    "BDCH": ["08", "22", "52"],
    "BDCJ": ["08", "22", "52"],
    "BDDE": ["08", "22", "52"],
    "BDDH": ["08", "22", "52"],
    "BDDJ": ["08", "22", "52"],
    "BDFE": ["08", "22", "52"],
    "BDFH": ["08", "22", "52"],
    "BDFJ": ["08", "22", "52"],
    "BDGE": ["37", "52"],
    "BDGH": ["37", "52"],
    "BDGJ": ["37", "52"],
    "BDHE": ["37", "52"],
    "BDHH": ["37", "52"],
    "BDHJ": ["37", "52"],
    "BEDE": ["01", "59"],
    "BEDH": ["01", "59"],
    "BEDJ": ["01", "59"],
    "CAAA": ["09", "10", "11", "12", "14", "17", "20", "21", "23", "24", "25", "26", "28", "30", "33", "38", "39", "46", "48", "49", "58", "60", "61", "62", "63"],
    "CAAB": ["09", "10", "11", "13", "14", "15", "16", "17", "18", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CAAC": ["11", "12", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CAAD": ["11", "12", "13", "15", "16", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "33", "38", "39", "40", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "CAAE": ["09", "10", "11", "12", "14", "17", "20", "21", "22", "23", "24", "25", "28", "30", "33", "38", "39", "45", "46", "48", "49", "58", "60", "61", "62", "63"],
    "CAAF": ["09", "10", "11", "12", "13", "14", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "58", "60", "61", "62", "63"],
    "CAAG": ["09", "10", "11", "12", "13", "14", "17", "20", "21", "23", "24", "25", "28", "30", "38", "39", "40", "46", "48", "49", "60", "62", "63"],
    "CAAH": ["09", "10", "11", "12", "14", "15", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "40", "42", "43", "44", "45", "46", "49", "60", "63"],
    "CAAI": ["09", "11", "12", "13", "15", "16", "18", "20", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "CAAJ": ["09", "11", "12", "13", "14", "15", "18", "20", "21", "22", "23", "24", "25", "28", "30", "31", "32", "38", "39", "40", "42", "43", "44", "45", "46", "49", "60", "63"],
    "CAAK": ["10", "11", "13", "14", "15", "16", "17", "18", "20", "21", "23", "24", "25", "28", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CAAL": ["09", "10", "11", "12", "13", "14", "15", "18", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CAAM": ["11", "12", "13", "15", "16", "18", "21", "22", "23", "24", "25", "30", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CABA": ["11", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61"],
    "CABB": ["11", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CABC": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CABD": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CABE": ["11", "21", "22", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61"],
    "CABF": ["11", "13", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61"],
    "CABG": ["11", "13", "21", "23", "24", "25", "38", "39", "46", "48", "49", "50", "60"],
    "CABH": ["11", "15", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "CABI": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CABJ": ["11", "13", "15", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "50", "60"],
    "CABK": ["11", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CABL": ["11", "13", "15", "18", "21", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CABM": ["11", "13", "15", "16", "18", "21", "22", "23", "24", "25", "31", "32", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CACA": ["09", "10", "11", "12", "14", "20", "21", "23", "24", "25", "26", "29", "30", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "CACB": ["09", "10", "11", "13", "14", "15", "16", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CACC": ["11", "12", "13", "15", "16", "21", "22", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CACD": ["11", "12", "13", "15", "16", "20", "21", "22", "23", "24", "25", "30", "33", "38", "39", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "CACE": ["09", "10", "11", "12", "14", "20", "21", "22", "23", "24", "25", "29", "30", "33", "38", "39", "45", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "CACF": ["09", "10", "11", "12", "13", "14", "20", "21", "23", "24", "25", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "CACG": ["09", "10", "11", "12", "13", "14", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "CACH": ["09", "10", "11", "12", "14", "15", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "42", "43", "44", "45", "46", "49", "60", "63"],
    "CACI": ["09", "11", "12", "13", "15", "16", "20", "21", "22", "23", "24", "25", "33", "38", "39", "42", "43", "44", "45", "46", "49", "58", "60", "61"],
    "CACJ": ["09", "11", "12", "13", "14", "15", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "42", "43", "44", "45", "46", "49", "50", "60", "63"],
    "CACK": ["10", "11", "13", "14", "15", "16", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CACL": ["09", "10", "11", "12", "13", "14", "15", "20", "21", "23", "24", "25", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CACM": ["11", "12", "13", "15", "16", "21", "22", "23", "24", "25", "30", "33", "38", "39", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CADA": ["09", "11", "12", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "49", "60"],
    "CADB": ["09", "11", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "CADC": ["11", "12", "21", "22", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "CADD": ["11", "12", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "CADE": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "38", "39", "45", "46", "49", "60"],
    "CADF": ["09", "11", "12", "20", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "CADG": ["09", "11", "12", "20", "21", "23", "24", "25", "29", "30", "38", "39", "46", "49", "60"],
    "CADH": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "CADI": ["09", "11", "12", "20", "21", "22", "23", "24", "25", "31", "32", "38", "39", "45", "46", "49", "60"],
    "CADJ": ["11", "12", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "CADK": ["11", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "CADL": ["09", "11", "12", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "CADM": ["11", "12", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "46", "49", "60"],
    "CAEA": ["09", "10", "11", "12", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "CAEB": ["09", "10", "11", "13", "15", "16", "17", "19", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CAEC": ["11", "12", "13", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "CAED": ["11", "12", "13", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "CAEE": ["09", "11", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "CAEF": ["09", "10", "11", "12", "13", "17", "20", "21", "23", "24", "25", "28", "33", "38", "39", "46", "48", "49", "50", "58", "60", "61", "62", "63"],
    "CAEG": ["09", "10", "11", "12", "13", "17", "20", "21", "23", "24", "25", "28", "38", "39", "40", "46", "48", "49", "50", "60", "62", "63"],
    "CAEH": ["09", "10", "11", "12", "20", "21", "23", "24", "25", "31", "32", "38", "39", "40", "46", "49", "60", "63"],
    "CAEI": ["09", "11", "12", "13", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "CAEJ": ["09", "11", "12", "13", "20", "21", "23", "24", "25", "28", "31", "32", "38", "39", "40", "46", "49", "50", "60", "63"],
    "CAEK": ["10", "11", "13", "15", "16", "17", "19", "20", "21", "23", "24", "25", "28", "31", "32", "33", "38", "39", "40", "42", "43", "44", "46", "49", "58", "60", "61"],
    "CAEL": ["09", "10", "11", "12", "13", "19", "20", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "CAEM": ["11", "12", "13", "21", "23", "24", "25", "31", "32", "33", "38", "39", "40", "46", "49", "58", "60", "61"],
    "CAFA": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "26", "28", "29", "30", "38", "39", "46", "48", "49", "60", "62", "63"],
    "CAFB": ["09", "11", "14", "17", "18", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "CAFC": ["11", "12", "18", "21", "22", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "CAFD": ["11", "12", "18", "20", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "45", "46", "49", "60"],
    "CAFE": ["09", "11", "12", "14", "17", "20", "21", "22", "23", "24", "25", "28", "29", "30", "38", "39", "45", "46", "48", "49", "60", "62", "63"],
    "CAFF": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "CAFG": ["09", "11", "12", "14", "17", "20", "21", "23", "24", "25", "28", "29", "30", "38", "39", "46", "48", "49", "60", "62", "63"],
    "CAFH": ["09", "11", "12", "14", "18", "20", "21", "22", "23", "24", "25", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60", "63"],
    "CAFI": ["09", "11", "12", "18", "20", "21", "22", "23", "24", "25", "31", "32", "38", "39", "45", "46", "49", "60"],
    "CAFJ": ["09", "11", "12", "14", "18", "20", "21", "22", "23", "24", "25", "28", "29", "30", "31", "32", "38", "39", "45", "46", "49", "60", "63"],
    "CAFK": ["11", "14", "17", "18", "20", "21", "23", "24", "25", "28", "31", "32", "38", "39", "46", "49", "60"],
    "CAFL": ["09", "11", "12", "14", "18", "20", "21", "23", "24", "25", "31", "32", "38", "39", "46", "49", "60"],
    "CAFM": ["11", "12", "18", "21", "22", "23", "24", "25", "30", "31", "32", "38", "39", "46", "49", "60"],
    "CAGA": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "26", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "CAGB": ["09", "10", "11", "17", "19", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "CAGC": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "CAGD": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "CAGE": ["09", "10", "11", "12", "17", "21", "22", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "CAGF": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "CAGG": ["09", "10", "11", "12", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "60", "62", "63"],
    "CAGH": ["09", "10", "11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60", "63"],
    "CAGI": ["09", "11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "CAGJ": ["09", "11", "12", "21", "22", "23", "24", "25", "28", "38", "39", "46", "49", "60", "63"],
    "CAGK": ["10", "11", "17", "19", "21", "23", "24", "25", "28", "38", "39", "46", "49", "60"],
    "CAGL": ["09", "10", "11", "12", "19", "21", "23", "24", "25", "38", "39", "46", "49", "60"],
    "CAGM": ["11", "12", "21", "22", "23", "24", "25", "38", "39", "46", "49", "60"],
    "CAHA": ["09", "10", "11", "12", "14", "17", "21", "23", "24", "25", "26", "28", "29", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "CAHB": ["09", "10", "11", "13", "14", "15", "16", "17", "18", "19", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "CAHC": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "CAHD": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60"],
    "CAHE": ["09", "10", "11", "12", "14", "17", "21", "23", "24", "25", "28", "29", "38", "39", "45", "46", "48", "49", "50", "60", "62", "63"],
    "CAHF": ["09", "10", "11", "12", "13", "14", "17", "21", "23", "24", "25", "28", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "CAHG": ["09", "10", "11", "12", "13", "14", "17", "21", "23", "24", "25", "28", "29", "38", "39", "46", "48", "49", "50", "60", "62", "63"],
    "CAHH": ["09", "10", "11", "12", "14", "15", "18", "21", "23", "24", "25", "29", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60", "63"],
    "CAHI": ["09", "11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "60"],
    "CAHJ": ["09", "11", "12", "13", "14", "15", "18", "21", "23", "24", "25", "28", "29", "31", "32", "38", "39", "42", "43", "44", "45", "46", "49", "50", "60", "63"],
    "CAHK": ["10", "11", "13", "14", "15", "16", "17", "18", "19", "21", "23", "24", "25", "28", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "CAHL": ["09", "10", "11", "12", "13", "14", "15", "18", "19", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "CAHM": ["11", "12", "13", "15", "16", "18", "21", "23", "24", "25", "31", "32", "38", "39", "42", "43", "44", "46", "49", "60"],
    "CBAA": ["02", "03", "04", "05", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "CBAB": ["02", "03", "04", "34"],
    "CBAC": ["03", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBAD": ["03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBAE": ["02", "03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBAF": ["02", "03", "04", "05", "06", "34", "41", "53", "54", "55", "56", "57"],
    "CBAG": ["02", "03", "04", "05", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBAH": ["02", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBAI": ["03", "04", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBAJ": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBAK": ["02", "03", "04", "34", "53", "54", "55", "56", "57"],
    "CBAL": ["02", "03", "04", "34", "41", "53", "54", "55", "56", "57"],
    "CBAM": ["03", "05", "06", "22", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBBA": ["03", "35"],
    "CBBB": ["03"],
    "CBBC": ["03", "22", "35"],
    "CBBD": ["03", "22", "35"],
    "CBBE": ["03", "22", "35"],
    "CBBF": ["03"],
    "CBBG": ["03", "35"],
    "CBBH": ["22", "35"],
    "CBBI": ["03", "22", "35"],
    "CBBJ": ["22", "35"],
    "CBBK": ["03"],
    "CBBL": ["03"],
    "CBBM": ["03", "22", "35"],
    "CBCA": ["03", "04", "05", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "CBCB": ["03", "04"],
    "CBCC": ["03", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBCD": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBCE": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBCF": ["03", "04", "05", "06", "41", "53", "54", "55", "56", "57"],
    "CBCG": ["03", "04", "05", "06", "35", "41", "53", "54", "55", "56", "57"],
    "CBCH": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBCI": ["03", "04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBCJ": ["04", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBCK": ["03", "04", "53", "54", "55", "56", "57"],
    "CBCL": ["03", "04", "41", "53", "54", "55", "56", "57"],
    "CBCM": ["03", "05", "06", "22", "35", "41", "53", "54", "55", "56", "57"],
    "CBDA": ["02", "03", "04", "05", "06", "35", "41", "55", "56", "57"],
    "CBDB": ["03", "04", "34"],
    "CBDC": ["03", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "CBDD": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "CBDE": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "CBDF": ["03", "04", "05", "06", "34", "41", "55", "56", "57"],
    "CBDG": ["03", "04", "05", "06", "34", "35", "41", "55", "56", "57"],
    "CBDH": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "CBDI": ["03", "04", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "CBDJ": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "CBDK": ["03", "04", "34", "55", "56", "57"],
    "CBDL": ["03", "04", "34", "41", "55", "56", "57"],
    "CBDM": ["03", "05", "06", "22", "34", "35", "41", "55", "56", "57"],
    "CBEA": ["02", "03", "04", "05", "06", "41", "53", "54"],
    "CBEB": ["02", "03", "04", "34"],
    "CBEC": ["03", "05", "06", "34", "41", "53", "54"],
    "CBED": ["03", "04", "05", "06", "34", "41", "53", "54"],
    "CBEE": ["34", "41", "53", "54"],
    "CBEF": ["02", "03", "04", "05", "06", "34", "41", "53", "54"],
    "CBEG": ["02", "03", "04", "05", "06", "34", "41", "53", "54"],
    "CBEH": ["02", "04", "05", "06", "41", "53", "54"],
    "CBEI": ["03", "04", "05", "06", "34", "41", "53", "54"],
    "CBEJ": ["04", "05", "06", "41", "53", "54"],
    "CBEK": ["02", "03", "04", "34", "53", "54"],
    "CBEL": ["02", "03", "04", "34", "41", "53", "54"],
    "CBEM": ["03", "05", "06", "34", "41", "53", "54"],
    "CBFA": ["02", "04", "05", "06", "27", "35", "41", "55", "56", "57"],
    "CBFB": ["02", "04"],
    "CBFC": ["05", "06", "22", "35", "41", "55", "56", "57"],
    "CBFD": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "CBFE": ["02", "04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "CBFF": ["02", "04", "05", "06", "41", "55", "56", "57"],
    "CBFG": ["02", "04", "05", "06", "35", "41", "55", "56", "57"],
    "CBFH": ["02", "04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "CBFI": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "CBFJ": ["04", "05", "06", "22", "35", "41", "55", "56", "57"],
    "CBFK": ["02", "04", "55", "56", "57"],
    "CBFL": ["02", "04", "41", "55", "56", "57"],
    "CBFM": ["05", "06", "22", "35", "41", "55", "56", "57"],
    "CBGA": ["02", "27", "41"],
    "CBGB": ["02"],
    "CBGC": ["22", "41"],
    "CBGD": ["22", "41"],
    "CBGE": ["02", "22", "41"],
    "CBGF": ["02", "41"],
    "CBGG": ["02", "41"],
    "CBGH": ["02", "22", "41"],
    "CBGI": ["22", "41"],
    "CBGJ": ["22", "41"],
    "CBGK": ["02"],
    "CBGL": ["02", "41"],
    "CBGM": ["22", "41"],
    "CBHA": ["02", "06", "27", "35", "41", "53", "54", "55", "56", "57"],
    "CBHB": ["02", "34"],
    "CBHC": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBHD": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBHE": ["02", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBHF": ["02", "06", "34", "41", "53", "54", "55", "56", "57"],
    "CBHG": ["02", "06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBHH": ["02", "06", "35", "41", "53", "54", "55", "56", "57"],
    "CBHI": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CBHJ": ["06", "35", "41", "53", "54", "55", "56", "57"],
    "CBHK": ["02", "34", "53", "54", "55", "56", "57"],
    "CBHL": ["02", "34", "41", "53", "54", "55", "56", "57"],
    "CBHM": ["06", "34", "35", "41", "53", "54", "55", "56", "57"],
    "CCAE": ["07", "36", "47", "51"],
    "CCAG": ["47"],
    "CCAH": ["07", "36", "47", "51"],
    "CCAJ": ["07", "36", "47", "51"],
    "CCBE": ["47"],
    "CCBG": ["47"],
    "CCBH": ["47"],
    "CCBJ": ["47"],
    "CCCE": ["07", "22", "47", "51"],
    "CCCG": ["47"],
    "CCCH": ["07", "22", "47", "51"],
    "CCCJ": ["07", "22", "47", "51"],
    "CCDE": ["07", "22", "47", "51"],
    "CCDG": ["47"],
    "CCDH": ["07", "22", "47", "51"],
    "CCDJ": ["07", "22", "47", "51"],
    "CCFE": ["07", "22", "47", "51"],
    "CCFG": ["47"],
    "CCFH": ["07", "22", "47", "51"],
    "CCFJ": ["07", "22", "47", "51"],
    "CCGE": ["36", "51"],
    "CCGH": ["36", "51"],
    "CCGJ": ["36", "51"],
    "CCHE": ["36", "51"],
    "CCHH": ["36", "51"],
    "CCHJ": ["36", "51"],
    "CDAE": ["08", "37", "52"],
    "CDAH": ["08", "37", "52"],
    "CDAJ": ["08", "37", "52"],
    "CDCE": ["08", "22", "52"],
    "CDCH": ["08", "22", "52"],
    "CDCJ": ["08", "22", "52"],
    "CDDE": ["08", "22", "52"],
    "CDDH": ["08", "22", "52"],
    "CDDJ": ["08", "22", "52"],
    "CDFE": ["08", "22", "52"],
    "CDFH": ["08", "22", "52"],
    "CDFJ": ["08", "22", "52"],
    "CDGE": ["37", "52"],
    "CDGH": ["37", "52"],
    "CDGJ": ["37", "52"],
    "CDHE": ["37", "52"],
    "CDHH": ["37", "52"],
    "CDHJ": ["37", "52"],
    "CEDE": ["01", "59"],
    "CEDH": ["01", "59"],
    "CEDJ": ["01", "59"]
  },
  "ocasiaoPlusQuem": {
    "AA": ["A", "B", "C", "D", "E", "F", "G", "H"],
    "AB": ["A", "B", "C", "D", "E", "F", "G", "H"],
    "AC": ["A", "B", "C", "D", "F", "G", "H"],
    "AD": ["A", "C", "D", "F", "G", "H"],
    "AE": ["D"],
    "BA": ["A", "B", "C", "D", "E", "F", "G", "H"],
    "BB": ["A", "B", "C", "D", "E", "F", "G", "H"],
    "BC": ["A", "B", "C", "D", "F", "G", "H"],
    "BD": ["A", "C", "D", "F", "G", "H"],
    "BE": ["D"],
    "CA": ["A", "B", "C", "D", "E", "F", "G", "H"],
    "CB": ["A", "B", "C", "D", "E", "F", "G", "H"],
    "CC": ["A", "B", "C", "D", "F", "G", "H"],
    "CD": ["A", "C", "D", "F", "G", "H"],
    "CE": ["D"]
  },
  "ocasiaoPlusQuemPlusPessoa": {
    "AAA": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "AAB": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "AAC": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "AAD": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "AAE": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "AAF": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "AAG": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "AAH": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ABA": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ABB": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ABC": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ABD": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ABE": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ABF": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ABG": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ABH": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "ACA": ["E", "G", "H", "J"],
    "ACB": ["E", "G", "H", "J"],
    "ACC": ["E", "G", "H", "J"],
    "ACD": ["E", "G", "H", "J"],
    "ACF": ["E", "G", "H", "J"],
    "ACG": ["E", "H", "J"],
    "ACH": ["E", "H", "J"],
    "ADA": ["E", "H", "J"],
    "ADC": ["E", "H", "J"],
    "ADD": ["E", "H", "J"],
    "ADF": ["E", "H", "J"],
    "ADG": ["E", "H", "J"],
    "ADH": ["E", "H", "J"],
    "AED": ["E", "H", "J"],
    "BAA": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BAB": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BAC": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BAD": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BAE": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BAF": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BAG": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BAH": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BBA": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BBB": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BBC": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BBD": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BBE": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BBF": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BBG": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BBH": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "BCA": ["E", "G", "H", "J"],
    "BCB": ["E", "G", "H", "J"],
    "BCC": ["E", "G", "H", "J"],
    "BCD": ["E", "G", "H", "J"],
    "BCF": ["E", "G", "H", "J"],
    "BCG": ["E", "H", "J"],
    "BCH": ["E", "H", "J"],
    "BDA": ["E", "H", "J"],
    "BDC": ["E", "H", "J"],
    "BDD": ["E", "H", "J"],
    "BDF": ["E", "H", "J"],
    "BDG": ["E", "H", "J"],
    "BDH": ["E", "H", "J"],
    "BED": ["E", "H", "J"],
    "CAA": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CAB": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CAC": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CAD": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CAE": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CAF": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CAG": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CAH": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CBA": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CBB": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CBC": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CBD": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CBE": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CBF": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CBG": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CBH": ["A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M"],
    "CCA": ["E", "G", "H", "J"],
    "CCB": ["E", "G", "H", "J"],
    "CCC": ["E", "G", "H", "J"],
    "CCD": ["E", "G", "H", "J"],
    "CCF": ["E", "G", "H", "J"],
    "CCG": ["E", "H", "J"],
    "CCH": ["E", "H", "J"],
    "CDA": ["E", "H", "J"],
    "CDC": ["E", "H", "J"],
    "CDD": ["E", "H", "J"],
    "CDF": ["E", "H", "J"],
    "CDG": ["E", "H", "J"],
    "CDH": ["E", "H", "J"],
    "CED": ["E", "H", "J"]
  }
};
window.Datacache = Datacache;
var $__default = Datacache;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/datacache.js
},{}],5:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Escolhidos = {init: function() {
    console.log('Escolhidos: Mode=ON');
  }};
var $__default = Escolhidos;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/escolhidos.js
},{}],6:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__datacache_46_js__,
    $__cndata_46_js__,
    $__produtos_46_js__;
var Datacache = ($__datacache_46_js__ = require("./datacache.js"), $__datacache_46_js__ && $__datacache_46_js__.__esModule && $__datacache_46_js__ || {default: $__datacache_46_js__}).default;
var CnData = ($__cndata_46_js__ = require("./cndata.js"), $__cndata_46_js__ && $__cndata_46_js__.__esModule && $__cndata_46_js__ || {default: $__cndata_46_js__}).default;
var Produtos = ($__produtos_46_js__ = require("./produtos.js"), $__produtos_46_js__ && $__produtos_46_js__.__esModule && $__produtos_46_js__ || {default: $__produtos_46_js__}).default;
var Filtro = {
  hasCn: ko.observable(false),
  datacache: Datacache,
  produtos: Produtos,
  step: ko.observable(0),
  selectedOcasiao: ko.observable({}),
  selectedQuem: ko.observable({}),
  selectedAPessoaE: ko.observable({}),
  selectedRelacao: ko.observable({}),
  selectedFaixa: ko.observable({}),
  rangeProdutos: [],
  cache2Keys: [],
  produtosKeys: [],
  produto0: ko.observable([]),
  produto1: ko.observable([]),
  produto2: ko.observable([]),
  frase0: ko.observable(''),
  frase1: ko.observable(''),
  frase2: ko.observable(''),
  frasepeq0: ko.observable(''),
  frasepeq1: ko.observable(''),
  frasepeq2: ko.observable(''),
  listaPessoas: ko.observableArray([]),
  listaRelacoes: ko.observableArray([]),
  listaFaixas: ko.observableArray([]),
  userCache: {},
  init: function() {
    console.log('Filtro: Mode=ON');
    this.reset();
    this.setEl();
    this.configureSlicks();
    this.cache2Keys = Filtro.getKeys(Filtro.datacache.cache2);
    this.produtosKeys = Filtro.getKeys(Filtro.datacache.produtos);
    this.hasCn(CnData.hasCn());
    console.log('/Filtro');
  },
  configureSlicks: function() {
    this.filtroSlick = {
      dots: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 3,
      centerMode: true,
      variableWidth: true,
      arrows: false,
      responsive: [{
        breakpoint: 1024,
        settings: {
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
          centerMode: true,
          nextArrow: '.btn-next0',
          prevArrow: '.btn-prev0',
          variableWidth: true,
          adaptiveHeight: true
        }
      }]
    };
    this.escolhidosSlick = {
      dots: false,
      infinite: true,
      slidesToShow: 1,
      slidesToScroll: 3,
      centerMode: true,
      variableWidth: true,
      arrows: false,
      responsive: [{
        breakpoint: 1024,
        settings: {
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 3,
          infinite: true,
          dots: true,
          centerMode: true,
          nextArrow: '.btn-next0',
          prevArrow: '.btn-prev0',
          variableWidth: true,
          adaptiveHeight: true
        }
      }]
    };
  },
  setEl: function() {
    this.$secFiltroResultado = $('#secFiltroResultado');
    this.$secFiltroResultadoClone = this.$secFiltroResultado.clone();
    this.$secEscolhidos1 = $('#secEscolhidos1');
    this.$secEscolhidos1Clone = this.$secEscolhidos1.clone();
    this.$secEscolhidos2 = $('#secEscolhidos2');
    this.$secEscolhidos2Clone = this.$secEscolhidos2.clone();
    console.log('Filtro', 'setEl', this.$secFiltroResultado);
  },
  cndata: function() {
    return window.Cndata;
  },
  getKeys: function(source) {
    var ret = [];
    for (var key in source) {
      ret.push(key);
    }
    return ret;
  },
  reset: function() {
    Filtro.produto0([]);
    Filtro.step(0);
    Filtro.selectedOcasiao({});
    Filtro.selectedQuem({});
    Filtro.selectedAPessoaE({});
    Filtro.selectedRelacao({});
  },
  naoCalcularFaixas: function() {
    return Filtro.step() < 3;
  },
  _keyAux: function(obj) {
    if (obj && obj.id)
      return obj.id;
    return "";
  },
  currentKey: function() {
    var key = Filtro._keyAux(this.selectedOcasiao()) + Filtro._keyAux(this.selectedQuem()) + Filtro._keyAux(this.selectedAPessoaE()) + Filtro._keyAux(this.selectedRelacao());
    return key;
  },
  currentKeyWithFaixa: function() {
    var key = Filtro.currentKey();
    key += Filtro._keyAux(this.selectedFaixa());
    return key;
  },
  genero: ko.pureComputed(function() {
    try {
      return Filtro.selectedQuem().genero;
    } catch (ex) {
      return "";
    }
  }),
  contemProdutos: ko.pureComputed(function() {
    var key = Filtro.currentKey();
    var len = key.length;
    if (len < 3) {
      return false;
    }
    var source = Filtro.datacache.cache2;
    if (len >= 4) {
      return source[key].length > 0;
    }
    var _res;
    var _reskey;
    var _same;
    var _total;
    for (var i = 0; i < Filtro.cache2Keys.length; ++i) {
      _res = Filtro.cache2Keys[i];
      _reskey = _res.substr(0, len);
      _same = key == _reskey;
      _total = source[_res].length;
      console.log('_res', _res, '_reskey', _reskey, '_same', _same, '_total', _total);
      if (_same && _total > 0) {
        return true;
      }
    }
    return false;
  }),
  selecionouProduto1: ko.pureComputed(function() {
    try {
      console.log('Filtro1', Filtro.produto1().id);
      var p = Filtro.produto1();
      return p && p.length && p.length > 0;
    } catch (ex) {
      console.log('err1', ex);
      return false;
    }
  }),
  selecionouProduto2: ko.pureComputed(function() {
    try {
      console.log('Filtro2', Filtro.produto2().id);
      var p = Filtro.produto2();
      return p && p.length && p.length > 0;
    } catch (ex) {
      console.log('err2', ex);
      return false;
    }
  }),
  selecionouAlgumProduto: ko.pureComputed(function() {
    var hasAny = Filtro.selecionouProduto1() || Filtro.selecionouProduto2();
    return hasAny;
  }),
  sort: function(obarr) {
    return obarr().sort(function(left, right) {
      return left.order() == right.order() ? 0 : (left.order() < right.order() ? -1 : 1);
    });
  },
  selOcasiao: function(item) {
    console.log('Filtro', 'selOcasiao()', 'item', item);
    Filtro.selectedOcasiao(item);
    Filtro.step(1);
    trackAnalytics('natal-pagina', 'p1_qual-a-ocasiao', gastring(item.ocasiao));
  },
  selQuem: function(item) {
    console.log('Filtro', 'selQuem()', 'item', item);
    Filtro.selectedQuem(item);
    Filtro.step(2);
    trackAnalytics('natal-pagina', 'p2_quem-vou-presentear', gastring(item.quem));
    Filtro.listaPessoas.removeAll();
    var pessoas = Filtro.datacache.filtros.apessoae;
    var key = Filtro.currentKey();
    var distinctPessoas = Filtro.datacache.ocasiaoPlusQuem[key];
    if (distinctPessoas && distinctPessoas.length) {
      for (var i = 0; i < distinctPessoas.length; ++i) {
        var _pid = distinctPessoas[i];
        for (var j = 0; j < pessoas.length; ++j) {
          var same = _pid == pessoas[j].id;
          if (same) {
            Filtro.listaPessoas.push(pessoas[j]);
          }
        }
      }
    }
  },
  selAPessoaE: function(item) {
    console.log('Filtro', 'selAPessoaE()', 'item', item);
    Filtro.selectedAPessoaE(item);
    Filtro.step(3);
    trackAnalytics('natal-pagina', 'p3_essa-pessoa-e', gastring(item[Filtro.genero()]));
    Filtro.listaRelacoes.removeAll();
    var relacoes = Filtro.datacache.filtros.relacao;
    var key = Filtro.currentKey();
    var distinctRelacoes = Filtro.datacache.ocasiaoPlusQuemPlusPessoa[key];
    if (distinctRelacoes && distinctRelacoes.length) {
      for (var i = 0; i < distinctRelacoes.length; ++i) {
        var _pid = distinctRelacoes[i];
        for (var j = 0; j < relacoes.length; ++j) {
          var same = _pid == relacoes[j].id;
          if (same) {
            Filtro.listaRelacoes.push(relacoes[j]);
          }
        }
      }
    }
  },
  selRelacao: function(item) {
    console.log('Filtro', 'selRelacao()', 'item', item);
    Filtro.selectedRelacao(item);
    Filtro.step(4);
    trackAnalytics('natal-pagina', 'p4_qual-a-sua-relacao', gastring(item[Filtro.genero()]));
    Filtro.listaFaixas.removeAll();
    var faixas = Filtro.datacache.filtros.valor;
    var buffer = {};
    var key = Filtro.currentKey();
    var prodIds = Filtro.datacache.cache2[key];
    console.log('Filtro', 'selRelacao', 'item', item, 'key', key, 'prodIds', prodIds, 'faixas', faixas.length);
    for (var j = 0; j < faixas.length; ++j) {
      var faixa = faixas[j];
      var existe = undefined != buffer && undefined != buffer[faixa.id];
      if (existe) {
        continue;
      }
      if (prodIds && prodIds.length && prodIds.length > 0) {
        for (var i = 0; i < prodIds.length; ++i) {
          var prodId = prodIds[i];
          var produto = Filtro.datacache.produtos[prodId];
          if (produto) {
            var preco = parseFloat(produto.preco);
            var valido = (faixa.min <= preco && preco <= faixa.max);
            if (valido) {
              buffer[faixa.id] = faixa;
              Filtro.listaFaixas.push(faixa);
              break;
            }
          }
        }
      }
    }
  },
  selFaixa: function(item) {
    console.log('Filtro', 'selFaixa()', 'item', item, this);
    Filtro.selectedFaixa(item);
    Filtro.step(5);
    trackAnalytics('natal-pagina', 'p5_valor-ate', gastring(item.label));
    Filtro.selecionarProduto();
  },
  resultadoFiltro: function() {
    var ret = [];
    var key = Filtro.currentKey();
    var source = Filtro.datacache.cache2;
    var lista = source[key];
    var valid = undefined != lista && undefined != lista.length && lista.length > 0;
    if (false === valid) {
      return ret;
    }
    var produto;
    if (lista.length == 1) {
      ret.push(Filtro.datacache.produtos[lista[0]]);
    } else {
      var min = Filtro.selectedFaixa().min;
      var max = Filtro.selectedFaixa().max;
      for (var i = 0; i < lista.length; ++i) {
        var p = Filtro.datacache.produtos[lista[i]];
        valid = min <= p.preco && p.preco <= max;
        if (true === valid) {
          ret.push(p);
        }
      }
    }
    return ret;
  },
  fillProdutosUnicos: function(key, result) {
    Filtro.userCache[key] = result;
  },
  produtosUnicos: function(result) {
    var valid = undefined != result && undefined != result.length;
    if (false === valid) {
      return [];
    }
    var MAX_RESULT = 3;
    if (0 <= result.length && result.length < MAX_RESULT) {
      return result;
    }
    var key = Filtro.currentKeyWithFaixa();
    var cache = Filtro.userCache[key];
    valid = cache && cache.length && cache.length > 0;
    if (!valid) {
      Filtro.fillProdutosUnicos(key, result);
    }
    var ret = [];
    for (var i = 1; i <= 3; ++i) {
      if (Filtro.userCache[key].length <= 0) {
        Filtro.fillProdutosUnicos(key, result);
      }
      ret.push(Filtro.userCache[key].shift());
    }
    return ret;
  },
  slickIt: function() {
    var carousel = $('.containerProducts');
    carousel.slick(Filtro.filtroSlick);
  },
  atualizarResultadoKnockout: function(produto) {
    Filtro.produto0(produto);
    var el = Filtro.$secFiltroResultado[0];
    ko.cleanNode(el);
    Filtro.$secFiltroResultado.empty();
    $(Filtro.$secFiltroResultadoClone.html()).appendTo(Filtro.$secFiltroResultado);
    ko.applyBindings(Filtro, el);
    if (window.is_mobile()) {
      var valid = produto != null && produto.length != null && produto.length > 1;
      if (true === valid) {
        Filtro.slickIt();
      }
    }
  },
  selecionarProduto: function() {
    var ret = Filtro.resultadoFiltro();
    var valid = undefined != ret && undefined != ret.length && ret.length > 0;
    if (false === valid) {
      Filtro.atualizarResultadoKnockout([]);
      return;
    }
    for (var i = 0; i < ret.length; ++i) {
      ret[i].filtro = window.Filtro;
      ret[i].produtos = window.Produtos;
      ret[i].cndata = window.Cndata;
    }
    var produto = Filtro.produtosUnicos(ret);
    Filtro.atualizarResultadoKnockout(produto);
    var _fraseMagica = Filtro.fraseMagica();
    Filtro.frase0(_fraseMagica);
    var _frasePeq = Filtro.frasePequena(_fraseMagica);
    Filtro.frasepeq0(_frasePeq);
    console.log('Filtro', 'selecionarProduto', _fraseMagica, _frasePeq);
  },
  frasePequena: function(f) {
    var regex = /(\b(([A-Z]|Ç|Á|À|Â|Ã|É|È|Ê|Ê|Í|Ì|Î|Ó|Ò|Ô|Õ|Ú|Ù|Û){2,})\s{0,1}(([A-Z]|Ç|Á|À|Â|Ã|É|È|Ê|Ê|Í|Ì|Î|Ó|Ò|Ô|Õ|Ú|Ù|Û){2,})\b)/;
    var exec = regex.exec(f);
    var resultado = (exec == null) ? false : exec[0];
    if (false === resultado) {
      return f;
    }
    var artigo = ('PRESENTÃO' == resultado || 'PRESENTE' == resultado) ? 'Um ' : 'Uma ';
    var destino = '';
    var quem = Filtro.selectedRelacao()[Filtro.genero()];
    if (quem == 'Amor') {
      destino = ' para o meu ';
    } else {
      destino = (Filtro.genero() == 'M') ? ' para o meu ' : ' para a minha ';
    }
    var final = artigo + resultado + destino + quem;
    return final;
  },
  slickEscolhidos1: function() {
    Filtro.slickEscolhidos(1);
  },
  slickEscolhidos2: function() {
    Filtro.slickEscolhidos(2);
  },
  slickEscolhidos: function(id) {
    var valid = (1 <= id && id <= 2);
    if (false === valid) {
      return;
    }
    var sel = '#secEscolhidos' + id;
    var $escolhidos = $(sel);
    var escolhidos = $escolhidos[0];
    ko.cleanNode(escolhidos);
    $escolhidos.empty();
    $escolhidos.empty();
    var $clone = (id == 1) ? Filtro.$secEscolhidos1Clone : Filtro.$secEscolhidos2Clone;
    $($clone.html()).appendTo($escolhidos);
    ko.applyBindings(Filtro, escolhidos);
    console.log('escolhidos-dbg m', window.is_mobile());
    if (window.is_mobile()) {
      var $carousel = $escolhidos.find('.escolhidos-wrapper-slick');
      console.log('escolhidos-dbg c', $carousel);
      if ($carousel) {
        var produtos = (id == 1) ? Filtro.produto1() : Filtro.produto2();
        valid = produtos != null && produtos.length != null && produtos.length > 1;
        console.log('escolhidos-dbg v', valid);
        if (true === valid) {
          $carousel.slick(Filtro.escolhidosSlick);
        }
      }
    }
  },
  escolherProduto: function() {
    try {
      var p = Filtro.produto0();
      var valid = p != null && p.length != null && p.length > 0;
      if (true === valid) {
        for (var i = 0; i < p.length; ++i) {
          var _p = p[i];
          trackAnalytics('natal-pagina', gastring(_p.nome), 'escolha-mais-presentes-multi');
        }
      }
    } catch (ex) {
      console.log('Filtro', 'escolherProduto', 'ga', ex);
    }
    if (!Filtro.selecionouProduto1()) {
      Filtro.produto1(Filtro.produto0());
      Filtro.frase1(Filtro.frase0());
      Filtro.frasepeq1(Filtro.frasepeq0());
      Filtro.slickEscolhidos1();
    } else if (!Filtro.selecionouProduto2()) {
      Filtro.produto2(Filtro.produto0());
      Filtro.frase2(Filtro.frase0());
      Filtro.frasepeq2(Filtro.frasepeq0());
      Filtro.slickEscolhidos2();
    } else {
      Filtro.produto2(Filtro.produto1());
      Filtro.frase2(Filtro.frase1());
      Filtro.frasepeq2(Filtro.frasepeq1());
      Filtro.produto1(Filtro.produto0());
      Filtro.frase1(Filtro.frase0());
      Filtro.frasepeq1(Filtro.frasepeq0());
      Filtro.slickEscolhidos1();
      Filtro.slickEscolhidos2();
    }
    console.log('Filtro', 'escolherProduto', 'p1', Filtro.produto1(), 'p2', Filtro.produto2(), 'any', Filtro.selecionouAlgumProduto());
    Filtro.reset();
  },
  fraseMagica: function() {
    var frase = "";
    try {
      var _genero = Filtro.genero();
      var _relacao = Filtro.selectedRelacao().id;
      var valid = _genero != "" && undefined != _relacao;
      if (valid) {
        var _frases = Filtro.datacache.frases;
        var _gfrases = _frases[_relacao][_genero];
        frase = Filtro.aleatorio(_gfrases);
        console.log('Filtro', 'frases', '_genero', _genero, '_relacao', _relacao, '_gfrases', _gfrases, 'frase', frase);
      }
    } catch (ex) {
      console.log('ERR', 'Filtro', 'frase()', ex);
    }
    return frase;
  },
  aleatorio: function(list) {
    var valid = list && list.length && list.length > 0;
    console.log('Filtro', 'aleatorio', 'valid', valid, 'list.length', list.length, list);
    if (!valid) {
      return false;
    }
    if (list.length == 1) {
      return list[0];
    }
    var idx;
    for (var i = 0; i < 10; ++i) {
      idx = Math.floor(Math.random() * list.length);
      var v = isvalid(idx, list);
      console.log('Filtro', 'aleatorio', 'idx', idx, 'v', v, 'list.length', list.length, list);
      if (v) {
        break;
      }
    }
    if (isvalid(idx, list)) {
      return list[idx];
    }
    return false;
    function isvalid(_idx, _list) {
      return 0 <= _idx && _idx < _list.length;
    }
  },
  removerProduto1: function() {
    Filtro.removerProduto(1);
  },
  removerProduto2: function() {
    Filtro.removerProduto(2);
  },
  removerProduto: function(id) {
    var valid = (1 <= id && id <= 2);
    console.log('Filtro', 'removerProduto()', 'id', id, 'valid', valid);
    if (!valid) {
      return;
    }
    if (id == 2) {
      Filtro.produto2([]);
      console.log('Filtro', 'tipo1');
      return;
    }
    Filtro.produto1([]);
    if (Filtro.selecionouProduto2()) {
      console.log('Filtro', 'tipo2');
      Filtro.produto1(Filtro.produto2());
      Filtro.produto2([]);
    }
  },
  backto: function(step) {
    console.log('Filtro', 'backto()', 'step', step);
    if (step <= 1)
      Filtro.selectedOcasiao({});
    if (step <= 2)
      Filtro.selectedQuem({});
    if (step <= 3)
      Filtro.selectedAPessoaE({});
    if (step <= 4)
      Filtro.selectedRelacao({});
    if (step <= 5)
      Filtro.selectedFaixa({});
    Filtro.step(step - 1);
  },
  backtoOcasiao: function() {
    Filtro.backto(1);
  },
  backtoQuem: function() {
    Filtro.backto(2);
  },
  backtoPessoa: function() {
    Filtro.backto(3);
  },
  backtoRelacao: function() {
    Filtro.backto(4);
  },
  backtoFaixa: function() {
    Filtro.backto(5);
  }
};
window.Filtro = Filtro;
var $__default = Filtro;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/filtro.js
},{"./cndata.js":3,"./datacache.js":4,"./produtos.js":12}],7:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__datacache_46_js__,
    $__filtro_46_js__,
    $__cndata_46_js__,
    $__produtos_46_js__;
var Datacache = ($__datacache_46_js__ = require("./datacache.js"), $__datacache_46_js__ && $__datacache_46_js__.__esModule && $__datacache_46_js__ || {default: $__datacache_46_js__}).default;
var Filtro = ($__filtro_46_js__ = require("./filtro.js"), $__filtro_46_js__ && $__filtro_46_js__.__esModule && $__filtro_46_js__ || {default: $__filtro_46_js__}).default;
var Cndata = ($__cndata_46_js__ = require("./cndata.js"), $__cndata_46_js__ && $__cndata_46_js__.__esModule && $__cndata_46_js__ || {default: $__cndata_46_js__}).default;
var Produtos = ($__produtos_46_js__ = require("./produtos.js"), $__produtos_46_js__ && $__produtos_46_js__.__esModule && $__produtos_46_js__ || {default: $__produtos_46_js__}).default;
var KnockoutMngr = {
  errors: 0,
  init: function() {
    console.log('KnockoutMngr Mode=ON');
    KnockoutMngr.createDirective();
    KnockoutMngr.createTemplates();
    KnockoutMngr.apply(Filtro, ["#presentes", "#produtos", "#lingueta"]);
    KnockoutMngr.apply(Datacache, ["#slick-carousel"]);
    console.log('/KnockoutMngr', 'ERRORS', this.errors);
  },
  createTemplates: function() {},
  createDirective: function() {
    ko.bindingHandlers.stopBinding = {init: function() {
        return {controlsDescendantBindings: true};
      }};
  },
  apply: function(obj, jqSelectors) {
    for (var i = 0; i < jqSelectors.length; ++i) {
      try {
        var selector = jqSelectors[i];
        var selectorInstances = $(selector);
        console.log('Knockout', 'apply', 'selector', selector, selectorInstances.length);
        selectorInstances.each(function(idx, el) {
          try {
            ko.applyBindings(obj, el);
          } catch (jex) {
            window.KnockoutMngr.errors++;
            console.log('Knockout Error', 'selector=', selector, 'el=', el, 'vm=', obj, jex);
          }
        });
      } catch (ex) {
        console.log('Knockout', ex);
      }
    }
  }
};
window.KnockoutMngr = KnockoutMngr;
var $__default = KnockoutMngr;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/knockoumngrt.js
},{"./cndata.js":3,"./datacache.js":4,"./filtro.js":6,"./produtos.js":12}],8:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__cndata__;
var Cndata = ($__cndata__ = require("./cndata"), $__cndata__ && $__cndata__.__esModule && $__cndata__ || {default: $__cndata__}).default;
var KoHandlers = {
  init: function() {
    console.log('KoHandlers Mode=ON');
    this.desktopFiltroClick();
    this.mobileFiltroClick();
    this.desktopEscolhidosClick();
    this.mobileEscolhidosClick();
    console.log('/KoHandlers');
  },
  mobileFiltroClick: function() {
    var obj = {};
    obj.work = function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      var value = ko.unwrap(valueAccessor());
      console.log('KoHandlers', 'mobileFiltroClick', value, element);
      $(element).click(function() {
        var current = $('.containerProducts').find('.slick-current');
        if (current) {
          var idx = current.attr('data-filtroidx');
          console.log('KoHandlers', 'mobileFiltroClick', 'idx', idx);
          window.Cndata.abrirFiltro(idx);
        }
      });
    };
    obj.init = function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      obj.work(element, valueAccessor, allBindings, viewModel, bindingContext);
    };
    obj.update = function(element, valueAccessor, allBindings, viewModel, bindingContext) {};
    ko.bindingHandlers.mobileFiltroClick = obj;
  },
  desktopFiltroClick: function() {
    var obj = {};
    obj.work = function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      var vm = bindingContext.$rawData;
      var idx = ko.unwrap(valueAccessor());
      $(element).click(function() {
        vm.abrirFiltro(idx);
      });
    };
    obj.init = function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      obj.work(element, valueAccessor, allBindings, viewModel, bindingContext);
    };
    obj.update = function(element, valueAccessor, allBindings, viewModel, bindingContext) {};
    ko.bindingHandlers.desktopFiltroClick = obj;
  },
  desktopEscolhidosClick: function() {
    var obj = {};
    obj.init = function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      try {
        $(element).click(function() {
          var value = ko.unwrap(valueAccessor());
          var valid = (value != null && value.length != null && value.length == 2);
          if (false === valid) {
            return;
          }
          var vm = bindingContext.$rawData;
          var idxEscolhido = value[0];
          var idxProduto = value[1];
          (0 == idxEscolhido) ? vm.abrirEscolhido1(idxProduto) : vm.abrirEscolhido2(idxProduto);
        });
      } catch (ex) {
        console.log('KoHandlers', 'desktopEscolhidosClick', ex);
      }
    };
    ko.bindingHandlers.desktopEscolhidosClick = obj;
  },
  mobileEscolhidosClick: function() {
    var obj = {};
    obj.init = function(element, valueAccessor, allBindings, viewModel, bindingContext) {
      $(element).click(function() {
        var value = ko.unwrap(valueAccessor());
        var valid = (0 <= value && value <= 1);
        if (false === valid) {
          return;
        }
        var $sec = (value == 0) ? $('#secEscolhidos1') : $('#secEscolhidos2');
        var $wrapper = $sec.find('.escolhidos-wrapper-slick');
        if ($sec && $wrapper) {
          var current = $wrapper.find('.slick-current');
          console.log('current', current);
          if (current) {
            var idx = current.attr('data-filtroidx');
            console.log('KoHandlers', 'mobileEscolhidosClick', 'idx', idx);
            (value == 0) ? window.Cndata.abrirEscolhido1(idx) : window.Cndata.abrirEscolhido2(idx);
          }
        }
      });
    };
    ko.bindingHandlers.mobileEscolhidosClick = obj;
  }
};
window.KoHandlers = KoHandlers;
var $__default = KoHandlers;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/kohandlers.js
},{"./cndata":3}],9:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__datacache_46_js__,
    $__filtro_46_js__,
    $__cndata__;
var Datacache = ($__datacache_46_js__ = require("./datacache.js"), $__datacache_46_js__ && $__datacache_46_js__.__esModule && $__datacache_46_js__ || {default: $__datacache_46_js__}).default;
var Filtro = ($__filtro_46_js__ = require("./filtro.js"), $__filtro_46_js__ && $__filtro_46_js__.__esModule && $__filtro_46_js__ || {default: $__filtro_46_js__}).default;
var Cndata = ($__cndata__ = require("./cndata"), $__cndata__ && $__cndata__.__esModule && $__cndata__ || {default: $__cndata__}).default;
var Lingueta = {
  hasCn: Filtro.hasCn,
  init: function() {}
};
window.Lingueta = Lingueta;
var $__default = Lingueta;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/lingueta.js
},{"./cndata":3,"./datacache.js":4,"./filtro.js":6}],10:[function(require,module,exports){
"use strict";
var $__banner__,
    $__presentes__,
    $__escolhidos__,
    $__produtos__,
    $__lingueta__,
    $__utils_47_scroll_45_anchor__,
    $__datacache_46_js__,
    $__filtro_46_js__,
    $__knockoumngrt_46_js__,
    $__cndata_46_js__,
    $__blacklist_46_js__,
    $__kohandlers__;
var Banner = ($__banner__ = require("./banner"), $__banner__ && $__banner__.__esModule && $__banner__ || {default: $__banner__}).default;
var Presentes = ($__presentes__ = require("./presentes"), $__presentes__ && $__presentes__.__esModule && $__presentes__ || {default: $__presentes__}).default;
var Escolhidos = ($__escolhidos__ = require("./escolhidos"), $__escolhidos__ && $__escolhidos__.__esModule && $__escolhidos__ || {default: $__escolhidos__}).default;
var Produtos = ($__produtos__ = require("./produtos"), $__produtos__ && $__produtos__.__esModule && $__produtos__ || {default: $__produtos__}).default;
var Lingueta = ($__lingueta__ = require("./lingueta"), $__lingueta__ && $__lingueta__.__esModule && $__lingueta__ || {default: $__lingueta__}).default;
var ScrollAnchor = ($__utils_47_scroll_45_anchor__ = require("./utils/scroll-anchor"), $__utils_47_scroll_45_anchor__ && $__utils_47_scroll_45_anchor__.__esModule && $__utils_47_scroll_45_anchor__ || {default: $__utils_47_scroll_45_anchor__}).default;
var Datacache = ($__datacache_46_js__ = require("./datacache.js"), $__datacache_46_js__ && $__datacache_46_js__.__esModule && $__datacache_46_js__ || {default: $__datacache_46_js__}).default;
var Filtro = ($__filtro_46_js__ = require("./filtro.js"), $__filtro_46_js__ && $__filtro_46_js__.__esModule && $__filtro_46_js__ || {default: $__filtro_46_js__}).default;
var KnockoutMngr = ($__knockoumngrt_46_js__ = require("./knockoumngrt.js"), $__knockoumngrt_46_js__ && $__knockoumngrt_46_js__.__esModule && $__knockoumngrt_46_js__ || {default: $__knockoumngrt_46_js__}).default;
var Cndata = ($__cndata_46_js__ = require("./cndata.js"), $__cndata_46_js__ && $__cndata_46_js__.__esModule && $__cndata_46_js__ || {default: $__cndata_46_js__}).default;
var Blacklist = ($__blacklist_46_js__ = require("./blacklist.js"), $__blacklist_46_js__ && $__blacklist_46_js__.__esModule && $__blacklist_46_js__ || {default: $__blacklist_46_js__}).default;
var KoHandlers = ($__kohandlers__ = require("./kohandlers"), $__kohandlers__ && $__kohandlers__.__esModule && $__kohandlers__ || {default: $__kohandlers__}).default;
var Main = {
  init: function() {
    Blacklist.init();
    Cndata.init();
    Datacache.init();
    Filtro.init();
    Banner.init();
    Presentes.init();
    Escolhidos.init();
    Produtos.init();
    Lingueta.init();
    ScrollAnchor.init();
    KoHandlers.init();
    KnockoutMngr.init();
    Produtos.update();
    this.trackPageview();
  },
  trackPageview: function() {
    var _that = this;
    if (typeof ga === "undefined") {
      setTimeout(function() {
        _that.trackPageview();
      }, 500);
      return false;
    }
    if (Cndata.isCommon()) {
      ga('send', 'pageview', {
        'page': "/www/presentes/natal-natura",
        'title': ""
      });
    } else {
      var tel = gastring(Cndata.telefone());
      ga('send', 'pageview', {
        'page': "/www/presentes/natal-natura/personalizada-" + tel,
        'title': ""
      });
    }
  }
};
$(document).ready(function() {
  Main.init();
});


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/main.js
},{"./banner":1,"./blacklist.js":2,"./cndata.js":3,"./datacache.js":4,"./escolhidos":5,"./filtro.js":6,"./knockoumngrt.js":7,"./kohandlers":8,"./lingueta":9,"./presentes":11,"./produtos":12,"./utils/scroll-anchor":13}],11:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var Presentes = {
  init: function() {
    console.log('Presentes: Mode=ON');
    this.animacao();
  },
  animacao: function() {
    var titulo = [];
    titulo[0] = '01.png';
    titulo[1] = '10.png';
    titulo[2] = '17.png';
    titulo[3] = '25.png';
    var el = $('#presentes .bloco-title h1');
    function animacao(step) {
      step = step < titulo.length - 1 ? step + 1 : 0;
      $(el).animate({opacity: 0}, 2000, function() {
        $(this).css({"background-image": "url(" + PersistData.baseUrl + "/img/PNGs/" + titulo[step] + ")"}).animate({opacity: 1}, 2000, function() {
          animacao(step);
        });
      });
    }
    animacao(0);
  }
};
var $__default = Presentes;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/presentes.js
},{}],12:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var $__datacache_46_js__,
    $__filtro_46_js__,
    $__cndata__;
var Datacache = ($__datacache_46_js__ = require("./datacache.js"), $__datacache_46_js__ && $__datacache_46_js__.__esModule && $__datacache_46_js__ || {default: $__datacache_46_js__}).default;
var Filtro = ($__filtro_46_js__ = require("./filtro.js"), $__filtro_46_js__ && $__filtro_46_js__.__esModule && $__filtro_46_js__ || {default: $__filtro_46_js__}).default;
var Cndata = ($__cndata__ = require("./cndata"), $__cndata__ && $__cndata__.__esModule && $__cndata__ || {default: $__cndata__}).default;
var Produtos = {
  hasCn: Filtro.hasCn,
  init: function() {
    console.log('Produtos: Mode=ON');
    this.setElements();
    this.bind();
    console.log('/Produtos');
  },
  update: function() {
    $('#slick-carousel').slick({
      infinite: true,
      slidesToShow: 6,
      slidesToScroll: 1,
      centerMode: true,
      variableWidth: true,
      nextArrow: '.btn-next',
      prevArrow: '.btn-prev'
    });
  },
  abrirProduto: function(sel) {
    var p = sel != null ? sel : this.currentCarrosel();
    if (p == null) {
      return;
    }
    var urlProduto = p.linkrede;
    var _utm_source = 'SiteCF_Natal';
    var _utm_campaign = 'Natal';
    var _utm_medium = p.utm_medium || 'Botao_Compre_Agora';
    var _utm_content = window.gastring(p.nome);
    if (window.Cndata.ctaExibirRede()) {
      var exp = /^((http:\/\/){0,1}rede\.{0,1}natura\.net\/espaco\/)(.*)$/i;
      var m = exp.exec(urlProduto);
      if (m != null) {
        var prefix = m[1];
        var sufix = m[3];
        var rede = window.Cndata.rede() + '/';
        urlProduto = prefix + rede + sufix;
        _utm_medium += '_CN';
      }
    }
    var utm = '?utm_source=' + _utm_source + '&utm_medium=' + _utm_medium + '&utm_campaign=' + _utm_campaign + '&utm_content=' + _utm_content;
    var url = urlProduto + utm;
    console.log('Produto', url);
    window.open(url);
  },
  setElements: function() {
    this.$vitrineModal = $("#vitrine-modal");
    this.$btnVitrineLightbox = $("#btn-vitrine-lightbox");
    this.$btnPresenteLightbox = $(".btn-presente-lightbox");
    this.$btnDetalhesLightbox = $(".btnDetalhesEscolhidos");
    this.$btnURLLightbox = $(".cta-consultora-resp");
    this.$btnModalFechar = $("#btn-modal-fechar");
    this.$produtosDivider = $('#produtos-divider');
    if (Produtos.hasCn()) {
      console.log('HASCN', Produtos.hasCn());
      this.$produtosDivider.show();
    }
  },
  currentCarrosel: function() {
    var $sec = $('#vitrine-carrosel-slick');
    var currentEl = $sec.find('.slick-current');
    var idprod = currentEl.data("idimg");
    if (!idprod) {
      return false;
    }
    var produto = Datacache.produtos[idprod];
    return produto;
  },
  currentProduct: function() {
    var currentEl = $('.produto-img');
    var idprod = currentEl.data("idimg");
    if (!idprod) {
      return false;
    }
    var produto = Datacache.produtos[idprod];
    return produto;
  },
  currentEscolhido: function() {
    var currentEl = $('.escolhido-thumb');
    var idprod = currentEl.data("idimg");
    if (!idprod) {
      return false;
    }
    var produto = Datacache.produtos[idprod];
    return produto;
  },
  bind: function() {
    var _that = this;
    this.$vitrineModal.on('click', function() {
      _that.hideModal();
    });
    this.$btnModalFechar.on('click', function() {
      _that.hideModal();
    });
    this.$btnVitrineLightbox.on('click', function() {
      var produto = Produtos.currentCarrosel();
      Produtos.abrirModal(produto);
    });
    this.$btnPresenteLightbox.on('click', function() {
      var produto = Produtos.currentProduct();
      console.log('Produto Presente', produto, Produtos);
      trackAnalytics('natal-pagina', gastring(produto.nome), 'veja-mais-detalhes');
      var img = PersistData.baseUrl + produto.lightbox;
      $('#vitrine-modal-img-lightbox').attr('src', img);
      setTimeout(function() {
        _that.showModal();
      }, 99);
    });
    this.$btnDetalhesLightbox.on('click', function() {
      var produto = Produtos.currentEscolhido();
      console.log('Produto Escolhido', produto, Produtos);
      trackAnalytics('natal-pagina', gastring(produto.nome), 'veja-mais-detalhes');
      var img = PersistData.baseUrl + produto.lightbox;
      $('#vitrine-modal-img-lightbox').attr('src', img);
      setTimeout(function() {
        _that.showModal();
      }, 99);
    });
  },
  abrirModal: function(produto) {
    console.log('Produto', produto, Produtos);
    trackAnalytics('natal-pagina', gastring(produto.nome), 'veja-mais-detalhes');
    var img = PersistData.baseUrl + produto.lightbox;
    $('#vitrine-modal-img-lightbox').attr('src', img);
    setTimeout(function() {
      window.Produtos.showModal();
    }, 99);
  },
  showModal: function() {
    this.$vitrineModal.show();
  },
  hideModal: function() {
    this.$vitrineModal.hide();
  }
};
window.Produtos = Produtos;
var $__default = Produtos;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/produtos.js
},{"./cndata":3,"./datacache.js":4,"./filtro.js":6}],13:[function(require,module,exports){
"use strict";
Object.defineProperties(exports, {
  default: {get: function() {
      return $__default;
    }},
  __esModule: {value: true}
});
var ScrollAnchor = {
  speed: 500,
  init: function() {
    this.bind();
  },
  bind: function() {
    var _that = this;
    $(document).on('click', '[data-anchor-go]', function() {
      _that.go($(this).data('anchor-go'));
    });
  },
  go: function(target, callback) {
    var $el = $(target),
        callback = callback || function() {};
    if (!$el.length)
      return false;
    var y = $el.offset().top;
    $('html, body').animate({scrollTop: y}, this.speed, function() {
      callback();
    });
  }
};
var $__default = ScrollAnchor;


//# sourceURL=C:/workspace/Natura-CF-Natal_2015-Produção/frontend/app/js/utils/scroll-anchor.js
},{}],14:[function(require,module,exports){
(function (process){
// Copyright Joyent, Inc. and other Node contributors.
//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the
// "Software"), to deal in the Software without restriction, including
// without limitation the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the Software, and to permit
// persons to whom the Software is furnished to do so, subject to the
// following conditions:
//
// The above copyright notice and this permission notice shall be included
// in all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
// OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
// MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN
// NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
// DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE
// USE OR OTHER DEALINGS IN THE SOFTWARE.

// resolves . and .. elements in a path array with directory names there
// must be no slashes, empty elements, or device names (c:\) in the array
// (so also no leading and trailing slashes - it does not distinguish
// relative and absolute paths)
function normalizeArray(parts, allowAboveRoot) {
  // if the path tries to go above the root, `up` ends up > 0
  var up = 0;
  for (var i = parts.length - 1; i >= 0; i--) {
    var last = parts[i];
    if (last === '.') {
      parts.splice(i, 1);
    } else if (last === '..') {
      parts.splice(i, 1);
      up++;
    } else if (up) {
      parts.splice(i, 1);
      up--;
    }
  }

  // if the path is allowed to go above the root, restore leading ..s
  if (allowAboveRoot) {
    for (; up--; up) {
      parts.unshift('..');
    }
  }

  return parts;
}

// Split a filename into [root, dir, basename, ext], unix version
// 'root' is just a slash, or nothing.
var splitPathRe =
    /^(\/?|)([\s\S]*?)((?:\.{1,2}|[^\/]+?|)(\.[^.\/]*|))(?:[\/]*)$/;
var splitPath = function(filename) {
  return splitPathRe.exec(filename).slice(1);
};

// path.resolve([from ...], to)
// posix version
exports.resolve = function() {
  var resolvedPath = '',
      resolvedAbsolute = false;

  for (var i = arguments.length - 1; i >= -1 && !resolvedAbsolute; i--) {
    var path = (i >= 0) ? arguments[i] : process.cwd();

    // Skip empty and invalid entries
    if (typeof path !== 'string') {
      throw new TypeError('Arguments to path.resolve must be strings');
    } else if (!path) {
      continue;
    }

    resolvedPath = path + '/' + resolvedPath;
    resolvedAbsolute = path.charAt(0) === '/';
  }

  // At this point the path should be resolved to a full absolute path, but
  // handle relative paths to be safe (might happen when process.cwd() fails)

  // Normalize the path
  resolvedPath = normalizeArray(filter(resolvedPath.split('/'), function(p) {
    return !!p;
  }), !resolvedAbsolute).join('/');

  return ((resolvedAbsolute ? '/' : '') + resolvedPath) || '.';
};

// path.normalize(path)
// posix version
exports.normalize = function(path) {
  var isAbsolute = exports.isAbsolute(path),
      trailingSlash = substr(path, -1) === '/';

  // Normalize the path
  path = normalizeArray(filter(path.split('/'), function(p) {
    return !!p;
  }), !isAbsolute).join('/');

  if (!path && !isAbsolute) {
    path = '.';
  }
  if (path && trailingSlash) {
    path += '/';
  }

  return (isAbsolute ? '/' : '') + path;
};

// posix version
exports.isAbsolute = function(path) {
  return path.charAt(0) === '/';
};

// posix version
exports.join = function() {
  var paths = Array.prototype.slice.call(arguments, 0);
  return exports.normalize(filter(paths, function(p, index) {
    if (typeof p !== 'string') {
      throw new TypeError('Arguments to path.join must be strings');
    }
    return p;
  }).join('/'));
};


// path.relative(from, to)
// posix version
exports.relative = function(from, to) {
  from = exports.resolve(from).substr(1);
  to = exports.resolve(to).substr(1);

  function trim(arr) {
    var start = 0;
    for (; start < arr.length; start++) {
      if (arr[start] !== '') break;
    }

    var end = arr.length - 1;
    for (; end >= 0; end--) {
      if (arr[end] !== '') break;
    }

    if (start > end) return [];
    return arr.slice(start, end - start + 1);
  }

  var fromParts = trim(from.split('/'));
  var toParts = trim(to.split('/'));

  var length = Math.min(fromParts.length, toParts.length);
  var samePartsLength = length;
  for (var i = 0; i < length; i++) {
    if (fromParts[i] !== toParts[i]) {
      samePartsLength = i;
      break;
    }
  }

  var outputParts = [];
  for (var i = samePartsLength; i < fromParts.length; i++) {
    outputParts.push('..');
  }

  outputParts = outputParts.concat(toParts.slice(samePartsLength));

  return outputParts.join('/');
};

exports.sep = '/';
exports.delimiter = ':';

exports.dirname = function(path) {
  var result = splitPath(path),
      root = result[0],
      dir = result[1];

  if (!root && !dir) {
    // No dirname whatsoever
    return '.';
  }

  if (dir) {
    // It has a dirname, strip trailing slash
    dir = dir.substr(0, dir.length - 1);
  }

  return root + dir;
};


exports.basename = function(path, ext) {
  var f = splitPath(path)[2];
  // TODO: make this comparison case-insensitive on windows?
  if (ext && f.substr(-1 * ext.length) === ext) {
    f = f.substr(0, f.length - ext.length);
  }
  return f;
};


exports.extname = function(path) {
  return splitPath(path)[3];
};

function filter (xs, f) {
    if (xs.filter) return xs.filter(f);
    var res = [];
    for (var i = 0; i < xs.length; i++) {
        if (f(xs[i], i, xs)) res.push(xs[i]);
    }
    return res;
}

// String.prototype.substr - negative index don't work in IE8
var substr = 'ab'.substr(-1) === 'b'
    ? function (str, start, len) { return str.substr(start, len) }
    : function (str, start, len) {
        if (start < 0) start = str.length + start;
        return str.substr(start, len);
    }
;

}).call(this,require('_process'))
},{"_process":15}],15:[function(require,module,exports){
// shim for using process in browser

var process = module.exports = {};
var queue = [];
var draining = false;

function drainQueue() {
    if (draining) {
        return;
    }
    draining = true;
    var currentQueue;
    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        var i = -1;
        while (++i < len) {
            currentQueue[i]();
        }
        len = queue.length;
    }
    draining = false;
}
process.nextTick = function (fun) {
    queue.push(fun);
    if (!draining) {
        setTimeout(drainQueue, 0);
    }
};

process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

// TODO(shtylman)
process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };

},{}],16:[function(require,module,exports){
(function (process,global){
(function(global) {
  'use strict';
  if (global.$traceurRuntime) {
    return;
  }
  var $Object = Object;
  var $TypeError = TypeError;
  var $create = $Object.create;
  var $defineProperties = $Object.defineProperties;
  var $defineProperty = $Object.defineProperty;
  var $freeze = $Object.freeze;
  var $getOwnPropertyDescriptor = $Object.getOwnPropertyDescriptor;
  var $getOwnPropertyNames = $Object.getOwnPropertyNames;
  var $keys = $Object.keys;
  var $hasOwnProperty = $Object.prototype.hasOwnProperty;
  var $toString = $Object.prototype.toString;
  var $preventExtensions = Object.preventExtensions;
  var $seal = Object.seal;
  var $isExtensible = Object.isExtensible;
  function nonEnum(value) {
    return {
      configurable: true,
      enumerable: false,
      value: value,
      writable: true
    };
  }
  var method = nonEnum;
  var counter = 0;
  function newUniqueString() {
    return '__$' + Math.floor(Math.random() * 1e9) + '$' + ++counter + '$__';
  }
  var symbolInternalProperty = newUniqueString();
  var symbolDescriptionProperty = newUniqueString();
  var symbolDataProperty = newUniqueString();
  var symbolValues = $create(null);
  var privateNames = $create(null);
  function isPrivateName(s) {
    return privateNames[s];
  }
  function createPrivateName() {
    var s = newUniqueString();
    privateNames[s] = true;
    return s;
  }
  function isShimSymbol(symbol) {
    return typeof symbol === 'object' && symbol instanceof SymbolValue;
  }
  function typeOf(v) {
    if (isShimSymbol(v))
      return 'symbol';
    return typeof v;
  }
  function Symbol(description) {
    var value = new SymbolValue(description);
    if (!(this instanceof Symbol))
      return value;
    throw new TypeError('Symbol cannot be new\'ed');
  }
  $defineProperty(Symbol.prototype, 'constructor', nonEnum(Symbol));
  $defineProperty(Symbol.prototype, 'toString', method(function() {
    var symbolValue = this[symbolDataProperty];
    if (!getOption('symbols'))
      return symbolValue[symbolInternalProperty];
    if (!symbolValue)
      throw TypeError('Conversion from symbol to string');
    var desc = symbolValue[symbolDescriptionProperty];
    if (desc === undefined)
      desc = '';
    return 'Symbol(' + desc + ')';
  }));
  $defineProperty(Symbol.prototype, 'valueOf', method(function() {
    var symbolValue = this[symbolDataProperty];
    if (!symbolValue)
      throw TypeError('Conversion from symbol to string');
    if (!getOption('symbols'))
      return symbolValue[symbolInternalProperty];
    return symbolValue;
  }));
  function SymbolValue(description) {
    var key = newUniqueString();
    $defineProperty(this, symbolDataProperty, {value: this});
    $defineProperty(this, symbolInternalProperty, {value: key});
    $defineProperty(this, symbolDescriptionProperty, {value: description});
    freeze(this);
    symbolValues[key] = this;
  }
  $defineProperty(SymbolValue.prototype, 'constructor', nonEnum(Symbol));
  $defineProperty(SymbolValue.prototype, 'toString', {
    value: Symbol.prototype.toString,
    enumerable: false
  });
  $defineProperty(SymbolValue.prototype, 'valueOf', {
    value: Symbol.prototype.valueOf,
    enumerable: false
  });
  var hashProperty = createPrivateName();
  var hashPropertyDescriptor = {value: undefined};
  var hashObjectProperties = {
    hash: {value: undefined},
    self: {value: undefined}
  };
  var hashCounter = 0;
  function getOwnHashObject(object) {
    var hashObject = object[hashProperty];
    if (hashObject && hashObject.self === object)
      return hashObject;
    if ($isExtensible(object)) {
      hashObjectProperties.hash.value = hashCounter++;
      hashObjectProperties.self.value = object;
      hashPropertyDescriptor.value = $create(null, hashObjectProperties);
      $defineProperty(object, hashProperty, hashPropertyDescriptor);
      return hashPropertyDescriptor.value;
    }
    return undefined;
  }
  function freeze(object) {
    getOwnHashObject(object);
    return $freeze.apply(this, arguments);
  }
  function preventExtensions(object) {
    getOwnHashObject(object);
    return $preventExtensions.apply(this, arguments);
  }
  function seal(object) {
    getOwnHashObject(object);
    return $seal.apply(this, arguments);
  }
  freeze(SymbolValue.prototype);
  function isSymbolString(s) {
    return symbolValues[s] || privateNames[s];
  }
  function toProperty(name) {
    if (isShimSymbol(name))
      return name[symbolInternalProperty];
    return name;
  }
  function removeSymbolKeys(array) {
    var rv = [];
    for (var i = 0; i < array.length; i++) {
      if (!isSymbolString(array[i])) {
        rv.push(array[i]);
      }
    }
    return rv;
  }
  function getOwnPropertyNames(object) {
    return removeSymbolKeys($getOwnPropertyNames(object));
  }
  function keys(object) {
    return removeSymbolKeys($keys(object));
  }
  function getOwnPropertySymbols(object) {
    var rv = [];
    var names = $getOwnPropertyNames(object);
    for (var i = 0; i < names.length; i++) {
      var symbol = symbolValues[names[i]];
      if (symbol) {
        rv.push(symbol);
      }
    }
    return rv;
  }
  function getOwnPropertyDescriptor(object, name) {
    return $getOwnPropertyDescriptor(object, toProperty(name));
  }
  function hasOwnProperty(name) {
    return $hasOwnProperty.call(this, toProperty(name));
  }
  function getOption(name) {
    return global.traceur && global.traceur.options[name];
  }
  function defineProperty(object, name, descriptor) {
    if (isShimSymbol(name)) {
      name = name[symbolInternalProperty];
    }
    $defineProperty(object, name, descriptor);
    return object;
  }
  function polyfillObject(Object) {
    $defineProperty(Object, 'defineProperty', {value: defineProperty});
    $defineProperty(Object, 'getOwnPropertyNames', {value: getOwnPropertyNames});
    $defineProperty(Object, 'getOwnPropertyDescriptor', {value: getOwnPropertyDescriptor});
    $defineProperty(Object.prototype, 'hasOwnProperty', {value: hasOwnProperty});
    $defineProperty(Object, 'freeze', {value: freeze});
    $defineProperty(Object, 'preventExtensions', {value: preventExtensions});
    $defineProperty(Object, 'seal', {value: seal});
    $defineProperty(Object, 'keys', {value: keys});
  }
  function exportStar(object) {
    for (var i = 1; i < arguments.length; i++) {
      var names = $getOwnPropertyNames(arguments[i]);
      for (var j = 0; j < names.length; j++) {
        var name = names[j];
        if (isSymbolString(name))
          continue;
        (function(mod, name) {
          $defineProperty(object, name, {
            get: function() {
              return mod[name];
            },
            enumerable: true
          });
        })(arguments[i], names[j]);
      }
    }
    return object;
  }
  function isObject(x) {
    return x != null && (typeof x === 'object' || typeof x === 'function');
  }
  function toObject(x) {
    if (x == null)
      throw $TypeError();
    return $Object(x);
  }
  function checkObjectCoercible(argument) {
    if (argument == null) {
      throw new TypeError('Value cannot be converted to an Object');
    }
    return argument;
  }
  function polyfillSymbol(global, Symbol) {
    if (!global.Symbol) {
      global.Symbol = Symbol;
      Object.getOwnPropertySymbols = getOwnPropertySymbols;
    }
    if (!global.Symbol.iterator) {
      global.Symbol.iterator = Symbol('Symbol.iterator');
    }
  }
  function setupGlobals(global) {
    polyfillSymbol(global, Symbol);
    global.Reflect = global.Reflect || {};
    global.Reflect.global = global.Reflect.global || global;
    polyfillObject(global.Object);
  }
  setupGlobals(global);
  global.$traceurRuntime = {
    checkObjectCoercible: checkObjectCoercible,
    createPrivateName: createPrivateName,
    defineProperties: $defineProperties,
    defineProperty: $defineProperty,
    exportStar: exportStar,
    getOwnHashObject: getOwnHashObject,
    getOwnPropertyDescriptor: $getOwnPropertyDescriptor,
    getOwnPropertyNames: $getOwnPropertyNames,
    isObject: isObject,
    isPrivateName: isPrivateName,
    isSymbolString: isSymbolString,
    keys: $keys,
    setupGlobals: setupGlobals,
    toObject: toObject,
    toProperty: toProperty,
    typeof: typeOf
  };
})(typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : this);
(function() {
  'use strict';
  var path;
  function relativeRequire(callerPath, requiredPath) {
    path = path || typeof require !== 'undefined' && require('path');
    function isDirectory(path) {
      return path.slice(-1) === '/';
    }
    function isAbsolute(path) {
      return path[0] === '/';
    }
    function isRelative(path) {
      return path[0] === '.';
    }
    if (isDirectory(requiredPath) || isAbsolute(requiredPath))
      return;
    return isRelative(requiredPath) ? require(path.resolve(path.dirname(callerPath), requiredPath)) : require(requiredPath);
  }
  $traceurRuntime.require = relativeRequire;
})();
(function() {
  'use strict';
  function spread() {
    var rv = [],
        j = 0,
        iterResult;
    for (var i = 0; i < arguments.length; i++) {
      var valueToSpread = $traceurRuntime.checkObjectCoercible(arguments[i]);
      if (typeof valueToSpread[$traceurRuntime.toProperty(Symbol.iterator)] !== 'function') {
        throw new TypeError('Cannot spread non-iterable object.');
      }
      var iter = valueToSpread[$traceurRuntime.toProperty(Symbol.iterator)]();
      while (!(iterResult = iter.next()).done) {
        rv[j++] = iterResult.value;
      }
    }
    return rv;
  }
  $traceurRuntime.spread = spread;
})();
(function() {
  'use strict';
  var $Object = Object;
  var $TypeError = TypeError;
  var $create = $Object.create;
  var $defineProperties = $traceurRuntime.defineProperties;
  var $defineProperty = $traceurRuntime.defineProperty;
  var $getOwnPropertyDescriptor = $traceurRuntime.getOwnPropertyDescriptor;
  var $getOwnPropertyNames = $traceurRuntime.getOwnPropertyNames;
  var $getPrototypeOf = Object.getPrototypeOf;
  var $__0 = Object,
      getOwnPropertyNames = $__0.getOwnPropertyNames,
      getOwnPropertySymbols = $__0.getOwnPropertySymbols;
  function superDescriptor(homeObject, name) {
    var proto = $getPrototypeOf(homeObject);
    do {
      var result = $getOwnPropertyDescriptor(proto, name);
      if (result)
        return result;
      proto = $getPrototypeOf(proto);
    } while (proto);
    return undefined;
  }
  function superConstructor(ctor) {
    return ctor.__proto__;
  }
  function superCall(self, homeObject, name, args) {
    return superGet(self, homeObject, name).apply(self, args);
  }
  function superGet(self, homeObject, name) {
    var descriptor = superDescriptor(homeObject, name);
    if (descriptor) {
      if (!descriptor.get)
        return descriptor.value;
      return descriptor.get.call(self);
    }
    return undefined;
  }
  function superSet(self, homeObject, name, value) {
    var descriptor = superDescriptor(homeObject, name);
    if (descriptor && descriptor.set) {
      descriptor.set.call(self, value);
      return value;
    }
    throw $TypeError(("super has no setter '" + name + "'."));
  }
  function getDescriptors(object) {
    var descriptors = {};
    var names = getOwnPropertyNames(object);
    for (var i = 0; i < names.length; i++) {
      var name = names[i];
      descriptors[name] = $getOwnPropertyDescriptor(object, name);
    }
    var symbols = getOwnPropertySymbols(object);
    for (var i = 0; i < symbols.length; i++) {
      var symbol = symbols[i];
      descriptors[$traceurRuntime.toProperty(symbol)] = $getOwnPropertyDescriptor(object, $traceurRuntime.toProperty(symbol));
    }
    return descriptors;
  }
  function createClass(ctor, object, staticObject, superClass) {
    $defineProperty(object, 'constructor', {
      value: ctor,
      configurable: true,
      enumerable: false,
      writable: true
    });
    if (arguments.length > 3) {
      if (typeof superClass === 'function')
        ctor.__proto__ = superClass;
      ctor.prototype = $create(getProtoParent(superClass), getDescriptors(object));
    } else {
      ctor.prototype = object;
    }
    $defineProperty(ctor, 'prototype', {
      configurable: false,
      writable: false
    });
    return $defineProperties(ctor, getDescriptors(staticObject));
  }
  function getProtoParent(superClass) {
    if (typeof superClass === 'function') {
      var prototype = superClass.prototype;
      if ($Object(prototype) === prototype || prototype === null)
        return superClass.prototype;
      throw new $TypeError('super prototype must be an Object or null');
    }
    if (superClass === null)
      return null;
    throw new $TypeError(("Super expression must either be null or a function, not " + typeof superClass + "."));
  }
  function defaultSuperCall(self, homeObject, args) {
    if ($getPrototypeOf(homeObject) !== null)
      superCall(self, homeObject, 'constructor', args);
  }
  $traceurRuntime.createClass = createClass;
  $traceurRuntime.defaultSuperCall = defaultSuperCall;
  $traceurRuntime.superCall = superCall;
  $traceurRuntime.superConstructor = superConstructor;
  $traceurRuntime.superGet = superGet;
  $traceurRuntime.superSet = superSet;
})();
(function() {
  'use strict';
  if (typeof $traceurRuntime !== 'object') {
    throw new Error('traceur runtime not found.');
  }
  var createPrivateName = $traceurRuntime.createPrivateName;
  var $defineProperties = $traceurRuntime.defineProperties;
  var $defineProperty = $traceurRuntime.defineProperty;
  var $create = Object.create;
  var $TypeError = TypeError;
  function nonEnum(value) {
    return {
      configurable: true,
      enumerable: false,
      value: value,
      writable: true
    };
  }
  var ST_NEWBORN = 0;
  var ST_EXECUTING = 1;
  var ST_SUSPENDED = 2;
  var ST_CLOSED = 3;
  var END_STATE = -2;
  var RETHROW_STATE = -3;
  function getInternalError(state) {
    return new Error('Traceur compiler bug: invalid state in state machine: ' + state);
  }
  function GeneratorContext() {
    this.state = 0;
    this.GState = ST_NEWBORN;
    this.storedException = undefined;
    this.finallyFallThrough = undefined;
    this.sent_ = undefined;
    this.returnValue = undefined;
    this.tryStack_ = [];
  }
  GeneratorContext.prototype = {
    pushTry: function(catchState, finallyState) {
      if (finallyState !== null) {
        var finallyFallThrough = null;
        for (var i = this.tryStack_.length - 1; i >= 0; i--) {
          if (this.tryStack_[i].catch !== undefined) {
            finallyFallThrough = this.tryStack_[i].catch;
            break;
          }
        }
        if (finallyFallThrough === null)
          finallyFallThrough = RETHROW_STATE;
        this.tryStack_.push({
          finally: finallyState,
          finallyFallThrough: finallyFallThrough
        });
      }
      if (catchState !== null) {
        this.tryStack_.push({catch: catchState});
      }
    },
    popTry: function() {
      this.tryStack_.pop();
    },
    get sent() {
      this.maybeThrow();
      return this.sent_;
    },
    set sent(v) {
      this.sent_ = v;
    },
    get sentIgnoreThrow() {
      return this.sent_;
    },
    maybeThrow: function() {
      if (this.action === 'throw') {
        this.action = 'next';
        throw this.sent_;
      }
    },
    end: function() {
      switch (this.state) {
        case END_STATE:
          return this;
        case RETHROW_STATE:
          throw this.storedException;
        default:
          throw getInternalError(this.state);
      }
    },
    handleException: function(ex) {
      this.GState = ST_CLOSED;
      this.state = END_STATE;
      throw ex;
    }
  };
  function nextOrThrow(ctx, moveNext, action, x) {
    switch (ctx.GState) {
      case ST_EXECUTING:
        throw new Error(("\"" + action + "\" on executing generator"));
      case ST_CLOSED:
        if (action == 'next') {
          return {
            value: undefined,
            done: true
          };
        }
        throw x;
      case ST_NEWBORN:
        if (action === 'throw') {
          ctx.GState = ST_CLOSED;
          throw x;
        }
        if (x !== undefined)
          throw $TypeError('Sent value to newborn generator');
      case ST_SUSPENDED:
        ctx.GState = ST_EXECUTING;
        ctx.action = action;
        ctx.sent = x;
        var value = moveNext(ctx);
        var done = value === ctx;
        if (done)
          value = ctx.returnValue;
        ctx.GState = done ? ST_CLOSED : ST_SUSPENDED;
        return {
          value: value,
          done: done
        };
    }
  }
  var ctxName = createPrivateName();
  var moveNextName = createPrivateName();
  function GeneratorFunction() {}
  function GeneratorFunctionPrototype() {}
  GeneratorFunction.prototype = GeneratorFunctionPrototype;
  $defineProperty(GeneratorFunctionPrototype, 'constructor', nonEnum(GeneratorFunction));
  GeneratorFunctionPrototype.prototype = {
    constructor: GeneratorFunctionPrototype,
    next: function(v) {
      return nextOrThrow(this[ctxName], this[moveNextName], 'next', v);
    },
    throw: function(v) {
      return nextOrThrow(this[ctxName], this[moveNextName], 'throw', v);
    }
  };
  $defineProperties(GeneratorFunctionPrototype.prototype, {
    constructor: {enumerable: false},
    next: {enumerable: false},
    throw: {enumerable: false}
  });
  Object.defineProperty(GeneratorFunctionPrototype.prototype, Symbol.iterator, nonEnum(function() {
    return this;
  }));
  function createGeneratorInstance(innerFunction, functionObject, self) {
    var moveNext = getMoveNext(innerFunction, self);
    var ctx = new GeneratorContext();
    var object = $create(functionObject.prototype);
    object[ctxName] = ctx;
    object[moveNextName] = moveNext;
    return object;
  }
  function initGeneratorFunction(functionObject) {
    functionObject.prototype = $create(GeneratorFunctionPrototype.prototype);
    functionObject.__proto__ = GeneratorFunctionPrototype;
    return functionObject;
  }
  function AsyncFunctionContext() {
    GeneratorContext.call(this);
    this.err = undefined;
    var ctx = this;
    ctx.result = new Promise(function(resolve, reject) {
      ctx.resolve = resolve;
      ctx.reject = reject;
    });
  }
  AsyncFunctionContext.prototype = $create(GeneratorContext.prototype);
  AsyncFunctionContext.prototype.end = function() {
    switch (this.state) {
      case END_STATE:
        this.resolve(this.returnValue);
        break;
      case RETHROW_STATE:
        this.reject(this.storedException);
        break;
      default:
        this.reject(getInternalError(this.state));
    }
  };
  AsyncFunctionContext.prototype.handleException = function() {
    this.state = RETHROW_STATE;
  };
  function asyncWrap(innerFunction, self) {
    var moveNext = getMoveNext(innerFunction, self);
    var ctx = new AsyncFunctionContext();
    ctx.createCallback = function(newState) {
      return function(value) {
        ctx.state = newState;
        ctx.value = value;
        moveNext(ctx);
      };
    };
    ctx.errback = function(err) {
      handleCatch(ctx, err);
      moveNext(ctx);
    };
    moveNext(ctx);
    return ctx.result;
  }
  function getMoveNext(innerFunction, self) {
    return function(ctx) {
      while (true) {
        try {
          return innerFunction.call(self, ctx);
        } catch (ex) {
          handleCatch(ctx, ex);
        }
      }
    };
  }
  function handleCatch(ctx, ex) {
    ctx.storedException = ex;
    var last = ctx.tryStack_[ctx.tryStack_.length - 1];
    if (!last) {
      ctx.handleException(ex);
      return;
    }
    ctx.state = last.catch !== undefined ? last.catch : last.finally;
    if (last.finallyFallThrough !== undefined)
      ctx.finallyFallThrough = last.finallyFallThrough;
  }
  $traceurRuntime.asyncWrap = asyncWrap;
  $traceurRuntime.initGeneratorFunction = initGeneratorFunction;
  $traceurRuntime.createGeneratorInstance = createGeneratorInstance;
})();
(function() {
  function buildFromEncodedParts(opt_scheme, opt_userInfo, opt_domain, opt_port, opt_path, opt_queryData, opt_fragment) {
    var out = [];
    if (opt_scheme) {
      out.push(opt_scheme, ':');
    }
    if (opt_domain) {
      out.push('//');
      if (opt_userInfo) {
        out.push(opt_userInfo, '@');
      }
      out.push(opt_domain);
      if (opt_port) {
        out.push(':', opt_port);
      }
    }
    if (opt_path) {
      out.push(opt_path);
    }
    if (opt_queryData) {
      out.push('?', opt_queryData);
    }
    if (opt_fragment) {
      out.push('#', opt_fragment);
    }
    return out.join('');
  }
  ;
  var splitRe = new RegExp('^' + '(?:' + '([^:/?#.]+)' + ':)?' + '(?://' + '(?:([^/?#]*)@)?' + '([\\w\\d\\-\\u0100-\\uffff.%]*)' + '(?::([0-9]+))?' + ')?' + '([^?#]+)?' + '(?:\\?([^#]*))?' + '(?:#(.*))?' + '$');
  var ComponentIndex = {
    SCHEME: 1,
    USER_INFO: 2,
    DOMAIN: 3,
    PORT: 4,
    PATH: 5,
    QUERY_DATA: 6,
    FRAGMENT: 7
  };
  function split(uri) {
    return (uri.match(splitRe));
  }
  function removeDotSegments(path) {
    if (path === '/')
      return '/';
    var leadingSlash = path[0] === '/' ? '/' : '';
    var trailingSlash = path.slice(-1) === '/' ? '/' : '';
    var segments = path.split('/');
    var out = [];
    var up = 0;
    for (var pos = 0; pos < segments.length; pos++) {
      var segment = segments[pos];
      switch (segment) {
        case '':
        case '.':
          break;
        case '..':
          if (out.length)
            out.pop();
          else
            up++;
          break;
        default:
          out.push(segment);
      }
    }
    if (!leadingSlash) {
      while (up-- > 0) {
        out.unshift('..');
      }
      if (out.length === 0)
        out.push('.');
    }
    return leadingSlash + out.join('/') + trailingSlash;
  }
  function joinAndCanonicalizePath(parts) {
    var path = parts[ComponentIndex.PATH] || '';
    path = removeDotSegments(path);
    parts[ComponentIndex.PATH] = path;
    return buildFromEncodedParts(parts[ComponentIndex.SCHEME], parts[ComponentIndex.USER_INFO], parts[ComponentIndex.DOMAIN], parts[ComponentIndex.PORT], parts[ComponentIndex.PATH], parts[ComponentIndex.QUERY_DATA], parts[ComponentIndex.FRAGMENT]);
  }
  function canonicalizeUrl(url) {
    var parts = split(url);
    return joinAndCanonicalizePath(parts);
  }
  function resolveUrl(base, url) {
    var parts = split(url);
    var baseParts = split(base);
    if (parts[ComponentIndex.SCHEME]) {
      return joinAndCanonicalizePath(parts);
    } else {
      parts[ComponentIndex.SCHEME] = baseParts[ComponentIndex.SCHEME];
    }
    for (var i = ComponentIndex.SCHEME; i <= ComponentIndex.PORT; i++) {
      if (!parts[i]) {
        parts[i] = baseParts[i];
      }
    }
    if (parts[ComponentIndex.PATH][0] == '/') {
      return joinAndCanonicalizePath(parts);
    }
    var path = baseParts[ComponentIndex.PATH];
    var index = path.lastIndexOf('/');
    path = path.slice(0, index + 1) + parts[ComponentIndex.PATH];
    parts[ComponentIndex.PATH] = path;
    return joinAndCanonicalizePath(parts);
  }
  function isAbsolute(name) {
    if (!name)
      return false;
    if (name[0] === '/')
      return true;
    var parts = split(name);
    if (parts[ComponentIndex.SCHEME])
      return true;
    return false;
  }
  $traceurRuntime.canonicalizeUrl = canonicalizeUrl;
  $traceurRuntime.isAbsolute = isAbsolute;
  $traceurRuntime.removeDotSegments = removeDotSegments;
  $traceurRuntime.resolveUrl = resolveUrl;
})();
(function() {
  'use strict';
  var types = {
    any: {name: 'any'},
    boolean: {name: 'boolean'},
    number: {name: 'number'},
    string: {name: 'string'},
    symbol: {name: 'symbol'},
    void: {name: 'void'}
  };
  var GenericType = function GenericType(type, argumentTypes) {
    this.type = type;
    this.argumentTypes = argumentTypes;
  };
  ($traceurRuntime.createClass)(GenericType, {}, {});
  var typeRegister = Object.create(null);
  function genericType(type) {
    for (var argumentTypes = [],
        $__1 = 1; $__1 < arguments.length; $__1++)
      argumentTypes[$__1 - 1] = arguments[$__1];
    var typeMap = typeRegister;
    var key = $traceurRuntime.getOwnHashObject(type).hash;
    if (!typeMap[key]) {
      typeMap[key] = Object.create(null);
    }
    typeMap = typeMap[key];
    for (var i = 0; i < argumentTypes.length - 1; i++) {
      key = $traceurRuntime.getOwnHashObject(argumentTypes[i]).hash;
      if (!typeMap[key]) {
        typeMap[key] = Object.create(null);
      }
      typeMap = typeMap[key];
    }
    var tail = argumentTypes[argumentTypes.length - 1];
    key = $traceurRuntime.getOwnHashObject(tail).hash;
    if (!typeMap[key]) {
      typeMap[key] = new GenericType(type, argumentTypes);
    }
    return typeMap[key];
  }
  $traceurRuntime.GenericType = GenericType;
  $traceurRuntime.genericType = genericType;
  $traceurRuntime.type = types;
})();
(function(global) {
  'use strict';
  var $__2 = $traceurRuntime,
      canonicalizeUrl = $__2.canonicalizeUrl,
      resolveUrl = $__2.resolveUrl,
      isAbsolute = $__2.isAbsolute;
  var moduleInstantiators = Object.create(null);
  var baseURL;
  if (global.location && global.location.href)
    baseURL = resolveUrl(global.location.href, './');
  else
    baseURL = '';
  var UncoatedModuleEntry = function UncoatedModuleEntry(url, uncoatedModule) {
    this.url = url;
    this.value_ = uncoatedModule;
  };
  ($traceurRuntime.createClass)(UncoatedModuleEntry, {}, {});
  var ModuleEvaluationError = function ModuleEvaluationError(erroneousModuleName, cause) {
    this.message = this.constructor.name + ': ' + this.stripCause(cause) + ' in ' + erroneousModuleName;
    if (!(cause instanceof $ModuleEvaluationError) && cause.stack)
      this.stack = this.stripStack(cause.stack);
    else
      this.stack = '';
  };
  var $ModuleEvaluationError = ModuleEvaluationError;
  ($traceurRuntime.createClass)(ModuleEvaluationError, {
    stripError: function(message) {
      return message.replace(/.*Error:/, this.constructor.name + ':');
    },
    stripCause: function(cause) {
      if (!cause)
        return '';
      if (!cause.message)
        return cause + '';
      return this.stripError(cause.message);
    },
    loadedBy: function(moduleName) {
      this.stack += '\n loaded by ' + moduleName;
    },
    stripStack: function(causeStack) {
      var stack = [];
      causeStack.split('\n').some((function(frame) {
        if (/UncoatedModuleInstantiator/.test(frame))
          return true;
        stack.push(frame);
      }));
      stack[0] = this.stripError(stack[0]);
      return stack.join('\n');
    }
  }, {}, Error);
  function beforeLines(lines, number) {
    var result = [];
    var first = number - 3;
    if (first < 0)
      first = 0;
    for (var i = first; i < number; i++) {
      result.push(lines[i]);
    }
    return result;
  }
  function afterLines(lines, number) {
    var last = number + 1;
    if (last > lines.length - 1)
      last = lines.length - 1;
    var result = [];
    for (var i = number; i <= last; i++) {
      result.push(lines[i]);
    }
    return result;
  }
  function columnSpacing(columns) {
    var result = '';
    for (var i = 0; i < columns - 1; i++) {
      result += '-';
    }
    return result;
  }
  var UncoatedModuleInstantiator = function UncoatedModuleInstantiator(url, func) {
    $traceurRuntime.superConstructor($UncoatedModuleInstantiator).call(this, url, null);
    this.func = func;
  };
  var $UncoatedModuleInstantiator = UncoatedModuleInstantiator;
  ($traceurRuntime.createClass)(UncoatedModuleInstantiator, {getUncoatedModule: function() {
      if (this.value_)
        return this.value_;
      try {
        var relativeRequire;
        if (typeof $traceurRuntime !== undefined) {
          relativeRequire = $traceurRuntime.require.bind(null, this.url);
        }
        return this.value_ = this.func.call(global, relativeRequire);
      } catch (ex) {
        if (ex instanceof ModuleEvaluationError) {
          ex.loadedBy(this.url);
          throw ex;
        }
        if (ex.stack) {
          var lines = this.func.toString().split('\n');
          var evaled = [];
          ex.stack.split('\n').some(function(frame) {
            if (frame.indexOf('UncoatedModuleInstantiator.getUncoatedModule') > 0)
              return true;
            var m = /(at\s[^\s]*\s).*>:(\d*):(\d*)\)/.exec(frame);
            if (m) {
              var line = parseInt(m[2], 10);
              evaled = evaled.concat(beforeLines(lines, line));
              evaled.push(columnSpacing(m[3]) + '^');
              evaled = evaled.concat(afterLines(lines, line));
              evaled.push('= = = = = = = = =');
            } else {
              evaled.push(frame);
            }
          });
          ex.stack = evaled.join('\n');
        }
        throw new ModuleEvaluationError(this.url, ex);
      }
    }}, {}, UncoatedModuleEntry);
  function getUncoatedModuleInstantiator(name) {
    if (!name)
      return;
    var url = ModuleStore.normalize(name);
    return moduleInstantiators[url];
  }
  ;
  var moduleInstances = Object.create(null);
  var liveModuleSentinel = {};
  function Module(uncoatedModule) {
    var isLive = arguments[1];
    var coatedModule = Object.create(null);
    Object.getOwnPropertyNames(uncoatedModule).forEach((function(name) {
      var getter,
          value;
      if (isLive === liveModuleSentinel) {
        var descr = Object.getOwnPropertyDescriptor(uncoatedModule, name);
        if (descr.get)
          getter = descr.get;
      }
      if (!getter) {
        value = uncoatedModule[name];
        getter = function() {
          return value;
        };
      }
      Object.defineProperty(coatedModule, name, {
        get: getter,
        enumerable: true
      });
    }));
    Object.preventExtensions(coatedModule);
    return coatedModule;
  }
  var ModuleStore = {
    normalize: function(name, refererName, refererAddress) {
      if (typeof name !== 'string')
        throw new TypeError('module name must be a string, not ' + typeof name);
      if (isAbsolute(name))
        return canonicalizeUrl(name);
      if (/[^\.]\/\.\.\//.test(name)) {
        throw new Error('module name embeds /../: ' + name);
      }
      if (name[0] === '.' && refererName)
        return resolveUrl(refererName, name);
      return canonicalizeUrl(name);
    },
    get: function(normalizedName) {
      var m = getUncoatedModuleInstantiator(normalizedName);
      if (!m)
        return undefined;
      var moduleInstance = moduleInstances[m.url];
      if (moduleInstance)
        return moduleInstance;
      moduleInstance = Module(m.getUncoatedModule(), liveModuleSentinel);
      return moduleInstances[m.url] = moduleInstance;
    },
    set: function(normalizedName, module) {
      normalizedName = String(normalizedName);
      moduleInstantiators[normalizedName] = new UncoatedModuleInstantiator(normalizedName, (function() {
        return module;
      }));
      moduleInstances[normalizedName] = module;
    },
    get baseURL() {
      return baseURL;
    },
    set baseURL(v) {
      baseURL = String(v);
    },
    registerModule: function(name, deps, func) {
      var normalizedName = ModuleStore.normalize(name);
      if (moduleInstantiators[normalizedName])
        throw new Error('duplicate module named ' + normalizedName);
      moduleInstantiators[normalizedName] = new UncoatedModuleInstantiator(normalizedName, func);
    },
    bundleStore: Object.create(null),
    register: function(name, deps, func) {
      if (!deps || !deps.length && !func.length) {
        this.registerModule(name, deps, func);
      } else {
        this.bundleStore[name] = {
          deps: deps,
          execute: function() {
            var $__0 = arguments;
            var depMap = {};
            deps.forEach((function(dep, index) {
              return depMap[dep] = $__0[index];
            }));
            var registryEntry = func.call(this, depMap);
            registryEntry.execute.call(this);
            return registryEntry.exports;
          }
        };
      }
    },
    getAnonymousModule: function(func) {
      return new Module(func.call(global), liveModuleSentinel);
    },
    getForTesting: function(name) {
      var $__0 = this;
      if (!this.testingPrefix_) {
        Object.keys(moduleInstances).some((function(key) {
          var m = /(traceur@[^\/]*\/)/.exec(key);
          if (m) {
            $__0.testingPrefix_ = m[1];
            return true;
          }
        }));
      }
      return this.get(this.testingPrefix_ + name);
    }
  };
  var moduleStoreModule = new Module({ModuleStore: ModuleStore});
  ModuleStore.set('@traceur/src/runtime/ModuleStore', moduleStoreModule);
  ModuleStore.set('@traceur/src/runtime/ModuleStore.js', moduleStoreModule);
  var setupGlobals = $traceurRuntime.setupGlobals;
  $traceurRuntime.setupGlobals = function(global) {
    setupGlobals(global);
  };
  $traceurRuntime.ModuleStore = ModuleStore;
  global.System = {
    register: ModuleStore.register.bind(ModuleStore),
    registerModule: ModuleStore.registerModule.bind(ModuleStore),
    get: ModuleStore.get,
    set: ModuleStore.set,
    normalize: ModuleStore.normalize
  };
  $traceurRuntime.getModuleImpl = function(name) {
    var instantiator = getUncoatedModuleInstantiator(name);
    return instantiator && instantiator.getUncoatedModule();
  };
})(typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : this);
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/utils.js";
  var $ceil = Math.ceil;
  var $floor = Math.floor;
  var $isFinite = isFinite;
  var $isNaN = isNaN;
  var $pow = Math.pow;
  var $min = Math.min;
  var toObject = $traceurRuntime.toObject;
  function toUint32(x) {
    return x >>> 0;
  }
  function isObject(x) {
    return x && (typeof x === 'object' || typeof x === 'function');
  }
  function isCallable(x) {
    return typeof x === 'function';
  }
  function isNumber(x) {
    return typeof x === 'number';
  }
  function toInteger(x) {
    x = +x;
    if ($isNaN(x))
      return 0;
    if (x === 0 || !$isFinite(x))
      return x;
    return x > 0 ? $floor(x) : $ceil(x);
  }
  var MAX_SAFE_LENGTH = $pow(2, 53) - 1;
  function toLength(x) {
    var len = toInteger(x);
    return len < 0 ? 0 : $min(len, MAX_SAFE_LENGTH);
  }
  function checkIterable(x) {
    return !isObject(x) ? undefined : x[Symbol.iterator];
  }
  function isConstructor(x) {
    return isCallable(x);
  }
  function createIteratorResultObject(value, done) {
    return {
      value: value,
      done: done
    };
  }
  function maybeDefine(object, name, descr) {
    if (!(name in object)) {
      Object.defineProperty(object, name, descr);
    }
  }
  function maybeDefineMethod(object, name, value) {
    maybeDefine(object, name, {
      value: value,
      configurable: true,
      enumerable: false,
      writable: true
    });
  }
  function maybeDefineConst(object, name, value) {
    maybeDefine(object, name, {
      value: value,
      configurable: false,
      enumerable: false,
      writable: false
    });
  }
  function maybeAddFunctions(object, functions) {
    for (var i = 0; i < functions.length; i += 2) {
      var name = functions[i];
      var value = functions[i + 1];
      maybeDefineMethod(object, name, value);
    }
  }
  function maybeAddConsts(object, consts) {
    for (var i = 0; i < consts.length; i += 2) {
      var name = consts[i];
      var value = consts[i + 1];
      maybeDefineConst(object, name, value);
    }
  }
  function maybeAddIterator(object, func, Symbol) {
    if (!Symbol || !Symbol.iterator || object[Symbol.iterator])
      return;
    if (object['@@iterator'])
      func = object['@@iterator'];
    Object.defineProperty(object, Symbol.iterator, {
      value: func,
      configurable: true,
      enumerable: false,
      writable: true
    });
  }
  var polyfills = [];
  function registerPolyfill(func) {
    polyfills.push(func);
  }
  function polyfillAll(global) {
    polyfills.forEach((function(f) {
      return f(global);
    }));
  }
  return {
    get toObject() {
      return toObject;
    },
    get toUint32() {
      return toUint32;
    },
    get isObject() {
      return isObject;
    },
    get isCallable() {
      return isCallable;
    },
    get isNumber() {
      return isNumber;
    },
    get toInteger() {
      return toInteger;
    },
    get toLength() {
      return toLength;
    },
    get checkIterable() {
      return checkIterable;
    },
    get isConstructor() {
      return isConstructor;
    },
    get createIteratorResultObject() {
      return createIteratorResultObject;
    },
    get maybeDefine() {
      return maybeDefine;
    },
    get maybeDefineMethod() {
      return maybeDefineMethod;
    },
    get maybeDefineConst() {
      return maybeDefineConst;
    },
    get maybeAddFunctions() {
      return maybeAddFunctions;
    },
    get maybeAddConsts() {
      return maybeAddConsts;
    },
    get maybeAddIterator() {
      return maybeAddIterator;
    },
    get registerPolyfill() {
      return registerPolyfill;
    },
    get polyfillAll() {
      return polyfillAll;
    }
  };
});
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Map.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Map.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      isObject = $__0.isObject,
      maybeAddIterator = $__0.maybeAddIterator,
      registerPolyfill = $__0.registerPolyfill;
  var getOwnHashObject = $traceurRuntime.getOwnHashObject;
  var $hasOwnProperty = Object.prototype.hasOwnProperty;
  var deletedSentinel = {};
  function lookupIndex(map, key) {
    if (isObject(key)) {
      var hashObject = getOwnHashObject(key);
      return hashObject && map.objectIndex_[hashObject.hash];
    }
    if (typeof key === 'string')
      return map.stringIndex_[key];
    return map.primitiveIndex_[key];
  }
  function initMap(map) {
    map.entries_ = [];
    map.objectIndex_ = Object.create(null);
    map.stringIndex_ = Object.create(null);
    map.primitiveIndex_ = Object.create(null);
    map.deletedCount_ = 0;
  }
  var Map = function Map() {
    var iterable = arguments[0];
    if (!isObject(this))
      throw new TypeError('Map called on incompatible type');
    if ($hasOwnProperty.call(this, 'entries_')) {
      throw new TypeError('Map can not be reentrantly initialised');
    }
    initMap(this);
    if (iterable !== null && iterable !== undefined) {
      for (var $__2 = iterable[$traceurRuntime.toProperty(Symbol.iterator)](),
          $__3; !($__3 = $__2.next()).done; ) {
        var $__4 = $__3.value,
            key = $__4[0],
            value = $__4[1];
        {
          this.set(key, value);
        }
      }
    }
  };
  ($traceurRuntime.createClass)(Map, {
    get size() {
      return this.entries_.length / 2 - this.deletedCount_;
    },
    get: function(key) {
      var index = lookupIndex(this, key);
      if (index !== undefined)
        return this.entries_[index + 1];
    },
    set: function(key, value) {
      var objectMode = isObject(key);
      var stringMode = typeof key === 'string';
      var index = lookupIndex(this, key);
      if (index !== undefined) {
        this.entries_[index + 1] = value;
      } else {
        index = this.entries_.length;
        this.entries_[index] = key;
        this.entries_[index + 1] = value;
        if (objectMode) {
          var hashObject = getOwnHashObject(key);
          var hash = hashObject.hash;
          this.objectIndex_[hash] = index;
        } else if (stringMode) {
          this.stringIndex_[key] = index;
        } else {
          this.primitiveIndex_[key] = index;
        }
      }
      return this;
    },
    has: function(key) {
      return lookupIndex(this, key) !== undefined;
    },
    delete: function(key) {
      var objectMode = isObject(key);
      var stringMode = typeof key === 'string';
      var index;
      var hash;
      if (objectMode) {
        var hashObject = getOwnHashObject(key);
        if (hashObject) {
          index = this.objectIndex_[hash = hashObject.hash];
          delete this.objectIndex_[hash];
        }
      } else if (stringMode) {
        index = this.stringIndex_[key];
        delete this.stringIndex_[key];
      } else {
        index = this.primitiveIndex_[key];
        delete this.primitiveIndex_[key];
      }
      if (index !== undefined) {
        this.entries_[index] = deletedSentinel;
        this.entries_[index + 1] = undefined;
        this.deletedCount_++;
        return true;
      }
      return false;
    },
    clear: function() {
      initMap(this);
    },
    forEach: function(callbackFn) {
      var thisArg = arguments[1];
      for (var i = 0; i < this.entries_.length; i += 2) {
        var key = this.entries_[i];
        var value = this.entries_[i + 1];
        if (key === deletedSentinel)
          continue;
        callbackFn.call(thisArg, value, key, this);
      }
    },
    entries: $traceurRuntime.initGeneratorFunction(function $__5() {
      var i,
          key,
          value;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              i = 0;
              $ctx.state = 12;
              break;
            case 12:
              $ctx.state = (i < this.entries_.length) ? 8 : -2;
              break;
            case 4:
              i += 2;
              $ctx.state = 12;
              break;
            case 8:
              key = this.entries_[i];
              value = this.entries_[i + 1];
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = (key === deletedSentinel) ? 4 : 6;
              break;
            case 6:
              $ctx.state = 2;
              return [key, value];
            case 2:
              $ctx.maybeThrow();
              $ctx.state = 4;
              break;
            default:
              return $ctx.end();
          }
      }, $__5, this);
    }),
    keys: $traceurRuntime.initGeneratorFunction(function $__6() {
      var i,
          key,
          value;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              i = 0;
              $ctx.state = 12;
              break;
            case 12:
              $ctx.state = (i < this.entries_.length) ? 8 : -2;
              break;
            case 4:
              i += 2;
              $ctx.state = 12;
              break;
            case 8:
              key = this.entries_[i];
              value = this.entries_[i + 1];
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = (key === deletedSentinel) ? 4 : 6;
              break;
            case 6:
              $ctx.state = 2;
              return key;
            case 2:
              $ctx.maybeThrow();
              $ctx.state = 4;
              break;
            default:
              return $ctx.end();
          }
      }, $__6, this);
    }),
    values: $traceurRuntime.initGeneratorFunction(function $__7() {
      var i,
          key,
          value;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              i = 0;
              $ctx.state = 12;
              break;
            case 12:
              $ctx.state = (i < this.entries_.length) ? 8 : -2;
              break;
            case 4:
              i += 2;
              $ctx.state = 12;
              break;
            case 8:
              key = this.entries_[i];
              value = this.entries_[i + 1];
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = (key === deletedSentinel) ? 4 : 6;
              break;
            case 6:
              $ctx.state = 2;
              return value;
            case 2:
              $ctx.maybeThrow();
              $ctx.state = 4;
              break;
            default:
              return $ctx.end();
          }
      }, $__7, this);
    })
  }, {});
  Object.defineProperty(Map.prototype, Symbol.iterator, {
    configurable: true,
    writable: true,
    value: Map.prototype.entries
  });
  function polyfillMap(global) {
    var $__4 = global,
        Object = $__4.Object,
        Symbol = $__4.Symbol;
    if (!global.Map)
      global.Map = Map;
    var mapPrototype = global.Map.prototype;
    if (mapPrototype.entries === undefined)
      global.Map = Map;
    if (mapPrototype.entries) {
      maybeAddIterator(mapPrototype, mapPrototype.entries, Symbol);
      maybeAddIterator(Object.getPrototypeOf(new global.Map().entries()), function() {
        return this;
      }, Symbol);
    }
  }
  registerPolyfill(polyfillMap);
  return {
    get Map() {
      return Map;
    },
    get polyfillMap() {
      return polyfillMap;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Map.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Set.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Set.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      isObject = $__0.isObject,
      maybeAddIterator = $__0.maybeAddIterator,
      registerPolyfill = $__0.registerPolyfill;
  var Map = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Map.js").Map;
  var getOwnHashObject = $traceurRuntime.getOwnHashObject;
  var $hasOwnProperty = Object.prototype.hasOwnProperty;
  function initSet(set) {
    set.map_ = new Map();
  }
  var Set = function Set() {
    var iterable = arguments[0];
    if (!isObject(this))
      throw new TypeError('Set called on incompatible type');
    if ($hasOwnProperty.call(this, 'map_')) {
      throw new TypeError('Set can not be reentrantly initialised');
    }
    initSet(this);
    if (iterable !== null && iterable !== undefined) {
      for (var $__4 = iterable[$traceurRuntime.toProperty(Symbol.iterator)](),
          $__5; !($__5 = $__4.next()).done; ) {
        var item = $__5.value;
        {
          this.add(item);
        }
      }
    }
  };
  ($traceurRuntime.createClass)(Set, {
    get size() {
      return this.map_.size;
    },
    has: function(key) {
      return this.map_.has(key);
    },
    add: function(key) {
      this.map_.set(key, key);
      return this;
    },
    delete: function(key) {
      return this.map_.delete(key);
    },
    clear: function() {
      return this.map_.clear();
    },
    forEach: function(callbackFn) {
      var thisArg = arguments[1];
      var $__2 = this;
      return this.map_.forEach((function(value, key) {
        callbackFn.call(thisArg, key, key, $__2);
      }));
    },
    values: $traceurRuntime.initGeneratorFunction(function $__7() {
      var $__8,
          $__9;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              $__8 = this.map_.keys()[Symbol.iterator]();
              $ctx.sent = void 0;
              $ctx.action = 'next';
              $ctx.state = 12;
              break;
            case 12:
              $__9 = $__8[$ctx.action]($ctx.sentIgnoreThrow);
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = ($__9.done) ? 3 : 2;
              break;
            case 3:
              $ctx.sent = $__9.value;
              $ctx.state = -2;
              break;
            case 2:
              $ctx.state = 12;
              return $__9.value;
            default:
              return $ctx.end();
          }
      }, $__7, this);
    }),
    entries: $traceurRuntime.initGeneratorFunction(function $__10() {
      var $__11,
          $__12;
      return $traceurRuntime.createGeneratorInstance(function($ctx) {
        while (true)
          switch ($ctx.state) {
            case 0:
              $__11 = this.map_.entries()[Symbol.iterator]();
              $ctx.sent = void 0;
              $ctx.action = 'next';
              $ctx.state = 12;
              break;
            case 12:
              $__12 = $__11[$ctx.action]($ctx.sentIgnoreThrow);
              $ctx.state = 9;
              break;
            case 9:
              $ctx.state = ($__12.done) ? 3 : 2;
              break;
            case 3:
              $ctx.sent = $__12.value;
              $ctx.state = -2;
              break;
            case 2:
              $ctx.state = 12;
              return $__12.value;
            default:
              return $ctx.end();
          }
      }, $__10, this);
    })
  }, {});
  Object.defineProperty(Set.prototype, Symbol.iterator, {
    configurable: true,
    writable: true,
    value: Set.prototype.values
  });
  Object.defineProperty(Set.prototype, 'keys', {
    configurable: true,
    writable: true,
    value: Set.prototype.values
  });
  function polyfillSet(global) {
    var $__6 = global,
        Object = $__6.Object,
        Symbol = $__6.Symbol;
    if (!global.Set)
      global.Set = Set;
    var setPrototype = global.Set.prototype;
    if (setPrototype.values) {
      maybeAddIterator(setPrototype, setPrototype.values, Symbol);
      maybeAddIterator(Object.getPrototypeOf(new global.Set().values()), function() {
        return this;
      }, Symbol);
    }
  }
  registerPolyfill(polyfillSet);
  return {
    get Set() {
      return Set;
    },
    get polyfillSet() {
      return polyfillSet;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Set.js" + '');
System.registerModule("traceur-runtime@0.0.79/node_modules/rsvp/lib/rsvp/asap.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/node_modules/rsvp/lib/rsvp/asap.js";
  var len = 0;
  function asap(callback, arg) {
    queue[len] = callback;
    queue[len + 1] = arg;
    len += 2;
    if (len === 2) {
      scheduleFlush();
    }
  }
  var $__default = asap;
  var browserGlobal = (typeof window !== 'undefined') ? window : {};
  var BrowserMutationObserver = browserGlobal.MutationObserver || browserGlobal.WebKitMutationObserver;
  var isWorker = typeof Uint8ClampedArray !== 'undefined' && typeof importScripts !== 'undefined' && typeof MessageChannel !== 'undefined';
  function useNextTick() {
    return function() {
      process.nextTick(flush);
    };
  }
  function useMutationObserver() {
    var iterations = 0;
    var observer = new BrowserMutationObserver(flush);
    var node = document.createTextNode('');
    observer.observe(node, {characterData: true});
    return function() {
      node.data = (iterations = ++iterations % 2);
    };
  }
  function useMessageChannel() {
    var channel = new MessageChannel();
    channel.port1.onmessage = flush;
    return function() {
      channel.port2.postMessage(0);
    };
  }
  function useSetTimeout() {
    return function() {
      setTimeout(flush, 1);
    };
  }
  var queue = new Array(1000);
  function flush() {
    for (var i = 0; i < len; i += 2) {
      var callback = queue[i];
      var arg = queue[i + 1];
      callback(arg);
      queue[i] = undefined;
      queue[i + 1] = undefined;
    }
    len = 0;
  }
  var scheduleFlush;
  if (typeof process !== 'undefined' && {}.toString.call(process) === '[object process]') {
    scheduleFlush = useNextTick();
  } else if (BrowserMutationObserver) {
    scheduleFlush = useMutationObserver();
  } else if (isWorker) {
    scheduleFlush = useMessageChannel();
  } else {
    scheduleFlush = useSetTimeout();
  }
  return {get default() {
      return $__default;
    }};
});
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Promise.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Promise.js";
  var async = System.get("traceur-runtime@0.0.79/node_modules/rsvp/lib/rsvp/asap.js").default;
  var registerPolyfill = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js").registerPolyfill;
  var promiseRaw = {};
  function isPromise(x) {
    return x && typeof x === 'object' && x.status_ !== undefined;
  }
  function idResolveHandler(x) {
    return x;
  }
  function idRejectHandler(x) {
    throw x;
  }
  function chain(promise) {
    var onResolve = arguments[1] !== (void 0) ? arguments[1] : idResolveHandler;
    var onReject = arguments[2] !== (void 0) ? arguments[2] : idRejectHandler;
    var deferred = getDeferred(promise.constructor);
    switch (promise.status_) {
      case undefined:
        throw TypeError;
      case 0:
        promise.onResolve_.push(onResolve, deferred);
        promise.onReject_.push(onReject, deferred);
        break;
      case +1:
        promiseEnqueue(promise.value_, [onResolve, deferred]);
        break;
      case -1:
        promiseEnqueue(promise.value_, [onReject, deferred]);
        break;
    }
    return deferred.promise;
  }
  function getDeferred(C) {
    if (this === $Promise) {
      var promise = promiseInit(new $Promise(promiseRaw));
      return {
        promise: promise,
        resolve: (function(x) {
          promiseResolve(promise, x);
        }),
        reject: (function(r) {
          promiseReject(promise, r);
        })
      };
    } else {
      var result = {};
      result.promise = new C((function(resolve, reject) {
        result.resolve = resolve;
        result.reject = reject;
      }));
      return result;
    }
  }
  function promiseSet(promise, status, value, onResolve, onReject) {
    promise.status_ = status;
    promise.value_ = value;
    promise.onResolve_ = onResolve;
    promise.onReject_ = onReject;
    return promise;
  }
  function promiseInit(promise) {
    return promiseSet(promise, 0, undefined, [], []);
  }
  var Promise = function Promise(resolver) {
    if (resolver === promiseRaw)
      return;
    if (typeof resolver !== 'function')
      throw new TypeError;
    var promise = promiseInit(this);
    try {
      resolver((function(x) {
        promiseResolve(promise, x);
      }), (function(r) {
        promiseReject(promise, r);
      }));
    } catch (e) {
      promiseReject(promise, e);
    }
  };
  ($traceurRuntime.createClass)(Promise, {
    catch: function(onReject) {
      return this.then(undefined, onReject);
    },
    then: function(onResolve, onReject) {
      if (typeof onResolve !== 'function')
        onResolve = idResolveHandler;
      if (typeof onReject !== 'function')
        onReject = idRejectHandler;
      var that = this;
      var constructor = this.constructor;
      return chain(this, function(x) {
        x = promiseCoerce(constructor, x);
        return x === that ? onReject(new TypeError) : isPromise(x) ? x.then(onResolve, onReject) : onResolve(x);
      }, onReject);
    }
  }, {
    resolve: function(x) {
      if (this === $Promise) {
        if (isPromise(x)) {
          return x;
        }
        return promiseSet(new $Promise(promiseRaw), +1, x);
      } else {
        return new this(function(resolve, reject) {
          resolve(x);
        });
      }
    },
    reject: function(r) {
      if (this === $Promise) {
        return promiseSet(new $Promise(promiseRaw), -1, r);
      } else {
        return new this((function(resolve, reject) {
          reject(r);
        }));
      }
    },
    all: function(values) {
      var deferred = getDeferred(this);
      var resolutions = [];
      try {
        var count = values.length;
        if (count === 0) {
          deferred.resolve(resolutions);
        } else {
          for (var i = 0; i < values.length; i++) {
            this.resolve(values[i]).then(function(i, x) {
              resolutions[i] = x;
              if (--count === 0)
                deferred.resolve(resolutions);
            }.bind(undefined, i), (function(r) {
              deferred.reject(r);
            }));
          }
        }
      } catch (e) {
        deferred.reject(e);
      }
      return deferred.promise;
    },
    race: function(values) {
      var deferred = getDeferred(this);
      try {
        for (var i = 0; i < values.length; i++) {
          this.resolve(values[i]).then((function(x) {
            deferred.resolve(x);
          }), (function(r) {
            deferred.reject(r);
          }));
        }
      } catch (e) {
        deferred.reject(e);
      }
      return deferred.promise;
    }
  });
  var $Promise = Promise;
  var $PromiseReject = $Promise.reject;
  function promiseResolve(promise, x) {
    promiseDone(promise, +1, x, promise.onResolve_);
  }
  function promiseReject(promise, r) {
    promiseDone(promise, -1, r, promise.onReject_);
  }
  function promiseDone(promise, status, value, reactions) {
    if (promise.status_ !== 0)
      return;
    promiseEnqueue(value, reactions);
    promiseSet(promise, status, value);
  }
  function promiseEnqueue(value, tasks) {
    async((function() {
      for (var i = 0; i < tasks.length; i += 2) {
        promiseHandle(value, tasks[i], tasks[i + 1]);
      }
    }));
  }
  function promiseHandle(value, handler, deferred) {
    try {
      var result = handler(value);
      if (result === deferred.promise)
        throw new TypeError;
      else if (isPromise(result))
        chain(result, deferred.resolve, deferred.reject);
      else
        deferred.resolve(result);
    } catch (e) {
      try {
        deferred.reject(e);
      } catch (e) {}
    }
  }
  var thenableSymbol = '@@thenable';
  function isObject(x) {
    return x && (typeof x === 'object' || typeof x === 'function');
  }
  function promiseCoerce(constructor, x) {
    if (!isPromise(x) && isObject(x)) {
      var then;
      try {
        then = x.then;
      } catch (r) {
        var promise = $PromiseReject.call(constructor, r);
        x[thenableSymbol] = promise;
        return promise;
      }
      if (typeof then === 'function') {
        var p = x[thenableSymbol];
        if (p) {
          return p;
        } else {
          var deferred = getDeferred(constructor);
          x[thenableSymbol] = deferred.promise;
          try {
            then.call(x, deferred.resolve, deferred.reject);
          } catch (r) {
            deferred.reject(r);
          }
          return deferred.promise;
        }
      }
    }
    return x;
  }
  function polyfillPromise(global) {
    if (!global.Promise)
      global.Promise = Promise;
  }
  registerPolyfill(polyfillPromise);
  return {
    get Promise() {
      return Promise;
    },
    get polyfillPromise() {
      return polyfillPromise;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Promise.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/StringIterator.js", [], function() {
  "use strict";
  var $__2;
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/StringIterator.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      createIteratorResultObject = $__0.createIteratorResultObject,
      isObject = $__0.isObject;
  var toProperty = $traceurRuntime.toProperty;
  var hasOwnProperty = Object.prototype.hasOwnProperty;
  var iteratedString = Symbol('iteratedString');
  var stringIteratorNextIndex = Symbol('stringIteratorNextIndex');
  var StringIterator = function StringIterator() {};
  ($traceurRuntime.createClass)(StringIterator, ($__2 = {}, Object.defineProperty($__2, "next", {
    value: function() {
      var o = this;
      if (!isObject(o) || !hasOwnProperty.call(o, iteratedString)) {
        throw new TypeError('this must be a StringIterator object');
      }
      var s = o[toProperty(iteratedString)];
      if (s === undefined) {
        return createIteratorResultObject(undefined, true);
      }
      var position = o[toProperty(stringIteratorNextIndex)];
      var len = s.length;
      if (position >= len) {
        o[toProperty(iteratedString)] = undefined;
        return createIteratorResultObject(undefined, true);
      }
      var first = s.charCodeAt(position);
      var resultString;
      if (first < 0xD800 || first > 0xDBFF || position + 1 === len) {
        resultString = String.fromCharCode(first);
      } else {
        var second = s.charCodeAt(position + 1);
        if (second < 0xDC00 || second > 0xDFFF) {
          resultString = String.fromCharCode(first);
        } else {
          resultString = String.fromCharCode(first) + String.fromCharCode(second);
        }
      }
      o[toProperty(stringIteratorNextIndex)] = position + resultString.length;
      return createIteratorResultObject(resultString, false);
    },
    configurable: true,
    enumerable: true,
    writable: true
  }), Object.defineProperty($__2, Symbol.iterator, {
    value: function() {
      return this;
    },
    configurable: true,
    enumerable: true,
    writable: true
  }), $__2), {});
  function createStringIterator(string) {
    var s = String(string);
    var iterator = Object.create(StringIterator.prototype);
    iterator[toProperty(iteratedString)] = s;
    iterator[toProperty(stringIteratorNextIndex)] = 0;
    return iterator;
  }
  return {get createStringIterator() {
      return createStringIterator;
    }};
});
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/String.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/String.js";
  var createStringIterator = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/StringIterator.js").createStringIterator;
  var $__1 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      maybeAddFunctions = $__1.maybeAddFunctions,
      maybeAddIterator = $__1.maybeAddIterator,
      registerPolyfill = $__1.registerPolyfill;
  var $toString = Object.prototype.toString;
  var $indexOf = String.prototype.indexOf;
  var $lastIndexOf = String.prototype.lastIndexOf;
  function startsWith(search) {
    var string = String(this);
    if (this == null || $toString.call(search) == '[object RegExp]') {
      throw TypeError();
    }
    var stringLength = string.length;
    var searchString = String(search);
    var searchLength = searchString.length;
    var position = arguments.length > 1 ? arguments[1] : undefined;
    var pos = position ? Number(position) : 0;
    if (isNaN(pos)) {
      pos = 0;
    }
    var start = Math.min(Math.max(pos, 0), stringLength);
    return $indexOf.call(string, searchString, pos) == start;
  }
  function endsWith(search) {
    var string = String(this);
    if (this == null || $toString.call(search) == '[object RegExp]') {
      throw TypeError();
    }
    var stringLength = string.length;
    var searchString = String(search);
    var searchLength = searchString.length;
    var pos = stringLength;
    if (arguments.length > 1) {
      var position = arguments[1];
      if (position !== undefined) {
        pos = position ? Number(position) : 0;
        if (isNaN(pos)) {
          pos = 0;
        }
      }
    }
    var end = Math.min(Math.max(pos, 0), stringLength);
    var start = end - searchLength;
    if (start < 0) {
      return false;
    }
    return $lastIndexOf.call(string, searchString, start) == start;
  }
  function includes(search) {
    if (this == null) {
      throw TypeError();
    }
    var string = String(this);
    if (search && $toString.call(search) == '[object RegExp]') {
      throw TypeError();
    }
    var stringLength = string.length;
    var searchString = String(search);
    var searchLength = searchString.length;
    var position = arguments.length > 1 ? arguments[1] : undefined;
    var pos = position ? Number(position) : 0;
    if (pos != pos) {
      pos = 0;
    }
    var start = Math.min(Math.max(pos, 0), stringLength);
    if (searchLength + start > stringLength) {
      return false;
    }
    return $indexOf.call(string, searchString, pos) != -1;
  }
  function repeat(count) {
    if (this == null) {
      throw TypeError();
    }
    var string = String(this);
    var n = count ? Number(count) : 0;
    if (isNaN(n)) {
      n = 0;
    }
    if (n < 0 || n == Infinity) {
      throw RangeError();
    }
    if (n == 0) {
      return '';
    }
    var result = '';
    while (n--) {
      result += string;
    }
    return result;
  }
  function codePointAt(position) {
    if (this == null) {
      throw TypeError();
    }
    var string = String(this);
    var size = string.length;
    var index = position ? Number(position) : 0;
    if (isNaN(index)) {
      index = 0;
    }
    if (index < 0 || index >= size) {
      return undefined;
    }
    var first = string.charCodeAt(index);
    var second;
    if (first >= 0xD800 && first <= 0xDBFF && size > index + 1) {
      second = string.charCodeAt(index + 1);
      if (second >= 0xDC00 && second <= 0xDFFF) {
        return (first - 0xD800) * 0x400 + second - 0xDC00 + 0x10000;
      }
    }
    return first;
  }
  function raw(callsite) {
    var raw = callsite.raw;
    var len = raw.length >>> 0;
    if (len === 0)
      return '';
    var s = '';
    var i = 0;
    while (true) {
      s += raw[i];
      if (i + 1 === len)
        return s;
      s += arguments[++i];
    }
  }
  function fromCodePoint() {
    var codeUnits = [];
    var floor = Math.floor;
    var highSurrogate;
    var lowSurrogate;
    var index = -1;
    var length = arguments.length;
    if (!length) {
      return '';
    }
    while (++index < length) {
      var codePoint = Number(arguments[index]);
      if (!isFinite(codePoint) || codePoint < 0 || codePoint > 0x10FFFF || floor(codePoint) != codePoint) {
        throw RangeError('Invalid code point: ' + codePoint);
      }
      if (codePoint <= 0xFFFF) {
        codeUnits.push(codePoint);
      } else {
        codePoint -= 0x10000;
        highSurrogate = (codePoint >> 10) + 0xD800;
        lowSurrogate = (codePoint % 0x400) + 0xDC00;
        codeUnits.push(highSurrogate, lowSurrogate);
      }
    }
    return String.fromCharCode.apply(null, codeUnits);
  }
  function stringPrototypeIterator() {
    var o = $traceurRuntime.checkObjectCoercible(this);
    var s = String(o);
    return createStringIterator(s);
  }
  function polyfillString(global) {
    var String = global.String;
    maybeAddFunctions(String.prototype, ['codePointAt', codePointAt, 'endsWith', endsWith, 'includes', includes, 'repeat', repeat, 'startsWith', startsWith]);
    maybeAddFunctions(String, ['fromCodePoint', fromCodePoint, 'raw', raw]);
    maybeAddIterator(String.prototype, stringPrototypeIterator, Symbol);
  }
  registerPolyfill(polyfillString);
  return {
    get startsWith() {
      return startsWith;
    },
    get endsWith() {
      return endsWith;
    },
    get includes() {
      return includes;
    },
    get repeat() {
      return repeat;
    },
    get codePointAt() {
      return codePointAt;
    },
    get raw() {
      return raw;
    },
    get fromCodePoint() {
      return fromCodePoint;
    },
    get stringPrototypeIterator() {
      return stringPrototypeIterator;
    },
    get polyfillString() {
      return polyfillString;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/String.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/ArrayIterator.js", [], function() {
  "use strict";
  var $__2;
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/ArrayIterator.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      toObject = $__0.toObject,
      toUint32 = $__0.toUint32,
      createIteratorResultObject = $__0.createIteratorResultObject;
  var ARRAY_ITERATOR_KIND_KEYS = 1;
  var ARRAY_ITERATOR_KIND_VALUES = 2;
  var ARRAY_ITERATOR_KIND_ENTRIES = 3;
  var ArrayIterator = function ArrayIterator() {};
  ($traceurRuntime.createClass)(ArrayIterator, ($__2 = {}, Object.defineProperty($__2, "next", {
    value: function() {
      var iterator = toObject(this);
      var array = iterator.iteratorObject_;
      if (!array) {
        throw new TypeError('Object is not an ArrayIterator');
      }
      var index = iterator.arrayIteratorNextIndex_;
      var itemKind = iterator.arrayIterationKind_;
      var length = toUint32(array.length);
      if (index >= length) {
        iterator.arrayIteratorNextIndex_ = Infinity;
        return createIteratorResultObject(undefined, true);
      }
      iterator.arrayIteratorNextIndex_ = index + 1;
      if (itemKind == ARRAY_ITERATOR_KIND_VALUES)
        return createIteratorResultObject(array[index], false);
      if (itemKind == ARRAY_ITERATOR_KIND_ENTRIES)
        return createIteratorResultObject([index, array[index]], false);
      return createIteratorResultObject(index, false);
    },
    configurable: true,
    enumerable: true,
    writable: true
  }), Object.defineProperty($__2, Symbol.iterator, {
    value: function() {
      return this;
    },
    configurable: true,
    enumerable: true,
    writable: true
  }), $__2), {});
  function createArrayIterator(array, kind) {
    var object = toObject(array);
    var iterator = new ArrayIterator;
    iterator.iteratorObject_ = object;
    iterator.arrayIteratorNextIndex_ = 0;
    iterator.arrayIterationKind_ = kind;
    return iterator;
  }
  function entries() {
    return createArrayIterator(this, ARRAY_ITERATOR_KIND_ENTRIES);
  }
  function keys() {
    return createArrayIterator(this, ARRAY_ITERATOR_KIND_KEYS);
  }
  function values() {
    return createArrayIterator(this, ARRAY_ITERATOR_KIND_VALUES);
  }
  return {
    get entries() {
      return entries;
    },
    get keys() {
      return keys;
    },
    get values() {
      return values;
    }
  };
});
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Array.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Array.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/ArrayIterator.js"),
      entries = $__0.entries,
      keys = $__0.keys,
      values = $__0.values;
  var $__1 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      checkIterable = $__1.checkIterable,
      isCallable = $__1.isCallable,
      isConstructor = $__1.isConstructor,
      maybeAddFunctions = $__1.maybeAddFunctions,
      maybeAddIterator = $__1.maybeAddIterator,
      registerPolyfill = $__1.registerPolyfill,
      toInteger = $__1.toInteger,
      toLength = $__1.toLength,
      toObject = $__1.toObject;
  function from(arrLike) {
    var mapFn = arguments[1];
    var thisArg = arguments[2];
    var C = this;
    var items = toObject(arrLike);
    var mapping = mapFn !== undefined;
    var k = 0;
    var arr,
        len;
    if (mapping && !isCallable(mapFn)) {
      throw TypeError();
    }
    if (checkIterable(items)) {
      arr = isConstructor(C) ? new C() : [];
      for (var $__2 = items[$traceurRuntime.toProperty(Symbol.iterator)](),
          $__3; !($__3 = $__2.next()).done; ) {
        var item = $__3.value;
        {
          if (mapping) {
            arr[k] = mapFn.call(thisArg, item, k);
          } else {
            arr[k] = item;
          }
          k++;
        }
      }
      arr.length = k;
      return arr;
    }
    len = toLength(items.length);
    arr = isConstructor(C) ? new C(len) : new Array(len);
    for (; k < len; k++) {
      if (mapping) {
        arr[k] = typeof thisArg === 'undefined' ? mapFn(items[k], k) : mapFn.call(thisArg, items[k], k);
      } else {
        arr[k] = items[k];
      }
    }
    arr.length = len;
    return arr;
  }
  function of() {
    for (var items = [],
        $__4 = 0; $__4 < arguments.length; $__4++)
      items[$__4] = arguments[$__4];
    var C = this;
    var len = items.length;
    var arr = isConstructor(C) ? new C(len) : new Array(len);
    for (var k = 0; k < len; k++) {
      arr[k] = items[k];
    }
    arr.length = len;
    return arr;
  }
  function fill(value) {
    var start = arguments[1] !== (void 0) ? arguments[1] : 0;
    var end = arguments[2];
    var object = toObject(this);
    var len = toLength(object.length);
    var fillStart = toInteger(start);
    var fillEnd = end !== undefined ? toInteger(end) : len;
    fillStart = fillStart < 0 ? Math.max(len + fillStart, 0) : Math.min(fillStart, len);
    fillEnd = fillEnd < 0 ? Math.max(len + fillEnd, 0) : Math.min(fillEnd, len);
    while (fillStart < fillEnd) {
      object[fillStart] = value;
      fillStart++;
    }
    return object;
  }
  function find(predicate) {
    var thisArg = arguments[1];
    return findHelper(this, predicate, thisArg);
  }
  function findIndex(predicate) {
    var thisArg = arguments[1];
    return findHelper(this, predicate, thisArg, true);
  }
  function findHelper(self, predicate) {
    var thisArg = arguments[2];
    var returnIndex = arguments[3] !== (void 0) ? arguments[3] : false;
    var object = toObject(self);
    var len = toLength(object.length);
    if (!isCallable(predicate)) {
      throw TypeError();
    }
    for (var i = 0; i < len; i++) {
      var value = object[i];
      if (predicate.call(thisArg, value, i, object)) {
        return returnIndex ? i : value;
      }
    }
    return returnIndex ? -1 : undefined;
  }
  function polyfillArray(global) {
    var $__5 = global,
        Array = $__5.Array,
        Object = $__5.Object,
        Symbol = $__5.Symbol;
    maybeAddFunctions(Array.prototype, ['entries', entries, 'keys', keys, 'values', values, 'fill', fill, 'find', find, 'findIndex', findIndex]);
    maybeAddFunctions(Array, ['from', from, 'of', of]);
    maybeAddIterator(Array.prototype, values, Symbol);
    maybeAddIterator(Object.getPrototypeOf([].values()), function() {
      return this;
    }, Symbol);
  }
  registerPolyfill(polyfillArray);
  return {
    get from() {
      return from;
    },
    get of() {
      return of;
    },
    get fill() {
      return fill;
    },
    get find() {
      return find;
    },
    get findIndex() {
      return findIndex;
    },
    get polyfillArray() {
      return polyfillArray;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Array.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Object.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Object.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      maybeAddFunctions = $__0.maybeAddFunctions,
      registerPolyfill = $__0.registerPolyfill;
  var $__1 = $traceurRuntime,
      defineProperty = $__1.defineProperty,
      getOwnPropertyDescriptor = $__1.getOwnPropertyDescriptor,
      getOwnPropertyNames = $__1.getOwnPropertyNames,
      isPrivateName = $__1.isPrivateName,
      keys = $__1.keys;
  function is(left, right) {
    if (left === right)
      return left !== 0 || 1 / left === 1 / right;
    return left !== left && right !== right;
  }
  function assign(target) {
    for (var i = 1; i < arguments.length; i++) {
      var source = arguments[i];
      var props = source == null ? [] : keys(source);
      var p,
          length = props.length;
      for (p = 0; p < length; p++) {
        var name = props[p];
        if (isPrivateName(name))
          continue;
        target[name] = source[name];
      }
    }
    return target;
  }
  function mixin(target, source) {
    var props = getOwnPropertyNames(source);
    var p,
        descriptor,
        length = props.length;
    for (p = 0; p < length; p++) {
      var name = props[p];
      if (isPrivateName(name))
        continue;
      descriptor = getOwnPropertyDescriptor(source, props[p]);
      defineProperty(target, props[p], descriptor);
    }
    return target;
  }
  function polyfillObject(global) {
    var Object = global.Object;
    maybeAddFunctions(Object, ['assign', assign, 'is', is, 'mixin', mixin]);
  }
  registerPolyfill(polyfillObject);
  return {
    get is() {
      return is;
    },
    get assign() {
      return assign;
    },
    get mixin() {
      return mixin;
    },
    get polyfillObject() {
      return polyfillObject;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Object.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/Number.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/Number.js";
  var $__0 = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js"),
      isNumber = $__0.isNumber,
      maybeAddConsts = $__0.maybeAddConsts,
      maybeAddFunctions = $__0.maybeAddFunctions,
      registerPolyfill = $__0.registerPolyfill,
      toInteger = $__0.toInteger;
  var $abs = Math.abs;
  var $isFinite = isFinite;
  var $isNaN = isNaN;
  var MAX_SAFE_INTEGER = Math.pow(2, 53) - 1;
  var MIN_SAFE_INTEGER = -Math.pow(2, 53) + 1;
  var EPSILON = Math.pow(2, -52);
  function NumberIsFinite(number) {
    return isNumber(number) && $isFinite(number);
  }
  ;
  function isInteger(number) {
    return NumberIsFinite(number) && toInteger(number) === number;
  }
  function NumberIsNaN(number) {
    return isNumber(number) && $isNaN(number);
  }
  ;
  function isSafeInteger(number) {
    if (NumberIsFinite(number)) {
      var integral = toInteger(number);
      if (integral === number)
        return $abs(integral) <= MAX_SAFE_INTEGER;
    }
    return false;
  }
  function polyfillNumber(global) {
    var Number = global.Number;
    maybeAddConsts(Number, ['MAX_SAFE_INTEGER', MAX_SAFE_INTEGER, 'MIN_SAFE_INTEGER', MIN_SAFE_INTEGER, 'EPSILON', EPSILON]);
    maybeAddFunctions(Number, ['isFinite', NumberIsFinite, 'isInteger', isInteger, 'isNaN', NumberIsNaN, 'isSafeInteger', isSafeInteger]);
  }
  registerPolyfill(polyfillNumber);
  return {
    get MAX_SAFE_INTEGER() {
      return MAX_SAFE_INTEGER;
    },
    get MIN_SAFE_INTEGER() {
      return MIN_SAFE_INTEGER;
    },
    get EPSILON() {
      return EPSILON;
    },
    get isFinite() {
      return NumberIsFinite;
    },
    get isInteger() {
      return isInteger;
    },
    get isNaN() {
      return NumberIsNaN;
    },
    get isSafeInteger() {
      return isSafeInteger;
    },
    get polyfillNumber() {
      return polyfillNumber;
    }
  };
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/Number.js" + '');
System.registerModule("traceur-runtime@0.0.79/src/runtime/polyfills/polyfills.js", [], function() {
  "use strict";
  var __moduleName = "traceur-runtime@0.0.79/src/runtime/polyfills/polyfills.js";
  var polyfillAll = System.get("traceur-runtime@0.0.79/src/runtime/polyfills/utils.js").polyfillAll;
  polyfillAll(Reflect.global);
  var setupGlobals = $traceurRuntime.setupGlobals;
  $traceurRuntime.setupGlobals = function(global) {
    setupGlobals(global);
    polyfillAll(global);
  };
  return {};
});
System.get("traceur-runtime@0.0.79/src/runtime/polyfills/polyfills.js" + '');

}).call(this,require('_process'),typeof global !== "undefined" ? global : typeof self !== "undefined" ? self : typeof window !== "undefined" ? window : {})
},{"_process":15,"path":14}]},{},[10,16]);

<head>
    <meta charset="UTF-8">
</head>
<body>
<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 14/10/2015
 * Time: 17:06
 */

require_once(__DIR__ . '/../core/BlacklistHelper.php');
$blh = new \Core\NomesBlacklistHelper();
$word = isset( $_GET["word"] ) ? $_GET["word"] : '';
$v = $blh->validate($word);
echo "A palavra '$word' é uma palavra ";
echo TRUE === $v ? 'válida' : 'inválida';
?>
</body>
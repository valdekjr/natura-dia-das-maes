<html>
<head>
    <meta charset="UTF-8">
</head>
<body>
<?php
$nome = "João Simão";
echo "Normal " . $nome . "<br>";
echo "strtoupper " . strtoupper($nome) . "<br>";
echo "mb_strtoupper " . mb_strtoupper($nome) . "<br>";
echo "mb_convert_case " . mb_convert_case($nome, MB_CASE_UPPER, 'UTF-8') . "<br>";
?>
</body>
</html>


<?php
/**
 * Created by PhpStorm.
 * User: Valdek
 * Date: 13/10/2015
 * Time: 04:39
 */

/*
 * Trabalha na geracao apenas da imagem da etiqueta. O resto
 * depende da logica de cada modulo, este ira exibir apenas
 * a etiqueta gerada.
 * */
require_once(__DIR__ . '/core/util/etiqueta_helper.php');

/** Render da imagem final */
\Core\Image\ImageHelper::renderHttpImage($labelImage->resource, "etiqueta.jpg");

/** Finaliza as variaveis */
imagedestroy($labelImage->resource);
<?php
/**
 * Created by PhpStorm.
 * User: PauloHenrique
 * Date: 14/10/2015
 * Time: 15:16
 */


header('Content-type: application/json');

date_default_timezone_set('America/Sao_Paulo');

include(__DIR__ .'/../core/util/cors_helper.php');

require 'rb.php';
require 'config.php';
require 'UUIDWriterMySQL.php';

// magic strings
$config = getConfigDb();
$server = $config['server'];
$database = $config['database'];
$user = $config['user'];
$pass = $config['pass'];
$table = 'cadastronatal2015';
$cookieId = 'cnid';
$connectionString ="mysql:host=$server;dbname=$database";

if (!isset($_POST['whats'])){
    echo json_encode([]);
    return;
}

$whats = $_POST['whats'];

// setup do redbean - configura mysql para usar uuid
R::setup( $connectionString, $user, $pass );
$oldToolBox = R::getToolBox();
$oldAdapter = $oldToolBox->getDatabaseAdapter();
$uuidWriter = new UUIDWriterMySQL( $oldAdapter );
$newRedBean = new RedBeanPHP\OODB( $uuidWriter );
$newToolBox = new RedBeanPHP\ToolBox( $newRedBean, $oldAdapter, $uuidWriter );
R::configureFacadeWithToolbox( $newToolBox );


$link = R::getCol( "SELECT link FROM $table WHERE whats = ? LIMIT 1",
    [ $whats ]
);
if (isset($link) && isset($link[0])){
    $link=$link[0];
}else{
    $link='';
}

echo json_encode( ['link' => $link]);
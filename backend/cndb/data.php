<?php
/**
 * Created by PhpStorm.
 * User: PauloHenrique
 * Date: 12/10/2015
 * Time: 03:56
 */

date_default_timezone_set('America/Sao_Paulo');

include(__DIR__ .'/../core/util/cors_helper.php');

require 'rb.php';
require 'config.php';
require 'UUIDWriterMySQL.php';

// campos aceitos no post
$fields = ['nome','telefone','rede','whats','link'];

// se nao houver nenhum, recusar chamada
if (!validateFields($fields)){
    echo "no fields \n";
    return;
}

// magic strings
$config = getConfigDb();
$server = $config['server'];
$database = $config['database'];
$user = $config['user'];
$pass = $config['pass'];
$table = 'cadastronatal2015';
$cookieId = 'cnid';
$connectionString ="mysql:host=$server;dbname=$database";


// setup do redbean - configura mysql para usar uuid
R::setup( $connectionString, $user, $pass );
$oldToolBox = R::getToolBox();
$oldAdapter = $oldToolBox->getDatabaseAdapter();
$uuidWriter = new UUIDWriterMySQL( $oldAdapter );
$newRedBean = new RedBeanPHP\OODB( $uuidWriter );
$newToolBox = new RedBeanPHP\ToolBox( $newRedBean, $oldAdapter, $uuidWriter );
R::configureFacadeWithToolbox( $newToolBox );


// se o id ja existir, apenas atualizar os dados
if (isset($_COOKIE[$cookieId]))
    $cnuser = R::load($table,$_COOKIE[$cookieId]);
else
    $cnuser = R::dispense($table);


foreach($fields as $field){
    if (isset($_POST[$field]))
        $cnuser[$field]= urldecode($_POST[$field]);
}

//
//if (isset($_POST['nome'])){
//    $cnuser->nome=$_POST['nome'];
//}
//if (isset($_POST['tel'])){
//    $cnuser->tel=$_POST['tel'];
//}
//if (isset($_POST['rede'])){
//    $cnuser->rede=$_POST['rede'];
//}
//if (isset($_POST['whats'])){
//    $cnuser->whats=$_POST['whats'];
//}
//if (isset($_POST['link'])){
//    $cnuser->link=$_POST['link'];
//}

$id = R::store($cnuser);
setcookie($cookieId, $id ,time() + (86400 * 120)); // 4 meses


//$dados = R::getAll("select * from $table");
//
//echo json_encode($dados);





function validateFields($fields){
    foreach($fields as $field){
        if (isset($_POST[$field]))
            return true;
    }

    return false;
}

